<?php
/**
 * @category    Markshust
 * @package     Markshust_Producthandling
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Producthandling_Model_Shipping_Carrier_Tablerate
    extends Mage_Shipping_Model_Carrier_Tablerate
{
    public function getFinalPriceWithHandlingFee($cost)
    {
        $cart = Mage::helper('checkout/cart')->getCart();
        $additional = 0;
        
        if ($cart->getQuote()->getItemsCount()){
            foreach ($cart->getQuote()->getAllVisibleItems() as $cartItem) {
                $product = Mage::getModel('catalog/product')->load($cartItem->getProductId());
                
                if ($product->getHandling()) {
                    $additional += $product->getHandling();
                }
            }
        }
        
        $finalPrice = parent::getFinalPriceWithHandlingFee($cost + $additional);
        
        return $finalPrice;
    }
}
