<?php
/**
 * @category    Markshust
 * @package     Markshust_Producthandling
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
$installer = $this;
/** @var $installer Mage_Catalog_Model_Resource_Setup */

$installer->startSetup();
//$installer->removeAttribute(Mage_Catalog_Model_Product::ENTITY, 'handling');

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'handling', array(
    'group'     => 'Prices',
    'type'      => 'decimal',
    'label'     => 'Handling',
    'input'     => 'price',
    'global'    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required'  => false,
));
//checkout_controller_onepage_save_shipping_method

$installer->endSetup();
