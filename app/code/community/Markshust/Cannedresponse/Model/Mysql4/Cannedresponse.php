<?php
/**
 * @category    Markshust
 * @package     Markshust_Cannedresponse
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Cannedresponse_Model_Mysql4_Cannedresponse extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('markshust_cannedresponse/cannedresponse', 'id');
    }
}
