<?php
/**
 * @category    Markshust
 * @package     Markshust_Cannedresponse
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Cannedresponse_Model_Observer
{
    public function hookToControllerActionPreDispatch($observer)
    {
        if ($observer->getEvent()->getControllerAction()->getFullActionName() == 'adminhtml_sales_order_addComment') {
            Mage::dispatchEvent('markshust_cannedresponse_comment_saved_before', array('request' => $observer->getControllerAction()->getRequest()));
        }
    }
 
    public function hookToControllerActionPostDispatch($observer)
    {
        if ($observer->getEvent()->getControllerAction()->getFullActionName() == 'adminhtml_sales_order_addComment') {
            Mage::dispatchEvent('markshust_cannedresponse_comment_saved_after', array('request' => $observer->getControllerAction()->getRequest()));
        }
    }
    
    public function hookToCommentSavedBefore($observer)
    {
    }
    
    public function hookToCommentSavedAfter($observer)
    {
        $salesOrderHistory = Mage::getModel('sales/order_status_history')->getCollection();
        
        $data = array(
            'comment_id' => $salesOrderHistory->getLastItem()->getEntityId(),
            'user_id' => Mage::getSingleton('admin/session')->getUser()->getId()
        );
        
        $commentUser = Mage::getModel('markshust_cannedresponse/commentuser')->setData($data);
        
        try {
            $commentUser->save();
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }
    }
}
