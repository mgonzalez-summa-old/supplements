<?php
/**
 * @category    Markshust
 * @package     Markshust_Cannedresponse
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Cannedresponse_Block_Adminhtml_List_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     *
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_list';
        $this->_blockGroup = 'markshust_cannedresponse';
        
        parent::__construct();
        
        $this->_updateButton('save', 'label', $this->__('Save Canned Response'));
        $this->_updateButton('delete', 'label', $this->__('Delete Canned Response'));
    }
    
    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('markshust_cannedresponse')->getId()) {
            return $this->__('Edit Canned Response');
        }
        else {
            return $this->__('New Canned Response');
        }
    }
}
