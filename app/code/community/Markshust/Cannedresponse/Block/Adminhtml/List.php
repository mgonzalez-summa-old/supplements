<?php
/**
 * @category    Markshust
 * @package     Markshust_Cannedresponse
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Cannedresponse_Block_Adminhtml_List extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_list';
        $this->_blockGroup = 'markshust_cannedresponse';
        $this->_headerText = $this->__('Manage Canned Responses');
        $this->_addButtonLabel = $this->__('Add New Canned Response');
        
        parent::__construct();
    }
}
