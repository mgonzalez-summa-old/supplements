<?php
/**
 * @category    Markshust
 * @package     Markshust_Cannedresponse
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Cannedresponse_Block_Adminhtml_Sales_Order_View_History extends Mage_Adminhtml_Block_Sales_Order_View_History
{
    protected function _prepareLayout()
    {
        $this->setTemplate('markshust/cannedresponse/sales/order/view/history.phtml');
        
        return parent::_prepareLayout();
    }
    
    public function getCannedResponses()
    {
        return Mage::getModel('markshust_cannedresponse/cannedresponse')->getCollection();
    }
}
