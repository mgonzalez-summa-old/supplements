<?php
/**
 * @category    Markshust
 * @package     Markshust_Cannedresponse
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create table 'markshust_cannedresponse_commentuser'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('markshust_cannedresponse/commentuser'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'ID')
    ->addColumn('comment_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Comment ID')
    ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'User ID');
$installer->getConnection()->createTable($table);

// Add autoincrement to id
$installer->run("
ALTER TABLE {$installer->getTable('markshust_cannedresponse/commentuser')} MODIFY id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE {$installer->getTable('markshust_cannedresponse/commentuser')} AUTO_INCREMENT = 1;
");

$installer->endSetup();
