<?php
/**
 * @category    Markshust
 * @package     Markshust_Cannedresponse
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Make id auto increment for 'markshust_cannedresponse' table
 */
$installer->run("
DELETE FROM {$installer->getTable('markshust_cannedresponse/cannedresponse')} WHERE id = 0;
ALTER TABLE {$installer->getTable('markshust_cannedresponse/cannedresponse')} MODIFY id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE {$installer->getTable('markshust_cannedresponse/cannedresponse')} AUTO_INCREMENT = 1;
");

$installer->endSetup();
