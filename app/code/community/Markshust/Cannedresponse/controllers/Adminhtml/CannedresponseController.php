<?php
/**
 * @category    Markshust
 * @package     Markshust_Cannedresponse
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Cannedresponse_Adminhtml_CannedresponseController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Canned responses
     *
     */
    public function indexAction()
    {
        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('markshust_cannedresponse/adminhtml_list'))
            ->renderLayout();
    }
    
    public function newAction()
    {
        $this->_forward('edit');
    }
    
    public function editAction()
    {
        $this->_initAction();
        
        // Get id if available
        $id  = $this->getRequest()->getParam('id');
        $model = Mage::getModel('markshust_cannedresponse/cannedresponse');
        
        if ($id) {
            // Load record
            $model->load($id);
            
            // Check if record is loaded
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This canned response no longer exists.'));
                $this->_redirect('*/*/');
                
                return;
            }
        }
        
        $this->_title($model->getId() ? $model->getName() : $this->__('New Canned Response'));
        
        $data = Mage::getSingleton('adminhtml/session')->getCannedresponseData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        
        Mage::register('markshust_cannedresponse', $model);
        
        $this->_initAction()
            ->_addBreadcrumb($id ? $this->__('Edit Canned Response') : $this->__('New Canned Response'), $id ? $this->__('Edit Canned Response') : $this->__('New Canned Response'))
            ->_addContent($this->getLayout()->createBlock('markshust_cannedresponse/adminhtml_list_edit')->setData('action', $this->getUrl('*/*/save')))
            ->renderLayout();
    }
    
    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            $model = Mage::getSingleton('markshust_cannedresponse/cannedresponse');
            $model->setData($postData);
            
            try {
                $model->save();
                
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The canned response has been saved.'));
                $this->_redirect('*/*/');

                return;
            }
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this canned response.'));
            }
            
            Mage::getSingleton('adminhtml/session')->setCannedresponseData($postData);
            $this->_redirectReferer();
        }
    }
    
    public function deleteAction()
    {
        $id = (int)$this->getRequest()->getParam('id');
        $model = Mage::getSingleton('markshust_cannedresponse/cannedresponse')
            ->load($id);
        
        if (!$model->getId()) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('This canned response no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }
        
        try {
            $model->delete();
            
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The canned response has been deleted'));
            $this->_redirect('*/*/');

            return;
        }
        catch (Mage_Core_Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while deleting this canned response.'));
        }
        
        $this->_redirectReferer();
    }
    
    public function messageAction()
    {
        $cannedresponse = Mage::getModel('markshust_cannedresponse/cannedresponse')->load($this->getRequest()->getParam('id'));
        echo $cannedresponse->getContent();
    }
    
    /**
     * Ajax action for canned responses
     *
     */
    public function gridAction()
    {
        $this->loadLayout(false)
            ->renderLayout();
    }
    
    /**
     * Initialize action
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/cannedresponse')
            ->_title($this->__('Sales'))->_title($this->__('Canned Responses'))
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Canned Responses'), $this->__('Canned Responses'));
        
        return $this;
    }
    
    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/cannedresponse');
    }
}
