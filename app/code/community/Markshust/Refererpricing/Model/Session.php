<?php
/**
 * @category    Markshust
 * @package     Markshust_Refererpricing
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Refererpricing_Model_Session extends Mage_Core_Model_Session
{
    public function __construct($data = array())
    {
        // Call parent constructor
        parent::__construct($data);
        
        // Get cookie
        $cookie = Mage::getSingleton('core/cookie');
        $session = Mage::getSingleton('customer/session');
        $layer = Mage::getSingleton('catalog/layer');
        $request = Mage::app()->getFrontController()->getRequest();
        $brand = Mage::getSingleton('brand/brand');
        
        // Check to see if this is initial request
        if (!$cookie->get('frontend')) {
            // Set pxc cookie if param exists
            if ($pxc = (int) $request->getParam('pxc')) {
                $cookie->set('pxc', $pxc);
                $session->setPxc($pxc);
            }
            
            // Set sh cookie if param exists
            if ($sh = (int) $request->getParam('sh')) {
                $session->setSh($sh);
            }
            
            // Check if product detail page
            if ($session->getPxc()
                && $request->getControllerName() == 'product'
                && $product = Mage::registry('current_product')) {
                // Check to see if there is a manufacturer is assigned to this product
                if ($currentBrand = (int) $product->getBrand()) {
                    // Set brand cookie from product brand id
                    $cookie->set('brand', $currentBrand);
                    $session->setBrand($currentBrand);
                }
            }
        
            // Check if brand page
            if ($session->getPxc()
                && $request->getModuleName() == 'brands'
                && $brandIds = Mage::registry('brand_manufacturers')) {
                // Get the brand id
                if ($brandId = (int) $brandIds[$request->getParam('id')]) {
                    // Set brand cookie from current brand id
                    $cookie->set('brand', $brandId);
                    $session->setBrand($brandId);
                }
            }
            
            // Check if category page
            if ($session->getPxc()
                && $request->getControllerName() == 'category'
                && $category = Mage::registry('current_category')) {
                // Check to see if there is an id assigned to this category
                if ($categoryId = (int) $category->getEntityId()) {
                    $cookie->set('category', $categoryId);
                    $session->setCategory($categoryId);
                }
            }
            
        }
    }
}
