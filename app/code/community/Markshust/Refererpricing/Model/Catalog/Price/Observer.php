<?php
/**
 * @category    Markshust
 * @package     Markshust_Refererpricing
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Refererpricing_Model_Catalog_Price_Observer
{
    public function applyRefererPricing($observer)
    {
        $session = Mage::getSingleton('customer/session');
        $event = $observer->getEvent();
        $product = $event->getProduct();
        $product = Mage::helper('refererpricing')->applyRefererTierPricing($product);
    }
}
