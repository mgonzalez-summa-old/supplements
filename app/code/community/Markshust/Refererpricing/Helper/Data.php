<?php
/**
 * @category    Markshust
 * @package     Markshust_Refererpricing
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Refererpricing_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function applyRefererTierPricing($product)
    {
        $session = Mage::getSingleton('customer/session');
        
        if ($session->getPxc()) {
            if (!$product->getData('tier_price')) {
                $product = Mage::getModel('catalog/product')->load($product->getId());
            }
            
            // Don't modify pricing if brand is set and doesn't match session brand
            if ($session->getBrand() && $product->getBrand() && $session->getBrand() != $product->getBrand()) {
                return $product;
            }
            
            if ($session->getCategory()) {
                $match = 0;
                if ($productCategoryIds = $product->getCategoryIds()) {
                    foreach ($productCategoryIds as $productCategoryId) {
                        $productCategoryId = (int) $productCategoryId;
                        if ($productCategoryId == $session->getCategory()) {
                            // Session match found in one of the product categories
                            $match++;
                        }
                    }
                    
                    if (!$match) {
                        // No matches, don't modify pricing
                        return $product;
                    }
                    // Else match found, continue with tier price processing...
                } else {
                    // Product doesn't have any categories but session category is set; don't modify price
                    return $product;
                }
            }
            
            // Get tier price data
            $tierPrices = $product->getData('tier_price');
            
            if (is_array($tierPrices)) {
                // Loop through tier prices
                foreach ($tierPrices AS $tierPrice) {
                    // Check if customger group matches pxc session
                    if ($tierPrice['price_qty'] && $tierPrice['cust_group'] == $session->getPxc()) {
                        // Match, set pricing to this tier
                        $product->setPrice($tierPrice['price']);
                        $product->setFinalPrice($tierPrice['price']);
                    }
                }
            }
        }
        
        return $product;
    }
}
