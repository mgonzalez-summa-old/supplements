<?php
/**
 * @category    Markshust
 * @package     Markshust_Refererpricing
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Refererpricing_Block_Catalog_Product_List extends Mage_Catalog_Block_Product_List
{
    /** 
     * Returns product price block html
     *
     * @param Mage_Catalog_Model_Product $product
     * @param boolean $displayMinimalPrice
     * @param string $idSuffix
     * @return string
     */
    public function getPriceHtml($product, $displayMinimalPrice = false, $idSuffix = '') 
    {
        $product = Mage::helper('refererpricing')->applyRefererTierPricing($product);
        
        return parent::getPriceHtml($product, $displayMinimalPrice = false, $idSuffix = '');
    }
}
