<?php
/**
 * @category    Markshust
 * @package     Markshust_Refererpricing
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Refererpricing_Block_Catalog_Product_View extends Mage_Catalog_Block_Product_View
{
    /**
     * Retrieve current product model
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        if (!Mage::registry('product') && $this->getProductId()) {
            $product = Mage::getModel('catalog/product')->load($this->getProductId());
            Mage::register('product', $product);
        }
        
        $product = Mage::registry('product');
        $product = Mage::helper('refererpricing')->applyRefererTierPricing($product);
        
        return $product;
    }
}
