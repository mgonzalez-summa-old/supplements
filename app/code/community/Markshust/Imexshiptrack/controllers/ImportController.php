<?php
/**
 * @category    Markshust
 * @package     Markshust_Imexshiptrack
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Imexshiptrack_ImportController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('system/convert/markshust_imexshiptrack');
        
        return $this;
    }
    
    public function indexAction()
    {
        $request = $this->getRequest();
        
        $block = $this->getLayout()
            ->createBlock('core/text', 'imexshiptrack')
            ->setText("
            	<h3>Import Shipping Tracking</h3>
            	<p>{$this->__('This script imports all files in base directory:')}
            	    <strong>var/import/shipping</strong></p>
            	<p><a href=\"{$this->getUrl('imexshiptrack/import/run')}\">{$this->__('Click here to run
            	    import')}</a></p>
            ");
        
        $this->_initAction()
            ->_addContent($block)
            ->renderLayout();
    }
    
    public function runAction()
    {
        Mage::getModel('markshust_imexshiptrack/observer')->scheduledImportShippingTracking('');
        
        $block = $this->getLayout()
            ->createBlock('core/text', 'imexshiptrack_success')
            ->setText("
                <h3>{$this->__('Import Shipping Tracking')}</h3>
            	<p>{$this->__('Import execution complete.')}</p>
            ");
        
        $this->_initAction()
            ->_addContent($block)
            ->renderLayout();
    }
}
