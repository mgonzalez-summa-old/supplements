<?php
/**
 * @category    Markshust
 * @package     Markshust_Imexshiptrack
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Imexshiptrack_Model_Observer
{
    const IMPORT_CRON_STRING_PATH = 'crontab/jobs/imexshiptrack_import/schedule/cron_expr';
    const IMPORT_ENABLE = 'imexshiptrack/import/enabled';
    const IMPORT_PATH = 'general/file/imexshiptrack/import_path';
    const IMPORT_EXECUTED_PATH = 'general/file/imexshiptrack/import_executed_path';

    /**
     * Create shipments and adds tracking information to orders from file import.
     *
     */
    public function scheduledImportShippingTracking()
    {
        // Ensure cron is enabled and setup
        if (!Mage::getStoreConfig(self::IMPORT_ENABLE) || !Mage::getStoreConfig(self::IMPORT_CRON_STRING_PATH)) {
            return;
        }

        // Get import paths from xml
        $importPath = Mage::getBaseDir() . '/' . Mage::getStoreConfig(self::IMPORT_PATH);
        $importExecutedPath = Mage::getBaseDir() . '/' . Mage::getStoreConfig(self::IMPORT_EXECUTED_PATH);
        // Get all files from import path
        $files = glob("$importPath/*.*");

        // Exit if no files to process
        if (!is_array($files) || !count($files)) {
            Mage::log("Markshust_Imexshiptrack import found no files to process in path $importPath");
            return;
        }

        // Loop through files
        foreach ($files AS $file) {
            // Get filename parts (ex. extension, etc.)
            $fileParts = pathinfo("$importPath/$file");
            $fileExtension = '.' . $fileParts['extension'];

            // Convert csv to array
            $data = $this->csvToArray($file);

            if ($data) {
                // Loop through each line and process
                foreach ($data AS $row) {
                    $this->processRow($row);
                }

                // Create executed directory if it doesn't exist
                if (!is_dir($importExecutedPath)) {
                    if (mkdir($importExecutedPath)) {
                        Mage::log("Markshust_Imexshiptrack created executed directory $importExecutedPath");
                    } else {
                        Mage::log("Markshust_Imexshiptrack unable to create executed directory $importExecutedPath");
                    }
                }

                // Create filename for executed file
                $fileExecuted = str_replace($fileExtension, '.executed.' . date('Ymd-His') . $fileExtension, $file);
                $fileExecuted = str_replace($importPath, $importExecutedPath, $fileExecuted);
                rename($file, $fileExecuted);

                // Delete files older than a week
                shell_exec("find $importExecutedPath -type f -mtime +1 -exec rm {} \;");

                Mage::log("Markshust_Imexshiptrack import file processed and moved to $fileExecuted");
            } else {
                Mage::log("Markshust_Imexshiptrack import found no records to process in $file");
            }
        }
    }

    /**
     * Process a specific row in the csv.
     *
     * @param array $item
     */
    public function processRow($row)
    {
        if (!isset($row['order_number'])) {
            Mage::log("Markshust_Imexshiptrack import row missing info for column 'order_number'");
            return;
        }

        // Load the order
        $order = Mage::getModel('sales/order')->loadByIncrementId($row['order_number']);

        if (!$order->getId()) {
            // Order not found in system, log and return
            Mage::log("Markshust_Imexshiptrack import cannot find order number {$row['order_number']}");
            return;
        }

        // Set default vals (if rows don't exist) and split out tracking info by commas
        $row['carrier_code'] = isset($row['carrier_code']) ? $row['carrier_code'] : NULL;
        $row['title'] = isset($row['title']) ? $row['title'] : NULL;
        $numbers = isset($row['number']) ? explode(',', $row['number']) : NULL;
        $skuQtyShippedItems = isset($row['sku_qty_shipped']) ? explode(',', $row['sku_qty_shipped']) : NULL;
        $row['notify_customer'] = isset($row['notify_customer']) ? $row['notify_customer'] : NULL;

        if ($skuQtyShippedItems) {
            // Loop through values and assign to vars
            foreach ($skuQtyShippedItems AS $key => $value) {
                // Get qty shipped if exists
                if ($value) {
                    // Append extra colon on explode to ensure all vars in list are defined
                    list($itemSku, $itemQtyShipped) = explode(':', $value . ':');
                } else {
                    $itemSku = NULL;
                    $itemQtyShipped = NULL;
                }

                // Make array search items accurate by only looking at strings (sku)
                $skuQtyShipped[$key]['sku'] = $itemSku;
                $skuQtyShipped[$key]['qty_shipped'] = $itemQtyShipped;
            }
        } else {
            $skuQtyShipped = NULL;
        }

        // Create shipment and create log
        $shipmentId = $this->createShipment($order, $row['carrier_code'], $row['title'], $numbers, $skuQtyShipped, $row['notify_customer']);

        if ($shipmentId) {
            Mage::log("Markshust_Imexshiptrack import inserted shipment $shipmentId for order {$row['order_number']}");

            // Check if grand total is zero and order doesn't have invoices and can be invoiced
            if ($order->getGrandTotal() == '0' && !$order->hasInvoices() && $order->canInvoice()) {
                // Create invoice for this order
                $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();

                // Make sure there is a qty on the invoice
                if (!$invoice->getTotalQty()) {
                    Mage::throwException("Markshust_Imexshiptrack cannot create invoice for order {$row['order_number']} without products");
                }

                // Register as invoice item
                $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                $invoice->register();

                // Save the invoice to the order
                $transaction = Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
                $transaction->save();
            }
        }
    }

    /**
     * Create shipment for a specific order.
     *
     * @param object $order
     * @param string $carrier_code
     * @param string $title
     * @param array $numbers
     * @param array $sku_qty_shipped
     * @param int $notify_customer
     * @return int
     */
    public function createShipment($order, $carrierCode = NULL, $title = NULL, $numbers = NULL, $skuQtyShipped = NULL, $notifyCustomer = NULL)
    {
        // Check if order can be shipped or has already shipped
        if (!$order->canShip()) {
            Mage::log("Markshust_Imexshiptrack import shipment for order {$order->getRealOrderId()} cannot be shipped");
            return;
        }

        // Initialize the order shipment object
        $convertOrder = Mage::getModel('sales/convert_order');
        $shipment = $convertOrder->toShipment($order);

        // Loop through order items
        foreach ($order->getAllItems() AS $orderItem) {
            // Check if order item has qty to ship or is virtual
            if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }

            // Check if sku_qty_shipped column exists and has a value for at least one sku
            if ($skuQtyShipped && NULL != $skuQtyShipped[0]['sku']) {
                // Look for match of order item to sku defined in sku_qty_shipped column
                $skuQtyShippedKey = $this->recursive_array_search($orderItem->getSku(), $skuQtyShipped, 'sku');

                // If match, ship that item, otherwise, don't ship anything for this item
                if (is_numeric($skuQtyShippedKey)) {
                    // Match in sku_qty_shipped column to order item, retrive that value
                    $itemQtyShipped = $skuQtyShipped[$skuQtyShippedKey]['qty_shipped'];

                    if (is_numeric($itemQtyShipped) && $itemQtyShipped < $orderItem->getQtyToShip()) {
                        // Qty shipped is defined, and is less or equal to qty to ship on order item
                        $qtyShipped = $itemQtyShipped;
                    } else {
                        // No qty shipped defined for this sku, or invalid amount
                        // Ship all remaining items on order.
                        $qtyShipped = $orderItem->getQtyToShip();
                    }
                } else {
                    // No match, don't ship anything for this order item
                    continue;
                }
            } else {
                // No value defined to ship a specific qty per sku, ship all qty for this order item
                $qtyShipped = $orderItem->getQtyToShip();
            }

            // Create shipment item with qty
            $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);

            // Add shipment item to shipment
            $shipment->addItem($shipmentItem);
        }

        // Register shipment
        $shipment->register();

        if ($carrierCode && !$title && !$numbers) {
            // Carrier code and title exist, but no tracking number
            // Create track object and setters
            $track = Mage::getModel('sales/order_shipment_track')
                ->setCarrierCode($carrierCode);

            // Add track to shipment and set order status to processing
            $shipment->addTrack($track);
        } else if ($carrierCode && $title && !$numbers) {
            // Carrier code and title exist, but no tracking number
            // Create track object and setters
            $track = Mage::getModel('sales/order_shipment_track')
                ->setCarrierCode($carrierCode)
                ->setTitle($title);

            // Add track to shipment and set order status to processing
            $shipment->addTrack($track);
        } else if ($carrierCode && $title && $numbers) {
            // Carrier code, title, and at least one tracking number exists
            // Loop through numbers and add track info to shipment
            foreach ($numbers AS $number) {
                // Create track object and setters
                $track = Mage::getModel('sales/order_shipment_track')
                    ->setCarrierCode($carrierCode)
                    ->setTitle($title)
                    ->setNumber($number);

                // Add track to shipment and set order status to processing
                $shipment->addTrack($track);
            }
        }

        // Set order to in process
        $shipment->getOrder()->setIsInProcess(true);

        // If notify customer, denote it on shipment
        if ($notifyCustomer) {
            $shipment->setEmailSent(true);
        }

        try {
            // Save created shipment and order
            $shipment->save();
            $shipment->getOrder()->save();

            // Send email
            if ($notifyCustomer) $shipment->sendEmail();
        } catch (Mage_Core_Exception $e) {
            Mage::log("Markshust_Imexshiptrack import creating shipment error for order {$order->getRealOrderId()}: {$e->getMessage()}");
            return;
        }

        // Return new shipment id
        return $shipment->getIncrementId();
    }

    /**
     * Convert csv to array.
     *
     * @param string $filename
     * @return array
     */
    public function csvToArray($filename)
    {
        if (!file_exists($filename)) {
            return;
        }

        // Open file to read
        $file = fopen($filename, 'r');
        $i = 0;

        while (!feof($file)) {
            $i++;
            $buffer = fgetcsv($file, 4096);

            // Set headers if first row
            if (1 == $i) {
                $headers = $buffer;
                continue;
            }

            $j = 0;
            // Set column data
            foreach ($headers AS $col) {
                $data[$i][$col] = $buffer[$j];
                $j++;
            }
        }

        fclose($file);

        // Remove empty rows
        for ($k = 0; $k <= $i; $k++) {
            foreach ($headers AS $col) {
                if (!isset($data[$k][$col])) {
                    unset($data[$k]);
                }
            }
        }

        return $data;
    }

    /**
     * Recursively searches haystack for needle and returns array key if found.
     *
     * @param string $needle
     * @param array $hatstack
     * @param array $index
     * @return int
     */
    function recursive_array_search($needle, $haystack, $index = null)
    {
        $ait = new RecursiveArrayIterator($haystack);
        $it = new RecursiveIteratorIterator($ait);

        while ($it->valid()) {
            if ($it->current() == $needle && (isset($index) && $it->key() == $index) || !isset($index)) {
                return $ait->key();
            }

            $it->next();
        }

        return false;
    }
}
