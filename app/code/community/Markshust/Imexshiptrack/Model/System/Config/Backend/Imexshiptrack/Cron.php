<?php
/**
 * @category    Markshust
 * @package     Markshust_Imexshiptrack
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Imexshiptrack_Model_System_Config_Backend_Imexshiptrack_Cron extends Mage_Core_Model_Config_Data
{
    const CRON_STRING_PATH = 'crontab/jobs/imexshiptrack_import/schedule/cron_expr';
    const CRON_MODEL_PATH = 'crontab/jobs/imexshiptrack_import/run/model';
    
    protected function _afterSave()
    {
        $enabled    = $this->getData('groups/import/fields/enabled/value');
        $time       = $this->getData('groups/import/fields/time/value');
        $frequency  = $this->getData('groups/import/fields/frequency/value');
        
        $frequencyMinute    = Markshust_Imexshiptrack_Model_System_Config_Source_Cron_Frequency::CRON_MINUTE;
        $frequencyHour      = Markshust_Imexshiptrack_Model_System_Config_Source_Cron_Frequency::CRON_HOUR;
        $frequencyDay       = Markshust_Imexshiptrack_Model_System_Config_Source_Cron_Frequency::CRON_DAY;
        
        $cronExprArray = array(
            ($frequency == $frequencyMinute) ? "*/$time"    : '*', # Minute
            ($frequency == $frequencyHour) ? "*/$time"      : '*', # Hour
            ($frequency == $frequencyDay) ? "*/$time"       : '*', # Day of the Month
            '*',                                                   # Month of the Year
            '*',                                                   # Day of the Week
        );
        
        $cronExprString = join(' ', $cronExprArray);
        
        try {
            if (0 == $enabled) {
                Mage::getModel('core/config_data')
                    ->load(self::CRON_STRING_PATH, 'path')
                    ->delete();
                Mage::getModel('core/config_data')
                    ->load(self::CRON_MODEL_PATH, 'path')
                    ->delete();
            } else {
                Mage::getModel('core/config_data')
                    ->load(self::CRON_STRING_PATH, 'path')
                    ->setValue($cronExprString)
                    ->setPath(self::CRON_STRING_PATH)
                    ->save();
                Mage::getModel('core/config_data')
                    ->load(self::CRON_MODEL_PATH, 'path')
                    ->setValue((string) Mage::getConfig()->getNode(self::CRON_MODEL_PATH))
                    ->setPath(self::CRON_MODEL_PATH)
                    ->save();
            }
        } catch (Exception $e) {
            throw new Exception(Mage::helper('cron')->__('Unable to save the cron expression.'));
        }
    }
}
