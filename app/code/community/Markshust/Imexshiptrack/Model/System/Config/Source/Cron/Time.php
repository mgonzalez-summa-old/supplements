<?php
/**
 * @category    Markshust
 * @package     Markshust_Imexshiptrack
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Imexshiptrack_Model_System_Config_Source_Cron_Time
{
    protected static $_options;
    
    public function toOptionArray()
    {
        if (!self::$_options) {
            self::$_options = array();
            for ($i = 1; $i < 60; $i++) {
                self::$_options[$i]['label'] = Mage::helper('cron')->__('Every') . ' ' . $i;
                self::$_options[$i]['value'] = $i;
            }
        }
        return self::$_options;
    }
}
