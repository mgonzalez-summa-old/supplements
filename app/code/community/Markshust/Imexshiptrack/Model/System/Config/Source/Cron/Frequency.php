<?php
/**
 * @category    Markshust
 * @package     Markshust_Imexshiptrack
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Imexshiptrack_Model_System_Config_Source_Cron_Frequency
{
    protected static $_options;
    
    const CRON_MINUTE   = 'M';
    const CRON_HOUR     = 'H';
    const CRON_DAY      = 'D';
    
    public function toOptionArray()
    {
        if (!self::$_options) {
            self::$_options = array(
                array(
                    'label' => Mage::helper('cron')->__('Minute(s)'),
                    'value' => self::CRON_MINUTE,
                ),
                array(
                    'label' => Mage::helper('cron')->__('Hour(s)'),
                    'value' => self::CRON_HOUR,
                ),
                array(
                    'label' => Mage::helper('cron')->__('Day(s)'),
                    'value' => self::CRON_DAY,
                ),
            );
        }
        return self::$_options;
    }
}
