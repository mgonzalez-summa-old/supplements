<?php
/**
 * @category    Markshust
 * @package     Markshust_Visualcompare
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

// Get config
$config = new Mage_Core_Model_Config();

// Set list mode as default sort for default store
$config->saveConfig('catalog/frontend/list_mode', 'list-grid', 'default', 0);
