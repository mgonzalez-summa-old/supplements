<?php
/**
 * @category    Markshust
 * @package     Markshust_Visualcompare
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Visualcompare_Model_Pagecache_Container extends Enterprise_PageCache_Model_Container_Abstract
{
    /**
     * Get customer identifier from cookie
     *
     * @return string
     */
    protected function _getIdentifier()
    {
        return $this->_getCookieValue(Enterprise_PageCache_Model_Cookie::COOKIE_COMPARE_LIST, '');
    }
    
    /**
     * Get container individual cache id
     *
     * @return string
     */
    protected function _getCacheId()
    {
        return 'VISUALCOMPARE' . md5($this->_placeholder->getAttribute('cache_id') . $this->_getIdentifier());
    }

    /**
     * Render block content
     *
     * @return string
     */
    protected function _renderBlock()
    {
        $template = $this->_placeholder->getAttribute('template');
        
        $block = Mage::app()->getLayout()->createBlock('visualcompare/catalog_product_compare_visualcompare');
        $block->setTemplate($template);
        $block->setLayout(Mage::app()->getLayout());
        
        return $block->toHtml();
    }
}
