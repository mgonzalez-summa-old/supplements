<?php
/**
 * @category    Markshust
 * @package     Markshust_Visualcompare
 * @author      Mark Shust <mark@shust.com>
 * @license     http://markshust.com/eula/
 */
class Markshust_Visualcompare_Block_Catalog_Product_Compare_Visualcompare extends Mage_Catalog_Block_Product_Compare_List
{
    /**
     * Preparing layout
     *
     * @return Mage_Catalog_Block_Product_Compare_Abstract
     */
    protected function _prepareLayout()
    {
        Mage_Catalog_Block_Product_Compare_Abstract::_prepareLayout();
    }
}
