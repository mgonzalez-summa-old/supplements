<?php
/**
 * @category    Markshust
 * @package     Markshust_Showhide
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Showhide_Helper_Data
    extends Mage_Core_Helper_Abstract
{
    const SHOWHIDE_GROUPS_ATTRIBUTE = 'showhide_groups';
    
    /**
     * Return all groups including NOT_LOGGED_IN (normally hidden)
     * 
     * @return Mage_Customer_Model_Resource_Group_Collection
     */
    public function getGroupsInts()
    {
        if (empty($this->_groups)) {
            $collection = Mage::getResourceModel('customer/group_collection');
            $this->_groups = $collection->load();
        }
        
        return $this->_groups;
    }
}
