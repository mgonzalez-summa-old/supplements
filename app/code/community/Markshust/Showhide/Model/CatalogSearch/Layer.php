<?php
/**
 * @category    Markshust
 * @package     Markshust_Showhide
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Showhide_Model_CatalogSearch_Layer
    extends Mage_CatalogSearch_Model_Layer
{
    /**
     * Add showhide_groups attribute and filtering to product collection
     *
     * @param Mage_Catalog_Model_Resource_Eav_Resource_Product_Collection $collection
     */
    public function prepareProductCollection($collection)
    {
        $session = Mage::getSingleton('customer/session');
        $sh = $session->getSh();
        
        if ($sh) {
            $collection->addAttributeToFilter(array(
                array('attribute' => 'showhide_groups', array('nlike' => "%$sh%")),
                array('attribute' => 'showhide_groups', 'null' => ''),
            ));
        }
        
        return parent::prepareProductCollection($collection);
    }
}
