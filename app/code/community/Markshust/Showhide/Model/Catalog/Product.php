<?php
/**
 * @category    Markshust
 * @package     Markshust_Showhide
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Showhide_Model_Catalog_Product
    extends Mage_Catalog_Model_Product
{
    /**
     * Add showhide_groups attribute and filtering to product collection
     *
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
     */
    public function isVisibleInCatalog()
    {
        $session = Mage::getSingleton('customer/session');
        $showhideGroups = $this->getShowhideGroups();
        $sh = $session->getSh();
        
        if ($sh && $showhideGroups && in_array($sh, $showhideGroups)) {
            return false;
        }
        
        return parent::isVisibleInCatalog();
    }
}
