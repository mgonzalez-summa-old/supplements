<?php
/**
 * @category    Markshust
 * @package     Markshust_Showhide
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Showhide_Model_Entity_Attribute_Source_Customergroup_Withdefault
    extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Return all options for showhide_groups attribute
     * List of all customer groups with additional option NONE
     */
    public function getAllOptions()
    {
        if (empty($this->_options)) {
            $this->_options[] = array(
                'value' => NULL,
                'label' => '[ NONE ]',
            );
            
            foreach (Mage::helper('markshust_showhide')->getGroupsInts() AS $group) {
                $this->_options[] = array(
                    'value' => $group->getId(),
                    'label' => '[' . $group->getId() . '] ' . $group->getCode(),
                );
            }
        }
        
        return $this->_options;
    }
}
