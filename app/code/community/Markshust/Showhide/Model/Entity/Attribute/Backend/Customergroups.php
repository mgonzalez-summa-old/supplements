<?php
/**
 * @category    Markshust
 * @package     Markshust_Showhide
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Showhide_Model_Entity_Attribute_Backend_Customergroups
    extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    /** 
     * Process the attribute value before saving
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Eav_Model_Entity_Attribute_Backend_Abstract
     */
    public function beforeSave($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $data = $object->getData($attributeCode);
        
        if (!is_array($data)) {
            $data = explode(',', $data);
        }
        
        $object->setData($attributeCode, implode(',', $data));
        
        return parent::beforeSave($object);
    }
    
    /**
     * Explode the saved array again, because otherwise the indexer will think the value changed,
     * even if it is the same (array(1,2,3) !== "1,2,3").
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Eav_Model_Entity_Attribute_Backend_Abstract
	 */
    public function afterSave($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $data = $object->getData($attributeCode);
        
        if (is_string($data)) {
            $object->setData($attributeCode, explode(',', $data));
        }
        
        return parent::afterSave($object);
    }
    
    /**
     * When data is loaded, explode it into an array
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Eav_Model_Entity_Attribute_Backend_Abstract
	 */
    public function afterLoad($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $data = $object->getData($attributeCode);
        
        if (NULL !== $data && is_string($data)) {
            $data = explode(',', $data);
            $object->setData($attributeCode, $data);
        }
        
        return parent::afterLoad($object);
    }
}
