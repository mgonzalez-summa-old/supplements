<?php
/**
 * @category    Markshust
 * @package     Markshust_Showhide
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', Markshust_Showhide_Helper_Data::SHOWHIDE_GROUPS_ATTRIBUTE, array(
    'group' => 'General',
    'label' => 'Hide from Groups',
    'type' => 'text',
    'input' => 'multiselect',
    'source' => 'markshust_showhide/entity_attribute_source_customergroup_withdefault',
    'backend' => 'markshust_showhide/entity_attribute_backend_customergroups',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => false,
    'user_defined' => false, // system attribute
    'filterable' => false,
    'comparable' => false,
    'visible_on_front' => false,
    'visible_in_advanced_search' => false,
    'unique' => false,
    'is_configurable' => false,
    'used_in_product_listing' => true,
));

$installer->endSetup();
