<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Pricematch_Model_Observer
{
    /**
     * Ban the use of full page cache on product and category pages (dynamic content)
     * 
     * @param Varien_Event_Observer $observer
     */
    public function processPreDispatch(Varien_Event_Observer $observer)
    {
        $action = $observer->getEvent()->getControllerAction();
        
        // check to see if action is a category controller
        if ($action instanceof Mage_Cms_IndexController || $action instanceof Mage_Catalog_CategoryController || $action instanceof Mage_Catalog_ProductController)
        {
            $request = $action->getRequest();
            $cache = Mage::app()->getCacheInstance();
    
            // ban FPC for this request
            $cache->banUse('full_page');
        }
    }
    
    /**
     * Store item in checkout session so it's available at insert time
     *
     * @param object $observer
     */
    public function storeApprovalItem(Varien_Event_Observer $observer)
    {
        $orderItem = $observer->getOrderItem();
        $options = $orderItem->getProductOptions();
        $checkoutSession = Mage::getSingleton('checkout/session');
    
        if (isset($options['options'])) {
            if ($pricematchedItemIds = $checkoutSession->getPricematchedItemIds()) {
                array_push($pricematchedItemIds, $orderItem->getProductId());
            } else {
                $pricematchedItemIds = array($orderItem->getProductId());
            }
    
            $checkoutSession->setPricematchedItemIds($pricematchedItemIds);
        }
    }
    
    /**
     * Create item in markshust_pricematch_item table when placing an order
     *
     * @param object $observer
     */
    public function createApprovalItems(Varien_Event_Observer $observer)
    {
        $order = $observer->getOrder();
        $items = $order->getAllItems();
        $checkoutSession = Mage::getSingleton('checkout/session');
        
        $pricematchedItemIds = $checkoutSession->getPricematchedItemIds();
        
        $updateOrderStatus = false;
        if ($pricematchedItemIds = $checkoutSession->getPricematchedItemIds()) {
            foreach ($items AS $item) {
                if (in_array($item->getProductId(), $pricematchedItemIds)) {
                    try {
                        // Item in pricematched array
                        
                        // Set item model
                        $orderApprovalItemModel = Mage::getModel('markshust_pricematch/item');
                        
                        // Insert item into markshust_pricematch_item table
                        $orderApprovalItemModel
                            ->setOrderId($order->getId())
                            ->setItemId($item->getId())
                            ->setApproved(0);
                        
                        $orderApprovalItemModel->save();
                        
                        // Reset model for next loop item
                        $orderApprovalItemModel = NULL;
                        
                        $updateOrderStatus = true;
                    } catch (Exception $e) {
                        Mage::log($e);
                    }
                }
            }
            
            if ($updateOrderStatus) {
                $order->setState('pending_approval', 'pending_approval_price_match');
                $order->save();
            }
            
            $checkoutSession->setPricematchedItemIds(NULL);
        }
    }
}
