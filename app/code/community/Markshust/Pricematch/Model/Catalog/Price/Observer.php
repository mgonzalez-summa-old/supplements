<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Pricematch_Model_Catalog_Price_Observer
{
    public function applyPricematchPricing($observer)
    {
        $session = Mage::getSingleton('customer/session');
        $event = $observer->getEvent();
        $product = $event->getProduct();
        $product = Mage::helper('markshust_pricematch')->applyPricematchPricing($product);
    }
}
