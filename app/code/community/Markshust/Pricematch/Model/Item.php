<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Pricematch_Model_Item
    extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('markshust_pricematch/item');
    }
}
