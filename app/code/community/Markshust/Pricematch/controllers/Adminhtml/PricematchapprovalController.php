<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Pricematch_Adminhtml_PricematchapprovalController
    extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_initAction()->renderLayout();
    }
    
    public function approveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getSingleton('markshust_pricematch/item');
                $orderStatusUpdated = array();
                
                // Loop through all posted items
                foreach ($postData['item_id'] AS $itemId) {
                    // Update item record to approved
                    $item = $model->getCollection()
                    ->addFilter('item_id', $itemId)
                    ->getFirstItem();
                    
                    $model->setData('id', $item->getId())
                    ->setData('approved', '1')
                    ->save();
                    
                    // Store order id from item record
                    $orderId = $item->getOrderId();
                    
                    // Update order status
                    $orderItems = $model->getCollection()
                    ->addFilter('order_id', $orderId)
                    ->addFilter('approved', '0')
                    ->getFirstItem();
                    
                    // No unapproved order items found, update order to pending payment
                    if (!$orderItems->getId()) {
                        $order = Mage::getSingleton('sales/order');
                        $order->load($orderId);
                        $order->setState('pending_payment', true);
                        $order->save();
                        
                        array_push($orderStatusUpdated, $order->getIncrementId());
                    }
                }
            }
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this order approval item.'));
            }
            
            if (isset($orderStatusUpdated[0])) {
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('All items on Order(s) #' . implode(', ', $orderStatusUpdated) . ' have been approved.'));
            } else {
                Mage::getSingleton('adminhtml/session')->addNotice($this->__('All of the selected order items have been approved.'));
            }
            
            $this->_redirect('*/*/index');
        }
    }
    
    /**
     * Initialize action
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/markshust_pricematch_approval')
            ->_title($this->__('Sales'))->_title($this->__('Price Match Approval'))
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Price Match Approval'), $this->__('Price Match Approval'));
        
        return $this;
    }
}
