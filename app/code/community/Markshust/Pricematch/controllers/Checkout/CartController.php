<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
require_once('Mage/Checkout/controllers/CartController.php');

class Markshust_Pricematch_Checkout_CartController extends Mage_Checkout_CartController
{
    /**
     * Add product to shopping cart action
     */
    public function addAction()
    {
        $cart = $this->_getCart();
        $params = $this->getRequest()->getParams();
        $product = $this->_initProduct();
        $cartPricematchItemUpdated = false;
        
        if ($cart->getQuote()->getItemsCount()) {
            foreach ($cart->getQuote()->getAllVisibleItems() as $cartItem) {
                if ($cartItem->getSku() == $product->getSku()) {
                    // Item being added is already in cart, increase quantity
                    $params['qty'] = isset($params['qty']) ? (int) $params['qty'] : 1;
                    $cartItem->setQty($cartItem->getQty() + $params['qty']);
                    $cartPricematchItemUpdated = true;
                    $cart->save();
                }
            }
        }
        
        if ($cartPricematchItemUpdated) {
            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } else {
            parent::addAction();
        }
    }
    
    /**
     * Update shopping cart data action
     */
    public function updatepricematchitemPostAction()
    {
        parent::updatePostAction();
        
        $cartData = $this->getRequest()->getParam('cart');
        
        if ($cartData) {
            foreach ($cartData as $cartIndex => $cartValue) {
                $params = $cartValue;
                $params['id'] = $cartIndex;
                $this->getRequest()->setParams($params);
                parent::updateItemOptionsAction();
            }
        }
    }

    /**
     * Add product to shopping cart action
     */
    public function updateCartFromCompareAction()
    {
        $cart = $this->_getCart();
        $params = $this->getRequest()->getParams();
        $product = $this->_initProduct();


       $params['qty'] = 1;
       if (!$product) {
           $this->_goBack();
           return;
       }

       $cart->addProduct($product, $params);
        $cart->save();
        $this->_getSession()->setCartWasUpdated(true);

        if (!$this->_getSession()->getNoCartRedirect(true)) {
            if (!$cart->getQuote()->getHasError()){
                $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
                $this->_getSession()->addSuccess($message);
            }
        }
        $this->_goBack();
    }

    public function updateCartFromCategoryAction()
    {
        $cart = $this->_getCart();

        $pid = $this->getRequest()->getParam('pid');
        $params = $this->getRequest()->getParams();

        if (!isset($params['options'])) {
            $params['options'] = array();
        }
        $product = Mage::getModel('catalog/product')->load($pid);
        //get cart item
        $item = false;
        if ($cart->getQuote()->getItemsCount()) {
            foreach ($cart->getQuote()->getAllVisibleItems() as $_item) {
                if($item) continue;
                if ($_item->getSku() == $product->getSku()) {
                    $item = $_item;
                }
            }
            if($item) {
                $cart->updateItem($item->getId(), new Varien_Object($params));
                $cart->save();
                $this->_getSession()->setCartWasUpdated(true);
            }
        }
        $this->_goBack();

    }

}
