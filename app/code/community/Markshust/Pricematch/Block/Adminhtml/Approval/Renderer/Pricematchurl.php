<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Pricematch_Block_Adminhtml_Approval_Renderer_Pricematchurl
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        $value = unserialize($value);
        
        // Find the array value for pricematch_url
        foreach ($value['options'] AS $optionKey => $optionValue) {
            if ($optionValue['label'] == 'pricematch_url') {
                $url = $optionValue['value'];
            }
        }
        
        return Mage::helper('markshust_pricematch')->getLinkFromUrl($url, array(
            'breakAfterChar' => 20,
            'trimSecondLine' => 20,
        ));
    }
}
