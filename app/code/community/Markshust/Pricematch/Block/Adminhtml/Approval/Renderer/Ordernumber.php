<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Pricematch_Block_Adminhtml_Approval_Renderer_Ordernumber
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        
        if ($order = Mage::getModel('sales/order')->loadByIncrementId($value)) {
            $url = $this->getUrl('*/sales_order/view', array('order_id' => $order->getId()));
            
            return "<a href=\"$url\">$value</a>";
        } else {
            // Safety, just return default if order doesn't load for some reason
            return $value;
        }
    }
}
