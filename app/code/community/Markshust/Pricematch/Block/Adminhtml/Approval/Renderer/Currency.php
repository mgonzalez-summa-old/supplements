<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Pricematch_Block_Adminhtml_Approval_Renderer_Currency
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        
        if ($value) {
            return '<div class="currency">' . Mage::helper('core')->currency($value) . '</div>';
        }
        
        return;
    }
}
