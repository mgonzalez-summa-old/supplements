<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Pricematch_Block_Adminhtml_Approval_Renderer_Currency_Bold
    extends Markshust_Pricematch_Block_Adminhtml_Approval_Renderer_Currency
{
    public function render(Varien_Object $row)
    {
        $row = parent::render($row);
        
        if ($row) {
            return "<strong>$row</strong>";
        }
        
        return;
    }
}
