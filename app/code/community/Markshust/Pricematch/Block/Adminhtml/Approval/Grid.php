<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Pricematch_Block_Adminhtml_Approval_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        
        $this->setId('markshust_pricematch_approval_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }
    
    protected function _getCollectionClass()
    {
        return 'sales/order_item_collection';
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        
        // Join up the order grid table for increment id
        $collection->join(array( 'order' => 'sales/order_grid'), 'order_id=entity_id', array('increment_id'));
        
        // Add brand attribute to the collection (uses third party module, catalog_product_index_eav needed)
        $collection->getSelect()->joinLeft(array('cpie_brand' => 'catalog_product_index_eav'), '`cpie_brand`.`entity_id` = `main_table`.`product_id` AND `cpie_brand`.`attribute_id` = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "brand")', array('attribute_id'));
        $collection->getSelect()->joinLeft(array('eaov_brand' => 'eav_attribute_option_value'), '`eaov_brand`.`option_id` = `cpie_brand`.`value`', array('brand' => 'value'));
        
        // Add flavor attribute to the collection (dropdown)
        $collection->getSelect()->joinLeft(array('cpei_flavor' => 'catalog_product_entity_int'), '`cpei_flavor`.`entity_id` = `main_table`.`product_id` AND `cpei_flavor`.`attribute_id` = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "flavor")', array('attribute_id'));
        $collection->getSelect()->joinLeft(array('eaov_flavor' => 'eav_attribute_option_value'), '`eaov_flavor`.`option_id` = `cpei_flavor`.`value`', array('flavor' => 'value'));
        
        // Add size attribute to the collection (text)
        $collection->getSelect()->joinLeft(array('cpev_size' => 'catalog_product_entity_varchar'), '`cpev_size`.`entity_id` = `main_table`.`product_id` AND `cpev_size`.`attribute_id` = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "size")', array('attribute_id', 'size' => 'value'));
        
        // Add in original product price (not purchased price)
        $collection->getSelect()->joinLeft(array('cpip_price' => 'catalog_product_index_price'), '`cpip_price`.`entity_id` = `main_table`.`product_id` AND `cpip_price`.`customer_group_id` = 0 AND `cpip_price`.`website_id` = 1', array('orig_price' => 'price'));
        
        // Add flavor attribute to the collection (decimal)
        $collection->getSelect()->joinLeft(array('cped_wholesale_price' => 'catalog_product_entity_decimal'), '`cped_wholesale_price`.`entity_id` = `main_table`.`product_id` AND `cped_wholesale_price`.`attribute_id` = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "wholesale_price")', array('attribute_id', 'wholesale_price' => 'value'));
        
        // Only show items pending approval for price match (check pricematch_url as our renderer works against that value)
        $collection->getSelect()->where('INSTR(`main_table`.`product_options`, "pricematch_url") > 0');
        
        // Only show items that haven't been approved yet
        $collection->join(array('mpi' => 'markshust_pricematch/item'), '`mpi`.`item_id` = `main_table`.`item_id` AND `mpi`.`approved` = 0');
        
        // Only show orders pending approval for price match
        $collection->addFilter('status', 'pending_approval_price_match');
        $collection->getSelect()->group('main_table.item_id');

        // Output collection query to screen
        //$collection->printlogquery(true);

        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $this->addColumn('item_id', array(
            'header'=> Mage::helper('sales')->__('Item ID'),
            'width' => '50px',
            'type'  => 'text',
            'index' => 'item_id',
            'filter_index'=>'main_table.item_id'
        ));

        $this->addColumn('increment_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
            'renderer' => 'markshust_pricematch/adminhtml_approval_renderer_ordernumber',
            'filter_index'=>'order.increment_id'
        ));
        
        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '160px',
            'filter_index'=>'order.created_at'
        ));
        
        $this->addColumn('name', array(
            'header' => Mage::helper('sales')->__('Product'),
            'index' => 'name',
            'type' => 'text',
            'width' => '100px',
        ));
        
        $this->addColumn('brand', array(
            'header' => Mage::helper('sales')->__('Brand'),
            'index' => 'brand',
            'type' => 'text',
            'width' => '60px',
            'filter' => false,
        ));

        $this->addColumn('flavor', array(
            'header' => Mage::helper('sales')->__('Flavor'),
            'index' => 'flavor',
            'type' => 'text',
            'width' => '60px',
            'filter' => false,
        ));
        
        $this->addColumn('size', array(
            'header' => Mage::helper('sales')->__('Size'),
            'index' => 'size',
            'type' => 'text',
            'width' => '60px',
            'filter' => false,
        ));
        
        $this->addColumn('pm_url', array(
            'header' => Mage::helper('sales')->__('PM URL'),
            'index' => 'product_options',
            'type' => 'text',
            'width' => '200px',
            'filter' => false,
            'renderer' => 'markshust_pricematch/adminhtml_approval_renderer_pricematchurl',
        ));
        
        $this->addColumn('orig_price', array(
            'header' => Mage::helper('sales')->__('Original Price'),
            'index' => 'orig_price',
            'type' => 'currency',
            'width' => '100px',
            'renderer' => 'markshust_pricematch/adminhtml_approval_renderer_currency',
            'filter_index'=>'cpip_price.price'
        ));
        
        $this->addColumn('wholesale_price', array(
            'header' => Mage::helper('sales')->__('Wholesale Price'),
            'index' => 'wholesale_price',
            'type' => 'currency',
            'width' => '100px',
            'renderer' => 'markshust_pricematch/adminhtml_approval_renderer_currency',
            'filter_index'=>'cped_wholesale_price.value'

        ));
        
        $this->addColumn('price', array(
            'header' => Mage::helper('sales')->__('Price Match'),
            'index' => 'price',
            'type' => 'currency',
            'width' => '100px',
            'renderer' => 'markshust_pricematch/adminhtml_approval_renderer_currency_bold',
            'filter_index'=>'main_table.price'
        ));
        
        $this->addColumn('qty_ordered', array(
            'header' => Mage::helper('sales')->__('Qty'),
            'index' => 'qty_ordered',
            'type' => 'currency',
            'width' => '100px',
        ));
        
        return parent::_prepareColumns();
    }
    
    public function getRowUrl($row)
    {
        $orderId = Mage::getModel('sales/order')->loadByIncrementId($row->getIncrementId());
        return $this->getUrl('*/sales_order_edit/start', array('order_id' => $orderId->getId()));
    }
    
    public function _prepareMassaction()
    {
        $this->setMassactionIdField('markshust_pricematch_approval_grid');
        $this->getMassactionBlock()->setFormFieldName('item_id');
        
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('sales')->__('Approve'),
            'url' => $this->getUrl('*/*/approve'),
            'confirm' => Mage::helper('sales')->__('Are you sure?'),
        ));
        
        return $this;
    }
}
