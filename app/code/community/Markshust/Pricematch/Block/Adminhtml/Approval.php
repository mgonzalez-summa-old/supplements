<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Pricematch_Block_Adminhtml_Approval
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_approval';
        $this->_blockGroup = 'markshust_pricematch';
        $this->_headerText = Mage::helper('markshust_pricematch')->__('Price Match Approval');

        parent::__construct();

        $this->_removeButton('add');
    }
}
