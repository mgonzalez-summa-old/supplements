<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create table 'markshust_pricematch_item'
*/
$table = $installer->getConnection()
    ->newTable($installer->getTable('markshust_pricematch/item'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'ID')
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Order ID')
    ->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Item ID')
        ->addColumn('approved', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 0, array(
        'nullable'  => false,
    ), 'Approved');

$installer->getConnection()->createTable($table);

$installer->endSetup();
