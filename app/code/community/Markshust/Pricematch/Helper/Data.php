<?php
/**
 * @category    Markshust
 * @package     Markshust_Pricematch
 * @author      Mark Shust <mark@shust.com>
 * @copyright   SupplementWarehouse.com
 */
class Markshust_Pricematch_Helper_Data
    extends Mage_Core_Helper_Abstract
{
    public function applyPricematchPricing($product)
    {
        // Check if product has custom options added to them, and if so, get option id's
        if ($product->hasCustomOptions() && $optionIds = $product->getCustomOption('option_ids')) {
            // Get product options so we can later match on option id and check for specific title
            $options = $product->getOptions();
            
            // Loop through custom options
            foreach (explode(',', $optionIds->getValue()) as $optionId) {
                // Loop through product options
                foreach ($options as $option) {
                    if ($option->getId() == $optionId && $option->getTitle() == 'pricematch_price') {
                        // Get option object by id
                        $optionPricematchPrice = $product->getCustomOption('option_' . $optionId);
                        
                        // Remove dollar sign and comma's from price value
                        $optionPricematchPriceValue = $optionPricematchPrice->getValue();
                        $removeChars = array(',', '$');
                        $replaceWith = array('', '');
                        $optionPricematchPriceValueRaw = doubleval(str_replace($removeChars, $replaceWith, $optionPricematchPriceValue));
                        
                        // Set price to value of custom option with 5 percent off
                        $newPrice = $optionPricematchPriceValueRaw * 0.95;
                        $product->setFinalPrice($newPrice);
                    }
                }
            }
            
            /*
            // Get cart object in session
            $quote = Mage::getSingleton('checkout/session')->getQuote();
            // Get all items in cart
            $cartItems = $quote->getAllVisibleItems();
            
            // If item exists in cart, make price same as item in cart
            if ($cartItems):
                foreach ($cartItems as $item):
                    if ($item->getSku() == $product->getSku()):
                        $product->setFinalPrice($item->getPrice());
                    endif;
                endforeach;
            endif;
            */
        } else {
            /*
            // No pricematch set, check if there is existing item in cart already at that price
            
            // Get cart object in session
            $quote = Mage::getSingleton('checkout/session')->getQuote();
            // Get all items in cart
            $cartItems = $quote->getAllVisibleItems();
            
            // If item exists in cart, make price same as item in cart
            if ($cartItems):
                foreach ($cartItems as $item):
                    echo $item->getPrice() .' <br/>';
                    if ($item->getSku() == $product->getSku()):
                        $product->setFinalPrice($item->getPrice());
                    endif;
                endforeach;
            endif;
            */
        }
        
        return $product;
    }
    
    /**
     * Get html for product options
     * 
     * @param Mage_Catalog_Model_Product $product
     */
    public function getProductOptionsHtml(Mage_Catalog_Model_Product $product)
    {
        $blockOption = Mage::app()->getLayout()->createBlock('Mage_Catalog_Block_Product_View_Options');
        $blockOption->addOptionRenderer('default', 'catalog/product_view_options_type_default', 'catalog/product/view/options/type/default.phtml');
        $blockOption->addOptionRenderer('text', 'catalog/product_view_options_type_text', 'catalog/product/view/options/type/text.phtml');
        $blockOption->addOptionRenderer('file', 'catalog/product_view_options_type_file', 'catalog/product/view/options/type/file.phtml');
        $blockOption->addOptionRenderer('select', 'catalog/product_view_options_type_select','catalog/product/view/options/type/select.phtml');
        $blockOption->addOptionRenderer('date', 'catalog/product_view_options_type_date', 'catalog/product/view/options/type/date.phtml');
        $blockOptionsHtml = null;
                
        if ($product->getTypeId() == 'simple' || $product->getTypeId() == 'virtual' || $product->getTypeId() == 'configurable') {
            $blockOption->setProduct($product);
            if ($product->getOptions()) {
                foreach ($product->getOptions() as $o) {
                    $blockOptionsHtml .= $blockOption->getOptionHtml($o);
                }
            }
        }
        
        if ($product->getTypeId() == 'configurable') {
            $blockViewType = Mage::app()->getLayout()->createBlock('Mage_Catalog_Block_Product_View_Type_Configurable');
            $blockViewType->setProduct($product);
            $blockViewType->setTemplate('catalog/product/view/type/options/configurable.phtml');
            $blockOptionsHtml .= $blockViewType->toHtml();
        }
        
        return $blockOptionsHtml;
    }
    
    /**
     * Get html for product pricematch options
     * 
     * @param Mage_Catalog_Model_Product $product
     */
    public function getProductPricematchOptionsHtml(Mage_Catalog_Model_Product $product)
    {
        $blockOption = Mage::app()->getLayout()->createBlock('Mage_Catalog_Block_Product_View_Options');
        $blockOption->addOptionRenderer('text','catalog/product_view_options_type_text', 'markshust/pricematch/catalog/product/view/options/type/pricematch.phtml');
        
        $blockOptionsHtml = null;
        
        if ($product->getTypeId() == 'simple' || $product->getTypeId() == 'virtual' || $product->getTypeId() == 'configurable') {
            $blockOption->setProduct($product);
            if ($product->getOptions()) {
                foreach ($product->getOptions() as $option) {
                    if ($option->getTitle() == 'pricematch_url' || $option->getTitle() == 'pricematch_price') {
                        $blockOptionsHtml .= $blockOption->getOptionHtml($option);
                    }
                }
            }
        }
        
        return $blockOptionsHtml;
    }
    
    /**
     * This function converts a url to clickable link (with params usage for line breaks)
     * 
     * Possible $params index values:
     * breakAfterChar = add line break at this url string index
     * trimSecondLine = trim length of second line to this string index
     * 
     * @param string $url
     * @param array $params
     * @return string
     */
    public function getLinkFromUrl($url, $params)
    {
        $breakAfterChar = $params['breakAfterChar'];
        $trimSecondLine = $params['trimSecondLine'];
        $maxLength = $breakAfterChar + $trimSecondLine;
        
        // Remove protocal from short url
        $shortUrl = str_replace('http://', '', $url);
        $shortUrl = str_replace('https://', '', $shortUrl);
        $ellipses = NULL;
        
        // Check if URL longer than maxLength
        if (isset($shortUrl[$maxLength])) {
            // Add ellipses, trim url length and add line break
            $ellipses = '...';
            $shortUrl = substr($shortUrl, 0, $breakAfterChar) . '<br />' . substr($shortUrl, $breakAfterChar, $trimSecondLine);
            // Check if URL longer than breakAfterChar (-1 because array index)
        } else if (isset($url[$breakAfterChar])) {
            // Just add line break
            $shortUrl = substr($shortUrl, 0, $breakAfterChar) . '<br />' . substr($shortUrl, $breakAfterChar);
        }
        
        // Add http:// if not exists to real url
        if (!strstr($url, 'http://') && !strstr($url, 'https://')) {
            $url = "http://$url";
        }
        
        // Return url
        return "<a href=\"$url\" target=\"_blank\">$shortUrl$ellipses</a>";
    }
}
