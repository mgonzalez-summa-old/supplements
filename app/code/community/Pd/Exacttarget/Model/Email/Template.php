<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 * 
 * NOTICE OF LICENSE
 * 
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE 
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING 
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA") 
 * is a legal agreement between you (either an individual or a single entity) and Precision 
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above 
 * which may include associated software components, media, printed materials, and "online" 
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying, 
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the 
 * terms of this EULA. This license agreement represents the entire agreement concerning 
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If 
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international 
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget 
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE. 
 *  The ExactTarget Integration for Magento is licensed as follows: 
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget 
 *  Integration for Magento on your development, staging and production servers running a validly 
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for 
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to 
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento, 
 *  except and only to the extent that such activity is expressly permitted by applicable law 
 *  notwithstanding this limitation. 
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget 
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as 
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento 
 *  and subject to the terms and conditions of this EULA. 
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION 
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail 
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies 
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento 
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual 
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration 
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright 
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content. 
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento. 
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of 
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness 
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy 
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget 
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused 
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision 
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any 
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation, 
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or 
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or 
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract, 
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the 
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions 
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, 
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */

class Pd_Exacttarget_Model_Email_Template extends Mage_Core_Model_Email_Template
{
    public function send($email, $name = null, array $variables = array())
    {
        $storeId    =    (!is_null($this->getStoreId()) ? $this->getStoreId() : $this->getDesignConfig()->getStore());
        /**
         * Check to make sure the connection to ExactTarget is up and running (i.e. proper connection information supplied and/or the connection
         * between the site and ET is running up without interruption).
         */
        if(Mage::getModel('exacttarget/failsafe')->hasValidConnection()):
            if(Mage::getStoreConfig('exacttarget/setup/active',$storeId) == 1):
                $helper = Mage::helper('exacttarget');
                require_once(Mage::helper('exacttarget')->getRequiredFiles());

                $emailTemplateId  = $this->getTemplateId();
                $emailTemplate    =  Mage::helper('exacttarget/exacttarget')->translateTemplateId($emailTemplateId,$storeId);
                
                //Start out with Asynchronous sends disabled
                $asyncEnabled	  = 0;
                
                //After getting the returned mapped value if the transactional template is not loaded the default 
                //magento template will be sent regardless that in the ET config settings the user has selected to use
                //their custom transactional template to pass to ET.
                if (is_numeric($emailTemplate)):
                    $this->setId($emailTemplate);
                    $emailObject    = $this->load($emailTemplate);
                 
                    //If the sender email and name is not set, using the general email id - if emails are not sending out verify
                    // in the config these values are present or switch to the system email address to have these sent from.
                    if ($emailObject->getTemplateSenderEmail() == '' || is_null($emailObject->getTemplateSenderEmail())):
                        $this->setSenderEmail(Mage::getStoreConfig('exacttarget/setup/et_from_email',$storeId));
                        $this->setSenderName(Mage::getStoreConfig('exacttarget/setup/et_from_name',$storeId));
                    endif;
                    $this->getProcessedTemplateSubject($variables);
                    $this->setTemplateSubject($emailObject->getTemplateSubject());
                endif;
                
                $config_array    = Mage::helper('exacttarget/exacttarget')->emailTemplateConfig();
                $customerKey     = '';
                foreach($config_array as $value):
                    if ($emailTemplate == Mage::getStoreConfig('exacttarget/emails/'.$value['template'],$storeId)):
                    
                    	//Is the email set to be Asynchronous or Synchronous
                    	$asyncEnabled	=	strtolower(Mage::getStoreConfig('exacttarget/emails/'.$value['sync'],$storeId));
                    		
                        $bccAllowed     =    false;
                        
                        if(array_key_exists('bcc_enabled', $value)):
                            if($value['bcc_enabled'] == 'yes'):
                                $bccAllowed    = true;
                                $bccList       = Mage::getStoreConfig($value['bcc_path'],$storeId);
                            endif;
                        endif;
                
                        if (strtolower(Mage::getStoreConfig('exacttarget/emails/'.$value['active'],$storeId)) == 'magento'):
                            return parent::send($email, $name, $variables);
                        endif;
                        
                        $customerKey    =    Mage::getStoreConfig('exacttarget/emails/'.$value['key'],$storeId);
                        
                        break;
                    endif;
                endforeach;
                
                if(empty($customerKey) || $customerKey == '' || $customerKey == NULL):
                    return parent::send($email, $name, $variables);
                endif;

                if (!$this->isValidForSend()) {
                    Mage::logException(new Exception('This letter cannot be sent.')); // translation is intentionally omitted
                    return false;
                }
                
                $emails = array_values((array)$email);
                $names = is_array($name) ? $name : (array)$name;
                $names = array_values($names);
                foreach ($emails as $key => $email) {
                    if (!isset($names[$key])) {
                        $names[$key] = substr($email, 0, strpos($email, '@'));
                    }
                }
        
                $variables['email'] = reset($emails);
                $variables['name'] = reset($names);
        
                ini_set('SMTP', Mage::getStoreConfig('system/smtp/host'));
                ini_set('smtp_port', Mage::getStoreConfig('system/smtp/port'));
        
                $mail = $this->getMail();
        
                $setReturnPath = Mage::getStoreConfig(self::XML_PATH_SENDING_SET_RETURN_PATH);
                
                switch ($setReturnPath) {
                    case 1:
                        $returnPathEmail = $this->getSenderEmail();
                        break;
                    case 2:
                        $returnPathEmail = Mage::getStoreConfig(self::XML_PATH_SENDING_RETURN_PATH_EMAIL);
                        break;
                    default:
                        $returnPathEmail = null;
                        break;
                }
        
                if ($returnPathEmail !== null) {
                    $mailTransport = new Zend_Mail_Transport_Sendmail("-f".$returnPathEmail);
                    Zend_Mail::setDefaultTransport($mailTransport);
                }
        
                foreach ($emails as $key => $email) {
                    $mail->addTo($email, '=?utf-8?B?' . base64_encode($names[$key]) . '?=');
                }
        
                $this->setUseAbsoluteLinks(true);
                $text = $this->getProcessedTemplate($variables, true);
                
                if($this->isPlain()) {
                    $mail->setBodyText($text);
                } else {
                    $mail->setBodyHTML($text);
                }
                
                //If $emailTemplate is a numeric value (i.e. a transactional email template was selected for the template file in the ET setup - see abandoned carts - must set up Abandon Cart
                //template file up for this in Magento after extension is installed) then set the template subject - prior it was not being set and emails not sent
                if(is_numeric($emailTemplate)):
                    $this->setId($emailTemplate);
                    $subject    =    $this->getProcessedTemplateSubject($variables);
                else:
                
                    if($mail->getSubject() == ''):
                        $mail->setSubject('=?utf-8?B?' . base64_encode($this->getProcessedTemplateSubject($variables)) . '?=');
                    endif;
                    $subject    =    $mail->getSubject();
                endif;
                
                if ($mail->getFrom() == ''):
                    $mail->setFrom($this->getSenderEmail(), $this->getSenderName());
                endif;
    
                try{          
                	/* Create the Soap Client */  
                    $client                            = new ExactTargetSoapClient(Mage::helper('exacttarget')->getWsdl(), array('trace'=>1));  
              
                    /* Set username and password here */  
                    $client->username                  = Mage::helper('exacttarget')->getApiUser($storeId); 
                    $client->password                  = Mage::helper('exacttarget')->getApiPassword($storeId);
                    
                    /*% ExactTarget_TriggeredSendDefinition */				
            		$tsd                               = new ExactTarget_TriggeredSendDefinition();
                    $tsd->CustomerKey                  = $customerKey;
                    
                    $subscriber                        = new ExactTarget_Subscriber();  
                    $subscriber->EmailAddress          = $email;  
                    $subscriber->SubscriberKey         = $email;                      
        		    $subscriber->Owner                 = new ExactTarget_Owner(); 
        		    $subscriber->Owner->FromAddress    = (!is_null(Mage::getStoreConfig('exacttarget/setup/et_from_email',$storeId))) ? Mage::getStoreConfig('exacttarget/setup/et_from_email',$storeId) : Mage::getStoreConfig('trans_email/ident_general/email',$storeId);
        		    $subscriber->Owner->FromName       = (!is_null(Mage::getStoreConfig('exacttarget/setup/et_from_name',$storeId))) ? Mage::getStoreConfig('exacttarget/setup/et_from_name',$storeId) : Mage::getStoreConfig('trans_email/ident_general/name',$storeId);
        		    
            		$attribute1                        = new ExactTarget_Attribute();
            		$attribute1->Name                  = "magento_lastemail_title";
            		$attribute1->Value                 = $subject;
            		
            		$attribute2                        = new ExactTarget_Attribute();
            		$attribute2->Name                  = "email_content";
            		$attribute2->Value                 = $text;
            		
            		$subscriber->Attributes=array($attribute1,$attribute2);

            		//If Asynchronous is disabled send via the triggered send; otherwise log the email content and then send via a cron that
            		//runs every 5 minutes to push emails to ET.
            		if ($asyncEnabled == 0):
            			Mage::helper('exacttarget/exacttarget')->triggeredSend($client,$tsd,$subscriber,$storeId);
            		else:
            		
            			$data								=	array();
            			$data['email_address']				=	$email;
            			$data['sub_key']					=	$email;
            			$data['api_external_key']			=	$customerKey;
            			$data['subject']					=	$subject;
            			$data['content']					=	serialize($text);
            			$data['storeId']					=	$storeId;
            			$data['timestamp']					=	now();
            			$data['date_created']				=	now();
            			$data['sent_to_exacttarget_flag']	=	0;
            			
            			$model	=	Mage::getModel('exacttarget/async');
            			$model->addData($data);
            			$model->save();
            		endif;
            		
            		if($bccAllowed):
            		    if(!is_null($bccList) && $bccList != ''):
            		        $this->sendBccEmails($subject,$text,$bccList,$storeId);
            		    endif;
            		endif;
            		
                } catch (Exception $e) {
                    Mage::logException($e);
                    //Zend_Debug::dump($e);
                    return false;
                } catch(SoapFault $e) {
                    Mage::log($e);
                   // Zend_Debug::dump($e);
                   return false;
                }
                
                Mage::getSingleton('adminhtml/session')->addNotice('This email went through ExactTarget');
                
                return true;
            
            else:
                return parent::send($email, $name, $variables);
            endif;
        else:
            return parent::send($email, $name, $variables);
        endif;
    }
    

    /**
     * Allows for sales emails to attach bcc users to emails and send through Magento only
     * @param string $subject
     * @param mixed $emailContent
     * @param string $bccList
     * @param int $storeId
     */
    public function sendBccEmails($subject,$emailContent,$bccList,$storeId)
    {
    
        $xheaders  = "";
        $xheaders .= "From: <".Mage::getStoreConfig('exacttarget/setup/et_from_email',$storeId).">\n";
        $xheaders .= "X-Sender: <".Mage::getStoreConfig('exacttarget/setup/et_from_email',$storeId).">\n";
        $xheaders .= "X-Mailer: PHP\n"; // mailer
        $xheaders .= "X-Priority: 3\n"; //1 Urgent Message, 3 Normal
        $xheaders .= "Content-Type:text/html; charset=\"iso-8859-1\"\n";
        $xheaders .= "Bcc:$bccList"."\n";
    
    
        mail(null,$subject,$emailContent,$xheaders);
    }
}