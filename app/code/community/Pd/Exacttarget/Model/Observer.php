<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 * 
 * NOTICE OF LICENSE
 * 
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE 
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING 
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA") 
 * is a legal agreement between you (either an individual or a single entity) and Precision 
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above 
 * which may include associated software components, media, printed materials, and "online" 
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying, 
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the 
 * terms of this EULA. This license agreement represents the entire agreement concerning 
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If 
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international 
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget 
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE. 
 *  The ExactTarget Integration for Magento is licensed as follows: 
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget 
 *  Integration for Magento on your development, staging and production servers running a validly 
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for 
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to 
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento, 
 *  except and only to the extent that such activity is expressly permitted by applicable law 
 *  notwithstanding this limitation. 
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget 
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as 
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento 
 *  and subject to the terms and conditions of this EULA. 
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION 
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail 
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies 
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento 
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual 
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration 
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright 
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content. 
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento. 
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of 
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness 
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy 
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget 
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused 
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision 
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any 
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation, 
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or 
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or 
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract, 
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the 
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions 
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, 
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Exacttarget_Model_Observer
{
    
    public function __construct(){}
    
    /**
     * This method will check to see if the connection to ExactTarget is back up and running, if it is
     * then a call to get all the data (i.e. newsletter users) and send those users up to be added to 
     * the proper list.
     */
    public function checkForConnection()
    {
    	
		if(Mage::getStoreConfig('exacttarget/setup/active') == 1):
	        $has_connection     = Mage::getModel('exacttarget/failsafe')->hasValidConnection();
	        $ledgerListArray    = array();
	        //If the connection is valid gather the users to send up to ET and add them to the 
	        //proper list otherwise run the check for the connection again.
	        if($has_connection):
	            
	            $ledgerListArray    =    Mage::getModel('exacttarget/failsafe')->fetchNewsletterLedgetList();
	            
	            require_once(Mage::helper('exacttarget')->getRequiredFiles());    
	            
	    	    $wsdl           =    Mage::helper('exacttarget')->getWsdl();
	    	    $et_password    =    Mage::getStoreConfig('exacttarget/setup/apipassword');
	    	    $et_username    =    Mage::getStoreConfig('exacttarget/setup/username');
	            
	            //loop through the list and call the API to send list users up
	            if ($ledgerListArray):
	                foreach($ledgerListArray as $listUser):
	                
	                    //Verifying that through each loop that the connection is still valid. if not then
	                    //break out of the loop and stop trying to send users to exacttarget
	                    if (!Mage::getModel('exacttarget/failsafe')->hasValidConnection()):
	                        break;
	                    endif;
	                    
	                    switch(strtolower($listUser['transaction_type']))
	                    {
	                        case 'subscribe':
	                            Mage::log('subscribe');
	                            try{
	                                $client = new ExactTargetSoapClient($wsdl, array('trace'=>1));
	                                
	                                $client->username = $et_username;
	                                $client->password = $et_password;
	                                
	                                $subscriber  = new ExactTarget_Subscriber();
	                                $subscriber->SubscriberKey = $listUser['email_address'];
	                                $subscriber->EmailAddress  = $listUser['email_address'];
	                                
	                                if ($listUser['customer_id'] > 0 && !is_null($listUser['customer_id'])):
	                                
	                                    Mage::log('Got in here');
	                        		    $customer     =    Mage::getModel('customer/customer')->load($listUser['customer_id']);
	                        		
	                        		
	                            		$addressId    =    $customer->getDefaultBilling();
	                            		$address      =    Mage::getModel('customer/address')->load($addressId);
	                            		
	                            		//Next several lines - down through $attributeOptionSingle - are used to get the
	                            		//text value for the customers gender selection.
	                            		$entityType   =    Mage::getModel('eav/config')->getEntityType('customer');
	                                    $entityTypeId =    $entityType->getEntityTypeId();
	                            
	                                    $attribute    =     Mage::getResourceModel('eav/entity_attribute_collection')
	                                                            ->setCodeFilter('gender')
	                                                            ->setEntityTypeFilter($entityTypeId)
	                                                            ->getFirstItem();
	                            
	                                    $attributeId  =    $attribute->getAttributeId();
	                                    $optionId     =    $customer->getGender();
	                            
	                                    $attributeOptionSingle = Mage::getResourceModel('eav/entity_attribute_option_collection')
	                                                                ->setPositionOrder('asc')
	                                                                ->setAttributeFilter($attributeId)
	                                                                ->addFieldToFilter('main_table.option_id', array('in' => $optionId))
	                                                                ->setStoreFilter()
	                                                                ->load()
	                                                                ->getFirstItem();
	                            	
	                                    //Customer Information Attributes - if enabled to have Magento send they will be sent when applicable.
	                            		if(strtolower(Mage::getStoreConfig('exacttarget/information/firstname')) == 'yes'):
	                            		    $attribute1            =    new ExactTarget_Attribute();
	                            		    $attribute1->Name      =    'firstname';
	                            		    $attribute1->Value     =    $customer->getFirstname();
	                            		endif;
	                            		
	                            		if(strtolower(Mage::getStoreConfig('exacttarget/information/lastname')) == 'yes'):
	                            		    $attribute2            =    new ExactTarget_Attribute();
	                            		    $attribute2->Name      =    'lastname';
	                            		    $attribute2->Value     =    $customer->getLastname();
	                            		endif;
	                            		
	                            		if(strtolower(Mage::getStoreConfig('exacttarget/information/gender')) == 'yes'):
	                            		    $attribute3            =    new ExactTarget_Attribute();
	                            		    $attribute3->Name      =    'gender';
	                            		    $attribute3->Value     =    $attributeOptionSingle->getDefaultValue();
	                            		endif;
	                            		
	                            		if(strtolower(Mage::getStoreConfig('exacttarget/information/address1')) == 'yes'):
	                            		    $attribute4            =    new ExactTarget_Attribute();
	                            		    $attribute4->Name      =    'address';
	                            		    $attribute4->Value     =    $address->getStreet(1);
	                            		endif;
	                            	
	                            	    if(strtolower(Mage::getStoreConfig('exacttarget/information/address2')) == 'yes'):
	                            	        $attribute5            =    new ExactTarget_Attribute();
	                            	        $attribute5->Name      =    'address_2';
	                            	        $attribute5->Value     =    $address->getStreet(2);
	                            	    endif;
	                            	
	                            		if(strtolower(Mage::getStoreConfig('exacttarget/information/city')) == 'yes'):
	                            		    $attribute6            =    new ExactTarget_Attribute();
	                            		    $attribute6->Name      =    'city';
	                            		    $attribute6->Value     =    $address->getCity();
	                            		endif;
	                            		
	                            		if(strtolower(Mage::getStoreConfig('exacttarget/information/state')) == 'yes'):
	                            		    $attribute7            =    new ExactTarget_Attribute();
	                            		    $attribute7->Name      =    'state';
	                            		    $attribute7->Value     =    $address->getRegion();
	                            		endif;
	                            		
	                            		if(strtolower(Mage::getStoreConfig('exacttarget/information/zipcode')) == 'yes'):
	                            		    $attribute8            =    new ExactTarget_Attribute();
	                            		    $attribute8->Name      =    'zipcode';
	                            		    $attribute8->Value     =    $address->getPostcode();
	                            		endif;
	                            		
	                            		if(strtolower(Mage::getStoreConfig('exacttarget/information/city')) == 'yes'):
	                            		    $attribute9            =    new ExactTarget_Attribute();
	                            		    $attribute9->Name      =    'phone_number';
	                            		    $attribute9->Value     =    $address->getTelephone();
	                            		endif;
	                            		
	                            		if(strtolower(Mage::getStoreConfig('exacttarget/information/country')) == 'yes'):
	                            		    $attribute10            =    new ExactTarget_Attribute();
	                            		    $attribute10->Name      =    'country';
	                            		    $attribute10->Value     =    $address->getCountryId();
	                            		endif;
	                            		
	                            		if(strtolower(Mage::getStoreConfig('exacttarget/information/birthday')) == 'yes'):
	                            		    $attribute11            =    new ExactTarget_Attribute();
	                            		    $attribute11->Name      =    'dob';
	                            		    $attribute11->Value     =    $customer->getDob();
	                            		endif;
	                            		
	                            		$subscriber->Attributes=array($attribute1,$attribute2,$attribute3,$attribute4,$attribute5,$attribute6,$attribute7,$attribute8,$attribute9,$attribute10,$attribute11);
	                        		endif;
	                                
	                                $list = new ExactTarget_SubscriberList();  
	                                $list->ID = Mage::getStoreConfig('exacttarget/emails/email_newsletter_listid');       
	                                $subscriber->Lists[] = $list;   
	                                $so = new ExactTarget_SaveOption();  
	                                $so->PropertyName = "*";  
	                                $so->SaveAction = ExactTarget_SaveAction::UpdateAdd;              
	                                $soe = new SoapVar($so, SOAP_ENC_OBJECT, 'SaveOption', "http://exacttarget.com/wsdl/partnerAPI");              
	                                $opts = new ExactTarget_UpdateOptions();              
	                                $opts->SaveOptions = array($soe);  
	                                $subscriber->Status = ExactTarget_SubscriberStatus::Active;  
	                                $object = new SoapVar($subscriber, SOAP_ENC_OBJECT, 'Subscriber', "http://exacttarget.com/wsdl/partnerAPI");  
	                                $request = new ExactTarget_CreateRequest();  
	                                $request->Options = $opts;  
	                                $request->Objects = array($object);  
	                                $results = $client->Create($request); 
	                            } catch(Exception $e) {
	                                Mage::log($e->getMessage());
	                            }
	                            
	                            
	                        break;
	                        
	                        case 'unsubscribe':
	                            try {
	                                $client = new ExactTargetSoapClient($wsdl, array('trace'=>1));
	                                
	                                $client->username = $et_username;
	                                $client->password = $et_password;
	                                
	                                $er = new ExactTarget_ExecuteRequest();
	                                $er->Name = 'LogUnsubEvent';
	                                
	                                $er->Parameters = array();
	                                
	                                    $prop = new ExactTarget_APIProperty();
	                                    $prop->Name    = "SubscriberKey";
	                                    $prop->Value   = $listUser['email_address'];
	                                    
	                                $er->Parameters[]  = $prop;
	                                
	                                    $prop = new ExactTarget_APIProperty();
	                                    $prop->Name    = 'EmailAddress';
	                                    $prop->Value   = $listUser['email_address'];
	                                    
	                                $er->Parameters[]  = $prop;
	                                $er->Options = NULL;
	                                
	                                    $prop = new ExactTarget_APIProperty();
	                                    $prop->Name    = 'ListID';
	                                    $prop->Value   = $listUser['exacttarget_list_id'];
	                                    
	                                $er->Parameters[]  = $prop;
	                                
	                                    $prop = new ExactTarget_APIProperty();
	                                    $prop->Name    = 'BatchID';
	                                    $prop->Value   = 0;
	                                    
	                                $er->Parameters[]  = $prop;
	                                
	                                $erm = new ExactTarget_ExecuteRequestMsg();
	                                $erm->Requests = array();
	                                
	                                $erm->Requests[] = new SoapVar($er,SOAP_ENC_OBJECT, 'ExecuteRequest', 'http://exacttarget.com/wsdl/partnerAPI');
	                                
	                                $result = $client->Execute($erm);
	                            } catch(Exception $e) {
	                                Mage::log($e->getMessage());
	                            }
	                            
	                        break;
	                    }
	                    
	                    //Updates the record that was just sent over to ExactTarget so it will not be sent again.
	                    $data = array();
	                    $data['sent_to_exacttarget_flag']        =    1;
	                    $data['date_sent_to_exacttarget']        =    date('Y-m-d H:i:s');
	                    
	                    $model = Mage::getModel('exacttarget/failsafe')->load($listUser['id'])->addData($data);
	                    $model->setId($listUser['id'])->save();
	                endforeach;
	            endif;
	            
	        else:
	           // Mage::log('Not a valid connection.');
	        endif;
        endif;
    }

    /**
     * After registration check to see if the customer registered for the newsletter, if they did then push them over to ET, if they did not do nothing for now.
     * @param mixed $observer
     */
    public function registration($observer)
    {
    	$customer	=	$observer->getCustomer();
    
    	if (Mage::getStoreConfig('exacttarget/setup/active') == 1):
    		if ($customer->getIsSubscribed()):
    			Mage::helper('exacttarget/exacttarget')->subscribe($customer->getEmail(),$observer->getCustomer()->getStoreId());
    		endif;
    	endif;
    }
}