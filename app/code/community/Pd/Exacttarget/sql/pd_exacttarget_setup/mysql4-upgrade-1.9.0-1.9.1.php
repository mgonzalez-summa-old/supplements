<?php
$installer = $this;
$installer->startSetup();

$installer->run("
        
        ALTER TABLE `exacttarget_async_email_table` CONVERT TO CHARACTER SET utf8;
");

$installer->endSetup();