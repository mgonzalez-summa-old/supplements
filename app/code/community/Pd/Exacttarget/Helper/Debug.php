<?php
class Pd_Exacttarget_Helper_Debug extends Mage_Core_Helper_Abstract
{
	
	/**
	 * A Generic debug logging method to capture api request/response from ET if the request passed in is a API request or just general
	 * debug information if the request is not a an API request.
	 * @param mix $logItem
	 * @param string $logFile
	 * @param boolean $isApiLogRequest
	 * @param string $wrapperText
	 */
	public function logDebugText($logItem,$logFile,$isApiLogRequest,$wrapperText)
	{
		
	   Mage::log('******** Start '.$wrapperText.' ********',1,$logFile);
		
       $startTime = time();
       Mage::log(PHP_EOL.'Started at '.date('Y.m.d H:i:s').PHP_EOL.PHP_EOL,1,$logFile);
		if($isApiLogRequest):
			Mage::log($logItem->__getLastRequestHeaders(),1,$logFile);
			Mage::log($logItem->__getLastRequest(),1,$logFile);
			Mage::log($logItem->__getLastResponseHeaders(),1,$logFile);
			Mage::log($logItem->__getLastResponse(),1,$logFile);
		else:
			Mage::log($logItem,1,$logFile);
		endif;

       Mage::log('Finished at '.date('Y.m.d H:i:s').PHP_EOL.PHP_EOL,1,$logFile);
       Mage::log('Time spent in total: '.(time() - $startTime).' second'.PHP_EOL,1,$logFile);
		
		Mage::log('******** End '.$wrapperText.' ********',1,$logFile);
	}
}