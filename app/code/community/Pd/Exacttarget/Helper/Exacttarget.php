<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 * 
 * NOTICE OF LICENSE
 * 
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE 
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING 
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA") 
 * is a legal agreement between you (either an individual or a single entity) and Precision 
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above 
 * which may include associated software components, media, printed materials, and "online" 
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying, 
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the 
 * terms of this EULA. This license agreement represents the entire agreement concerning 
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If 
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international 
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget 
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE. 
 *  The ExactTarget Integration for Magento is licensed as follows: 
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget 
 *  Integration for Magento on your development, staging and production servers running a validly 
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for 
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to 
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento, 
 *  except and only to the extent that such activity is expressly permitted by applicable law 
 *  notwithstanding this limitation. 
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget 
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as 
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento 
 *  and subject to the terms and conditions of this EULA. 
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION 
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail 
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies 
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento 
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual 
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration 
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright 
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content. 
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento. 
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of 
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness 
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy 
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget 
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused 
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision 
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any 
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation, 
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or 
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or 
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract, 
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the 
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions 
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, 
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Exacttarget_Helper_Exacttarget extends Mage_Core_Helper_Data
{
    protected $_batchExport;
    
    public function triggeredSend($client,$tsd,$subscriber,$storeId)
    {
		/*% ExactTarget_TriggeredSend */				
		$ts = new ExactTarget_TriggeredSend();
		$ts->TriggeredSendDefinition = new SoapVar($tsd, SOAP_ENC_OBJECT, 'TriggeredSendDefinition', "http://exacttarget.com/wsdl/partnerAPI");
		
		// Associate a subscriber and attributes to the triggered send
		$ts->Subscribers = array();

		$ts->Subscribers[] = $subscriber; // add the subscriber to the send
		
		//Necessary for Enterprise 2.0 accounts
		if(strtolower(Mage::getStoreConfig('exacttarget/setup/et_account',$storeId)) == 'enterprise 2.0'):
            $clientID = new ExactTarget_ClientID();
            $clientID->ID = Mage::getStoreConfig('exacttarget/setup/member_id',$storeId);
            $ts->Client = $clientID;
		endif;
		
		// create SoapVar object
		$object = new SoapVar($ts, SOAP_ENC_OBJECT, 'TriggeredSend', "http://exacttarget.com/wsdl/partnerAPI");

		// create request object
		$request = new ExactTarget_CreateRequest();
		$request->Options = NULL;
		$request->Objects = array($object);
		$results = $client->Create($request);
		
		if(Mage::getStoreConfig('exacttarget/setup/debug',$storeId) ==1):
			$logFile			=	'exacttarget_email.log';
			$isApiLogRequest	=	true;
			$wrapperText		=	'Transactional Email Send Debug';
			Mage::helper('exacttarget/debug')->logDebugText($client,$logFile,$isApiLogRequest,$wrapperText);
		endif;
    }
    
    
    public function dataExtension($client_de,$data_extension,$numberOfSends,$rowId,$storeId)
    {
		//Necessary for Enterprise 2.0 accounts
		if(strtolower(Mage::getStoreConfig('exacttarget/setup/et_account',$storeId)) == 'enterprise 2.0'):
            $clientID = new ExactTarget_ClientID();
            $clientID->ID = Mage::getStoreConfig('exacttarget/setup/member_id',$storeId);
            $data_extension->Client = $clientID;
		endif;
        
		$de_soap_object                 =    new SoapVar($data_extension,SOAP_ENC_OBJECT,'DataExtensionObject','http://exacttarget.com/wsdl/partnerAPI');
		
		$update_options                 =    new ExactTarget_UpdateOptions();
		
// 		$update_options->RequestType    =    ExactTarget_RequestType::Asynchronous;
// 		$update_options->RequestTypeSpecified    =    true;
// 		$update_options->QueuePriority   =    ExactTarget_Priority::High;
// 		$update_options->QueuePrioritySpecified  = true;
// 		$update_options->SendResponseTo    =    array();
		
// 		$ar = new ExactTarget_AsyncResponse();
// 		$ar->ConversationID = rand();
// 		$ar->RespondWhen = ExactTarget_RespondWhen::OnCallComplete;
// 		$ar->ResponseType = ExactTarget_AsyncResponseType::email;
// // 		$ar->ResponseAddress = (Mage::getStoreConfig('exacttarget/setup/async_email') != '') ? Mage::getStoreConfig('exacttarget/setup/de_async_email') : ''; //not a real email address that I'm aware but we need one if it's not set - catch all just in case.
// 		$ar->ResponseAddress = 'darren.brink@gmail.com';
// 		$ar->IncludeResults = true;
// 		$ar->IncludeObjects = true;
// 		$ar->OnlyIncludeBase = false;
// 		$update_options->SendResponseTo[] = $ar;
		
		$save_option                    =    new ExactTarget_SaveOption();
		$save_option->PropertyName      =    'DataExtensionObject';
		$save_option->SaveAction        =    ExactTarget_SaveAction::UpdateAdd;
		
		$update_options->SaveOptions[]  =    new SoapVar($save_option,SOAP_ENC_OBJECT,'SaveOption','http://exacttarget.com/wsdl/partnerAPI');
		
		$de_request                     =    new ExactTarget_UpdateRequest();
		$de_request->Options            =    $update_options;
		$de_request->Objects            =    array($de_soap_object);
		
		$de_results                     =    $client_de->Update($de_request);    
		
		if(Mage::getStoreConfig('exacttarget/setup/debug',$storeId) ==1):
			$logFile			=	'exacttarget_dataextension.log';
			$isApiLogRequest	=	true;
			$wrapperText		=	'Data Extension Debug';
			Mage::helper('exacttarget/debug')->logDebugText($client_de,$logFile,$isApiLogRequest,$wrapperText);
		endif;

        $model    =    Mage::getModel('dataextensions/dataextensionlog');
        $data     =    array();
        
        if($numberOfSends >= 1):
            $data['resend_date']        =    date('Y-m-d H:i:s');
            $data['number_of_sends']    =    $numberOfSends + 1;
        endif;
        
        //Update the status of the item.
		if(strtolower($de_results->OverallStatus) == 'ok'):
		
		    $data['sent_to_exacttarget']    =    1;
		    $data['send_result']            =    strtoupper($de_results->OverallStatus);
		    $data['status_message']         =    $de_results->Results->StatusMessage;
		    $data['error_code']             =    '';
		    
		    //Only set if the number of sends is null (i.e. first send)
		    if(is_null($numberOfSends)):
		        $data['number_of_sends']    =    1;
		        $data['date_sent']          =    date('Y-m-d H:i:s');
		    endif;
		else:
		    $data['send_result']            =    strtoupper($de_results->OverallStatus);
		    $data['status_message']         =    $de_results->Results->StatusMessage;
		endif;
		
        $model->load($rowId)->addData($data);
        $model->setId($rowId)->save();
    }
    
    
    public function subscribe($email,$storeId)
    {
    	if(Mage::getModel('exacttarget/failsafe')->hasValidConnection()):
    		if (Mage::getStoreConfig('exacttarget/setup/active',$storeId) == 1):
		        require_once(Mage::helper('exacttarget')->getRequiredFiles());    
			    $wsdl = Mage::helper('exacttarget')->getWsdl();
			    
		        $client = new ExactTargetSoapClient($wsdl, array('trace'=>1));  
		        $client->username = Mage::getStoreConfig('exacttarget/setup/username',$storeId); 
		        $client->password = Mage::getStoreConfig('exacttarget/setup/apipassword',$storeId);
		  
		        $subscriber = new ExactTarget_Subscriber();  
		        $subscriber->EmailAddress = $email;  
		        $subscriber->SubscriberKey = $email;
		        
		        $websiteId = Mage::app()->getStore()->getWebsiteId();
		                        
		        $customer = Mage::getModel('customer/customer')
		                        ->setWebsiteId($websiteId)
		                        ->loadByEmail($email);
		        
		        $customerWebsite	=	$customer->getWebsiteId();
				
				if ($customer->getId() > 0):
		    		$addressId    =    $customer->getDefaultBilling();
		    		$address      =    Mage::getModel('customer/address')->load($addressId);
		    		
		    		//Next several lines - down through $attributeOptionSingle - are used to get the
		    		//text value for the customers gender selection.
		    		$entityType   =    Mage::getModel('eav/config')->getEntityType('customer');
		            $entityTypeId =    $entityType->getEntityTypeId();
		    
		            $attribute    =     Mage::getResourceModel('eav/entity_attribute_collection')
		                                    ->setCodeFilter('gender')
		                                    ->setEntityTypeFilter($entityTypeId)
		                                    ->getFirstItem();
		    
		            $attributeId  =    $attribute->getAttributeId();
		            $optionId     =    $customer->getGender();
		    
		            $attributeOptionSingle = Mage::getResourceModel('eav/entity_attribute_option_collection')
		                                        ->setPositionOrder('asc')
		                                        ->setAttributeFilter($attributeId)
		                                        ->addFieldToFilter('main_table.option_id', array('in' => $optionId))
		                                        ->setStoreFilter()
		                                        ->load()
		                                        ->getFirstItem();
		            $firstname				=	Mage::getStoreConfig('newsletter/attribute/firstname',$storeId);
		            $lastname				=	Mage::getStoreConfig('newsletter/attribute/lastname',$storeId);
		            $gender					=	Mage::getStoreConfig('newsletter/attribute/gender',$storeId);
		            $address_1				=	Mage::getStoreConfig('newsletter/attribute/address',$storeId);
		            $address_2				=	Mage::getStoreConfig('newsletter/attribute/address_2',$storeId);
		            $city					=	Mage::getStoreConfig('newsletter/attribute/city',$storeId);
		            $state					=	Mage::getStoreConfig('newsletter/attribute/state',$storeId);
		            $postCode				=	Mage::getStoreConfig('newsletter/attribute/zipcode',$storeId);
		            $phoneNumber			=	Mage::getStoreConfig('newsletter/attribute/phone_number',$storeId);
		            $country				=	Mage::getStoreConfig('newsletter/attribute/country',$storeId);
		            $dob					=	Mage::getStoreConfig('newsletter/attribute/dob',$storeId);
		             
		            $attribute1            =    new ExactTarget_Attribute();
		            $attribute1->Name      =    $firstname;
		            $attribute1->Value     =    $customer->getFirstname();
		             
		            $attribute2            =    new ExactTarget_Attribute();
		            $attribute2->Name      =    $lastname;
		            $attribute2->Value     =    $customer->getLastname();
		             
		            $attribute3            =    new ExactTarget_Attribute();
		            $attribute3->Name      =    $gender;
		            if($customer->getGender()):
		            	$attribute3->Value     =    $attributeOptionSingle->getDefaultValue();
		            else:
		            	$attribute3->Value	   =	'';
		            endif;
		             
		            $attribute4            =    new ExactTarget_Attribute();
		            $attribute4->Name      =    $address_1;
		            	
		            if($address->getStreet(1)):
		            	$attribute4->Value     =    $address->getStreet(1);
		            else:
		            	$attribute4->Value     =    '';
		            endif;
		            
		            $attribute5            =    new ExactTarget_Attribute();
		            $attribute5->Name      =    $address_2;
		             
		            if($address->getStreet(2)):
		            	$attribute5->Value     =    $address->getStreet(2);
		            else:
		            	$attribute5->Value     =    '';
		            endif;
		            
		            $attribute6            =    new ExactTarget_Attribute();
		            $attribute6->Name      =    $city;
		            
		            if($address->getCity()):
		            	$attribute6->Value     =    $address->getCity();
		            else:
		            	$attribute6->Value     =    '';
		            endif;
		             
		            $attribute7            =    new ExactTarget_Attribute();
		            $attribute7->Name      =    $state;
		            	
		            if($address->getRegion()):
		            	$attribute7->Value     =    $address->getRegion();
		            else:
		            	$attribute7->Value     =    '';
		            endif;
		            
		            $attribute8            =    new ExactTarget_Attribute();
		            $attribute8->Name      =    $postCode;
		            
		            if($address->getPostcode()):
		            	$attribute8->Value     =    $address->getPostcode();
		            else:
		            	$attribute8->Value     =    '';
		            endif;
		             
		            $attribute9            =    new ExactTarget_Attribute();
		            $attribute9->Name      =    $phoneNumber;
		            
		            if($address->getTelephone()):
		            	$attribute9->Value     =    $address->getTelephone();
		            else:
		            	$attribute9->Value     =    '';
		            endif;
		            
		            $attribute10            =    new ExactTarget_Attribute();
		            $attribute10->Name      =    $country;
		            	
		            if($address->getCountryId()):
		            	$attribute10->Value     =    $address->getCountryId();
		            else:
		            	$attribute10->Value     =    '';
		            endif;
		            
		            $attribute11            =    new ExactTarget_Attribute();
		            $attribute11->Name      =    $dob;
		            if($customer->getDob()):
		            	$attribute11->Value     =    $customer->getDob();
		            else:
		            	$attribute11->Value     =    date('Y-m-d',mktime(0,0,0,1,1,1970));
		            endif;
		            
		            $store					=	Mage::app()->getStore();
		            $website				=	Mage::app()->getWebsite();
		             
		            $storeName				=	$store->getName();
		            $websiteName			=	$website->getName();
		    		
		    		$subscriber->Attributes=array($attribute1,$attribute2,$attribute3,$attribute4,$attribute5,$attribute6,$attribute7,$attribute8,$attribute9,$attribute10,$attribute11);
		    	else:
		    		
				endif;
				
				//Necessary for Enterprise 2.0 accounts
				if(strtolower(Mage::getStoreConfig('exacttarget/setup/et_account')) == 'enterprise 2.0'):
		            $clientID = new ExactTarget_ClientID();
		            $clientID->ID = Mage::getStoreConfig('exacttarget/setup/member_id');
		            $subscriber->Client = $clientID;
				endif;
				
				//The issue is in the admin side when subscribing to the newsletter - check the customers website association and compare the newsletter listing to the store id passed in and use the right
				//newsletter to associate to the customer
				if(Mage::app()->getWebsite($customerWebsite)->getConfig('exacttarget/emails/email_newsletter_listid') != Mage::getStoreConfig('exacttarget/emails/email_newsletter_listid',$storeId)):
					$listId		=	Mage::app()->getWebsite($customerWebsite)->getConfig('exacttarget/emails/email_newsletter_listid');
				else:
					$listId		=	Mage::getStoreConfig('exacttarget/emails/email_newsletter_listid',$storeId);
				endif;
		    
		        $list = new ExactTarget_SubscriberList();  
		        $list->ID = $listId;       
		        $list->Status = 'Active';
		        $subscriber->Lists[] = $list;   
		        $so = new ExactTarget_SaveOption();  
		        $so->PropertyName = "*";  
		        $so->SaveAction = ExactTarget_SaveAction::UpdateAdd;              
		        $soe = new SoapVar($so, SOAP_ENC_OBJECT, 'SaveOption', "http://exacttarget.com/wsdl/partnerAPI");              
		        $opts = new ExactTarget_UpdateOptions();              
		        $opts->SaveOptions = array($soe);  
		        $subscriber->Status = ExactTarget_SubscriberStatus::Active;  
		        $object = new SoapVar($subscriber, SOAP_ENC_OBJECT, 'Subscriber', "http://exacttarget.com/wsdl/partnerAPI");  
		        $request = new ExactTarget_CreateRequest();  
		        $request->Options = $opts;  
		        $request->Objects = array($object);  
		        $results = $client->Create($request);
		        
		        if(Mage::getStoreConfig('exacttarget/setup/debug',$storeId) ==1):
		        	$logFile			=	'exacttarget_newsletter.log';
		        	$isApiLogRequest	=	true;
		        	$wrapperText		=	'Newsletter Subscription Debug';
		        	Mage::helper('exacttarget/debug')->logDebugText($client,$logFile,$isApiLogRequest,$wrapperText);
		        endif;
        	endif;
		endif;
    }
    
    public function unsubscribe($customerSession,$storeId)
    {
    	if(Mage::getModel('exacttarget/failsafe')->hasValidConnection()):
    		if (Mage::getStoreConfig('exacttarget/setup/active',$storeId) == 1):
		        require_once(Mage::helper('exacttarget')->getRequiredFiles());    
			    $wsdl = Mage::helper('exacttarget')->getWsdl();
			    
		        $client = new ExactTargetSoapClient($wsdl, array('trace'=>1));  
		        $client->username = Mage::getStoreConfig('exacttarget/setup/username',$storeId); 
		        $client->password = Mage::getStoreConfig('exacttarget/setup/apipassword',$storeId);
		        
				//Necessary for Enterprise 2.0 accounts
				if(strtolower(Mage::getStoreConfig('exacttarget/setup/et_account')) == 'enterprise 2.0'):
		            $clientID = new ExactTarget_ClientID();
		            $clientID->ID = Mage::getStoreConfig('exacttarget/setup/member_id');
		            $er->Client = $clientID;
				endif;
				
		        $subscriber = new ExactTarget_Subscriber();  
		        $subscriber->EmailAddress = $customerSession->getCustomer()->getEmail();  
		        $subscriber->SubscriberKey = $customerSession->getCustomer()->getEmail();
		        
		        $customerWebsite		=	$customerSession->getCustomer()->getWebsiteId();
		        
		        //The issue is in the admin side when subscribing to the newsletter - check the customers website association and compare the newsletter listing to the store id passed in and use the right
		        //newsletter to associate to the customer
		        if(Mage::app()->getWebsite($customerWebsite)->getConfig('exacttarget/emails/email_newsletter_listid') != Mage::getStoreConfig('exacttarget/emails/email_newsletter_listid',$storeId)):
		        	$listId		=	Mage::app()->getWebsite($customerWebsite)->getConfig('exacttarget/emails/email_newsletter_listid');
		        else:
		        	$listId		=	Mage::getStoreConfig('exacttarget/emails/email_newsletter_listid',$storeId);
		        endif;
		        
		        $list = new ExactTarget_SubscriberList();  
		        $list->ID = $listId;       
		        $list->Status = 'Unsubscribed';
		        $subscriber->Lists[] = $list;   
		        $so = new ExactTarget_SaveOption();  
		        $so->PropertyName = "*";  
		        $so->SaveAction = ExactTarget_SaveAction::UpdateAdd;              
		        $soe = new SoapVar($so, SOAP_ENC_OBJECT, 'SaveOption', "http://exacttarget.com/wsdl/partnerAPI");              
		        $opts = new ExactTarget_UpdateOptions();              
		        $opts->SaveOptions = array($soe);  
		        $subscriber->Status = ExactTarget_SubscriberStatus::Unsubscribed;  
		        $object = new SoapVar($subscriber, SOAP_ENC_OBJECT, 'Subscriber', "http://exacttarget.com/wsdl/partnerAPI");  
		        $request = new ExactTarget_UpdateRequest();  
		        $request->Options = $opts;  
		        $request->Objects = array($object);  
		        $results = $client->Update($request);
		        
		        if(Mage::getStoreConfig('exacttarget/setup/debug',$storeId) ==1):
		        	$logFile			=	'exacttarget_newsletter.log';
		        	$isApiLogRequest	=	true;
		        	$wrapperText		=	'Newsletter Unsubscription Debug';
		        	Mage::helper('exacttarget/debug')->logDebugText($client,$logFile,$isApiLogRequest,$wrapperText);
		        endif;
			endif;
		endif;
    }
    
    
    /**
     * Translates the email template id's that are in magento and sets the variable to the ET equivalent so the proper API key
     * is returned and the transactional send is associated to the proper triggered send in ET. 
     * @param mixed $templateId
     */
    public function translateTemplateId($templateId,$storeId)
    {
        switch($templateId){
            
            case 'sales_email_order_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_neworder_template',$storeId);
            break;

            case 'sales_email_order_guest_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_neworderguest_template',$storeId);
            break;
            
            case 'sales_email_order_comment_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_orderupdate_template',$storeId);
            break;
            
            case 'sales_email_order_comment_guest_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_orderupdateguest_template',$storeId);
            break;
            
            case 'sales_email_invoice_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newinvoice_template',$storeId);
            break;
            
            case 'sales_email_invoice_guest_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newinvoiceguest_template',$storeId);
            break;
            
            case 'sales_email_invoice_comment_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_invoiceupdate_template',$storeId);
            break;
            
            case 'sales_email_invoice_comment_guest_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_invoiceupdateguest_template',$storeId);
            break;
            
            case 'sales_email_creditmemo_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newcreditmemo_template',$storeId);
            break;
            
            case 'sales_email_creditmemo_guest_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newcreditmemoguest_template',$storeId);
            break;
            
            case 'sales_email_creditmemo_comment_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_creditmemo_template',$storeId);
            break;
            
            case 'sales_email_creditmemo_comment_guest_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_creditmemoguest_template',$storeId);
            break;
            
            case 'sales_email_shipment_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newshipment_template',$storeId);
            break;
            
            case 'sales_email_shipment_guest_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newshipmentguest_template',$storeId);
            break;
            
            case 'sales_email_shipment_comment_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_shipmentupdate_template',$storeId);
            break;
            
            case 'sales_email_shipment_comment_guest_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_shipmentupdateguest_template',$storeId);
            break;
            
            case 'contacts_email_email_template':            
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_contactus_template',$storeId);
            break;
            
            case 'currency_import_error_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_currencyupdate_template',$storeId); 
            break;
            
            case 'customer_enterprise_customerbalance_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_customerbalanceup_template_file',$storeId);
            break;
            
            case 'giftcard_giftcardaccount_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_gcaccount_template_file',$storeId);  
            break;
            
            case 'giftcard_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_gcgenerated_template_file',$storeId);  
            break;
            
            case 'enterprise_giftregistry_owner_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_giftregowner_template_file',$storeId);  
            break;
            
            case 'enterprise_giftregistry_sharing_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_giftregshare_template_file',$storeId);  
            break;
            
            case 'enterprise_giftregistry_update_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_giftregupdate_template_file',$storeId);  
            break;
            
            case 'enterprise_invitation_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_invitation_template_file',$storeId);  
            break;
            
            case 'customer_create_account_email_confirmation_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newaccountconfirmationkey_template',$storeId);  
            break;
            
            case 'customer_create_account_email_confirmed_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newaccountconfirmed_template',$storeId);  
            break;
            
            case 'customer_create_account_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newaccount_template',$storeId);  
            break;
            
            //Customer Forgot Password
            case 'customer_password_forgot_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_accountreset_password_template',$storeId);  
            break;
            
            //Customer New (remind) Password
            case 'customer_password_remind_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newpassword_template',$storeId);  
            break;  

            case 'newsletter_subscription_confirm_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newslettersubscriptionconfirmation_template',$storeId);  
            break;
            
            case 'newsletter_subscription_success_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newslettersubscriptionsuccess_template',$storeId);  
            break;
            
            case 'newsletter_subscription_un_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_newsletterunsubscriptionsuccess_template',$storeId);  
            break;
            
            case 'checkout_payment_failed_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_paymentfailed_template',$storeId);  
            break;

            case 'catalog_productalert_email_stock_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_productstockalert_template',$storeId);  
            break;
            
            case 'catalog_productalert_email_price_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_productpricealert_template',$storeId);  
            break;
            
            case 'enterprise_reward_notification_balance_update_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_rewardbalup_template_file',$storeId);  
            break;
            
            case 'enterprise_reward_notification_expiry_warning_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_rewardbalwarn_template_file',$storeId);  
            break;
            
            case 'sales_email_enterprise_rma_guest_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_rmanewguest_template_file',$storeId);  
            break;
            
            case 'sales_email_enterprise_rma_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_rmanew_template_file',$storeId);  
            break;

            case 'sales_email_enterprise_rma_auth_guest_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_rmaauthguest_template_file',$storeId);  
            break;
            
            case 'sales_email_enterprise_rma_auth_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_rmaauth_template_file',$storeId);  
            break;
            
            case 'sales_email_enterprise_rma_comment_guest_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_rmacommentguest_template_file',$storeId);  
            break;
            
            case 'sales_email_enterprise_rma_comment_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_rmacomment_template_file',$storeId);  
            break;
            
            case 'sales_email_enterprise_rma_customer_comment_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_rmacommentcust_template_file',$storeId);  
            break;
            
            case 'wishlist_email_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_sharewishlist_template',$storeId);  
            break;
            
            case 'sendfriend_email_template':
                $template    =    Mage::getStoreConfig('exacttarget/emails/email_sendproducttofriend_template',$storeId);  
            break;
            
            default:
                $template = $templateId;
            break;
        }
            return $template;
    }
    
    
   public function emailTemplateConfig()
    {
        $config_array    =    array(
                                     'abandoncart' => array('active'=>'email_abandoncart_template','template'=>'email_abandoncart_template_file','key'=>'email_abandoncart_key','sync'=>'email_abandoncart_async'),
                                     'abandoncart2' => array('active'=>'email_abandoncart_2_template','template'=>'email_abandoncart_2_template_file','key'=>'email_abandoncart_2_key','sync'=>'email_abandoncart2_async'),
                                     'abandoncart3' => array('active'=>'email_abandoncart_3_template','template'=>'email_abandoncart_3_template_file','key'=>'email_abandoncart_3_key','sync'=>'email_abandoncart3_async'),
                                     'contactus' => array('active'=>'email_contactus','template'=>'email_contactus_template','key'=>'email_contactus_key','sync'=>'email_contactus_async'),
                                     'creditmemo' => array('active'=>'email_creditmemo','template'=>'email_creditmemo_template','key'=>'email_creditmemo_key','sync'=>'email_creditmemo_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/creditmemo_comment/copy_to'),
                                     'creditmemoguest' => array('active'=>'email_creditmemoguest','template'=>'email_creditmemoguest_template','key'=>'email_creditmemoguest_key','sync'=>'email_creditmemoguest_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/creditmemo_comment/copy_to'),
                                     'currencyupdate' => array('active'=>'email_currencyupdate','template'=>'email_currencyupdate_template','key'=>'email_currencyupdate_key','sync'=>'email_currencyupdate_async'),
                                     'customerbalance' => array('active'=>'email_customerbalanceup_template','template'=>'email_customerbalanceup_template_file','key'=>'email_customerbalanceup_key','sync'=>'email_customerbalanceup_async'),
                                     'giftcardacct' => array('active'=>'email_gcaccount_template','template'=>'email_gcaccount_template_file','key'=>'email_gcaccount_key','sync'=>'email_gcaccount_async'),
                                     'giftcardgen' => array('active'=>'email_gcgenerated_template','template'=>'email_gcgenerated_template_file','key'=>'email_gcgenerated_key','sync'=>'email_gcgenerated_async'),
                                     'giftregowner' => array('active'=>'email_giftregowner_template','template'=>'email_giftregowner_template_file','key'=>'email_giftregowner_key','sync'=>'email_giftregowner_async'),
                                     'giftregshare' => array('active'=>'email_giftregshare_template','template'=>'email_giftregshare_template_file','key'=>'email_giftregshare_key','sync'=>'email_giftregshare_async'),
                                     'giftregupdate' => array('active'=>'email_giftregupdate_template','template'=>'email_giftregupdate_template_file','key'=>'email_giftregupdate_key','sync'=>'email_giftregupdate_async'),
                                     'invitation' => array('active'=>'email_invitation_template','template'=>'email_invitation_template_file','key'=>'email_invitation_key','sync'=>'email_invitation_async'),
                                     'invoiceupdate' => array('active'=>'email_invoiceupdate','template'=>'email_invoiceupdate_template','key'=>'email_invoiceupdate_key','sync'=>'email_invoiceupdate_async'),
                                     'invoiceupdateguest' => array('active'=>'email_invoiceupdateguest','template'=>'email_invoiceupdateguest_template','key'=>'email_invoiceupdateguest_key','sync'=>'email_newaccount_async'),
                                     'newaccount' => array('active'=>'email_newaccount','template'=>'email_newaccount_template','key'=>'email_newaccount_key','sync'=>'email_newaccount_async'),
                                     'newaccountconfirmationkey' => array('active'=>'email_newaccountconfirmationkey','template'=>'email_newaccountconfirmationkey_template','key'=>'email_newaccountconfirmationkey_key','sync'=>'email_newaccountconfirmationkey_async'),
                                     'newaccountconfirmed' => array('active'=>'email_newaccountconfirmed','template'=>'email_newaccountconfirmed_template','key'=>'email_newaccountconfirmed_key','sync'=>'email_newaccountconfirmed_async'),
                                     'remindacctpassword' => array('active'=>'email_accountreset_password','template'=>'email_accountreset_password_template','key'=>'email_accountreset_password_key','sync'=>'email_accountreset_password_async'),
                                     'newcreditmemo' => array('active'=>'email_newcreditmemo','template'=>'email_newcreditmemo_template','key'=>'email_newcreditmemo_key','sync'=>'email_newcreditmemo_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/creditmemo/copy_to'),
                                     'newcreditmemoguest' => array('active'=>'email_newcreditmemoguest','template'=>'email_newcreditmemoguest_template','key'=>'email_newcreditmemoguest_key','sync'=>'email_newcreditmemoguest_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/creditmemo/copy_to'),
                                     'newinvoice' => array('active'=>'email_newinvoice','template'=>'email_newinvoice_template','key'=>'email_newinvoice_key','sync'=>'email_newinvoice_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/invoice/copy_to'),
                                     'newinvoiceguest' => array('active'=>'email_newinvoiceguest','template'=>'email_newinvoiceguest_template','key'=>'email_newinvoiceguest_key','sync'=>'email_newinvoiceguest_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/invoice/copy_to'),
                                     'neworder' => array('active'=>'email_neworder','template'=>'email_neworder_template','key'=>'email_neworder_key','sync'=>'email_neworder_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/order/copy_to'),
                                     'neworderguest' => array('active'=>'email_neworderguest','template'=>'email_neworderguest_template','key'=>'email_neworderguest_key','sync'=>'email_neworderguest_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/order/copy_to'),
                                     'newpassword' => array('active'=>'email_newpassword','template'=>'email_newpassword_template','key'=>'email_newpassword_key','sync'=>'email_newpassword_async'),
                                     'newshipment' => array('active'=>'email_newshipment','template'=>'email_newshipment_template','key'=>'email_newshipment_key','sync'=>'email_newshipment_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/shipment/copy_to'),
                                     'newshipmentguest' => array('active'=>'email_newshipmentguest','template'=>'email_newshipmentguest_template','key'=>'email_newshipmentguest_key','sync'=>'email_newshipmentguest_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/shipment/copy_to'),
                                     'newslettersubconfirmation' => array('active'=>'email_newslettersubscriptionconfirmation','template'=>'email_newslettersubscriptionconfirmation_template','key'=>'email_newslettersubscriptionconfirmation_key','sync'=>'email_newslettersubscriptionconfirmation_async'),
                                     'newslettersubsuccess' => array('active'=>'email_newslettersubscriptionsuccess','template'=>'email_newslettersubscriptionsuccess_template','key'=>'email_newslettersubscriptionsuccess_key','sync'=>'email_newslettersubscriptionsuccess_async'),
                                     'newsletterunsubsuccess' => array('active'=>'email_newsletterunsubscriptionsuccess','template'=>'email_newsletterunsubscriptionsuccess_template','key'=>'email_newsletterunsubscriptionsuccess_key','sync'=>'email_newsletterunsubscriptionsuccess_async'),
                                     'orderupdate' => array('active'=>'email_orderupdate','template'=>'email_orderupdate_template','key'=>'email_orderupdate_key','sync'=>'email_orderupdate_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/order_comment/copy_to'),
                                     'orderupdateguest' => array('active'=>'email_orderupdateguest','template'=>'email_orderupdateguest_template','key'=>'email_orderupdateguest_key','sync'=>'email_orderupdateguest_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/order_comment/copy_to'),
                                     'paymentfailed' => array('active'=>'email_paymentfailed','template'=>'email_paymentfailed_template','key'=>'email_paymentfailed_key','sync'=>'email_paymentfailed_async'),
                                     'productpricealert' => array('active'=>'email_productpricealert','template'=>'email_productpricealert_template','key'=>'email_productpricealert_key','sync'=>'email_productpricealert_async'),
                                     'productstockalert' => array('active'=>'email_productstockalert','template'=>'email_productstockalert_template','key'=>'email_productstockalert_key','sync'=>'email_productstockalert_async'),
                                     'rewardbalanceupdate' => array('active'=>'email_rewardbalup_template','template'=>'email_rewardbalup_template_file','key'=>'email_rewardbalup_key','sync'=>'email_rewardbalup_async'),
                                     'rewardbalancewarning' => array('active'=>'email_rewardbalwarn_template','template'=>'email_rewardbalwarn_template_file','key'=>'email_rewardbalwarn_key','sync'=>'email_rewardbalwarn_async'),
                                     'rmaauth' => array('active'=>'email_rmaauth_template','template'=>'email_rmaauth_template_file','key'=>'email_rmaauth_key','sync'=>'email_rmaauth_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/enterprise_rma_auth/copy_to'),
                                     'rmaauthguest' => array('active'=>'email_rmaauthguest_template','template'=>'email_rmaauthguest_template_file','key'=>'email_rmaauthguest_key','sync'=>'email_rmaauthguest_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/enterprise_rma_auth/copy_to'),
                                     'rmacomment' => array('active'=>'email_rmacomment_template','template'=>'email_rmacomment_template_file','key'=>'email_rmacomment_key','sync'=>'email_rmacomment_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/enterprise_rma_comment/copy_to'),
                                     'rmacommentguest' => array('active'=>'email_rmacommentguest_template','template'=>'email_rmacommentguest_template_file','key'=>'email_rmacommentguest_key','sync'=>'email_rmacommentguest_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/enterprise_rma_comment/copy_to'),
                                     'rmacommentcust' => array('active'=>'email_rmacommentcust_template','template'=>'email_rmacommentcust_template_file','key'=>'email_rmacommentcust_key','sync'=>'email_rmacommentcust_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/enterprise_rma_comment/copy_to'),
                                     'rmanew' => array('active'=>'email_rmanew_template','template'=>'email_rmanew_template_file','key'=>'email_rmanew_key','sync'=>'email_rmanew_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/enterprise_rma/copy_to'),
                                     'rmaguest' => array('active'=>'email_rmanewguest_template','template'=>'email_rmanewguest_template_file','key'=>'email_rmanewguest_key','sync'=>'email_rmanewguest_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/enterprise_rma/copy_to'),
                                     'sendproduct' => array('active'=>'email_sendproducttofriend','template'=>'email_sendproducttofriend_template','key'=>'email_sendproducttofriend_key','sync'=>'email_sendproducttofriend_async'),
                                     'sharewish' => array('active'=>'email_sharewishlist','template'=>'email_sharewishlist_template','key'=>'email_sharewishlist_key','sync'=>'email_sharewishlist_async'),
                                     'shipmentupdate' => array('active'=>'email_shipmentupdate','template'=>'email_shipmentupdate_template','key'=>'email_shipmentupdate_key','sync'=>'email_shipmentupdate_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/shipment_comment/copy_to'),
                                     'shipmentupdateguest' => array('active'=>'email_shipmentupdateguest','template'=>'email_shipmentupdateguest_template','key'=>'email_shipmentupdateguest_key','sync'=>'email_shipmentupdateguest_async','bcc_enabled'=>'yes','bcc_path'=>'sales_email/shipment_comment/copy_to'),
                                     'reminder' => array('active'=>'email_reminder_template','template'=>'email_reminder_template_file','key'=>'email_reminder_key','sync'=>'email_reminder_async'),
                                    );
                                    
        return $config_array;
    }
    
    /**
     * This method is used to verify the var/export directory exist on the server before the batch process can created and
     * dump the CSV files for the ExactTarget Data extensions
     * @param string $directoryName
     */
    public function verifyDirectory($directoryPath)
    {
    	if(!is_dir($directoryPath)):
    		mkdir($directoryPath,0777,true);
    	else:
    		$directoryPermission	=	 substr(sprintf('%o', fileperms($directoryPath)), -4);
    	
    		if($directoryPermission != 0777):
    			chmod($directoryPath, 0777);
    		endif;
    	endif;
    }
    
    /**
     * This method was taken from the Varien_File_Csv.php file - it will write the data passed in on a new row for the CSV file
     * it needs to write to.
     * @param mixed $data
     * @param mixed $fh
     */
    public function writeCsv($batchId,$header,$filePath)
    {
    	$delimiter			=	',';
    	$enclosure			=	'"';
    	
    	$batchExportIds     =    $this->getIdCollection($batchId);
    	
    	$fh    =    fopen($filePath,'w+');
    	
    	if (!$batchExportIds) {
    	    fwrite($fh,"");
    	    fclose($fh);
    	    return true;
    	}
    	
    	if ($header) {
    	    $csvData = Mage::getModel('dataflow/convert_parser_csv')->getCsvString($header);
    	    fwrite($fh,$csvData);
    	}
    	
    	foreach($batchExportIds as $batchExportId):
        	$csvData = array();
    	    $row = '';
        	$batchExport    =    Mage::getModel('dataextensions/batchexport')->load($batchExportId);
            $row            =    $this->_getBatchData($batchExport['batch_data']);      

//         	foreach($header as $column):
//                 $csvData[]    =    !empty($row[$column]) ? $row[$column] : '';        	    
//         	endforeach;
//         	$csvData          =    Mage::getModel('dataflow/convert_parser_csv')->getCsvString($csvData);
//         	fwrite($fh,$csvData);
        	fputcsv($fh,$row);
        	unset($row);
        endforeach;
        
        fclose($fh);
        
        $this->_deleteExport($batchId);
        
        return true;
    }
    
    protected function _getBatchData($data)
    {
        $data    =    unserialize($data);
        return $data;
    }
    
    public function getIdCollection($batchId)
    {
        $connection    =    Mage::getSingleton('core/resource')->getConnection('core_write');
        
        $select = $connection->select()->from('exacttarget_dataextension_batch_export', array('batch_export_id'))->where('batch_id = :batch_id');
        $ids = $connection->fetchCol($select, array('batch_id' => $batchId));
        return $ids;
    }
    
    protected function _deleteExport($batchId)
    {
        $model        =    Mage::getModel('dataextensions/export');
        $model->setId($batchId)->delete();
    }
    
    public function getBatchId($type)
    {
        $exportModel             =    Mage::getModel('dataextensions/export');
        $fields                  =    array();
        $fields['type']          =    $type;
        $fields['created_at']    =    now();
         
        $exportModel->addData($fields);
        return $exportModel->save()->getId();
    }
}