<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 * 
 * NOTICE OF LICENSE
 * 
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE 
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING 
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA") 
 * is a legal agreement between you (either an individual or a single entity) and Precision 
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above 
 * which may include associated software components, media, printed materials, and "online" 
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying, 
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the 
 * terms of this EULA. This license agreement represents the entire agreement concerning 
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If 
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international 
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget 
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE. 
 *  The ExactTarget Integration for Magento is licensed as follows: 
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget 
 *  Integration for Magento on your development, staging and production servers running a validly 
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for 
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to 
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento, 
 *  except and only to the extent that such activity is expressly permitted by applicable law 
 *  notwithstanding this limitation. 
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget 
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as 
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento 
 *  and subject to the terms and conditions of this EULA. 
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION 
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail 
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies 
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento 
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual 
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration 
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright 
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content. 
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento. 
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of 
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness 
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy 
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget 
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused 
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision 
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any 
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation, 
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or 
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or 
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract, 
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the 
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions 
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, 
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Abandoncart_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_EMAIL_SENDER      =    'abandoncart/abandoncart_email/sender_email_identity';
    const XML_PATH_EMAIL_LABEL       =    'abandoncart/abandoncart_email/label';
    /**
     * Because the user has multiple time "units" to select from we need to convert their time down to a standard "unit"
     * to compare against the timestamp in the database.  So the times will be converted down to seconds and compared.
     * 
     * If there are no defaults specified the assumed cart age to look for will be 3 days old.
     * @param int $abandonCartAge
     * @param string $abandonCartAgeUnit
     * @return $seconds_old
     */
    public function convertTimestamp($abandonCartAge=3,$abandonCartAgeUnit='days',$modifier,$date=null)
    {
        if (is_null($date)):
            $date                =    date('Y-m-d H:i:s');
        endif;
        
        switch ($abandonCartAgeUnit):
            case 'minutes':
                $new_cart_age    =    date('Y-m-d H:i:s',strtotime($modifier.$abandonCartAge.' minutes'.$date));
            break;
            
            case 'hours':
                $new_cart_age    =    date('Y-m-d H:i:s',strtotime($modifier.$abandonCartAge.' hours'.$date));
            break;
            
            case 'days':
            case 'default':
                $new_cart_age    =    date('Y-m-d H:i:s',strtotime($modifier.$abandonCartAge.' days'.$date));
            break;
        endswitch;
        
        return $new_cart_age;
    }
    
    /**
     * Collects the quote information based off the quote id passed in and then calles the core send class (in most cases
     * will use the ExactTarget override class (if module is installed)) and sends the quote object, store name and builds the opt out
     * url link for the email content.
     * 
     * @param int $quoteId
     */
    public function sendAbandonCartEmail($quoteId,$storeId,$emailToken,$sendNumber)
    {
       //Get the sender type and email label of the Abandon Cart email.   
       $sender_type     =    Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER);
       $emailLabel      =    Mage::getStoreConfig(self::XML_PATH_EMAIL_LABEL);
       
       //The sender type in the config.xml specifies this value, by default custom2 is used.  If you'd like a different value specify the
       //email address you'd like go to System > Configuration > General > Store Email Address and fill in the email address you'd like
       //Then in the config.xml file replace the value in the <default><abandoncart><abandoncart_email><sender_email_identity> value.
       //Valid values are:
       // General Contact = general
       // Sales Rep       = sales
       // Customer Supp.  = support
       // Custom 1        = custom1
       // Custom 2        = custom2
       $sender_name     =    (!is_null(Mage::getStoreConfig('exacttarget/setup/et_from_name',$storeId))) ? Mage::getStoreConfig('exacttarget/setup/et_from_name',$storeId) : Mage::getStoreConfig('trans_email/ident_'.$sender_type.'/name',$storeId);
       $sender_email    =    (!is_null(Mage::getStoreConfig('exacttarget/setup/et_from_email',$storeId))) ? Mage::getStoreConfig('exacttarget/setup/et_from_email',$storeId) : Mage::getStoreConfig('trans_email/ident_'.$sender_type.'/email',$storeId);
           
       $model           =    Mage::getModel('abandoncart/abandonedlog');
       $quote_object    =    $model->getQuoteObject($quoteId);
       //$storeId         =    $quote_object->getStoreId();
       $store_name      =    (!is_null(Mage::getStoreConfig('general/store_information/name',$storeId))? Mage::getStoreConfig('general/store_information/name',$storeId) : 'Please Set Store Name');

       $translate       =    Mage::getSingleton('core/translate');
       $email_template  =    Mage::getModel('core/email_template');
       
       $url             =    Mage::app()->getStore($storeId)->getBaseUrl().'abandoncart/abandoncart/optOut/id/'.$quote_object->getEntityId().'/';
       
       if(Mage::getStoreConfig('et_abandoncart/abandonedcarts/test_mode',$storeId) == 1):
       		$customerEmail	=	Mage::getStoreConfig('et_abandoncart/abandonedcarts/test_email',$storeId);
       else:
       		$customerEmail	=	$quote_object->getCustomerEmail();
       endif;
       
       //Setting the proper template, coupon code and description based off which email is being sent to the user.
       if($sendNumber > 1):
       		if($sendNumber	==	2):
       			if(Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_apply_incentive_second_email',$storeId) == 1):
       				$salesRuleId			=	Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_incentive_second_email',$storeId);
       				$salesRuleCollection	=	Mage::getModel('salesrule/rule')->load($salesRuleId);
       				
       				$couponCollection		=	Mage::getModel('salesrule/coupon')->loadPrimaryByRule($salesRuleCollection->getRuleId());
       				$couponCode				=	$couponCollection->getCode();
       				$couponDescription		=	$salesRuleCollection->getDescription();
       			else:
       				$couponCode				=	'';
       				$couponDescription		=	'';
       			endif;
       			$email_template->setId(Mage::getStoreConfig('exacttarget/emails/email_abandoncart_2_template_file',$storeId));
       		elseif($sendNumber == 3):
       			if(Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_apply_incentive_third_email',$storeId) == 1):
       				$salesRuleId			=	Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_incentive_third_email',$storeId);
       				$salesRuleCollection	=	Mage::getModel('salesrule/rule')->load($salesRuleId);
       		 
       				$couponCollection		=	Mage::getModel('salesrule/coupon')->loadPrimaryByRule($salesRuleCollection->getRuleId());
       				$couponCode				=	$couponCollection->getCode();
       				$couponDescription		=	$salesRuleCollection->getDescription();
       			else:
       				$couponCode				=	'';
       				$couponDescription		=	'';
       			endif;
       			
       			$email_template->setId(Mage::getStoreConfig('exacttarget/emails/email_abandoncart_3_template_file',$storeId));
       		endif;
       else:
       		$couponCode				=	'';
       		$couponDescription		=	'';
       		$email_template->setId(Mage::getStoreConfig('exacttarget/emails/email_abandoncart_template_file',$storeId));
       endif;
       
       //The template file id is stored in this core config data item so rather than do a look up we'll just grab it from the database.
       //$email_template->setId(Mage::getStoreConfig('exacttarget/emails/email_abandoncart_template_file',$storeId));
       $email_template->setStoreId($storeId);
       $email_template->setSenderName($sender_name);
       $email_template->setSenderEmail($sender_email);
       $email_template->setDesignConfig(array('area'=>'frontend','store'=>$storeId));
       
       
       $email_template->send($customerEmail,$quote_object->getCustomerFirstname(),array('quote'=>$quote_object,'store'=>$store_name,'url'=>$url,'emailToken'=>$emailToken,'couponCode'=>$couponCode,'couponDescription'=>$couponDescription));
       
       $translate->setTranslateInLine(true);
    }
    
    /**
     * Logs the email sent for the quote based off the quote informtion in the database
     * @param int $quoteId
     * @param int $customerId
     * @param string $email
     * @param timestamp $date
     * @param int $numberOfEmailsSent
     */
    public function logAbandonCartEmail($quoteId,$customerId,$email,$date,$numberOfEmailsSent,$emailToken)
    {
        $data                             =    array();
        $data['quote_id']                 =    $quoteId;
        $data['customer_id']              =    $customerId;
        $data['email']                    =    $email;
        $data['date_sent']                =    $date;
        $data['email_sent_number']        =    $numberOfEmailsSent;
        $data['email_token']			  =	   $emailToken;
        
        if($numberOfEmailsSent == 1):
            $data['first_email_alert_flag']    =    1;
        endif;
        
        $model       =    Mage::getModel('abandoncart/abandonedlog');
        $model->addData($data);
        $model->save();
    }
    
    public function logConversionEntry($quoteId,$storeId,$customerId,$emailToken)
    {
    	$abandonEntry					=	Mage::getModel('abandoncart/conversion')->loadByQuoteId($quoteId);
    	$id								=	0;
    	foreach($abandonEntry as $result):
    		if($result->getId()):
    			$id		=	$result->getId();
    		else:
    			$id		=	0;
    		endif;
    	endforeach;
    	
    	//Check to see if an entry for a quote already exist, if not then insert the entry.
    	if($id == 0):
	    	$data							=	array();
	    	$data['quote_id']				=	$quoteId;
	    	$data['timestamp']				=	date('Y-m-d H:i:s');
	    	$data['abandon_email_token']	=	$emailToken;
	    	$data['customer_id']			=	$customerId;
	    	$data['store_id']				=	$storeId;
	    	$data['store_name']				=	Mage::app()->getStore($storeId)->getName();
	    	$data['is_completed']			=	0;
	    	$data['is_converted']			=	0;
	    	
	    	$model    =    Mage::getModel('abandoncart/conversion');
	    	$model->addData($data);
	    	$model->save();
	    	
	    	$abandonCartArray		=	array('id' =>$model->getId(),
	    										'quote_id' => $quoteId,
	    										'timestamp'=>date('Y-m-d H:i:s'),
	    										'abandon_email_token'=>$emailToken,
	    										'customer_id'=>$customerId,
	    										'store_id'=>$storeId,
	    										'store_name'=>Mage::app()->getStore($storeId)->getName(),
	    										'is_completed'=>0,
	    										'is_converted'=>0);
	    	
	    	Mage::dispatchEvent('abandoncart_dataextension_entry',$abandonCartArray);
	    	//Add Data Extension Entry
    	endif;
    }    
    
    /**
     * Call to remove expired abandon carts.
     * @return boolean
     */
    public function expiredCarts()
    {
        $emptyRecords    =    Mage::getModel('abandoncart/exclude')->getCollection();
        
        $emptied         =    false;
        if(isset($emptyRecords)):
            foreach($emptyRecords as $record):
                $record->delete();
            endforeach;
            $emptied     =    true;
        endif;
        
        return $emptied;
    }
    
    /**
     * returns a comma separate string of quote id's to exclude when searching for an abandon cart.
     * @param int $storeId
     * @return string
     */
    public function getCartsToExclude($storeId)
    {
        $quotesToExclude    =    Mage::getModel('abandoncart/exclude')->getCollection()->addFieldToFilter('store_id',$storeId);
        $quotes             =    array();
        
        if(isset($quotesToExclude)):
            foreach($quotesToExclude as $exclude):
                $quotes[]       =    $exclude->getQuoteId();            
            endforeach;
        endif;
        
        return $quotes;
    }
    
    public function getSendTime($timeString,$modifier,$date=null)
    {
        if (is_null($date)):
        $date                =    date('Y-m-d H:i');
        endif;
    
        $newSendTime             =    date('Y-m-d H:i',strtotime($modifier.$timeString.$date));
    
        return $newSendTime;
    }
}
	