<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 *
 * NOTICE OF LICENSE
 *
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA")
 * is a legal agreement between you (either an individual or a single entity) and Precision
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above
 * which may include associated software components, media, printed materials, and "online"
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying,
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the
 * terms of this EULA. This license agreement represents the entire agreement concerning
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE.
 *  The ExactTarget Integration for Magento is licensed as follows:
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget
 *  Integration for Magento on your development, staging and production servers running a validly
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento,
 *  except and only to the extent that such activity is expressly permitted by applicable law
 *  notwithstanding this limitation.
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento
 *  and subject to the terms and conditions of this EULA.
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content.
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation,
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract,
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption,
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Abandoncart_Model_Observer
{
	
	/**
	 * After the order is placed check to see if the order was placed due the result of an abandon cart email sent.  If so capture this information and
	 * store it in a local table as well as queue up a data extension push entry for abandon carts.  This will only be triggered if the user clicked
	 * the "Buy Now" button in the email which assigns a special token to the session in Magento.
	 */
	public function completeConversion($observer)
	{
		try{
			$abandonCartToken					=	Mage::getSingleton('customer/session')->getAbandonCartToken();
			
			//Check to make sure the abandon cart token is set.
			if(isset($abandonCartToken)):
				
				$orderInfo						=	$observer->getOrder();
				$abandonCartInfo				=	Mage::getModel('abandoncart/abandonedlog')->loadByQuoteIdAndToken($orderInfo->getQuoteId(),$abandonCartToken);
				
				$abandonCartConversion			=	Mage::getModel('abandoncart/conversion')->loadByQuoteId($orderInfo->getQuoteId());
				
				foreach($abandonCartInfo as $abandonCart):
					$email_sent_number			=	$abandonCart->getEmailSentNumber();
					$date_sent					=	$abandonCart->getDateSent();
					break;
				endforeach;
				
				foreach($abandonCartConversion as $result):
					$id		=	$result->getId();
					break;
				endforeach;
			
				//To prevent customers who have received multiple emails from updating information if they previously completed an order
				//and then clicked on another emails "Buy Now" button.
				if($id > 0):
					$data							=	array();
					$data['order_id']				=	$orderInfo->getId();
					$data['increment_id']			=	$orderInfo->getIncrementId();
					$data['date_completed']			=	date('Y-m-d H:i:s');
					$data['abandon_email_token']	=	$abandonCartToken;
					$data['customer_id']			=	$orderInfo->getCustomerId();
					$data['store_id']				=	$orderInfo->getStoreId();
					$data['store_name']				=	Mage::app()->getStore($orderInfo->getStoreId())->getName();
					$data['is_completed']			=	1;
					$data['is_converted']			=	1;		
					$data['email_sent_number']		=	$email_sent_number;
					$data['date_sent']				=	$date_sent;
					
					$model    =    Mage::getModel('abandoncart/conversion');
					$model->load($id)->addData($data);
					$model->setId($id)->save();
					
					$abandonCartArray		=	array('id' =>$model->getId(),
														'order_id' => $orderInfo->getId(),
														'date_completed'=>date('Y-m-d H:i:s'),
														'abandon_email_token'=>$abandonCartToken,
														'customer_id'=>$orderInfo->getCustomerId(),
														'store_id'=>$orderInfo->getStoreId(),
														'store_name'=>Mage::app()->getStore($orderInfo->getStoreId())->getName(),
														'is_completed'=>1,
														'is_converted'=>1,
														'increment_id'=>$orderInfo->getIncrementId(),
														'email_sent_number' => $email_sent_number,
														'date_sent'=>$date_sent);
					
					Mage::dispatchEvent('abandoncart_dataextension_entry',$abandonCartArray);
				endif;
				
				Mage::getSingleton('customer/session')->unsAbandonCartToken();
				
			endif;
		} catch(Exception $e){
			Mage::logException($e);
		}
	}
	
	/**
	 * Event that logs the abandon cart entry to push over to the data extension set up in ET.
	 * @param object $observer
	 */
	public function abandonCartEntry($observer)
	{
		$abandonObjectArray =    array();
		
		$abandonObjectArray['abandoncart']['key']            =   'Abandon_Cart_Conversion';
 		$abandonObjectArray['abandoncart']['storeId']        =   $observer->getStoreId();
 		$abandonObjectArray['abandoncart']['attributes']     =   array();
		
		$abandonObjectArray['abandoncart']['attributes']['date_modified']			=    date('Y-m-d H:i:s');
		
		//Items only set when an order is placed.
		if($observer->getOrderId()):
			$abandonObjectArray['abandoncart']['attributes']['order_id']			=	$observer->getOrderId();
			$abandonObjectArray['abandoncart']['attributes']['increment_id']		=	$observer->getIncrementId();
			$abandonObjectArray['abandoncart']['attributes']['date_completed']		=	$observer->getDateCompleted();
			$abandonObjectArray['abandoncart']['attributes']['email_sent_number']	=	$observer->getEmailSentNumber();
			$abandonObjectArray['abandoncart']['attributes']['date_sent']			=	$observer->getDateSent();
		endif;
		
		//Items only set when a quote is passed in
		if($observer->getQuoteId()):
			$abandonObjectArray['abandoncart']['attributes']['quote_id']			=	$observer->getQuoteId();
			$abandonObjectArray['abandoncart']['attributes']['date_created']		=	$observer->getTimestamp();
		endif;
		
		//Items set every time
		$abandonObjectArray['abandoncart']['attributes']['row_id']					=	$observer->getId();
		$abandonObjectArray['abandoncart']['attributes']['customer_id']				=	$observer->getCustomerId();
		$abandonObjectArray['abandoncart']['attributes']['abandon_email_token']		=	$observer->getAbandonEmailToken();
		$abandonObjectArray['abandoncart']['attributes']['store_id']				=	$observer->getStoreId();
		$abandonObjectArray['abandoncart']['attributes']['store_name']				=	$observer->getStoreName();
		$abandonObjectArray['abandoncart']['attributes']['is_completed']			=	$observer->getIsCompleted();
		$abandonObjectArray['abandoncart']['attributes']['is_converted']			=	$observer->getIsConverted();
		
		$data                             =    array();
		$data['object_array']             =    serialize($abandonObjectArray);
		$data['send_type']                =    'abandoncart';
		
		$model    =    Mage::getModel('dataextensions/dataextensionlog');
		$model->addData($data);
		$model->save();
		
	}
	
	/**
	 * On install will create the abandon cart data extension in ExactTarget.
	 * @param unknown_type $observer
	 */
	public function createDataExtension($observer)
	{
		require_once(Mage::helper('exacttarget')->getRequiredFiles());
		
		$client_de                 =    new ExactTargetSoapClient(Mage::helper('exacttarget')->getWsdl(), array('trace'=>1));
		$client_de->username       =    Mage::getStoreConfig('exacttarget/setup/username');
		$client_de->password       =    Mage::getStoreConfig('exacttarget/setup/apipassword');
		
		$rr = new ExactTarget_RetrieveRequest();
		$rr->ObjectType = 'DataExtension';
		
		//Set the properties to return
		$props = array("ObjectID", "CustomerKey", "Name", "IsSendable", "SendableSubscriberField.Name");
		$rr->Properties = $props;
		
		//Setup account filtering, to look for a given account MID
		$filterPart = new ExactTarget_SimpleFilterPart();
		$filterPart->Property = 'CustomerKey';
		$values = array($observer->getCustomerKey());
		$filterPart->Value = $values;
		$filterPart->SimpleOperator = ExactTarget_SimpleOperators::equals;
		
		//Encode the SOAP package
		$filterPart = new SoapVar($filterPart, SOAP_ENC_OBJECT,'SimpleFilterPart', "http://exacttarget.com/wsdl/partnerAPI");
		
		//Set the filter to NULL to return all MIDs, otherwise set to filter object
		//$rr->Filter = NULL;
		$rr->Filter = $filterPart;
		
		//Setup and execute request
		$rrm = new ExactTarget_RetrieveRequestMsg();
		$rrm->RetrieveRequest = $rr;
		$results = $client_de->Retrieve($rrm);
		
		//Check to see if the data extension already exist in ET
		if(!property_exists($results,'Results')):
		
    		$create_de                 =    new ExactTarget_DataExtension();
    		$create_de->CategoryID     =    $observer->getCategoryId();
    		$create_de->Name           =    $observer->getName();
    		$create_de->CustomerKey    =    $observer->getCustomerKey();
    		
    		$create_de->IsTestable     =    false;
    		$create_de->IsSendable     =    false;
    		$create_de->Description    =    "Abandon Cart conversion storage.";
    		
    		$fieldArray			   	   =	array(
    											'fields' => array(
    															array('name'=>'row_id','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>true,'required'=>true),
    															array('name'=>'date_created','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
    															array('name'=>'date_modified','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
    															array('name'=>'quote_id','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>false,'required'=>false),
    															array('name'=>'customer_id','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>false,'required'=>false),
    															array('name'=>'order_id','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>false,'required'=>false),
    															array('name'=>'increment_id','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
    															array('name'=>'date_completed','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
    															array('name'=>'date_sent','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
    															array('name'=>'is_completed','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
    															array('name'=>'is_converted','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
    															array('name'=>'email_sent_number','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
    															array('name'=>'abandon_email_token','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'500','max_length_specified'=>true),
    															array('name'=>'store_id','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
    															array('name'=>'store_name','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'500','max_length_specified'=>true)
    														)				
    										);
    		
    		$tempArray							=	array();
    		foreach($fieldArray as $fieldList):
    			foreach($fieldList as $field):
    			
    				$genericObj						=	new ExactTarget_DataExtensionField();
    				$genericObj->Name				=	$field['name'];
    				$genericObj->FieldType			=	$field['type'];
    				$genericObj->IsPrimaryKey		=	$field['primary'];
    				$genericObj->IsRequired			=	$field['required'];
    				
    				if(array_key_exists('max_length', $field) && array_key_exists('max_length_specified', $field)):
    					$genericObj->MaxLength        	=    $field['max_length'];
    					$genericObj->MaxLengthSpecified	=    $field['max_length_specified'];
    				endif;
    				array_push($tempArray,$genericObj);
    			endforeach;
    		endforeach;
    		
    		$create_de->Fields		  =	$tempArray;
    		
    		$de_object                =    new SoapVar($create_de,SOAP_ENC_OBJECT,'DataExtension','http://exacttarget.com/wsdl/partnerAPI');
    		
    		$create_request           =    new ExactTarget_CreateRequest();
    		$create_request->Options  =    null;
    		$create_request->Objects  =    array($de_object);
    		
    		$results                  =    $client_de->Create($create_request);
		
		endif;
	}
}