<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 * 
 * NOTICE OF LICENSE
 * 
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE 
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING 
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA") 
 * is a legal agreement between you (either an individual or a single entity) and Precision 
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above 
 * which may include associated software components, media, printed materials, and "online" 
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying, 
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the 
 * terms of this EULA. This license agreement represents the entire agreement concerning 
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If 
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international 
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget 
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE. 
 *  The ExactTarget Integration for Magento is licensed as follows: 
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget 
 *  Integration for Magento on your development, staging and production servers running a validly 
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for 
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to 
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento, 
 *  except and only to the extent that such activity is expressly permitted by applicable law 
 *  notwithstanding this limitation. 
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget 
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as 
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento 
 *  and subject to the terms and conditions of this EULA. 
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION 
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail 
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies 
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento 
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual 
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration 
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright 
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content. 
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento. 
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of 
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness 
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy 
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget 
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused 
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision 
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any 
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation, 
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or 
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or 
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract, 
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the 
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions 
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, 
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Abandoncart_Model_Findabandonedcartobserver
{
    public function __construct(){}
   
    /**
     * Inital method that is ran to find all abandon carts (assuming the feature is enabled by the end user) that meet the 
     * criteria supplied by the store owner.
     */
    public function findCarts()
    {
        
        $stores = Mage::app()->getStores();
        
        foreach($stores as $store):
            //Only execute the process if the abandon cart module is active
            if(Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_active',$store->getStoreId()) == 1):
                
                //The time unit used to classify a cart as abandoned
                $abandon_cart_age_unit            =    Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_age_classification',$store->getStoreId());
                
                //The age of the cart that must be met/exceeded to qualify a cart as abandoned.
                $abandon_cart_age                 =    Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_age',$store->getStoreId());
                
                //The dollar threshold that must be met/exceeded (if set) to send a reminder email
                $abandon_cart_dollar_threshold    =    Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_dollars',$store->getStoreId());
                
                $install_date					  =	   Mage::getStoreConfig('et_abandoncart/abandonedcarts/install_date',$store->getStoreId());
                
		        if($install_date != '' && !is_null($install_date)):
		        	$temp_array					  =	   explode('/',$install_date);
		        
		        	if(Mage::getStoreConfig('et_abandoncart/abandonedcarts/dateformat',$store->getStoreId()) == 'us'):
		        		$install_date				  =	   $temp_array[2].'-'.$temp_array[0].'-'.$temp_array[1];
		        	elseif(Mage::getStoreConfig('et_abandoncart/abandonedcarts/dateformat',$store->getStoreId()) == 'international'):
		        		$install_date				  =	   $temp_array[2].'-'.$temp_array[1].'-'.$temp_array[0]; 
		        	endif;
		        endif;
                
                //If the value is null set to 0.
                if (is_null($abandon_cart_dollar_threshold)):
                    $abandon_cart_dollar_threshold    =    0.00;
                endif;
                
                //If if the user specified a value with a dollar $, strip that off.
                if(strstr($abandon_cart_dollar_threshold,'$')):
                    $dollar_value_pieces              =    explode('$',$abandon_cart_dollar_threshold);
                    $abandon_cart_dollar_threshold    =    (float)$dollar_value_pieces[1];
                else:
                    $abandon_cart_dollar_threshold    =    (float)$abandon_cart_dollar_threshold;
                endif;
                
                //Number of days to wait to send the next email reminder
                $abandon_cart_email_days_to_wait  =    Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_email_reminders',$store->getStoreId());
                
                //The max number of sends to send reminder emails
                $max_number_of_sends              =    (int)Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_max_send',$store->getStoreId());
                
                //If the value is null then set the value to 3.
                if(is_null($max_number_of_sends)):
                    $max_number_of_sends          =    (int)3;
                endif;
                
                $excludedCartIds                  =    Mage::helper('abandoncart')->getCartsToExclude($store->getStoreId());
                $advanced_cart_age                =    Mage::helper('abandoncart')->convertTimestamp($abandon_cart_age,$abandon_cart_age_unit,'-');
                $quote_object                     =    Mage::getModel('sales/quote')->getCollection()
                                                        ->join(array('customer'=>'customer/entity'),'customer.entity_id = `main_table`.customer_id','customer.entity_id as custId')
                                                        ->addFieldToFilter('main_table.store_id', array('eq'=>$store->getStoreId()))
                                                        ->addFieldToFilter('main_table.items_count', array('neq' => '0'))
                                                        ->addFieldToFilter('main_table.is_active', '1')
                                                        ->addFieldToFilter('main_table.updated_at', array('lteq' => $advanced_cart_age))
                                                        ->addFieldToFilter('main_table.checkout_method',array(array('eq'=>'register'), array('null'=>null)))
                                                        ->addFieldToFilter('main_table.base_subtotal', array('gteq' => $abandon_cart_dollar_threshold));
                
                                                        if(!empty($excludedCartIds) && is_array($excludedCartIds)):
                                                            $quote_object->addFieldToFilter('main_table.entity_id',array('nin'=>$excludedCartIds));
                                                        endif;
                
                										if($install_date):
                											$quote_object->addFieldToFilter('main_table.updated_at', array('gteq' => $install_date));
                										endif;
                										
				if(Mage::getStoreConfig('exacttarget/setup/debug',$store->getStoreId()) ==1):
					$query				=	$quote_object->getSelect()->__toString();
					$logFile			=	'abandoncart_filter.log';
					$isApiLogRequest	=	false;
					$wrapperText		=	'Abandon Cart Filter Debug';
					Mage::helper('exacttarget/debug')->logDebugText($query,$logFile,$isApiLogRequest,$wrapperText);
				endif;
                										
                $today                 =    date('Y-m-d H:i:s');
            
                //Loop through each quote returned
                foreach($quote_object as $quote):
                
                    if(Mage::getStoreConfig('et_abandoncart/abandonedcarts/test_mode',$store->getStoreId()) == 1):
                    	$customerEmail =    Mage::getStoreConfig('et_abandoncart/abandonedcarts/test_email',$store->getStoreId());
                    else:
                    	$customerEmail =    $quote->getCustomerEmail();
                    endif;
                
                	$email_token	   =	md5(uniqid($quote->getCustomerEmail(), true));
                
                    //Check if quote has previous email sent, check if the user has chosen to opt out of receiving emails and verify
                    //the max amount of emails have not been exceeded.
                    $previously_sent    =    Mage::getModel('abandoncart/abandonedlog')
                                                ->getCollection()
                                                ->addAttributeToFilter('quote_id',$quote->getEntityId())
                                                ->setOrder('date_sent','DESC');
                                                
                    $previously_sent->getSelect()->limit(1);
                    $has_data           =    $previously_sent->getData();
                    
                    //Verifying that the quote id passed in has data assoicated to it, if not then we need to send an email and log the results.
                    if(!empty($has_data)):
                        foreach($previously_sent as $sent_email):
                            
                            //Do not allow quotes that users have opted out in.
                            if($sent_email->getOptOutFlag() != 1):
                    
                                //Get the last email number sent - based off that we'll load the proper next send date
                                $lastEmailSent    =    $sent_email->getEmailSentNumber();
                                $sendTime         =    '';
                                
                                //Check which send number the email is on, if it's 2 or 3 find the right sendTime values entered, if not then we default to the basic send time
                                if($lastEmailSent == 1):
                                    $sendTime        =    Mage::getStoreConfig('et_abandoncart/abandonedcarts/second_email_send_time',$store->getStoreId());
                                elseif($lastEmailSent == 2):
                                    $sendTime        =    Mage::getStoreConfig('et_abandoncart/abandonedcarts/third_email_send_time',$store->getStoreId());
                                else:
                                    $next_date_to_send    =    Mage::helper('abandoncart')->convertTimestamp($abandon_cart_email_days_to_wait,'days','+',$sent_email->getDateSent());
                                endif;
                                
                                //Send time formats will be in 6D 23H 58M the letter (D,H or M will signify Days, Hours or Minutes) each time unit will need spaces between the values
                                //After exploding these values we'll check the letter of each potion of the value entered to get the right time to send out the next email.
                                if($sendTime != ''):
                                    $sendTimeArray    =    array();
                                    $sendTimeArray    =    explode(' ',$sendTime);
                                    $timeString       =    '';
                                    
                                    foreach($sendTimeArray as $timePiece):
                                        //Checking to see what the time they passed in
                                        if(strpos($timePiece,strtoupper('D')) !== false):
                                            $days            =    explode('D',strtoupper($timePiece));
                                            $timeString      .=   $days[0].' days ';
                                        elseif(strpos($timePiece,strtoupper('H')) !== false):
                                            $hours            =    explode('H',strtoupper($timePiece));
                                            $timeString       .=   $hours[0].' hours ';
                                        elseif(strpos($timePiece,strtoupper('M')) !== false):
                                            $minutes           =    explode('M',strtoupper($timePiece));
                                            $timeString       .=   $minutes[0].' minutes';
                                        else:
                                            //do nothing
                                        endif;
                                    endforeach;
                                    
                                    if($timeString != ''):
                                        $next_date_to_send = Mage::helper('abandoncart')->getSendTime(trim($timeString),'+',$sent_email->getDateSent());
                                    endif;
                                 endif;
                                
                                //Based on the config option of how often to send reminder emails make sure that an email needs sent.
                                if($today >= $next_date_to_send):
                                
                                    //if the max number of sends have not been met then continue to send out the emails, otherwise this will prevent 
                                    //additional emails from being sent to annoy the customer.
                                    if($sent_email->getEmailSentNumber() < $max_number_of_sends):
                                    
                                        $email_sent_number_incremented    =    $sent_email->getEmailSentNumber() + 1;
                                        //Send email
                                        Mage::helper('abandoncart')->sendAbandonCartEmail($sent_email->getQuoteId(),$store->getStoreId(),$email_token,$email_sent_number_incremented);
                                        //Log send
                                        if(Mage::getStoreConfig('et_abandoncart/abandonedcarts/test_mode',$store->getStoreId()) == 1):
                                       		Mage::helper('abandoncart')->logAbandonCartEmail($sent_email->getQuoteId(),$sent_email->getCustomerId(),Mage::getStoreConfig('et_abandoncart/abandonedcarts/test_email',$store->getStoreId()),$today,$email_sent_number_incremented,$email_token);
                                        else:
                                       		Mage::helper('abandoncart')->logAbandonCartEmail($sent_email->getQuoteId(),$sent_email->getCustomerId(),$customerEmail,$today,$email_sent_number_incremented,$email_token);
                                        endif;
                                    endif;
                                endif;
                            endif;
                        endforeach;
                    else:
                        //Send email
                        Mage::helper('abandoncart')->sendAbandonCartEmail($quote->getEntityId(),$store->getStoreId(),$email_token,1);
                    	//Log Conversion Table Entry
                    	Mage::helper('abandoncart')->logConversionEntry($quote->getEntityId(),$store->getStoreId(),$customerEmail,$email_token);
                        //Log send
                        Mage::helper('abandoncart')->logAbandonCartEmail($quote->getEntityId(),$quote->getCustomerId(),$customerEmail,$today,1,$email_token);
                    endif;
                endforeach;
            else:
                $message    =    Mage::helper('abandoncart')->__('The Abandon Cart Module is not enabled for store id '.$store->getStoreId().'.');
               // Mage::throwException($message);
                Mage::log($message,3,'exception.log');
                //return;
            endif;
        
        endforeach;
    }
    
    /**
     * Process that will run at 12:30 am every day to automatically update the expired carts in the system. These carts will be ignored by the system
     * when looking for new expired carts.
     */
    public function expiredCarts()
    {
        $expiredCarts    =    Mage::helper('abandoncart')->expiredCarts();
        
        $stores = Mage::app()->getStores();
        
        foreach($stores as $store):
            $maxReminder        =    Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_max_send',$store->getStoreId());
            $quotesToExclude    =    Mage::getModel('abandoncart/abandonedlog')->getCollection();
            
            $quotesToExclude->getSelect()->where('email_sent_number = ?',$maxReminder)->orwhere('opt_out_flag = ?',1);
            $quotesToExclude->join('sales/quote','`sales/quote`.entity_id = main_table.quote_id AND `sales/quote`.is_active = 1','store_id');
            
            foreach($quotesToExclude as $quote):
                $data                =    array();
                $data['quote_id']    =    $quote->getQuoteId();
                $data['timestamp']   =    now();
                $data['store_id']    =    $quote->getStoreId();
                
                print_r($data);
                $model           =    Mage::getModel('abandoncart/exclude');
                $model->addData($data);
                $model->save();
            endforeach;
        endforeach;
    }
}