<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 * 
 * NOTICE OF LICENSE
 * 
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE 
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING 
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA") 
 * is a legal agreement between you (either an individual or a single entity) and Precision 
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above 
 * which may include associated software components, media, printed materials, and "online" 
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying, 
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the 
 * terms of this EULA. This license agreement represents the entire agreement concerning 
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If 
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international 
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget 
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE. 
 *  The ExactTarget Integration for Magento is licensed as follows: 
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget 
 *  Integration for Magento on your development, staging and production servers running a validly 
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for 
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to 
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento, 
 *  except and only to the extent that such activity is expressly permitted by applicable law 
 *  notwithstanding this limitation. 
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget 
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as 
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento 
 *  and subject to the terms and conditions of this EULA. 
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION 
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail 
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies 
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento 
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual 
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration 
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright 
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content. 
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento. 
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of 
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness 
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy 
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget 
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused 
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision 
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any 
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation, 
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or 
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or 
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract, 
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the 
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions 
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, 
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Abandoncart_AbandoncartController extends Mage_Core_Controller_Front_Action
{
    /**
     * If the user clicks the opt out link they will update the latest record for that
     * shopping cart quote entry in the exacttarget_abandon_cart_email_log table and 
     * will no longer receive emails about quotes.
     */
    public function optOutAction()
    {
        $this->loadLayout();
        
        $get          =    $this->getRequest()->getParams();
        
        $log_array    =    Mage::getModel('abandoncart/abandonedlog')
                            ->getCollection()
                            ->addAttributeToFilter('quote_id',$get['id'])
                            ->setOrder('date_sent','DESC');
        $log_array->getSelect()->limit(1);
        
        $model       =    Mage::getModel('abandoncart/abandonedlog');
        $data        =    array();                
        foreach($log_array as $log_entry):
            $data['opt_out_flag']    =    1;
            
            $model->load($log_entry->getId())->addData($data);
            $model->setId($log_entry->getId())->save();
        endforeach;
        
        $text_content    =    Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_opt_out_text');
        
        //If the config setting isn't set then default text.
        if($text_content == '' || is_null($text_content)):
            $text_content    =    'You have opted out of receiving further notifications.';        
        endif;
        
        $text            =    $this->getLayout()->createBlock('core/text','abandoned_cart');
        $text->setText("<p style='padding:10px;font-size:11px;'>".$text_content."</p><script type='text/javascript'>
//<![CDATA[
    setTimeout(function(){ location.href = '".Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)."'},3000);
//]]>
</script>");
        $this->getLayout()->getBlock('content')->insert($text);
        
        $this->renderLayout();
    }
    
    /**
     * Setting the Token session variable for those users clicking the
     * button in the email to complete their purchase.
     */
    public function conversionAction()
    {
    	$get					=	$this->getRequest()->getParams();
    	
    	//Unset the session if it was previously set.
    	Mage::getSingleton('customer/session')->unsAbandonCartToken();
    	
    	$abandonCartInfo		=	Mage::getModel('abandoncart/abandonedlog')->loadByToken($get['token']);
    	
    	//Looping through results returned - should only be one.
    	foreach($abandonCartInfo as $abandonCart):
    		$quoteId			=	$abandonCart->getQuoteId();
    	endforeach;
    	
    	$conversionInfo			=	Mage::getModel('abandoncart/conversion')->loadByQuoteId($quoteId);
    	
    	//Looping throught the results returned - should only be one.
    	foreach($conversionInfo as $conversion):
    		$isConverted		=	$conversion->getIsConverted();
    	endforeach;
    	
    	//Only set if the quote was not previously converted.
    	if($isConverted != 1):
    		$emailToken			=	Mage::getSingleton('customer/session')->setAbandonCartToken($get['token']);
    	endif;
    	
    	$this->_redirect('customer/account/');
    }
    
    /**
     * Process to get the quotes in the system that are still considered active but
     * have opted out of receiving further emails or have reached the max number of reminders.
     * These carts will be inserted into a table that will be used to exclude these from the the serach for abandon carts.
     * 
     * This process can be manually ran or it will run at 12:30 am every night on the server to get the most up to date carts to exclude from the search
     */
    public function expiredAction()
    {
        $logFile        =    'expiredCarts.log';
        $wrapperText    =    'Logging start/stop time of the expired cart process.';
        Mage::log('******** Start '.$wrapperText.' ********',1,$logFile);
        
        $startTime = time();
        Mage::log(PHP_EOL.'Started at '.date('Y.m.d H:i:s').PHP_EOL.PHP_EOL,1,$logFile);
        
        $this->loadLayout();
        
        $text            =    $this->getLayout()->createBlock('core/text','abandoned_cart');
        $text->setText("<p style='padding:10px;font-size:13px;'>Updating expired carts...</p>");
        $this->getLayout()->getBlock('content')->insert($text);
        
        $expiredCarts    =    Mage::helper('abandoncart')->expiredCarts();
        
        $stores = Mage::app()->getStores();
        
        foreach($stores as $store):
            $maxReminder        =    Mage::getStoreConfig('et_abandoncart/abandonedcarts/abandon_cart_max_send',$store->getStoreId());
            $quotesToExclude    =    Mage::getModel('abandoncart/abandonedlog')->getCollection();

            $quotesToExclude->getSelect()->where('email_sent_number = ?',$maxReminder)->orwhere('opt_out_flag = ?',1);
            $quotesToExclude->join('sales/quote','`sales/quote`.entity_id = main_table.quote_id AND `sales/quote`.is_active = 1','store_id');
            
            foreach($quotesToExclude as $quote):
                $data                =    array();
                $data['quote_id']    =    $quote->getQuoteId();
                $data['timestamp']   =    now();
                $data['store_id']    =    $quote->getStoreId();
                
                $model           =    Mage::getModel('abandoncart/exclude');
                $model->addData($data);
                $model->save();
            endforeach;
        endforeach;
        
        Mage::log('Finished at '.date('Y.m.d H:i:s').PHP_EOL.PHP_EOL,1,$logFile);
        Mage::log('Time spent in total: '.(time() - $startTime).' second'.PHP_EOL,1,$logFile);
        
        Mage::log('******** End '.$wrapperText.' ********',1,$logFile);
        
        $this->_redirect('abandoncart/abandoncart/expiredUpdated');
        $this->renderLayout();
    }
    
    /**
     * Area where the user is directed after the update has ran.
     */
    public function expiredUpdatedAction()
    {
        $this->loadLayout();
        
        $text            =    $this->getLayout()->createBlock('core/text','abandoned_cart');
        $text->setText("<p style='padding:10px;font-size:13px;'>The manual update of expired abandon carts has been completed! This window can now be closed.</p>");
        $this->getLayout()->getBlock('content')->insert($text);
        
        $this->renderLayout();
    }
}