<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 *
 * NOTICE OF LICENSE
 *
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA")
 * is a legal agreement between you (either an individual or a single entity) and Precision
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above
 * which may include associated software components, media, printed materials, and "online"
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying,
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the
 * terms of this EULA. This license agreement represents the entire agreement concerning
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE.
 *  The ExactTarget Integration for Magento is licensed as follows:
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget
 *  Integration for Magento on your development, staging and production servers running a validly
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento,
 *  except and only to the extent that such activity is expressly permitted by applicable law
 *  notwithstanding this limitation.
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento
 *  and subject to the terms and conditions of this EULA.
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content.
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation,
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract,
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption,
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Retrievedata_Model_Observer
{
	public function getUnsubList()
	{
		$stores = Mage::app()->getStores();
		//Loop through each store on the install
		foreach($stores as $store):
			//Make sure the api is active, there is a valid connection and the newsletter id is set for the store
			if(Mage::getStoreConfig('exacttarget/setup/active') == 1):
				if(Mage::getModel('exacttarget/failsafe')->hasValidConnection()):
					if(Mage::getStoreConfig('exacttarget/emails/email_newsletter_listid') != ''):
					
						require_once(Mage::helper('exacttarget')->getRequiredFiles());
						$client = new ExactTargetSoapClient(Mage::helper('exacttarget')->getWsdl(), array('trace'=>1));
							
						/* Set username and password here */
						$client->username = Mage::helper('exacttarget')->getApiUser($store->getStoreId());
						$client->password = Mage::helper('exacttarget')->getApiPassword($store->getStoreId());
							
							
						try
						{
							//Filter by the List Id
							$simpleFilter_1					= new ExactTarget_SimpleFilterPart();
							$simpleFilter_1->Property 		= 'ListID';
							$simpleFilter_1->Value 			= array(Mage::getStoreConfig('exacttarget/emails/email_newsletter_listid',$store->getStoreId()));
							$simpleFilter_1->SimpleOperator = ExactTarget_SimpleOperators::equals;
						
							//Encode the SOAP package
							$simpleFilter_1 				= new SoapVar($simpleFilter_1, SOAP_ENC_OBJECT,'SimpleFilterPart', "http://exacttarget.com/wsdl/partnerAPI");
						
							//Filter by Status (can pass in an array of status for this item)
							$simpleFilter_2					= new ExactTarget_SimpleFilterPart();
							$simpleFilter_2->Property		= 'Status';
							$simpleFilter_2->Value			= 'Unsubscribed';
							$simpleFilter_2->SimpleOperator	= ExactTarget_SimpleOperators::equals;
						
							$simpleFilter_2					= new SoapVar($simpleFilter_2, SOAP_ENC_OBJECT,'SimpleFilterPart', 'http://exacttarget.com/wsdl/partnerAPI');
		
							//Filter by Unsubscribe Date
							$simpleFilter_3					= new ExactTarget_SimpleFilterPart();
							$simpleFilter_3->Property		= "UnsubscribedDate";
							
 							$yesterday						= date('Y-m-d',strtotime('-1 days',strtotime(now())));
							$yesterdayArray					= explode('-',$yesterday);
							
							$startDate						= date(DATE_ATOM, mktime(0,0,0,$yesterdayArray[1],$yesterdayArray[2],$yesterdayArray[0]));
							$endDate						= date(DATE_ATOM, mktime(23,59,59,$yesterdayArray[1],$yesterdayArray[2],$yesterdayArray[0]));
							$simpleFilter_3->DateValue		= array($startDate,$endDate);
							$simpleFilter_3->SimpleOperator	= ExactTarget_SimpleOperators::between;
							
							$simpleFilter_3					= new SoapVar($simpleFilter_3, SOAP_ENC_OBJECT,'SimpleFilterPart', 'http://exacttarget.com/wsdl/partnerAPI');
												
							//Complex filter to combine the first two simple filters
							$complexFilter					= new ExactTarget_ComplexFilterPart();
							$complexFilter->LeftOperand		= $simpleFilter_1;
							$complexFilter->RightOperand	= $simpleFilter_2;
							$complexFilter->LogicalOperator	= ExactTarget_LogicalOperators::_AND;
							
							$complexFilter = new SoapVar($complexFilter, SOAP_ENC_OBJECT, 'ComplexFilterPart', "http://exacttarget.com/wsdl/partnerAPI");
							
							//Complex filter to combine the first complex filter and the last simple filter
							$complexFilter_2				= new ExactTarget_ComplexFilterPart();
							$complexFilter_2->LeftOperand	= $complexFilter;
							$complexFilter_2->RightOperand	= $simpleFilter_3;
							$complexFilter_2->LogicalOperator = ExactTarget_LogicalOperators::_AND;
							
							
							$rr = new ExactTarget_RetrieveRequest();
							
							$rr->ObjectType = 'ListSubscriber';
							$rr->Properties = array("SubscriberKey", "Status", "UnsubscribedDate", "ListID","Client.ID");
							
							$rr->Filter = new SoapVar($complexFilter_2, SOAP_ENC_OBJECT, 'ComplexFilterPart', "http://exacttarget.com/wsdl/partnerAPI");
							
							
							$request = new ExactTarget_RetrieveRequestMsg();
							$request->RetrieveRequest = $rr;
							$results = $client->Retrieve($request);
							
							if(property_exists($results,'Results')):
							//Need to make sure the result array has values
							if(count($results->Results) > 0):
							
								//If the result array has 1 value the results are not passed back as an array
								if(count($results->Results) == 1):
								
									$data								=	array();
									$data['client_id']					=	$results->Results->Client->ID;
									$data['unsubscribe_date']			=	$results->Results->PartnerProperties->Value;
									$data['status']						=	$results->Results->Status;
									$data['list_id']					=	$results->Results->ListID;
									$data['subscriber_key']				=	$results->Results->SubscriberKey;
									$data['timestamp']					=	now();
									$data['magento_updated']			=	0;
									$data['date_magento_updated']		=	'0000-00-00 00:00:00';
									
									if(!empty($data)):
										$model							=	Mage::getModel('pd_retrieve/unsub');
										$model->addData($data);
										$model->save();
									endif;
								endif;
								
								//if there is more than 1 value then the results are passed back as an array
								if(count($results->Results) > 1):
									foreach($results->Results as $unsubscriber):
									
										$data								=	array();
										$data['client_id']					=	$unsubscriber->Client->ID;
										$data['unsubscribe_date']			=	$unsubscriber->PartnerProperties->Value;
										$data['status']						=	$unsubscriber->Status;
										$data['list_id']					=	$unsubscriber->ListID;
										$data['subscriber_key']				=	$unsubscriber->SubscriberKey;
										$data['timestamp']					=	now();
										$data['magento_updated']			=	0;
										$data['date_magento_updated']		=	'0000-00-00 00:00:00';
										
										if(!empty($data)):
											$model							=	Mage::getModel('pd_retrieve/unsub');
											$model->addData($data);
											$model->save();
										endif;
									endforeach;
								endif;
							endif;
							endif;
						}
						catch (Exception  $e) {
							//Log any exceptions/errors to a specific log for this particular process
							Mage::log($e,'1','et_retrieve_newsletter_unsub.log');
						}
					
					endif;
				endif;
			endif;
		endforeach;
		
		//Get all the newly added values
		$unsubList			=	Mage::getModel('pd_retrieve/unsub')->getCollection()
																		->addAttributeToFilter('magento_updated',0)
																		->addAttributeToFilter('date_magento_updated','0000-00-00 00:00:00');
		
		$unsubUpdate			=	Mage::getModel('pd_retrieve/unsub');
		
		//Loop through each new unsub
		foreach($unsubList as $unsub):
		
			$subscriberModel		=	Mage::getModel('newsletter/subscriber')->loadByEmail($unsub->getSubscriberKey());
		
			//Update the unsub record so it won't be looped through again
			$unsubData							=	array();
			$unsubData['magento_updated']		=	1;
			$unsubData['date_magento_updated']	=	now();
			
			$unsubUpdate->load($unsub->getUnsubId())->addData($unsubData);
			$unsubUpdate->setId($unsub->getUnsubId())->save();
		
			//If the subscriber is found then update the subscriber status in magento.
			if($subscriberModel->getSubscriberId() > 0):
				$data						=  array();
				$data['subscriber_status']	=  '3';
				
				$newsletter				=	Mage::getModel('newsletter/subscriber');
				
				$newsletter->load($subscriberModel->getSubscriberId())->addData($data);
				$newsletter->setId($subscriberModel->getSubscriberId())->save();

			endif;
		endforeach;
	}
}