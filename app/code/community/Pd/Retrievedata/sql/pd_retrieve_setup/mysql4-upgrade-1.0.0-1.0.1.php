<?php
$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE `{$installer->getTable('pd_retrieve/unsub')}` ADD `list_id` VARCHAR(255) NULL AFTER `client_id`;");

$installer->endSetup();