<?php
$installer = $this;
$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$installer->getTable('pd_retrieve/unsub')};
CREATE TABLE {$installer->getTable('pd_retrieve/unsub')} (
  `unsub_id` int(20) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `client_id` varchar(255) NOT NULL COMMENT 'The client (member) id of the ET client',
  `unsubscribe_date` timestamp NULL COMMENT 'The timestamp of when a record was unsubed in ET',
  `status` varchar(255) COMMENT 'the subscriber status of the user',
  `subscriber_key` VARCHAR(255) NULL COMMENT 'The Email address of the unsub',
  `magento_updated` TINYINT(1) NULL COMMENT 'Flag field used to tell if Magento was updated or not',
  `date_magento_updated` TIMESTAMP COMMENT 'the date magento was updated',
  PRIMARY KEY (`unsub_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='This table is used to store all unsubs that come down from ExactTarget';
");

$installer->endSetup();