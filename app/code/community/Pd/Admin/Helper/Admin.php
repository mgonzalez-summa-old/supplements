<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 * 
 * NOTICE OF LICENSE
 * 
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE 
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING 
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA") 
 * is a legal agreement between you (either an individual or a single entity) and Precision 
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above 
 * which may include associated software components, media, printed materials, and "online" 
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying, 
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the 
 * terms of this EULA. This license agreement represents the entire agreement concerning 
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If 
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international 
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget 
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE. 
 *  The ExactTarget Integration for Magento is licensed as follows: 
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget 
 *  Integration for Magento on your development, staging and production servers running a validly 
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for 
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to 
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento, 
 *  except and only to the extent that such activity is expressly permitted by applicable law 
 *  notwithstanding this limitation. 
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget 
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as 
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento 
 *  and subject to the terms and conditions of this EULA. 
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION 
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail 
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies 
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento 
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual 
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration 
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright 
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content. 
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento. 
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of 
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness 
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy 
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget 
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused 
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision 
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any 
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation, 
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or 
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or 
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract, 
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the 
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions 
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, 
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Admin_Helper_Admin extends Mage_Core_Helper_Abstract
{
    
   public function unsubscribe($subscriberEmail,$storeId)
    {
    	try{
	    	if(Mage::getModel('exacttarget/failsafe')->hasValidConnection()):
		    	if(Mage::getStoreConfig('exacttarget/setup/active',$storeId) == 1):
			        require_once(Mage::helper('exacttarget')->getRequiredFiles());    
				    $wsdl = Mage::helper('exacttarget')->getWsdl();
				    
			        $client = new ExactTargetSoapClient($wsdl, array('trace'=>1));  
			        $client->username = Mage::getStoreConfig('exacttarget/setup/username',$storeId); 
			        $client->password = Mage::getStoreConfig('exacttarget/setup/apipassword',$storeId);
			        
					//Necessary for Enterprise 2.0 accounts
					if(strtolower(Mage::getStoreConfig('exacttarget/setup/et_account')) == 'enterprise 2.0'):
			            $clientID = new ExactTarget_ClientID();
			            $clientID->ID = Mage::getStoreConfig('exacttarget/setup/member_id');
			            $er->Client = $clientID;
					endif;
					
			        $subscriber = new ExactTarget_Subscriber();  
			        $subscriber->EmailAddress = $subscriberEmail;  
			        $subscriber->SubscriberKey = $subscriberEmail;
			        
			        $websiteId = Mage::app()->getStore()->getWebsiteId();
			        
			        $customer = Mage::getModel('customer/customer')
			        			->setWebsiteId($websiteId)
			        			->loadByEmail($subscriberEmail);
			        
			        $customerWebsite		=	$customer->getWebsiteId();
			        
			        //The issue is in the admin side when subscribing to the newsletter - check the customers website association and compare the newsletter listing to the store id passed in and use the right
			        //newsletter to associate to the customer
			        if(Mage::app()->getWebsite($customerWebsite)->getConfig('exacttarget/emails/email_newsletter_listid') != Mage::getStoreConfig('exacttarget/emails/email_newsletter_listid',$storeId)):
			        	$listId		=	Mage::app()->getWebsite($customerWebsite)->getConfig('exacttarget/emails/email_newsletter_listid');
			        else:
			        	$listId		=	Mage::getStoreConfig('exacttarget/emails/email_newsletter_listid',$storeId);
			        endif;
			        
			        $list = new ExactTarget_SubscriberList();  
			        $list->ID = $listId;       
			        $list->Status = 'Unsubscribed';
			        $subscriber->Lists[] = $list;   
			        $so = new ExactTarget_SaveOption();  
			        $so->PropertyName = "*";  
			        $so->SaveAction = ExactTarget_SaveAction::UpdateAdd;              
			        $soe = new SoapVar($so, SOAP_ENC_OBJECT, 'SaveOption', "http://exacttarget.com/wsdl/partnerAPI");              
			        $opts = new ExactTarget_UpdateOptions();              
			        $opts->SaveOptions = array($soe);  
			        $subscriber->Status = ExactTarget_SubscriberStatus::Unsubscribed;  
			        $object = new SoapVar($subscriber, SOAP_ENC_OBJECT, 'Subscriber', "http://exacttarget.com/wsdl/partnerAPI");  
			        $request = new ExactTarget_UpdateRequest();  
			        $request->Options = $opts;  
			        $request->Objects = array($object);  
			        $results = $client->Update($request);
		        
		        endif;
	        endif;
    	}catch(Exception $e){
    		Mage::logException($e);
    	}
    }
}