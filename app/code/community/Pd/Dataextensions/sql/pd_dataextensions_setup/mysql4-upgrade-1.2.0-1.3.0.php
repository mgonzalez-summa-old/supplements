<?php
$installer = $this;
$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$installer->getTable('dataextensions/export')};
CREATE TABLE {$installer->getTable('dataextensions/export')} (
  `batch_id` int(20) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  PRIMARY KEY (`batch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
");

$installer->run("
        DROP TABLE IF EXISTS {$installer->getTable('dataextensions/batchexport')};
        CREATE TABLE {$installer->getTable('dataextensions/batchexport')} (
        `batch_export_id` int(20) NOT NULL AUTO_INCREMENT,
        `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `batch_id` int(20) NOT NULL,
        `batch_data` text NOT NULL,
        `type` varchar(255) NOT NULL,
        `created_at` timestamp NOT NULL,
        PRIMARY KEY (`batch_export_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
");

$installer->endSetup();