<?php
$installer = $this;
$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$installer->getTable('dataextensions/dataextensionlog')};
CREATE TABLE {$installer->getTable('dataextensions/dataextensionlog')} (
  `exacttarget_dataextension_log_id` int(20) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `object_array` text NOT NULL COMMENT 'A serialized object array of the data extension type that is going to be sending up the data to ET',
  `sent_to_exacttarget` TINYINT(1) NULL COMMENT 'Has the data been sent up to exacttarget',
  `date_sent` TIMESTAMP COMMENT 'the date the transaction was sent',
  `send_result` VARCHAR(255) NULL COMMENT 'Identifier if the transaction was a success or failure (OK or ERROR)',
  `resend_date` TIMESTAMP COMMENT 'the date a resend was tried if the first attempt was a failure',
  `number_of_sends` int(10) COMMENT 'records the number of times it was attempted to be sent',
  PRIMARY KEY (`exacttarget_dataextension_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='This table is used to log all data extension transactions';
");

$installer->endSetup();