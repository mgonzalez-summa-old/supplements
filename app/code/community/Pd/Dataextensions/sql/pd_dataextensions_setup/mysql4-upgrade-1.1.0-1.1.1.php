<?php
$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE `{$installer->getTable('dataextensions/dataextensionlog')}` ADD `send_type` VARCHAR(255) NULL AFTER `object_array`;");

$installer->endSetup();