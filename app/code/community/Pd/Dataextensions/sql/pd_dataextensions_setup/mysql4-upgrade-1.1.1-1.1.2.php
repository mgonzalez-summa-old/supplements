<?php
$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE `{$installer->getTable('dataextensions/dataextensionlog')}` ADD `status_message` VARCHAR(255) NULL AFTER `send_result`;");

$installer->run("
ALTER TABLE `{$installer->getTable('dataextensions/dataextensionlog')}` ADD `error_code` VARCHAR(255) NULL AFTER `status_message`;");

$installer->endSetup();