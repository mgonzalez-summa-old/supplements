<?php 
/**
 * @category    Pd
 * @package     Pd_Dataextensions
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 * 
 * NOTICE OF LICENSE
 * 
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE 
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING 
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA") 
 * is a legal agreement between you (either an individual or a single entity) and Precision 
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above 
 * which may include associated software components, media, printed materials, and "online" 
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying, 
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the 
 * terms of this EULA. This license agreement represents the entire agreement concerning 
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If 
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international 
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget 
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE. 
 *  The ExactTarget Integration for Magento is licensed as follows: 
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget 
 *  Integration for Magento on your development, staging and production servers running a validly 
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for 
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to 
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento, 
 *  except and only to the extent that such activity is expressly permitted by applicable law 
 *  notwithstanding this limitation. 
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget 
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as 
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento 
 *  and subject to the terms and conditions of this EULA. 
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION 
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail 
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies 
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento 
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual 
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration 
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright 
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content. 
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento. 
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of 
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness 
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy 
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget 
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused 
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision 
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any 
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation, 
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or 
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or 
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract, 
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the 
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions 
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, 
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Dataextensions_Helper_Customer extends Mage_Core_Helper_Abstract
{

    /**
     * Getting the value of the Customer Data Extension if one is supplied or setting the default if one was not supplied.
     * @return $ext_name
     */
    public function getDataExtensionKey($storeId)
    {
        if ((Mage::getStoreConfig('et_dataextensions/customers_de/data_extension_name',$storeId) != null) && (Mage::getStoreConfig('et_dataextensions/customers_de/data_extension_name',$storeId) != '')):
            $ext_key    =    Mage::getStoreConfig('et_dataextensions/customers_de/data_extension_name',$storeId);
        else:
            $message     =    Mage::helper('dataflow')->__("The external key for the Customer data extension is missing or invalid. Please ensure your external key is correct.");
            Mage::throwException($message,Mage_Dataflow_Model_Convert_Exception::FATAL);
        endif;
        
        return $ext_key;
    }
    
    /**
     * Generic method that will take a parameter that identifies the map to field name in the system config to see if
     * that field has a value, if none is present then the default value will be passed along.
     * @param string $identifer
     * @return $field_name
     */
    public function getFieldMappedName($identifer,$storeId)
    {
        if ((Mage::getStoreConfig('et_dataextensions/customers_de/'.$identifer.'_map_to_name',$storeId) != '') && (!is_null(Mage::getStoreConfig('et_dataextensions/customers_de/'.$identifer.'_map_to_name',$storeId)))):
        
            $field_name    =    Mage::getStoreConfig('et_dataextensions/customers_de/'.$identifer.'_map_to_name',$storeId);
        else:
            switch($identifer){

                default:
                case 'customer_id':
                    $field_name = 'customer_id';
                break;
                
                case 'firstname':
                    $field_name = 'firstname';
                break;
                
                case 'middlename':
                    $field_name = 'middlename';
                break;
                
                case 'lastname':
                    $field_name = 'lastname';
                break;
                
                case 'email':
                    $field_name = 'email';
                break;
                
                case 'gender':
                    $field_name = 'gender';
                break;
                
                case 'dob':
                    $field_name = 'dob';
                break;
                
                case 'prefix':
                    $field_name = 'prefix';
                break;
                
                case 'suffix':
                    $field_name = 'suffix';
                break;
                
                case 'join_date':
                    $field_name = 'join_date';
                break;
            }
        endif;
        
        return $field_name;
    }
    
    /**
     * Associates the attributes set up in the system.xml file with their object reference equivalent.
     * @param string $identifier
     * @return $field
     */
    public function getObjectReference($identifier)
    {
        switch ($identifier)
        {
            case 'customer_id':
            default:
                $field = 'getEntityId';
            break;
            
            case 'firstname':
                $field = 'getFirstname';
            break;
            
            case 'middlename':
                $field = 'getMiddlename';
            break;
            
            case 'lastname':
                $field = 'getLastname';
            break;
            
            case 'email':
                $field = 'getEmail';
            break;
            
            case 'gender':
                $field = 'getGender';
            break;
            
            case 'dob':
                $field = 'getDob';
            break;
            
            case 'prefix':
                $field = 'getPrefix';
            break;
            
            case 'suffix':
                $field = 'getSuffix';
            break;
            
            case 'join_date':
                $field = 'getCreatedAt';
            break;
        }
        
        return $field;
    }
    
    /**
     * Ties each attribute to a type which will determine which object reference will be used
     * to pull back the data.
     * @param string $identifier
     * @return $type
     */
    public function getAssociationType($identifier)
    {
        switch($identifier)
        {
            case 'customer_id':
            case 'firstname':
            case 'middlename':
            case 'lastname':
            case 'email':
            case 'gender':
            case 'dob':
            case 'prefix':
            case 'suffix':
            case 'join_date':
            default:
                $type = 'customer';
            break;
        }
        
        return $type;
    }
    
    /**
     * Ties each attribute to an input type which will determine how the object reference is returned.
     * to pull back the data.
     * @param string $identifier
     * @return $type
     */
    public function getInputType($identifier)
    {
        switch($identifier)
        {
            case 'customer_id':
            case 'firstname':
            case 'middlename':
            case 'lastname':
            case 'email':
            case 'prefix':
            case 'suffix':
            default:
                $type = 'text';
            break;
            
            case 'dob':
            case 'join_date':
                $type = 'date';
            break;
             
            case 'gender':
                $type = 'select';
            break;
        }
        
        return $type;
    }
    
    /**
     * Based off the attribute list in the system.xml, finds all the active items and builds an array of options for each attribute.
     * @return mixed $active_attributes
     */
    public function getActiveAttributes($storeId)
    {
        $available_attributes    =    array('customer_id','firstname','middlename','lastname','email','gender','dob','prefix','suffix','join_date');
        $active_attributes       =    array();
        
        foreach($available_attributes as $attribute):
            if(Mage::getStoreConfig('et_dataextensions/customers_de/'.$attribute,$storeId) == 1):
                $attribute    =    array('key' => $attribute,'association_type' => $this->getAssociationType($attribute), 'object_reference' => $this->getObjectReference($attribute), 'mapped_field' => $this->getFieldMappedName($attribute,$storeId),'input_type'=>$this->getInputType($attribute));

                array_push($active_attributes,$attribute);
            endif;
        endforeach;
        
        return $active_attributes;
    }
}