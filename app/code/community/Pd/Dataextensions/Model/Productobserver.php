<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 * 
 * NOTICE OF LICENSE
 * 
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE 
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING 
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA") 
 * is a legal agreement between you (either an individual or a single entity) and Precision 
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above 
 * which may include associated software components, media, printed materials, and "online" 
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying, 
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the 
 * terms of this EULA. This license agreement represents the entire agreement concerning 
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If 
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international 
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget 
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE. 
 *  The ExactTarget Integration for Magento is licensed as follows: 
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget 
 *  Integration for Magento on your development, staging and production servers running a validly 
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for 
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to 
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento, 
 *  except and only to the extent that such activity is expressly permitted by applicable law 
 *  notwithstanding this limitation. 
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget 
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as 
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento 
 *  and subject to the terms and conditions of this EULA. 
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION 
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail 
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies 
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento 
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual 
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration 
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright 
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content. 
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento. 
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of 
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness 
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy 
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget 
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused 
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision 
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any 
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation, 
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or 
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or 
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract, 
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the 
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions 
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, 
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Dataextensions_Model_Productobserver
{
   public function __construct(){}
   
   /**
    * This method batches the desired information from Magento and writes it to a CSV file which then
    * can be imported into ExactTarget
    */
   public function batchCSV()
   {
	   	//Verify the extension is active
	   	if(Mage::getStoreConfig('et_dataextensions/product_de/active') == 1):
		   	try{
   		
   				Mage::helper('exacttarget/exacttarget')->verifyDirectory(Mage::getBaseDir('var').'/export');
   		
		   		$product_file  		= 'product.csv';
		   		$relationship_file	= 'relationship.csv';
		   		$product_path		= Mage::getBaseDir('var').DS.'export'.DS.$product_file; //file path of the CSV file in which the data to be saved
		   		$relationship_path	= Mage::getBaseDir('var').DS.'export'.DS.$relationship_file; //file path of the CSV file in which the data to be saved

		   		$connection                =    Mage::getSingleton('core/resource')->getConnection('core_write');
		   		$productBatchId            =    Mage::helper('exacttarget/exacttarget')->getBatchId('products');
		   		$relationshipBatchId       =    Mage::helper('exacttarget/exacttarget')->getBatchId('relationships');	   		
		   		
		   		$productIds                =    Mage::getResourceModel('catalog/product_collection')->getAllIds();
		   		
		   		//Get the product attributes for the headers
		   		foreach($productIds as $product):
		   			$activeAttributes    =    Mage::helper('dataextensions/product')->getActiveAttributes('base',0);
		   			$relatedAttributes   =    Mage::helper('dataextensions/product')->getActiveAttributes('related',0);
		   			break;
		   		endforeach;
		   		
		   		
		   		$header['date_modified']		=	'date_modified';
		   		
		   		//Get all the header column values
		   		foreach($activeAttributes as $productAttribute):
		   			$header[$productAttribute['mapped_field']]					=	$productAttribute['mapped_field'];
		   		endforeach;
		   		
		   		
		   		//Item Headers
		   		$relatedHeader			  =	   array();
		   		
		   		$relatedHeader['date_modified']		   =	'date_modified';
		   		
		   		foreach($relatedAttributes as $relatedAttribute):
		   			$relatedHeader[$relatedAttribute['mapped_field']]			=	$relatedAttribute['mapped_field'];
		   		endforeach;
		   		
		   	
		   		$date_modified       =    date('Y-m-d H:i:s');
		   	
		   		//Loop through each product found
		   		foreach($productIds as $i => $productId):
		   		
		   		    $product             =    Mage::getModel('catalog/product')->setData(array())->load($productId);
			   		$activeAttributes    =    Mage::helper('dataextensions/product')->getActiveAttributes('base',$product->getStoreId());
			   		$relatedAttributes   =    Mage::helper('dataextensions/product')->getActiveAttributes('related',$product->getStoreId());
			   		
			   		$data				 =	  array();
			   		$relatedData		 =	  array();
			   	
			   		$data['date_modified']    =   $date_modified;

			   		$baseDir = Mage::getSingleton('catalog/product_media_config')->getBaseMediaPath();
			   	
			   		//Loop through the product attributes that are active and adding them to the api propries object
			   		foreach($activeAttributes as $productAttribute):
				   		if($productAttribute['input_type'] == 'select'):
				   			$data[$productAttribute['mapped_field']]    =    $product->getAttributeText($productAttribute['object_reference']);
			   	
				   		//Protecting against NULL price or weight values that will error out when sending over to ET.
				   		elseif($productAttribute['input_type'] == 'decimal'):
					   		if(is_null($product->$productAttribute['object_reference']()) || $product->$productAttribute['object_reference']() == ''):
					   			$data[$productAttribute['mapped_field']]    =     (float)0.000;
				   		    else:
				   		        $data[$productAttribute['mapped_field']]    =    $product->$productAttribute['object_reference']();
					   		endif;
				   		elseif($productAttribute['input_type'] == 'image'):

    				   		if($productAttribute['key'] == 'small_image'):
        				   		if($product->getSmallImage() != 'no_selection'):
        				   		    if(Mage::helper('dataextensions')->checkVersion(Mage::getVersion())):
//          				   		        $data[$productAttribute['mapped_field']]		=	file_exists($baseDir.$product->getSmallImage()) ? $product->$productAttribute['object_reference']() : '';
         				   		        $data[$productAttribute['mapped_field']]		=	file_exists($baseDir.$product->getSmallImage()) ?  file_exists(Mage::helper('catalog/image')->init($product, 'small_image')) ? Mage::helper('catalog/image')->init($product, 'small_image') : Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getSmallImage()) : '';
    				   		        else:
    				   		            $data[$productAttribute['mapped_field']]		=	file_exists($baseDir.$product->getSmallImage()) ? Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getSmallImage()) : '';    				   		        
    				   		        endif;
				   		        else:
				   		            $data[$productAttribute['mapped_field']]        =    '';
        				   		endif;
    				   		endif;
    				   		 
    				   		if($productAttribute['key'] == 'thumbnail'):
    				   		    if($product->getThumbnail() != 'no_selection'):
        				   		    if(Mage::helper('dataextensions')->checkVersion(Mage::getVersion())):
//          				   		        $data[$productAttribute['mapped_field']]		=	file_exists($baseDir.$product->getThumbnail()) ? $product->$productAttribute['object_reference']() : '';
         				   		        $data[$productAttribute['mapped_field']]		=	file_exists($baseDir.$product->getThumbnail()) ?  file_exists(Mage::helper('catalog/image')->init($product, 'thumbnail')) ? Mage::helper('catalog/image')->init($product, 'thumbnail') : Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getThumbnail()) : '';
    				   		        else:
    				   		            $data[$productAttribute['mapped_field']]		=	file_exists($baseDir.$product->getThumbnail()) ? Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getThumbnail()) : '';    				   		        
    				   		        endif;
    				   		    else:
    				   		        $data[$productAttribute['mapped_field']]        =    '';
    				   		    endif;
    				   		endif;
    				   		
    				   		if($productAttribute['key'] == 'image'):
    				   		    if($product->getImage() != 'no_selection'):
        				   		    if(Mage::helper('dataextensions')->checkVersion(Mage::getVersion())):
//          				   		        $data[$productAttribute['mapped_field']]		=	file_exists($baseDir.$product->getImage()) ? $product->$productAttribute['object_reference']() : '';
         				   		        $data[$productAttribute['mapped_field']]		=	file_exists($baseDir.$product->getImage()) ?  file_exists(Mage::helper('catalog/image')->init($product, 'image')) ? Mage::helper('catalog/image')->init($product, 'image') : Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getImage()) : '';
    				   		        else:
    				   		            $data[$productAttribute['mapped_field']]		=	file_exists($baseDir.$product->getImage()) ? Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getImage()) : '';    				   		        
    				   		        endif;
    				   		    else:
    				   		        $data[$productAttribute['mapped_field']]        =    '';
    				   		    endif;
				   		    endif;
				   		else:
				   		    $data[$productAttribute['mapped_field']]    =    $product->$productAttribute['object_reference']();
				   		endif;
			   	
			   		endforeach;
			   		
			   		$batchData['batch_data']             =    serialize($data);
			   		$batchData['type']                   =    'products';
			   		$batchData['batch_id']               =    $productBatchId;
			   		$batchData['created_at']             =    now();
			   		
			   		$connection->insert('exacttarget_dataextension_batch_export',$batchData);
			   		
			   		unset($batchData);
			   		unset($data);
			   		unset($productAttribute);
			   		unset($activeAttributes);
			   		
			   		
			   		$relatedProductIds       =    $product->getRelatedProductIds();
			   		$upsellProductIds        =    $product->getUpSellProductIds();
			   		$crosssellProductIds     =    $product->getCrossSellProductIds();
			   		
// 			   		$related_collection      =    $product->getRelatedProductCollection();
// 			   		$upsell_collection       =    $product->getUpSellProductCollection();
// 			   		$cross_collection        =    $product->getCrossSellProductCollection();
		   	
			   		//Loop through the related products
			   		$i = 0;
			   		
			   		if(!empty($relatedProductIds)):
//     			   		foreach($related_collection as $related):
    			   		foreach($relatedProductIds as $relatedId):
    			   		
    				   		//date_modified (from up top)
    				   		$relatedData['date_modified']    =     $date_modified;
			   		        $relatedProduct                  =    Mage::getModel('catalog/product')->load($relatedId);
    				   		foreach($relatedAttributes as $attribute):
    					   		if($attribute['key'] == 'parent_product_id'):
    					   			$relatedData[$attribute['mapped_field']]  =    $product->getEntityId();
    					   		elseif($attribute['key'] == 'parent_product_sku'):
    					   			$relatedData[$attribute['mapped_field']]  =    $product->getSku();
    					   		elseif($attribute['key'] == 'relationship_type'):
    					   			$relatedData[$attribute['mapped_field']]  =    'related';
    					   		else:
    					   			$relatedData[$attribute['mapped_field']]  =    $relatedProduct->$attribute['object_reference']();
    					   		endif;
    				   		endforeach;
    				   		$relatedBatchData['batch_data']             =    serialize($relatedData);
    				   		$relatedBatchData['type']                   =    'relationships';
    				   		$relatedBatchData['batch_id']               =    $relationshipBatchId;
    				   		$relatedBatchData['created_at']             =    now();
    				   		
    				   		$connection->insert('exacttarget_dataextension_batch_export',$relatedBatchData);
    				   		$relatedProduct->clearInstance();
    				   		unset($relatedProduct);
    				   		unset($relatedData);
    				   		unset($relatedBatchData);
    			   			$i++;
    			   		endforeach;
    			   		unset($relatedProductIds);
			   		endif;

			   		if(!empty($upsellProductIds)):
    			   		//Loop through the upsell products
    			   		$i = ($i > 0) ? $i++ : 0; //if $i already is greater than zero we increment the counter and keep going, otherwise we start a zero.
//     			   		foreach($upsell_collection as $upsell):
    			   		foreach($upsellProductIds as $upsellId):
    				   		$relatedData['date_modified']    =     $date_modified;
    			   		    $relatedProduct                  =    Mage::getModel('catalog/product')->load($upsellId);
    				   		foreach($relatedAttributes as $attribute):
    					   		//These attributes are pulled from the parent product so they will not be present in the collection.
    					   		if($attribute['key'] == 'parent_product_id'):
    					   			$relatedData[$attribute['mapped_field']]  =    $product->getEntityId();
    					   		elseif($attribute['key'] == 'parent_product_sku'):
    					   			$relatedData[$attribute['mapped_field']]  =    $product->getSku();
    					   		elseif($attribute['key'] == 'relationship_type'):
    					   			$relatedData[$attribute['mapped_field']]  =    'up_sell';
    					   		else:
//     					   			$relatedData[$attribute['mapped_field']]  =    $upsell->$attribute['object_reference']();
    					   		    
    					   			$relatedData[$attribute['mapped_field']]  =    $relatedProduct->$attribute['object_reference']();
    					   		endif;
    				   		endforeach;
    				   		$relatedBatchData['batch_data']             =    serialize($relatedData);
    				   		$relatedBatchData['type']                   =    'relationships';
    				   		$relatedBatchData['batch_id']               =    $relationshipBatchId;
    				   		$relatedBatchData['created_at']             =    now();
    				   		 
    				   		$connection->insert('exacttarget_dataextension_batch_export',$relatedBatchData);
    				   		$relatedProduct->clearInstance();
    				   		unset($relatedProduct);
    				   		unset($relatedData);
    				   		unset($relatedBatchData);
    				   		$i++;
    			   		endforeach;
    			   		unset($upsellProductIds);
			   		endif;
		   	
			   		if(!empty($crosssellProductIds)):
    			   		//Loop through the cross sell products
    			   		$i = ($i > 0) ? $i++ : 0; //if $i already is greater than zero we increment the counter and keep going, otherwise we start a zero.
//     			   		foreach($cross_collection as $cross):
    			   		foreach($crosssellProductIds as $crossId):
    				   		//date_modified (from up top)
    				   		$relatedData['date_modified']    =     $date_modified;
    			   		    $relatedProduct                  =    Mage::getModel('catalog/product')->load($crossId);
    				   		//Loops through the related attributes
    				   		foreach($relatedAttributes as $attribute):
    					   		//These attributes are pulled from the parent product so they will not be present in the collection.
    					   		if($attribute['key'] == 'parent_product_id'):
    					   			$relatedData[$attribute['mapped_field']]  =    $product->getEntityId();
    					   		elseif($attribute['key'] == 'parent_product_sku'):
    					   			$relatedData[$attribute['mapped_field']]  =    $product->getSku();
    					   		elseif($attribute['key'] == 'relationship_type'):
    					   			$relatedData[$attribute['mapped_field']]  =    'cross_sell';
    					   		else:
//     					   			$relatedData[$attribute['mapped_field']]  =    $cross->$attribute['object_reference']();
    					   			$relatedData[$attribute['mapped_field']]  =    $relatedProduct->$attribute['object_reference']();
    					   		endif;
    				   		endforeach;
    				   		$relatedBatchData['batch_data']             =    serialize($relatedData);
    				   		$relatedBatchData['type']                   =    'relationships';
    				   		$relatedBatchData['batch_id']               =    $relationshipBatchId;
    				   		$relatedBatchData['created_at']             =    now();
    				   		 
    				   		$connection->insert('exacttarget_dataextension_batch_export',$relatedBatchData);
    				   		$relatedProduct->clearInstance();
    				   		unset($relatedProduct);
    				   		unset($relatedData);
    				   		unset($relatedBatchData);
    				   		$i++;
    			   		endforeach;
    			   		unset($crosssellProductIds);
			   		endif;
			   		
			   		$product->clearInstance(); //Done to help with memory leaks.
			   		unset($product);
			   		//unset($cross);
			   		//unset($cross_collection);
			   		//unset($related);
			   		//unset($related_collection);
			   		//unset($upsell);
			   		//unset($upsell_collection);
			   		unset($relatedAttributes);
			   		unset($attribute);
		   		endforeach;
		   		
		   		$csv           =    Mage::helper('exacttarget/exacttarget')->writeCsv($productBatchId,$header,$product_path);
		   		$relatedCsv    =    Mage::helper('exacttarget/exacttarget')->writeCsv($relationshipBatchId,$relatedHeader,$relationship_path);
		   		
 		   		if($csv && $relatedCsv):
    		   		return array(
    		   		        array(
    		   		                'type'  => 'filename',
    		   		                'value' => $product_file,
    		   		                'rm'    => true // can delete file after use
    		   		        ),
    		   		        array(
    		   		                'type'  => 'filename',
    		   		                'value' => $relationship_file,
    		   		                'rm'    => true // can delete file after use
    		   		        ),
    		   		);
		   		endif;
		   	}catch (Exception $e) {
		   		Mage::logException($e);
		   	}
	   	endif;
   }
   
   /**
    * This method will take the data upon save and push it to the exacttarget_dataextension_log table and then that information
    * will be pushed up to exacttarget when the cron runs
    * @param object $observer
    */
   public function singleProduct($observer)
   {
        if (Mage::getStoreConfig('et_dataextensions/product_de/active',$observer->getProduct()->getStoreId()) == 1):
            try{
                $product             =    $observer->getProduct();
                
                $productObjectArray['product']['base']            =    array();
                $productObjectArray['product']['relationship']    =    array();
                
                $productObjectArray['product']['base']['key']            =    Mage::helper('dataextensions/product')->getDataExtensionKey('base',$observer->getProduct()->getStoreId());
                $productObjectArray['product']['relationship']['key']    =    Mage::helper('dataextensions/product')->getDataExtensionKey('related',$observer->getProduct()->getStoreId());
                
                $activeAttributes    =    Mage::helper('dataextensions/product')->getActiveAttributes('base',$observer->getProduct()->getStoreId());
                $relatedAttributes   =    Mage::helper('dataextensions/product')->getActiveAttributes('related',$observer->getProduct()->getStoreId());
                $productObjectArray['product']['storeId']                =    $observer->getProduct()->getStoreId();
                $storeId                                                 =    $observer->getProduct()->getStoreId();
                
                $date_modified       =    date('Y-m-d H:i:s');
                
                $productObjectArray['product']['base']['attributes']['date_modified']    =   $date_modified; 
                
                $baseDir = Mage::getSingleton('catalog/product_media_config')->getBaseMediaPath();
                        
                //Loop through the product attributes that are active and adding them to the api propries object
                foreach($activeAttributes as $productAttribute):
                    if($productAttribute['input_type'] == 'select'):
                        $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']] =    $product->getAttributeText($productAttribute['object_reference']);
                    
                    //Protecting against NULL price or weight values that will error out when sending over to ET.
                    elseif($productAttribute['input_type'] == 'decimal'):
                        if(is_null($product->$productAttribute['object_reference']()) || $product->$productAttribute['object_reference']() == ''):
                            $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']]    =    (float)0.0000;
                        else:
                            $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']] =    $product->$productAttribute['object_reference']();
                        endif;
                    elseif($productAttribute['input_type'] == 'image'):
                         
                        if($productAttribute['key'] == 'small_image'):
                            if($product->getSmallImage() != 'no_selection'):
                                if(Mage::helper('dataextensions')->checkVersion(Mage::getVersion())):
                                    $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']]		=	file_exists($baseDir.$product->getSmallImage()) ? file_exists(Mage::helper('catalog/image')->init($product, 'small_image')) ? Mage::helper('catalog/image')->init($product, 'small_image') : Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getSmallImage()) : '';
                                else:
                                    $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']]		=   file_exists($baseDir.$product->getSmallImage()) ? Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getSmallImage()) : '';
                                endif;
                            else:
                                 $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']]		=	'';
                            endif;
                        endif;
                         
                        if($productAttribute['key'] == 'thumbnail'):
                            if($product->getThumbnail() != 'no_selection'):
                                if(Mage::helper('dataextensions')->checkVersion(Mage::getVersion())):
                                    $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']]		=	file_exists($baseDir.$product->getThumbnail()) ?  file_exists(Mage::helper('catalog/image')->init($product, 'thumbnail')) ? Mage::helper('catalog/image')->init($product, 'thumbnail') : Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getThumbnailImage()) : '';
                                else:
                                    $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']]		=   file_exists($baseDir.$product->getThumbnail()) ? Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getThumbnailImage()) : '';
                                endif;
                            else:
                                $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']]		=	'';
                            endif;
                        endif;
                        
                        if($productAttribute['key'] == 'image'):
                            if($product->getImage() != 'no_selection'):
                                if(Mage::helper('dataextensions')->checkVersion(Mage::getVersion())):
                                    $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']]		=	file_exists($baseDir.$product->getImage()) ? file_exists(Mage::helper('catalog/image')->init($product, 'image')) ? Mage::helper('catalog/image')->init($product, 'image') : Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getImage()) : '';
                                else:
                                    $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']]		=   file_exists($baseDir.$product->getImage()) ? Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getImage()) : '';
                                endif;
                            else:
                                $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']]		=	'';
                            endif;
                        endif;
                    else:
                        $productObjectArray['product']['base']['attributes'][$productAttribute['mapped_field']] =    $product->$productAttribute['object_reference']();
                    endif;
                endforeach;
                        
                $related_collection      =    $product->getRelatedProductCollection();
                $upsell_collection       =    $product->getUpSellProductCollection();
                $cross_collection        =    $product->getCrossSellProductCollection();
                        
                //Loop through the related products
                
                $i = 0;
                foreach($related_collection as $related):
                    $productObjectArray['product']['relationship']['attributes'][$i]['date_modified']    =     $date_modified;
                
                    //Looping through each of the related attributes
                    foreach($relatedAttributes as $attribute):
                        //These attributes are pulled from the parent product so they will not be present in the collection.
                        if($attribute['key'] == 'parent_product_id'):
                            $productObjectArray['product']['relationship']['attributes'][$i][$attribute['mapped_field']]  =    $product->getEntityId();
                        elseif($attribute['key'] == 'parent_product_sku'):
                            $productObjectArray['product']['relationship']['attributes'][$i][$attribute['mapped_field']]  =    $product->getSku();
                        elseif($attribute['key'] == 'relationship_type'):
                            $productObjectArray['product']['relationship']['attributes'][$i][$attribute['mapped_field']]  =    'related';
                        else:
                            $productObjectArray['product']['relationship']['attributes'][$i][$attribute['mapped_field']]  =    $related->$attribute['object_reference']();
                        endif;
                    endforeach;
                    $i++;
                endforeach;
                        
                //Loop through the upsell products
                $i = ($i > 0) ? $i++ : 0; //if $i already is greater than zero we increment the counter and keep going, otherwise we start a zero.
                foreach($upsell_collection as $upsell):
                    
                    $productObjectArray['product']['relationship']['attributes'][$i]['date_modified']    =     $date_modified;                    
                    foreach($relatedAttributes as $attribute):
                        //These attributes are pulled from the parent product so they will not be present in the collection.
                        if($attribute['key'] == 'parent_product_id'):
                            $productObjectArray['product']['relationship']['attributes'][$i][$attribute['mapped_field']]  =    $product->getEntityId();
                        elseif($attribute['key'] == 'parent_product_sku'):
                            $productObjectArray['product']['relationship']['attributes'][$i][$attribute['mapped_field']]  =    $product->getSku();
                        elseif($attribute['key'] == 'relationship_type'):
                            $productObjectArray['product']['relationship']['attributes'][$i][$attribute['mapped_field']]  =    'up_sell';
                        else:
                            $productObjectArray['product']['relationship']['attributes'][$i][$attribute['mapped_field']]  =    $upsell->$attribute['object_reference']();
                        endif;
                    endforeach;  
                    $i++;
                endforeach;
                        
                //Loop through the cross sell products
                $i = ($i > 0) ? $i++ : 0; //if $i already is greater than zero we increment the counter and keep going, otherwise we start a zero.
                foreach($cross_collection as $cross):
                    //date_modified (from up top)
                    $productObjectArray['product']['relationship']['attributes'][$i]['date_modified']    =     $date_modified;
                    //Loops through the related attributes
                    foreach($relatedAttributes as $attribute):
                        //These attributes are pulled from the parent product so they will not be present in the collection.
                        if($attribute['key'] == 'parent_product_id'):
                            $productObjectArray['product']['relationship']['attributes'][$i][$attribute['mapped_field']]  =    $product->getEntityId();
                        elseif($attribute['key'] == 'parent_product_sku'):
                            $productObjectArray['product']['relationship']['attributes'][$i][$attribute['mapped_field']]  =    $product->getSku();
                        elseif($attribute['key'] == 'relationship_type'):
                            $productObjectArray['product']['relationship']['attributes'][$i][$attribute['mapped_field']]  =    'cross_sell';
                        else:
                            $productObjectArray['product']['relationship']['attributes'][$i][$attribute['mapped_field']]  =    $cross->$attribute['object_reference']();
                        endif;
                    endforeach;  
                    $i++;
                endforeach;
                
                $data                             =    array();
                $data['object_array']             =    serialize($productObjectArray);
                $data['send_type']                =    'product';
                
                $model    =    Mage::getModel('dataextensions/dataextensionlog');
                $model->addData($data);
                $model->save();
                
            }catch (Exception $e) {
                Mage::logException($e);
            }
        endif;
   }
}