<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 * 
 * NOTICE OF LICENSE
 * 
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE 
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING 
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA") 
 * is a legal agreement between you (either an individual or a single entity) and Precision 
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above 
 * which may include associated software components, media, printed materials, and "online" 
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying, 
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the 
 * terms of this EULA. This license agreement represents the entire agreement concerning 
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If 
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international 
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget 
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE. 
 *  The ExactTarget Integration for Magento is licensed as follows: 
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget 
 *  Integration for Magento on your development, staging and production servers running a validly 
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for 
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to 
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento, 
 *  except and only to the extent that such activity is expressly permitted by applicable law 
 *  notwithstanding this limitation. 
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget 
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as 
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento 
 *  and subject to the terms and conditions of this EULA. 
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION 
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail 
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies 
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento 
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual 
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration 
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright 
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content. 
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento. 
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of 
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness 
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy 
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget 
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused 
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision 
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any 
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation, 
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or 
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or 
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract, 
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the 
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions 
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, 
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Dataextensions_Model_Customerobserver
{
   public function __construct(){}
   
   /**
    * This method batches the desired information from Magento and writes it to a CSV file which then
    * can be imported into ExactTarget
    */
   public function batchCSV()
   {
        //Verify the exension is active
        if (Mage::getStoreConfig('et_dataextensions/customers_de/active') == 1):
            try{
    			Mage::helper('exacttarget/exacttarget')->verifyDirectory(Mage::getBaseDir('var').'/export');
    			
    	        $file_name = 'customer.csv';
    			$file_path = Mage::getBaseDir('var').DS.'export'.DS.$file_name; //file path of the CSV file in which the data to be saved
    			
    			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    			
    			$exportModel        =    Mage::getModel('dataextensions/export');
    			$fields             =    array();
    			$fields['type']     =    'customer';
    			$fields['created_at']    =    now();
    			
    			$exportModel->addData($fields);
    			$batchId            =    $exportModel->save()->getId();
    			$customerIds        =    Mage::getResourceModel('customer/customer_collection')->getAllIds();
                $activeAttributes   =    Mage::helper('dataextensions/customer')->getActiveAttributes(0); //get default store config values for the headers
                $customerAddress    =    Mage::getModel('customer/address');
                
                //Headers
                $header_row			 =	array();
                
                $header['date_modified']		=	'date_modified';
                $header['store_id']				=	'store_id';
                $header['store_name']			=	'store_name';
                $header['website_id']			=	'website_id';
                $header['website_name']			=	'website_name';
                
                if(Mage::getStoreConfig('et_dataextensions/customers_de/lifetime_sales')):
                    $header['lifetime_sales']   =   'lifetime_sales';
                    $header['num_orders']       =   'num_orders';
                    $header['avg_sales']        =   'avg_sales';
                endif;
                
                //Get all the header column values
                foreach($activeAttributes as $customerAttribute):
                	$header[$customerAttribute['mapped_field']]					=	$customerAttribute['mapped_field'];
                endforeach;
                
                if(Mage::getStoreConfig('et_dataextensions/customers_de/primary_billing')):
                    $header['billing_company']    =   'billing_company';
                    $header['billing_address_1']  =   'billing_address_1';
                    $header['billing_address_2']  =   'billing_address_2';
                    $header['billing_city']       =   'billing_city';
                    $header['billing_state']      =   'billing_state';
                    $header['billing_postcode']   =   'billing_postcode';
                    $header['billing_country']    =   'billing_country';
                endif;
                
                if(Mage::getStoreConfig('et_dataextensions/customers_de/primary_shipping')):
                    $header['shipping_company']    =   'shipping_company';
                    $header['shipping_address_1']  =   'shipping_address_1';
                    $header['shipping_address_2']  =   'shipping_address_2';
                    $header['shipping_city']       =   'shipping_city';
                    $header['shipping_state']      =   'shipping_state';
                    $header['shipping_postcode']   =   'shipping_postcode';
                    $header['shipping_country']    =   'shipping_country';
                endif;
                

                 if($customerIds):
                    //Loop through each customer found
                     foreach($customerIds as $i => $customerId):
                        $customer                   =    Mage::getModel('customer/customer')->setData(array())->load($customerId);
                 
                    	$data	  =	array();
                		$data['date_modified']		=	date('Y-m-d H:i:s');
                    	$data['store_id']    	    =    $customer['store_id'];
                    	$data['store_name']   	    =    Mage::app()->getStore($customer['store_id'])->getName();
                    	$data['website_id']			=	$customer['website_id'];
                    	$data['website_name']		=	Mage::app()->getWebsite($customer['store_id'])->getName();
                    	
                    	if(Mage::getStoreConfig('et_dataextensions/customers_de/lifetime_sales',$customer->getStoreId())):
                        	$customerOrderTotals                                           =   Mage::getResourceModel('sales/sale_collection')
                                                                                                	->setOrderStateFilter(Mage_Sales_Model_Order::STATE_CANCELED, true)
                                                                                                	->setCustomerFilter($customer)
                                                                                                	->load()
                                                                                                	->getTotals();
                        	$data['lifetime_sales'] = number_format($customerOrderTotals->getLifetime(),2);
                        	$data['num_orders']     = number_format($customerOrderTotals->getNumOrders(),2);
                        	$data['avg_sales']      = number_format($customerOrderTotals->getAvgsale(),2);
                    	endif;
                    
                        //Loop through the customer attributes that are active and adding them to the api propries object
                        foreach($activeAttributes as $customerAttribute):
	                        //Select attributes store their data a little differnetly so we need the following to pull the correct text value from
	                        //the database.  Should work with all (customer) attributes.
	                        if($customerAttribute['input_type'] == 'select'):
    	                        $attribute = Mage::getSingleton('eav/config')->getAttribute('customer', $customerAttribute['key']);
    	                        if ($attribute->usesSource()):
    	                            $options = $attribute->getSource()->getAllOptions(false);
    	                            //for multiselects
    	                            $isArray = false;
    	                            
    	                            $value                 =    $customer[$customerAttribute['key']];
        	                        if ($isArray) {
        	                            foreach ($options as $item) {
        	                                if (in_array($item['label'], $value)) {
        	                                    $setValue[] = $item['value'];
        	                                }
        	                            }
        	                        }
        	                        else {
        	                            $setValue = null;
        	                            foreach ($options as $item) {
        	                                if ($item['value'] == $value) {
        	                                    $setValue = $item['label'];
        	                                }
        	                            }
        	                        }
    	                        endif;
    	                        
	                            $attributeValue    = $setValue;
	                        else:
	                            if($customerAttribute['key'] == 'join_date'):
	                                $customerAttribute['key']    = 'created_at';
	                            endif;
		                        if ($customerAttribute['input_type'] == 'date' && is_null($customer[$customerAttribute['key']])):
		                        	$attributeValue =    null;
		                        else:
		                            if($customerAttribute['key'] == 'customer_id'):
		                                $customerAttribute['key']    = 'entity_id';
		                            endif;
		                        	$attributeValue =    $customer[$customerAttribute['key']];
		                        endif;
	                        endif;
	                        
	                        if(is_numeric($attributeValue)):
	                            $attributeValue    = (string)$attributeValue;
	                        endif;
	                        $data[$customerAttribute['mapped_field']]    =    $attributeValue;
                        endforeach;
                        
                        if(Mage::getStoreConfig('et_dataextensions/customers_de/primary_billing')):
                            if($customer->getDefaultBilling()):
                                $primaryBilling             =   $customerAddress->load($customer->getDefaultBilling());
                                $data['billing_company']    =   $primaryBilling->getCompany();
                                $data['billing_address_1']  =   $primaryBilling->getStreet(1);
                                $data['billing_address_2']  =   $primaryBilling->getStreet(2);
                                $data['billing_city']       =   $primaryBilling->getCity();
                                $data['billing_state']      =   $primaryBilling->getRegion();
                                $data['billing_postcode']   =   $primaryBilling->getPostcode();
                                $data['billing_country']    =   $primaryBilling->getCountryId();
                            else:
                                $data['billing_company']    =   '';
                                $data['billing_address_1']  =   '';
                                $data['billing_address_2']  =   '';
                                $data['billing_city']       =   '';
                                $data['billing_state']      =   '';
                                $data['billing_postcode']   =   '';
                                $data['billing_country']    =   '';
                            endif;
                        endif;
                        
                        if(Mage::getStoreConfig('et_dataextensions/customers_de/primary_shipping')):
                            if($customer->getDefaultShipping()):
                                $primaryShipping             =   $customerAddress->load($customer->getDefaultShipping());
                                $data['shipping_company']    =   $primaryShipping->getCompany();
                                $data['shipping_address_1']  =   $primaryShipping->getStreet(1);
                                $data['shipping_address_2']  =   $primaryShipping->getStreet(2);
                                $data['shipping_city']       =   $primaryShipping->getCity();
                                $data['shipping_state']      =   $primaryShipping->getRegion();
                                $data['shipping_postcode']   =   $primaryShipping->getPostcode();
                                $data['shipping_country']    =   $primaryShipping->getCountryId();
                            else:
                                $data['shipping_company']    =   '';
                                $data['shipping_address_1']  =   '';
                                $data['shipping_address_2']  =   '';
                                $data['shipping_city']       =   '';
                                $data['shipping_state']      =   '';
                                $data['shipping_postcode']   =   '';
                                $data['shipping_country']    =   '';
                            endif;
                        endif;
                        
                        $batchData['batch_data']             =    serialize($data);
                        $batchData['type']                   =    'customer';
                        $batchData['batch_id']               =    $batchId;
                        $batchData['created_at']             =    now();
                        
                        $connection->insert('exacttarget_dataextension_batch_export',$batchData);
                        
                        $customer->clearInstance();
                        unset($customer);
                        unset($data);
                        unset($batchData);
                    endforeach;
                    
                   $csv    =     Mage::helper('exacttarget/exacttarget')->writeCsv($batchId,$header,$file_path);
                endif;
                
                if($csv):
                    return array(
                            'type'  => 'filename',
                            'value' => $file_path,
                            'rm'    => true // can delete file after use
                    );
                endif;
                
    			}catch(Exception $e) {
    				Mage::logException($e);
    			}
        endif;
   }

    
   public function singleCustomer($observer)
   {
       if(Mage::getStoreConfig('et_dataextensions/customers_de/active',$observer->getCustomer()->getStoreId()) == 1):
           try{
                $customer            =    $observer->getCustomer();
                $activeAttributes    =    Mage::helper('dataextensions/customer')->getActiveAttributes($observer->getCustomer()->getStoreId());
                
                $customerObjectArray =    array();
                
                $customerObjectArray['customer']['key']            =   Mage::helper('dataextensions/customer')->getDataExtensionKey($observer->getCustomer()->getStoreId()); 
                $customerObjectArray['customer']['storeId']        =   $observer->getCustomer()->getStoreId(); 
                $customerObjectArray['customer']['attributes']     =    array();
                
                
                $customerObjectArray['customer']['attributes']['date_modified']    =    date('Y-m-d H:i:s');
                $customerObjectArray['customer']['attributes']['store_id']    	   =    $customer->getStoreId();
                $customerObjectArray['customer']['attributes']['store_name']   	   =    Mage::app()->getStore($customer->getStoreId())->getName();
                
                $customerObjectArray['customer']['attributes']['website_id']   	   =    $customer->getWebsiteId();
                $customerObjectArray['customer']['attributes']['website_name'] 	   =    Mage::app()->getWebsite($customer->getWebsiteId())->getName();
                
                if(Mage::getStoreConfig('et_dataextensions/customers_de/lifetime_sales',$customer->getStoreId())):
                    $customerOrderTotals                                           =   Mage::getResourceModel('sales/sale_collection')
                                                                                            ->setOrderStateFilter(Mage_Sales_Model_Order::STATE_CANCELED, true)
                                                                                            ->setCustomerFilter($customer)
                                                                                            ->load()
                                                                                            ->getTotals();
                    
                    $customerObjectArray['customer']['attributes']['lifetime_sales'] = $customerOrderTotals->getLifetime();
                    $customerObjectArray['customer']['attributes']['num_orders']     = $customerOrderTotals->getNumOrders();
                    $customerObjectArray['customer']['attributes']['avg_sales']      = number_format($customerOrderTotals->getAvgSale(),2);
                endif;
                
                //Loop through the customer attributes that are active and adding them to the api propries object
                foreach($activeAttributes as $customerAttribute):
                
                    //Select attributes store their data a little differnetly so we need the following to pull the correct text value from
                    //the database.  Should work with all (customer) attributes.
                    if($customerAttribute['input_type'] == 'select'):
                    
                		//Next several lines - down through $attributeOptionSingle - are used to get the
                		//text value for select input types for customers.
                		$entityType   =    Mage::getModel('eav/config')->getEntityType('customer');
                        $entityTypeId =    $entityType->getEntityTypeId();
                        $attribute    =     Mage::getResourceModel('eav/entity_attribute_collection')
                                                ->setCodeFilter($customerAttribute['key'])
                                                ->setEntityTypeFilter($entityTypeId)
                                                ->getFirstItem();
                
                        $attributeId  =    $attribute->getAttributeId();
                        $optionId     =    $customer->$customerAttribute['object_reference']();
 
                        $attributeOptionSingle = Mage::getResourceModel('eav/entity_attribute_option_collection')
                                                    ->setPositionOrder('asc')
                                                    ->setAttributeFilter($attributeId)
                                                    ->addFieldToFilter('main_table.option_id', array('in' => $optionId))
                                                    ->setStoreFilter()
                                                    ->load()
                                                    ->getFirstItem();
                                                    
                        $customerObjectArray['customer']['attributes'][$customerAttribute['mapped_field']] =    $attributeOptionSingle->getDefaultValue();
                    else:
                        if ($customerAttribute['input_type'] == 'date' && is_null($customer->$customerAttribute['object_reference']())):
                            $customerObjectArray['customer']['attributes'][$customerAttribute['mapped_field']] =    date('m/d/Y',mktime(0,0,0,1,1,1900));
                        else:
                            $customerObjectArray['customer']['attributes'][$customerAttribute['mapped_field']] =    $customer->$customerAttribute['object_reference']();
                        endif;
                    endif;
                endforeach;
                
                if(Mage::getStoreConfig('et_dataextensions/customers_de/primary_billing',$customer->getStoreId())):
                    if($customer->getDefaultBilling()):
                        $primaryBilling             =    Mage::getModel('customer/address')->load($customer->getDefaultBilling());
                        $customerObjectArray['customer']['attributes']['billing_company']    =   $primaryBilling->getCompany();
                        $customerObjectArray['customer']['attributes']['billing_address_1']  =   $primaryBilling->getStreet(1);
                        $customerObjectArray['customer']['attributes']['billing_address_2']  =   $primaryBilling->getStreet(2);
                        $customerObjectArray['customer']['attributes']['billing_city']       =   $primaryBilling->getCity();
                        $customerObjectArray['customer']['attributes']['billing_state']      =   $primaryBilling->getRegion();
                        $customerObjectArray['customer']['attributes']['billing_postcode']   =   $primaryBilling->getPostcode();
                        $customerObjectArray['customer']['attributes']['billing_country']    =   $primaryBilling->getCountryId();
                    else:
                        $customerObjectArray['customer']['attributes']['billing_company']    =   '';
                        $customerObjectArray['customer']['attributes']['billing_address_1']  =   '';
                        $customerObjectArray['customer']['attributes']['billing_address_2']  =   '';
                        $customerObjectArray['customer']['attributes']['billing_city']       =   '';
                        $customerObjectArray['customer']['attributes']['billing_state']      =   '';
                        $customerObjectArray['customer']['attributes']['billing_postcode']   =   '';
                        $customerObjectArray['customer']['attributes']['billing_country']    =   '';
                    endif;
                endif;
                
                if(Mage::getStoreConfig('et_dataextensions/customers_de/primary_shipping',$customer->getStoreId())):
                    if($customer->getDefaultShipping()):
                        $primaryShipping             =    Mage::getModel('customer/address')->load($customer->getDefaultShipping());
                        $customerObjectArray['customer']['attributes']['shipping_company']    =   $primaryShipping->getCompany();
                        $customerObjectArray['customer']['attributes']['shipping_address_1']  =   $primaryShipping->getStreet(1);
                        $customerObjectArray['customer']['attributes']['shipping_address_2']  =   $primaryShipping->getStreet(2);
                        $customerObjectArray['customer']['attributes']['shipping_city']       =   $primaryShipping->getCity();
                        $customerObjectArray['customer']['attributes']['shipping_state']      =   $primaryShipping->getRegion();
                        $customerObjectArray['customer']['attributes']['shipping_postcode']   =   $primaryShipping->getPostcode();
                        $customerObjectArray['customer']['attributes']['shipping_country']    =   $primaryShipping->getCountryId();
                    else:
                        $customerObjectArray['customer']['attributes']['shipping_company']    =   '';
                        $customerObjectArray['customer']['attributes']['shipping_address_1']  =   '';
                        $customerObjectArray['customer']['attributes']['shipping_address_2']  =   '';
                        $customerObjectArray['customer']['attributes']['shipping_city']       =   '';
                        $customerObjectArray['customer']['attributes']['shipping_state']      =   '';
                        $customerObjectArray['customer']['attributes']['shipping_postcode']   =   '';
                        $customerObjectArray['customer']['attributes']['shipping_country']    =   '';
                    endif;
                endif;
                
                $data                             =    array();
                $data['object_array']             =    serialize($customerObjectArray);
                $data['send_type']                =    'customer';
                
                $model    =    Mage::getModel('dataextensions/dataextensionlog');
                $model->addData($data);
                $model->save();
            
            }catch (Exception $e) {
                Mage::logException($e);
            }
       
       endif;
   }
}