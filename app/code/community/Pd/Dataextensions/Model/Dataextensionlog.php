<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 * 
 * NOTICE OF LICENSE
 * 
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE 
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING 
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA") 
 * is a legal agreement between you (either an individual or a single entity) and Precision 
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above 
 * which may include associated software components, media, printed materials, and "online" 
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying, 
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the 
 * terms of this EULA. This license agreement represents the entire agreement concerning 
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If 
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international 
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget 
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE. 
 *  The ExactTarget Integration for Magento is licensed as follows: 
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget 
 *  Integration for Magento on your development, staging and production servers running a validly 
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for 
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to 
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento, 
 *  except and only to the extent that such activity is expressly permitted by applicable law 
 *  notwithstanding this limitation. 
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget 
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as 
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento 
 *  and subject to the terms and conditions of this EULA. 
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION 
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail 
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies 
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento 
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual 
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration 
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright 
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content. 
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento. 
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of 
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness 
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy 
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget 
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused 
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision 
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any 
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation, 
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or 
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or 
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract, 
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the 
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions 
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, 
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Dataextensions_Model_Dataextensionlog extends Mage_Core_Model_Abstract
{
    protected function _construct() {
        // Set the resource model name
        $this->_init('dataextensions/dataextensionlog');
    }
    
    public function sendToExactTarget()
    {
        if(Mage::getStoreConfig('exacttarget/setup/active') == 1):
            if(Mage::getModel('exacttarget/failsafe')->hasValidConnection()):   
                require_once(Mage::helper('exacttarget')->getRequiredFiles());
                $client = new ExactTargetSoapClient(Mage::helper('exacttarget')->getWsdl(), array('trace'=>1));  
                
                /* Set username and password here */  
//                 $client->username = Mage::helper('exacttarget')->getApiUser(); 
//                 $client->password = Mage::helper('exacttarget')->getApiPassword();
                
                $collection    =    Mage::getModel('dataextensions/dataextensionlog')->getCollection();
                $collection->addFieldToFilter('send_result',array(array('null' => true),array('eq' => '')));
                //Limit to 500 records at a time
                $collection->getSelect()->limit(500);

                foreach($collection as $data_extension):
                    switch($data_extension['send_type'])
                    {
                        case 'customer':
                            $this->sendCustomer($data_extension['object_array'],$data_extension['number_of_sends'],$data_extension['exacttarget_dataextension_log_id'],$client);
                        break;
                        
                        case 'order':
                            $this->sendOrder($data_extension['object_array'],$data_extension['number_of_sends'],$data_extension['exacttarget_dataextension_log_id'],$client);
                        break;
                        
                        case 'product':
                            $this->sendProduct($data_extension['object_array'],$data_extension['number_of_sends'],$data_extension['exacttarget_dataextension_log_id'],$client);
                        break;
                        
                        case 'abandoncart':
                        	$this->sendCart($data_extension['object_array'],$data_extension['number_of_sends'],$data_extension['exacttarget_dataextension_log_id'],$client);
                        break;
                    }  
                endforeach;
                
                $today               =    date('Y-m-d H:i:s');
                $two_days_ago        =    date('Y-m-d H:i:s',strtotime('-2 days'.$today));
                $model               =    Mage::getModel('dataextensions/dataextensionlog');
                $deleteCollection    =    Mage::getModel('dataextensions/dataextensionlog')->getCollection();
                $deleteCollection->addFieldToFilter('timestamp',array('lteq'=> $two_days_ago));
                //Limit to 500 Records at a time
                $deleteCollection->getSelect()->limit(500);
                
//                                $startTime = time();
//                                Mage::log(PHP_EOL.'Started at '.date('Y.m.d H:i:s').PHP_EOL.PHP_EOL);
                
                foreach($deleteCollection as $record):
                	$model->setId($record->getExacttargetDataextensionLogId())->delete();
                endforeach;
                
//                                Mage::log('Finished at '.date('Y.m.d H:i:s').PHP_EOL.PHP_EOL);
//                                Mage::log('Time spent in total: '.(time() - $startTime).' second'.PHP_EOL);
            endif;
         endif;
         
    }
    
   public function sendCustomer($serialzedArray,$numberOfSends,$rowId,$client)
    {
        try{
        	$unserializedArray	=	array();
        	$stringLength		=	strlen($serialzedArray);
        	$lastCharacter		=	substr($serialzedArray,($stringLength - 1),1);
        	
        	//Check for the last character in the serialized array to make sure it's a complete array.
        	if($lastCharacter != '}'):
        		$message = 'An error occured with the Row Id '.$rowId.' in the exacttarget_dataextension_log table on '.date("Y-m-d H:i:s").'.  The serialized array was incomplete. This record will not be sent to ExactTarget.';
        		Mage::log($message,1,'et_serialized_failure.log');
        	else:
        		$unserializedArray             =    unserialize($serialzedArray);
        	endif;
        	
        	//If the unserialized array is not empty
        	if(!empty($unserializedArray)):
            
	            $customer_de                   =    new ExactTarget_DataExtensionObject();
	            $customer_de->CustomerKey      =    $unserializedArray['customer']['key'];
	            $storeId                       =    $unserializedArray['customer']['storeId'];
	            $customer_de->Properties       =    array();
	            
	            $client->username = Mage::helper('exacttarget')->getApiUser($storeId);
	            $client->password = Mage::helper('exacttarget')->getApiPassword($storeId);
	            
	            foreach($unserializedArray['customer']['attributes'] as $key => $customer):
	            
	                $apiProperty               =    new ExactTarget_APIProperty();
	                $apiProperty->Name         =    $key;
	                $apiProperty->Value        =    $customer;
	                
	                $customer_de->Properties[] = $apiProperty;
	
	            endforeach;
	            
	            //Call the data extension api - is an update method so primary key (in the data extension) needs to be a field that can be
	            //updated on (i.e. email address for customers).
	            Mage::helper('exacttarget/exacttarget')->dataExtension($client,$customer_de,$numberOfSends,$rowId,$storeId);
	    	endif;
            
        } catch(Exception $e){
            Mage::log('There was an issue with pushing information to the customer data extension.');
            Mage::logException($e);
        } catch(SoapFault $e) {
            Mage::logException($e);
        }
    }
    
    public function sendOrder($serialzedArray,$numberOfSends,$rowId,$client)
    {
        try{
        	$unserializedArray	=	array();
        	$stringLength		=	strlen($serialzedArray);
        	$lastCharacter		=	substr($serialzedArray,($stringLength - 1),1);
        	 
        	//Check for the last character in the serialized array to make sure it's a complete array.
        	if($lastCharacter != '}'):
        		$message = 'An error occured with the Row Id '.$rowId.' in the exacttarget_dataextension_log table on '.date("Y-m-d H:i:s").'.  The serialized array was incomplete. This record will not be sent to ExactTarget.';
        		Mage::log($message,1,'et_serialized_failure.log');
        	else:
        		$unserializedArray             =    unserialize($serialzedArray);
        	endif;
        	 
        	//If the unserialized array is not empty
        	if(!empty($unserializedArray)):
	            $base_de                       =    new ExactTarget_DataExtensionObject();
	            $base_de->CustomerKey          =    $unserializedArray['order']['base']['key'];
	            $base_de->Properties           =    array();
	            
	            $item_de                       =    new ExactTarget_DataExtensionObject();
	            $item_de->CustomerKey          =    $unserializedArray['order']['items']['key'];
	            
	            $address_de                    =    new ExactTarget_DataExtensionObject();
	            $address_de->CustomerKey       =    $unserializedArray['order']['addresses']['key'];
	            
	            $storeId                       =    $unserializedArray['order']['storeId'];
	            
	            $client->username = Mage::helper('exacttarget')->getApiUser($storeId);
	            $client->password = Mage::helper('exacttarget')->getApiPassword($storeId);
	            
	            foreach($unserializedArray['order']['base']['attributes'] as $key => $order):
	                $apiProperty               =    new ExactTarget_APIProperty();
	                $apiProperty->Name         =    $key;
	                $apiProperty->Value        =    $order;
	                
	                $base_de->Properties[]     = $apiProperty;
	            endforeach;
	            
	            Mage::helper('exacttarget/exacttarget')->dataExtension($client,$base_de,$numberOfSends,$rowId,$storeId);
	            
	            foreach($unserializedArray['order']['items']['attributes'] as $items):
	                $item_de->Properties           =    array();
	                foreach($items as $key => $item):
	                    $apiProperty               =    new ExactTarget_APIProperty();
	                    $apiProperty->Name         =    $key;
	                    $apiProperty->Value        =    $item;
	                    
	                    $item_de->Properties[]     = $apiProperty;
	                 endforeach;
	                Mage::helper('exacttarget/exacttarget')->dataExtension($client,$item_de,$numberOfSends,$rowId,$storeId);
	            endforeach;
	            
	            foreach($unserializedArray['order']['addresses']['attributes'] as $addresses):
	                $address_de->Properties        =    array();
	                foreach($addresses as $key => $address):
	                    $apiProperty               =    new ExactTarget_APIProperty();
	                    $apiProperty->Name         =    $key;
	                    $apiProperty->Value        =    $address;
	                    
	                    $address_de->Properties[]  = $apiProperty;
	                endforeach;
	                
	               Mage::helper('exacttarget/exacttarget')->dataExtension($client,$address_de,$numberOfSends,$rowId,$storeId);
	            endforeach;
	    	endif;
        } catch(Exception $e){
            Mage::log('There was an issue with pushing information to one of the order data extensions.');
            Mage::logException($e);
        } catch(SoapFault $e) {
            Mage::logException($e);
        }
    }
    
    public function sendProduct($serialzedArray,$numberOfSends,$rowId,$client)
    {
        try{
        	$unserializedArray	=	array();
        	$stringLength		=	strlen($serialzedArray);
        	$lastCharacter		=	substr($serialzedArray,($stringLength - 1),1);
        	
        	//Check for the last character in the serialized array to make sure it's a complete array.
        	if($lastCharacter != '}'):
        		$message = 'An error occured with the Row Id '.$rowId.' in the exacttarget_dataextension_log table on '.date("Y-m-d H:i:s").'.  The serialized array was incomplete. This record will not be sent to ExactTarget.';
        		Mage::log($message,1,'et_serialized_failure.log');
        	else:
        		$unserializedArray             =    unserialize($serialzedArray);
        	endif;
        	
        	//If the unserialized array is not empty
        	if(!empty($unserializedArray)):
	            $base_de                       =    new ExactTarget_DataExtensionObject();
	            $base_de->CustomerKey          =    $unserializedArray['product']['base']['key'];
	            $base_de->Properties           =    array();
	            
	            $relationship_de               =    new ExactTarget_DataExtensionObject();
	            $relationship_de->CustomerKey  =    $unserializedArray['product']['relationship']['key'];
	            
	            $storeId                       =    $unserializedArray['product']['storeId'];
	            
	            $client->username = Mage::helper('exacttarget')->getApiUser($storeId);
	            $client->password = Mage::helper('exacttarget')->getApiPassword($storeId);
	            
	            $retrieve_request               =    new ExactTarget_RetrieveRequest();
	            $retrieve_request->ObjectType   =    "DataExtensionObject[".$unserializedArray['product']['relationship']['key']."]";
	            
	            $retrieve_request->Properties   =    array();
	            
	            $retrieve_request->Properties[] =    "product_id";
	            $retrieve_request->Properties[] =    "related_product_id";
	            $retrieve_request->Properties[] =    "relationship_type";
	            
	            $filter        =    new ExactTarget_SimpleFilterPart();
	            $filter->Value =    $unserializedArray['product']['base']['attributes']['product_id'];
	            $filter->SimpleOperator = ExactTarget_SimpleOperators::equals;
	            $filter->Property = 'product_id';
	            
	            $retrieve_request->Filter = new SoapVar($filter, SOAP_ENC_OBJECT, 'SimpleFilterPart', "http://exacttarget.com/wsdl/partnerAPI");
	            $retrieve_request->Options = NULL;
	            $retrieve_request_message = new ExactTarget_RetrieveRequestMsg();
	            
	            $retrieve_request_message->RetrieveRequest = $retrieve_request;
	            
	            $retrieve_results = $client->Retrieve($retrieve_request_message);
	            
	            $delete_relationships_object   =    new ExactTarget_DataExtensionObject();
	            $delete_relationships_object->CustomerKey = $unserializedArray['product']['relationship']['key'];
	            
	            $delete_key_array              =    array();
	            
	            //Verifying that the property exist before trying to delete
	            $delete_relationships_object->Keys = array();
	            if(property_exists($retrieve_results,'Results')):
	            	if(property_exists($retrieve_results->Results, 'Properties')):
	            		if(property_exists($retrieve_results->Results->Properties, 'Property')):
		            		foreach($retrieve_results->Results->Properties->Property as $properties):
			                    $delete_key        =    new ExactTarget_APIProperty();
			                    $delete_key->Name  =    $properties->Name;
			                    $delete_key->Value =    $properties->Value;
			                    
			                    $delete_relationships_object->Keys[] = $delete_key;
				            endforeach;
				            $object  = new SoapVar($delete_relationships_object,SOAP_ENC_OBJECT,'DataExtensionObject','http://exacttarget.com/wsdl/partnerAPI');
				            
				            $request = new ExactTarget_DeleteRequest();
				            $request->Options = NULL;
				            $request->Objects = array($object);
				            $delete_results = $client->Delete($request);
		           		endif;
		            endif;
	            endif;
	            
	            foreach($unserializedArray['product']['base']['attributes'] as $key => $product):
	                $apiProperty               =    new ExactTarget_APIProperty();
	                $apiProperty->Name         =    $key;
	                $apiProperty->Value        =    $product;
	                $base_de->Properties[]     =    $apiProperty;
	            endforeach;
	            
	            Mage::helper('exacttarget/exacttarget')->dataExtension($client,$base_de,$numberOfSends,$rowId,$storeId);
	            
	            if(array_key_exists('attributes', $unserializedArray['product']['relationship'])):
		            foreach($unserializedArray['product']['relationship']['attributes'] as $relationships):
		                $relationship_de->Properties           =    array();
		                foreach($relationships as $key => $relationship):
		                    $apiProperty                       =    new ExactTarget_APIProperty();
		                    $apiProperty->Name                 =    $key;
		                    $apiProperty->Value                =    $relationship;
		                    $relationship_de->Properties[]     =    $apiProperty;
		                 endforeach;
		                Mage::helper('exacttarget/exacttarget')->dataExtension($client,$relationship_de,$numberOfSends,$rowId,$storeId);
		            endforeach;
		         endif;
			endif;            
        } catch(Exception $e){
            Mage::log('There was an issue with pushing information to one of the product data extensions.');
            Mage::logException($e);
        } catch(SoapFault $e) {
            Mage::logException($e);
        }
    }
    
    public function sendCart($serialzedArray,$numberOfSends,$rowId,$client)
    {
    	try{
    		$unserializedArray	=	array();
    		$stringLength		=	strlen($serialzedArray);
    		$lastCharacter		=	substr($serialzedArray,($stringLength - 1),1);
    		 
    		//Check for the last character in the serialized array to make sure it's a complete array.
    		if($lastCharacter != '}'):
    			$message = 'An error occured with the Row Id '.$rowId.' in the exacttarget_dataextension_log table on '.date("Y-m-d H:i:s").'.  The serialized array was incomplete. This record will not be sent to ExactTarget.';
    			Mage::log($message,1,'et_serialized_failure.log');
    		else:
    			$unserializedArray             =    unserialize($serialzedArray);
    		endif;
    		 
    		//If the unserialized array is not empty
    		if(!empty($unserializedArray)):
    
	    		$abandoncart_de                =    new ExactTarget_DataExtensionObject();
	    		$abandoncart_de->CustomerKey   =    $unserializedArray['abandoncart']['key'];
	    		$storeId                       =    $unserializedArray['abandoncart']['storeId'];
	    		$abandoncart_de->Properties    =    array();
	    		
	    		$client->username = Mage::helper('exacttarget')->getApiUser($storeId);
	    		$client->password = Mage::helper('exacttarget')->getApiPassword($storeId);
	    		 
	    		foreach($unserializedArray['abandoncart']['attributes'] as $key => $cart):
	    		 
		    		$apiProperty                  =    new ExactTarget_APIProperty();
		    		$apiProperty->Name            =    $key;
		    		$apiProperty->Value        	  =    $cart;
		    		 
		    		$abandoncart_de->Properties[] = $apiProperty;
	    
	    		endforeach;
	    		 
	    		//Call the data extension api - is an update method so primary key (in the data extension) needs to be a field that can be
	    		//updated on (i.e. email address for customers).
	    		Mage::helper('exacttarget/exacttarget')->dataExtension($client,$abandoncart_de,$numberOfSends,$rowId,$storeId);
    		endif;
    
    	} catch(Exception $e){
    		Mage::log('There was an issue with pushing information to the abandoncart data extension.');
    		Mage::logException($e);
    	} catch(SoapFault $e) {
    		Mage::logException($e);
    	}
    }
}