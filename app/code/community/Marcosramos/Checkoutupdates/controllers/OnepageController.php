<?php

require_once 'Mage/Checkout/controllers/OnepageController.php';

class Marcosramos_Checkoutupdates_OnepageController extends Mage_Checkout_OnepageController
{
    /**
     * Get order review step html
     *
     * @return string
     */
    protected function _getReviewHtml()
    {
        $text = $this->getLayout()->getBlock('root')->toHtml();

        $text .= '<form id="discount-coupon-form" action="/checkout/onepage/coupon/" method="post">';
        $text .= '<label for="coupon_code"> ' . $this->__('Enter your coupon code if you have one.') . '</label><br />';
        $text .= '<input type="hidden" name="remove" id="remove-coupone" value="0" />';
        $text .= '<input type="hidden" id="coupon" name="coupon" value="1"/>';
        if(!strlen($this->getOnepage()->getQuote()->getCouponCode())) {
            $text .= '<div class="v-fix"><input class="input-text" id="coupon_code" name="coupon_code" value="' . $this->getOnepage()->getQuote()->getCouponCode() . '"/></div>';
            $text .= '<button type="button" class="button" onclick="discountForm.submit(false)" value="' . $this->__('Apply Coupon') .'"><span>' . $this->__('Apply Coupon') .'</span></button>';
        } else {
            $text .= '<div class="v-fix"><input class="input-text" id="coupon_code" name="coupon_code" value="' . $this->getOnepage()->getQuote()->getCouponCode() . '"/></div>';
            $text .= '<button type="button" class="button" onclick="discountForm.submit(false)" value="' . $this->__('Apply Coupon') .'"><span>' . $this->__('Apply Coupon') .'</span></button>';
            $text .= '<button type="button" class="button cancel-btn" onclick="discountForm.submit(true)" value="Cancel Coupon"><span><span>Cancel Coupon</span></span></button>';
        }
        $text .= '</form>';

        return $text;
    }

    /**
    * Coupon check
    */
    function couponAction()
    {
        $this->loadLayout('checkout_onepage_review');

        $this->couponCode = (string) $this->getRequest()->getParam('coupon_code');

        Mage::getSingleton('checkout/cart')->getQuote()->getShippingAddress()->setCollectShippingRates(true);
        Mage::getSingleton('checkout/cart')->getQuote()->setCouponCode(strlen($this->couponCode) ? $this->couponCode : '')
                ->collectTotals()
                ->save();

        $result['goto_section'] = 'review';
        $result['update_section'] = array(
            'name' => 'review',
            'html' => $this->_getReviewHtml()
        );

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
}
