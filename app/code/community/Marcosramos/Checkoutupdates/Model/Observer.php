<?php

class Marcosramos_Checkoutupdates_Model_Observer
{

    public function doSomething(Varien_Event_Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();
		
		if ($quote->getData("newslettersignup")) {
		
			$customer = $quote->getCustomer();
			
			if ($customer && $customer->getId()) {
				$customer->setIsSubscribed(true);
				Mage::getModel('newsletter/subscriber')->subscribeCustomer($customer);
			} else {
				Mage::getModel('newsletter/subscriber')->subscribe($observer->getEvent()->getOrder()->getBillingAddress()->getEmail());
			}
		}
    }

}

?>
