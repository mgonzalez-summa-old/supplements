<?php

$installer = $this;

$installer->startSetup();

//Attribute to add
$newAttributeName = "newslettersignup";

//b) Add Quote attributes (one page step to step save field)
$setup = new Mage_Sales_Model_Mysql4_Setup('sales_setup');
$setup->getConnection()->addColumn(
    $setup->getTable('sales_flat_quote'), $newAttributeName, 'text NULL DEFAULT NULL'
);
$setup->addAttribute('quote', $newAttributeName, array('type' => 'static', 'visible' => false));

$installer->endSetup();
