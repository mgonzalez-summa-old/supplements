<?php
class Marcosramos_Printedorders_Model_Invoice extends Mage_Sales_Model_Order_Pdf_Invoice
{
    protected $_pageTopY1AxisValue    =    835;
    protected $_pageTopY2AxisValue    =    780;
    protected $_boldFont              =    '';
    
	public function getPdf($invoices = array())
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('invoice');

        $pdf = new Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($invoices as $invoice) {
            if ($invoice->getStoreId()) {
                Mage::app()->getLocale()->emulate($invoice->getStoreId());
                Mage::app()->setCurrentStore($invoice->getStoreId());
            }
            $page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
            $pdf->pages[] = $page;

            $order = $invoice->getOrder();
            
            
            /**
             * Invoice format flow
             *
             * --------------------------------------------------------------------------------------
             * |    BarCode        |      Order Date    |        Logo         |        Store        |
             * |                   |      Order #       |                     |       Address       |
             * --------------------------------------------------------------------------------------
             * |                                               |                                    |
             * |                                               |                                    |
             * |                                               |                                    |
             * |                Billing/Shipping               |            Fluff Text              |
             * |                                               |                                    |
             * |                                               |                                    |
             * |                                               |                                    |
             * |-----------------------------------------------|-------------------------------------
             * |                                                                                    |
             * |                                                                                    |
             * |                                Warehouse Work Grid                                 |
             * |                                                                                    |
             * |                                                                                    |
             * --------------------------------------------------------------------------------------
             * |                                                                                    |
             * |                                                                                    |
             * |                                Order Items Grid                                    |
             * |                                                                                    |
             * |                                                                                    |
             * --------------------------------------------------------------------------------------
             * |                                                |                                   |
             * |                                                |                                   |
             * |                    Order Notes                 |            Order Totals           |
             * |                                                |                                   |
             * |                                                |                                   |
             * --------------------------------------------------------------------------------------
             */
            
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawRectangle(5, $this->_pageTopY1AxisValue, 180, $this->_pageTopY2AxisValue,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            
            /* Add Barcode */
            $barcodeString = $this->convertToBarcodeString($order->getRealOrderId());
            $page->setFillColor(new Zend_Pdf_Color_Rgb(0, 0, 0));
            $page->setFont(Zend_Pdf_Font::fontWithPath(dirname(__FILE__)  . '/' . 'Code128bWin.ttf'), 18);
            //Formats  - string, x-axis, y-axis (800 top), encoding);
            $page->drawText($barcodeString, 35, 800, 'CP1252');
            
            //Border around Order Information
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawRectangle(180, $this->_pageTopY1AxisValue, 290, $this->_pageTopY2AxisValue,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            
            //Gray background for "ORDER DATE
            $this->_setFontRegular($page);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.7));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawRectangle(180, $this->_pageTopY1AxisValue, 290, 823,Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE);
            //Reset colors for font
            $this->_boldFont    =    Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
            $page->setFont($this->_boldFont,9);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawText(Mage::helper('sales')->__('ORDER DATE '), 202, 826, 'UTF-8');
            
            //Reset font to human readable font
            $this->_setFontRegular($page);
            //Setting the white background where the order date/time is displayed
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1.0));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawRectangle(180, 823, 290, 810,Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE);
            //Reset line color for font
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawText(Mage::helper('core')->formatDate($order->getCreatedAtStoreDate(), 'short', true), 202, 813, 'UTF-8');
            
            //Gray box for Order number
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.7));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawRectangle(180, 810, 290, 795,Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE);
            //Reset font color
            $this->_boldFont    =    Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
            $page->setFont($this->_boldFont,9);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawText(Mage::helper('sales')->__('ORDER NUMBER'), 198, 800, 'UTF-8');
            
            //Reset font to human readable font
            $this->_setFontRegular($page);
            //Resetting values for the order number
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1.0));
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawText($order->getRealOrderId(), 215, 785, 'UTF-8');

            /* Add image */
            $this->insertLogo($page, $invoice->getStore());

            /* Add address */
            $this->insertHeaderAddress($page, $invoice->getStore());
            
            //Get the order billing address
            $this->insertBillingAddress($page,$order,$invoice->getStore());
            
            //Get the order Shipping Address
            $this->insertShippingAddress($page,$order,$invoice->getStore());
            
            $this->insertClientMessage($page,$invoice->getStore());
            
            $this->insertWarehouseGrid($page,$order,$invoice->getStore());
            
            
            $this->insertLineItems($page,$invoice,$order,$style,$invoice->getStore());

            if ($invoice->getStoreId()) {
                Mage::app()->getLocale()->revert();
            }
        }

        $this->_afterGetPdf();

        return $pdf;
    }
	
    protected function insertLogo(&$page, $store = null)
    {
        $image = Mage::getStoreConfig('sales/identity/logo', $store);
        if ($image) {
            $image = Mage::getBaseDir('media') . '/sales/store/logo/' . $image;
            if (is_file($image)) {
                $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
                $page->drawRectangle(290, $this->_pageTopY1AxisValue, 425, $this->_pageTopY2AxisValue,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
                $image = Zend_Pdf_Image::imageWithPath($image);
                
                //Images are drawn upside down so that is why y coordinates are in reverse
                $page->drawImage($image, 305, 790, 410, 830);
            }
        }
    }
	
	public function insertHeaderAddress(&$page, $store = null)
    {
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page);

        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(425, $this->_pageTopY1AxisValue, 590, $this->_pageTopY2AxisValue,Zend_Pdf_Page::SHAPE_DRAW_STROKE);

        //$page->setLineWidth(0);
        $this->y = 820;
        foreach (explode("\n", Mage::getStoreConfig('sales/identity/address', $store)) as $value){
            if ($value!=='') {
                $page->drawText(trim(strip_tags($value)), 430, $this->y, 'UTF-8');
                $this->y -=10;
            }
        }
    }
    
    public function insertBillingAddress(&$page,$order,$store=null)
    {
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(5, 780, 275, 675,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        
        $this->_boldFont    =    Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $page->setFont($this->_boldFont,11);
        

        //Grabing address elements as needed - the previous method of formatting for pdf was resulting in the City on the same line as address line 1.
        $billingAddressArray = array();
        $streetArray         = array();
        $multiLineAddress = strstr($order->getBillingAddress()->getData('street'),"\n");
        
        //Checking for 2nd address in street
        if($multiLineAddress != false):
            $streetArray    =    explode("\n",$order->getBillingAddress()->getData('street'));
        endif;
        
        $billingAddressArray['fullname']        = $order->getBillingAddress()->getData('firstname').' '.$order->getBillingAddress()->getData('lastname');
        
        if($order->getBillingAddress()->getData('company') != ''):
            $billingAddressArray['company']         = $order->getBillingAddress()->getData('company');
        endif;
        
        if(is_array($streetArray) && count($streetArray) > 1):
            $billingAddressArray['street_1']    = $streetArray[0];
            $billingAddressArray['street_2']    = $streetArray[1];
        else:
            $billingAddressArray['street_1']    = $order->getBillingAddress()->getData('street');
        endif;
        
        $billingAddressArray['city_state_zip']  = $order->getBillingAddress()->getData('city').', '.$order->getBillingAddress()->getData('region').' '.$order->getBillingAddress()->getData('postcode');
        $billingAddressArray['country']         = $order->getBillingAddress()->getData('country_id');
        $billingAddressArray['phone']           = $order->getBillingAddress()->getData('telephone');
        
        
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.7));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(5, 780, 275, 760,Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE);
        
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawText(Mage::helper('sales')->__('BILLING ADDRESS:'), 20, 765, 'UTF-8');
        
        $this->_setFontRegular($page);
        //Reset line color for font
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        
        $billingAddressYAxis    =    745;
        
        foreach ($billingAddressArray as $value):
            if ($value!==''):
                $page->drawText(strip_tags(ltrim(strtoupper($value))), 20, $billingAddressYAxis, 'UTF-8');
            endif;
            $billingAddressYAxis-=10;
        endforeach;
    }
    
    public function insertShippingAddress(&$page,$order,$store=null)
    {
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(5, 675, 275, 570,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
    
        $this->_boldFont    =    Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $page->setFont($this->_boldFont,11);
    
    
        //Grabing address elements as needed - the previous method of formatting for pdf was resulting in the City on the same line as address line 1.
        $shippingAddressArray = array();
        $streetArray         = array();
        $multiLineAddress = strstr($order->getShippingAddress()->getData('street'),"\n");
    
        //Checking for 2nd address in street
        if($multiLineAddress != false):
            $streetArray    =    explode("\n",$order->getShippingAddress()->getData('street'));
        endif;
    
        $shippingAddressArray['fullname']        = $order->getShippingAddress()->getData('firstname').' '.$order->getShippingAddress()->getData('lastname');
    
        if($order->getShippingAddress()->getData('company') != ''):
            $shippingAddressArray['company']         = $order->getShippingAddress()->getData('company');
        endif;
    
        if(is_array($streetArray) && count($streetArray) > 1):
            $shippingAddressArray['street_1']    = $streetArray[0];
            $shippingAddressArray['street_2']    = $streetArray[1];
        else:
            $shippingAddressArray['street_1']    = $order->getShippingAddress()->getData('street');
        endif;
    
        $shippingAddressArray['city_state_zip']  = $order->getShippingAddress()->getData('city').', '.$order->getShippingAddress()->getData('region').' '.$order->getShippingAddress()->getData('postcode');
        $shippingAddressArray['country']         = $order->getShippingAddress()->getData('country_id');
        $shippingAddressArray['phone']           = $order->getShippingAddress()->getData('telephone');
    
    
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.7));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(5, 675, 275, 655,Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE);
    
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawText(Mage::helper('sales')->__('SHIPPING ADDRESS:'), 20, 660, 'UTF-8');
    
        $this->_setFontRegular($page);
        //Reset line color for font
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
    
        $shippingAddressYAxis    =    640;
    
        foreach ($shippingAddressArray as $value):
            if ($value!==''):
                $page->drawText(strip_tags(ltrim(strtoupper($value))), 20, $shippingAddressYAxis, 'UTF-8');
            endif;
            $shippingAddressYAxis-=10;
        endforeach;
    }
    
    public function insertClientMessage(&$page,$storeId=null)
    {
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(275, 780, 590, 570,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $this->_setFontRegular($page);
        $y = 725;
        $x = 320;
        $page->drawText(Mage::helper('sales')->__('5% Price Match Guarantee!'), $x, $y - (0 * 12) , 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('SupplementWarehouse.com Price match Guarantee: Take'), $x, $y - (2 * 12) , 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('5% off MULTIPLE competitors prices right in our state of'), $x, $y - (3 * 12) , 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('the art shopping cart! One stop shopping... for just ONE'), $x, $y - (4 * 12) , 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('reasonable shipping charge! FREE merchandise on any size'), $x, $y - (5 * 12) , 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('order & CRAZY deals make us the best value on the web'), $x, $y - (6 * 12) , 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('for ALL your supplement needs!'), $x, $y - (7 * 12) , 'UTF-8');
    }
    /**
     * Draws the warehouse grid - this will eventually go away.
     * @param unknown_type $page
     * @param unknown_type $order
     * @param unknown_type $storeId
     */
    public function insertWarehouseGrid(&$page,$order,$storeId=null)
    {
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(5, 570, 590, 470,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $this->_setFontRegular($page);
        $y = 555;
        $x = 20;
        
        for($i=1;$i<=20;$i++):
            $page->drawText($i,$x+($i * 25),$y,'UTF-8');
        endfor;
        
        //Row 1 of boxes
        $y = 550;
        $x = 10;
        
        for($i=1;$i<=20;$i++):
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1.0));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawRectangle($x+($i * 25), $y, $x+(($i+1) * 25), $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        endfor;
        
        //Row 2 of boxes
        $y = 535;
        $x = 10;
        
        for($i=1;$i<=20;$i++):
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1.0));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawRectangle($x+($i * 25), $y, $x+(($i+1) * 25), $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        endfor;
        
        //Row 3 of boxes
        $y = 520;
        $x = 10;
        
        for($i=1;$i<=20;$i++):
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1.0));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawRectangle($x+($i * 25), $y, $x+(($i+1) * 25), $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        endfor;
        
        //Row 4 of boxes
        $y = 505;
        $x = 10;
        
        for($i=1;$i<=20;$i++):
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1.0));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawRectangle($x+($i * 25), $y, $x+(($i+1) * 25), $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        endfor;
        
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawText(strtoupper(Mage::helper('sales')->__('Total Products: ')).number_format($order->getTotalQtyOrdered(),0),385,480,'UTF-8');
        
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1.0));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(475, 490, 535, 475,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
    }
    
    public function insertLineItems(&$page,$invoice,$order,$zendPdfStyle,$storeId=null)
    {
        /* Add table */
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.7));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        //$page->setLineWidth(0.5);
        $y = 465;
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $page->drawRectangle(5, $y, 590, $y-15);
        
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1.0));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(5, $y, 25, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawRectangle(25, $y, 95, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawRectangle(95, $y, 180, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawRectangle(180, $y, 400, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawRectangle(400, $y, 450, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawRectangle(450, $y, 490, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawRectangle(490, $y, 535, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawRectangle(535, $y, 590, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        
        $y -=10;
        
        /* Add table head */
        $this->_setFontRegular($page);
        $page->setFillColor(new Zend_Pdf_Color_Rgb(0, 0, 0));
        $page->drawText(strtoupper(Mage::helper('sales')->__('Qty')), 7, $y, 'UTF-8');
        $page->drawText(strtoupper(Mage::helper('sales')->__('Item Id')), 35, $y, 'UTF-8');
        $page->drawText(strtoupper(Mage::helper('sales')->__('Brand')), 115, $y, 'UTF-8');
        $page->drawText(strtoupper(Mage::helper('sales')->__('Description')), 190, $y, 'UTF-8');
        $page->drawText(strtoupper(Mage::helper('sales')->__('Size')), 418, $y, 'UTF-8');
        $page->drawText(strtoupper(Mage::helper('sales')->__('Amount')), 453, $y, 'UTF-8');
        $page->drawText(strtoupper(Mage::helper('sales')->__('Subtotal')), 493, $y, 'UTF-8');
        $page->drawText(strtoupper(Mage::helper('sales')->__('Location')), 538, $y, 'UTF-8');
        
        $y -=25;
        
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        
        
        $startingY    =    $y + 20;
        $endingY      =    $y-15;
        
        /* Add body */
        foreach ($invoice->getAllItems() as $item){
            if ($item->getOrderItem()->getParentItem()) {
                continue;
            }
            
            $product    =    Mage::getModel('catalog/product')->load($item->getProductId());
            
//             if ($y < 15) {
            if ($startingY < 50) {
                $page = $this->newPage(array('table_header' => true));
                $startingY = 780;
                $endingY = $startingY-25;
            }
            
            //The boxes around each line item piece.
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1.0));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawRectangle(5, $startingY, 25, $endingY,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(25, $startingY, 95, $endingY,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(95, $startingY, 180, $endingY,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(180, $startingY, 400, $endingY,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(400, $startingY, 450, $endingY,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(450, $startingY, 490, $endingY,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(490, $startingY, 535, $endingY,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(535, $startingY, 590, $endingY,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            
            
            //If UPC2 is populated, use that field over the barcode field, otherwise use the barcode field
            if(!is_null($product->getData('UPC2')) && $product->getData('UPC2') != '' && $product->getData('UPC2') != 'NULL'):
                $itemId        =    $item->getSku()." ".$product->getData('UPC2');
            else:
                $itemId        =    $item->getSku()." ".$product->getData('Barcode');
            endif;
            
            
            //If Europa is set then show it.
            if($product->getData('Europa') != '' && !is_null($product->getData('Europa')) && $product->getData('Europa') != 'NULL'):
                $itemId        =    $itemId." Europa:".$product->getData('Europa');
            endif;
            
            $description   =    $item->getName()." ".$product->getAttributeText('flavor')." ".$product->getPromoDescription();
            
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $this->_setFontRegular($page,8);
            
            $page->drawText(number_format($item->getQty(),0), 12, $startingY-8, 'UTF-8');
            
            
            $itemId = explode("\n",$this->getWrappedText($itemId,$zendPdfStyle,50));
            
            $wrappedY    =    $startingY-8;
            foreach($itemId as $id)
            {
                $page->drawText($id, 30, $wrappedY,'UTF-8');
                if(count($itemId > 1)):
                    $wrappedY-=8;
                endif;
            }
            
            
            $brand = explode("\n",$this->getWrappedText($product->getAttributeText('brand'),$zendPdfStyle,100));
            $wrappedY    =    $startingY-8;
            foreach($brand as $b)
            {
                $page->drawText($b, 97, $wrappedY,'UTF-8');
                if(count($brand > 1)):
                    $wrappedY-=8;
                endif;
            }
            
            
            $description = explode("\n",$this->getWrappedText($description,$zendPdfStyle,275));
            $wrappedY    =    $startingY-8;
            foreach($description as $d)
            {
                $page->drawText($d, 185, $wrappedY,'UTF-8');
                if(count($description > 1)):
                    $wrappedY-=8;
                endif;
            }
            
            $productSize = explode("\n",$this->getWrappedText($product->getSize(),$zendPdfStyle,60));
            $wrappedY    =    $startingY-8;
            foreach($productSize as $p)
            {
                $page->drawText($p, 402, $wrappedY,'UTF-8');
                if(count($productSize > 1)):
                    $wrappedY-=8;
                endif;
            }
            
            
            $page->drawText(Mage::helper('core')->currency($item->getPrice(),true,false), 453, $startingY-8, 'UTF-8');
            $page->drawText(Mage::helper('core')->currency($item->getBaseRowTotal(),true,false), 493, $startingY-8, 'UTF-8');
            
            //Get the inventory information - we want the shelf location of the main warehouse (stock_id = 1).
            $inventoryInformation    =    Mage::getModel('cataloginventory/stock_item')->getCollection()
                                                                                       ->addFieldToFilter('product_id',$item->getProductId())
                                                                                       ->addFieldToFilter('stock_id',1);
            $shelfLocation        =    '';
            foreach($inventoryInformation as $location):
                $shelfLocation    =    $location->getShelfLocation();
            endforeach;
           
           if($shelfLocation != ''):
               $shelfLocation = explode("\n",$this->getWrappedText($shelfLocation,$zendPdfStyle,50));
               $wrappedY    =    $startingY-8;
               foreach($shelfLocation as $s)
               {
                   $page->drawText($s, 538, $wrappedY,'UTF-8');
                   if(count($shelfLocation > 1)):
                       $wrappedY-=10;
                   endif;
               }
           else:
               $page->drawText($shelfLocation, 15, $wrappedY,'UTF-8');
           endif;
            
            $y -=15;
            
            $startingY    =    $endingY;
            $endingY      =    $startingY - 35;
        }
        
        //Insert Order Notes - need to know where to pull from - Invoice comments or Order comments???
        
        
        $y = $endingY+35;
        
        $orderStatusComments = Mage::getModel('sales/order_status_history')->getCollection()
                                                                           ->addAttributeToFilter('parent_id',$order->getId())
                                                                            ->addAttributeToFilter('is_customer_notified',1)
                                                                           ->addAttributeToFilter('entity_name','order');
        $orderComment        =    '';

        if(isset($orderStatusComments)):
            foreach($orderStatusComments as $comment):
                $orderComment    .=   $comment->getComment().' ';        
            endforeach;
        endif;
        
        //Order Comments - order comments left in the backend that are marked as "is_customer_notified"
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $this->_boldFont    =    Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $page->setFont($this->_boldFont,8);
        $fontSize           =    8;
        
        
        //Shipping and Handling Text
        $page->drawText(strtoupper(Mage::helper('sales')->__('Order Notes')), 10, $y-10, 'UTF-8');
        
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $this->_setFontRegular($page,8);
        
        if($orderComment != ''):
            $orderComment = explode("\n",$this->getWrappedText($orderComment,$zendPdfStyle,340));
            $wrappedY    =    $startingY-25;
            foreach($orderComment as $o)
            {
                $page->drawText($o, 15, $wrappedY,'UTF-8');
                if(count($orderComment > 1)):
                    $wrappedY-=10;
                endif;
            }
        else:
            $page->drawText($orderComment, 15, $wrappedY,'UTF-8');
        endif;
        
        //Get order Totals
        /* Add table */
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        //$page->setLineWidth(0.5);
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $page->drawRectangle(290, $y, 590, $y-60);
        
        $this->_boldFont    =    Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $page->setFont($this->_boldFont,8);
        $fontSize           =    8;
        
        
        //Shipping and Handling Gray Box
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.7));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(290,$y,435,$y-15);
        
        //Shipping and Handling Text
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $textWidth    =    $this->getTextWidth(strtoupper(Mage::helper('sales')->__('Shipping and Handling')), $this->_boldFont,8);
        $x            =    435 - $textWidth - $fontSize; //page width, text width, font size;
        $page->drawText(strtoupper(Mage::helper('sales')->__('Shipping and Handling')), $x, $y-10, 'UTF-8');
        
        //Shipping and Handling White box
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(435,$y,590,$y-15);
        
        //Shipping and Handling amount
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $textWidth    =    $this->getTextWidth(Mage::helper('core')->currency($order->getBaseShippingAmount(),true,false), $this->_boldFont,8);
        $x            =    590 - $textWidth - $fontSize; //page width, text width, font size;
        $page->drawText(Mage::helper('core')->currency($order->getBaseShippingAmount(),true,false), $x, $y-10, 'UTF-8');
        
        //Discounts Gray Box
        $shippingEndPoint    = $y-15;
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.7));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(290,$shippingEndPoint,435,$shippingEndPoint-15);
        
        //Discount Text
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $textWidth    =    $this->getTextWidth(strtoupper(Mage::helper('sales')->__('Discounts')), $this->_boldFont,8);
        $x            =    435 - $textWidth - $fontSize; //page width, text width, font size;
        $page->drawText(strtoupper(Mage::helper('sales')->__('Discounts')), $x, $shippingEndPoint-10, 'UTF-8');
        
        //Discount White box
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(435,$shippingEndPoint,590,$shippingEndPoint-15);
        
        //Discount coupon code
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $textWidth    =    $this->getTextWidth($order->getCouponCode(), $this->_boldFont,8);
        $x            =    590 - $textWidth - $fontSize;//page width, text width, font size;
        $page->drawText($order->getCouponCode(), $x, $shippingEndPoint-10, 'UTF-8');
        
        //Sales Tax Gray box
        $discountEndPoint = $shippingEndPoint - 15;
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.7));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(290,$discountEndPoint,435,$discountEndPoint-15);
        
        //Sales Tax Text
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $textWidth    =    $this->getTextWidth(strtoupper(Mage::helper('sales')->__('Sales Tax')), $this->_boldFont,8);
        $x            =    435 - $textWidth - $fontSize;//page width, text width, font size;
        $page->drawText(strtoupper(Mage::helper('sales')->__('Sales Tax')), $x, $discountEndPoint-10, 'UTF-8');
        
        //Sales Tax White box
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(435,$discountEndPoint,590,$discountEndPoint-15);
        
        //Sales Tax Amount
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $textWidth    =    $this->getTextWidth(Mage::helper('core')->currency($order->getBaseTaxAmount(),true,false), $this->_boldFont,8);
        $x            =    590 - $textWidth - $fontSize;//page width, text width, font size;
        $page->drawText(Mage::helper('core')->currency($order->getBaseTaxAmount(),true,false), $x, $discountEndPoint-10, 'UTF-8');
        
        //Total Gray Box
        $taxEndPoint    = $discountEndPoint - 15;
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.7));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(290,$taxEndPoint,435,$taxEndPoint-15);
        
        //Total Text
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $textWidth    =    $this->getTextWidth(strtoupper(Mage::helper('sales')->__('Total')), $this->_boldFont,8);
        $x            =    435 - $textWidth - $fontSize;//page width, text width, font size;
        $page->drawText(strtoupper(Mage::helper('sales')->__('Total')), $x, $taxEndPoint-10, 'UTF-8');
        
        //Total White box
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->drawRectangle(435,$taxEndPoint,590,$taxEndPoint-15);
        
        //Total Amount
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
        $textWidth    =    $this->getTextWidth(Mage::helper('core')->currency($order->getGrandTotal(),true,false), $this->_boldFont,8);
        $x            =    590 - $textWidth - $fontSize;
        $page->drawText(Mage::helper('core')->currency($order->getGrandTotal(),true,false), $x, $taxEndPoint-10, 'UTF-8');
        
//         //Status Gray box
//         $statusEndPoint = $taxEndPoint - 15;
//         $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.7));
//         $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
//         $page->drawRectangle(290,$statusEndPoint,435,$statusEndPoint-15);
        
//         //Status Text
//         $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
//         $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
//         $textWidth    =    $this->getTextWidth(strtoupper(Mage::helper('sales')->__('Status')), $this->_boldFont,8);
//         $x            =    435 - $textWidth - $fontSize;//page width, text width, font size;
//         $page->drawText(strtoupper(Mage::helper('sales')->__('Status')), $x, $statusEndPoint-10, 'UTF-8');
        
//         //Status White box
//         $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
//         $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
//         $page->drawRectangle(435,$statusEndPoint,590,$statusEndPoint-15);
        
//         //Order Status
//         $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.2));
//         $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
//         $textWidth    =    $this->getTextWidth(strtoupper($order->getStatus()), $this->_boldFont,8);
//         $x            =    590 - $textWidth - $fontSize;//page width, text width, font size;
//         $page->drawText(strtoupper($order->getStatus()), $x, $statusEndPoint-10, 'UTF-8');
    }
	
    protected function _setFontRegular($object, $size = 8)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $object->setFont($font, $size);
        return $font;
    }
	
	protected function _setFontTimesRegular($object, $size = 7)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES);
        $object->setFont($font, $size);
        return $font;
    }

    protected function _setFontBold($object, $size = 7)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $object->setFont($font, $size);
        return $font;
    }

    protected function _setFontItalic($object, $size = 7)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_ITALIC);
        $object->setFont($font, $size);
        return $font;
    }
	
	protected function convertToBarcodeString($toBarcodeString)
    {
        $str = $toBarcodeString;
        $barcode_data = str_replace(' ', chr(128), $str);

        $checksum = 104; # must include START B code 128 value (104) in checksum
        for($i=0;$i<strlen($str);$i++) {
                $code128 = '';
                if (ord($barcode_data{$i}) == 128) {
                        $code128 = 0;
                } elseif (ord($barcode_data{$i}) >= 32 && ord($barcode_data{$i}) <= 126) {
                        $code128 = ord($barcode_data{$i}) - 32;
                } elseif (ord($barcode_data{$i}) >= 126) {
                        $code128 = ord($barcode_data{$i}) - 50;
                }
        $checksum_position = $code128 * ($i + 1);
        $checksum += $checksum_position;
        }
        $check_digit_value = $checksum % 103;
        $check_digit_ascii = '';
        if ($check_digit_value <= 94) {
            $check_digit_ascii = $check_digit_value + 32;
        } elseif ($check_digit_value > 94) {
            $check_digit_ascii = $check_digit_value + 50;
        }
        $barcode_str = chr(154) . $barcode_data . chr($check_digit_ascii) . chr(156);
            
        return $barcode_str;

    }
    
    /**
     * Create new page and assign to PDF object
     *
     * @param array $settings
     * @return Zend_Pdf_Page
     */
    public function newPage(array $settings = array())
    {
        /* Add new table head */
        $page = $this->_getPdf()->newPage(Zend_Pdf_Page::SIZE_A4);
        $this->_getPdf()->pages[] = $page;
        $y = 800;
    
        if (!empty($settings['table_header'])) {
            $this->_setFontRegular($page);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
            $page->drawRectangle(5, $y, 590, $y-15);
            
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1.0));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));
            $page->drawRectangle(5, $y, 25, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(25, $y, 95, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(95, $y, 180, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(180, $y, 400, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(400, $y, 450, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(450, $y, 490, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(490, $y, 535, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $page->drawRectangle(535, $y, 590, $y-15,Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            
            $y -=10;
            
            /* Add table head */          
            $this->_setFontRegular($page);
            $page->setFillColor(new Zend_Pdf_Color_Rgb(0, 0, 0));
            $page->drawText(strtoupper(Mage::helper('sales')->__('Qty')), 7, $y, 'UTF-8');
            $page->drawText(strtoupper(Mage::helper('sales')->__('Item Id')), 35, $y, 'UTF-8');
            $page->drawText(strtoupper(Mage::helper('sales')->__('Brand')), 115, $y, 'UTF-8');
            $page->drawText(strtoupper(Mage::helper('sales')->__('Description')), 190, $y, 'UTF-8');
            $page->drawText(strtoupper(Mage::helper('sales')->__('Size')), 418, $y, 'UTF-8');
            $page->drawText(strtoupper(Mage::helper('sales')->__('Amount')), 453, $y, 'UTF-8');
            $page->drawText(strtoupper(Mage::helper('sales')->__('Subtotal')), 493, $y, 'UTF-8');
            $page->drawText(strtoupper(Mage::helper('sales')->__('Location')), 538, $y, 'UTF-8');
    
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $y -=25;
        }
    
        return $page;
    }
    
    /**
     * Return length of generated string in points
     *
     * @param string                     $text
     * @param Zend_Pdf_Resource_Font|Zend_Pdf_Page     $font
     * @param int                         $fontSize
     * @return double
     */
    public static function getTextWidth($text, $resource, $fontSize = null/*, $encoding = null*/) {
        //if( $encoding == null ) $encoding = 'UTF-8';
    
        if( $resource instanceof Zend_Pdf_Page ){
            $font = $resource->getFont();
            $fontSize = $resource->getFontSize();
        }elseif( $resource instanceof Zend_Pdf_Resource_Font ){
            $font = $resource;
            if( $fontSize === null ) throw new Exception('The fontsize is unknown');
        }
    
        if( !$font instanceof Zend_Pdf_Resource_Font ){
            throw new Exception('Invalid resource passed');
        }
    
        $drawingText = $text;//iconv ( '', $encoding, $text );
        $characters = array ();
        for($i = 0; $i < strlen ( $drawingText ); $i ++) {
            $characters [] = ord ( $drawingText [$i] );
        }
        $glyphs = $font->glyphNumbersForCharacters ( $characters );
        $widths = $font->widthsForGlyphs ( $glyphs );
    
        $textWidth = (array_sum ( $widths ) / $font->getUnitsPerEm ()) * $fontSize;
        return $textWidth;
    }
    
    /**
     * Function that will take in the string, pdf style and the max width to allow characters to span and then return
     * back a wrapped string to fit in the area needed on the pdf.  
     * @param unknown_type $string
     * @param Zend_Pdf_Style $style
     * @param unknown_type $max_width
     * @return string
     */
    public function getWrappedText($string, Zend_Pdf_Style $style,$max_width)
    {
        $wrappedText = '' ;
        $lines = explode("\n",$string) ;
        foreach($lines as $line) {
            $words = explode(' ',$line) ;
            $word_count = count($words) ;
            $i = 0 ;
            $wrappedLine = '' ;
            while($i < $word_count)
            {
                /* if adding a new word isn't wider than $max_width,
                 we add the word */
                if($this->widthForStringUsingFontSize($wrappedLine.' '.$words[$i]
                        ,$style->getFont()
                        , $style->getFontSize()) < $max_width) {
                    if(!empty($wrappedLine)) {
                        $wrappedLine .= ' ' ;
                    }
                    $wrappedLine .= $words[$i] ;
                } else {
                    $wrappedText .= $wrappedLine."\n" ;
                    $wrappedLine = $words[$i] ;
                }
                $i++ ;
            }
            $wrappedText .= $wrappedLine."\n" ;
        }
        
        return $wrappedText ;
    }
}