<?php
class Gorilla_AuthorizenetCim_Model_Gateway_Source_PaymentAction
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => Gorilla_AuthorizenetCim_Model_Gateway::ACTION_AUTHORIZE,
                'label' => Mage::helper('paygate')->__('Authorize Only')
            ),
            array(
                'value' => Gorilla_AuthorizenetCim_Model_Gateway::ACTION_AUTHORIZE_CAPTURE,
                'label' => Mage::helper('paygate')->__('Authorize and Capture')
            ),
        );
    }
}