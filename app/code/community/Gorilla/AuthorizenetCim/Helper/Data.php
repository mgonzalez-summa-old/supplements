<?php
class Gorilla_AuthorizenetCim_Helper_Data extends Mage_Paygate_Helper_Data
{
    /**
     * Return credit card add URL
     *
     * @return string
     */
    public function getCustomerAddCreditCardUrl()
    {
        return $this->_getUrl('authorizenetcim/account/add');
    }
}