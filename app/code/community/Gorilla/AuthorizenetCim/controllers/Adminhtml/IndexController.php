<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Inna
 * Date: 2/24/12
 * Time: 2:06 PM
 * To change this template use File | Settings | File Templates.
 */
class Gorilla_AuthorizenetCim_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{

    /**
     * About action
     */
    public function aboutAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('gorilla/authorizenetcim');
        $this->_title('Gorilla')->_title('Authorize.net Customer Information Manager (CIM) Module')->_title('About');
        $this->_addContent($this->getLayout()->createBlock('authorizenetcim/adminhtml_about', 'authorizenetcim_about'));
        $this->renderLayout();
    }

}