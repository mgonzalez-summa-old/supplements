<?php

class Redstage_Brand_Block_Manufacturers_Brief extends Mage_Core_Block_Template

{

    public function getItems()

    {

        if (Mage::getStoreConfig('catalog/brand/manufacturers_show_brands_from_category_only') && !is_null(Mage::registry('current_category')))

        {

            return $this->getCategoryItems();

        }

        

        if (Mage::getStoreConfig('catalog/brand/manufacturers_show_brands_withproducts_only'))

        {

            $items = array();

            $collection = Mage::getModel('brand/brand')->getCollection()

                ->addStoreFilter(Mage::app()->getStore()->getId())

                ->addStatusFilter()

                ->addSortOrder();

            $this->_totalNum = $collection->count();

            $collection->clear();

            $collection->load();

            

            foreach ($collection as $item)

            {

                $productIds = Mage::getModel('brand/brand')->getProductsByManufacturer($item->getManufacturerId(), Mage::app()->getStore()->getId());

                if (!empty($productIds))

                {

                    $items[] = $item;

                }

                if (count($items) >= Mage::helper('brand')->getBriefNum())

                {

                    return $items;

                }

            }

            

            return $items;

        } else 

        {

            $collection = Mage::getModel('brand/brand')->getCollection()

                ->addStoreFilter(Mage::app()->getStore()->getId())

                ->addStatusFilter()

                ->addSortOrder();

            $this->_totalNum = $collection->count();

            $collection->clear();

            $collection->addLimit(Mage::helper('brand')->getBriefNum());

            $collection->load();

            return $collection->getItems();

        }

    }

    

    public function getCategoryItems()

    {

        $productCollection = Mage::getModel('catalog/product')->getCollection();

        $productCollection->addCategoryFilter(Mage::registry('current_category'))->load();

        $productIds = array();

        foreach ($productCollection as $product)

        {

            $productIds[] = $product->getId();

        }

        $manufacturerIds = Mage::getModel('brand/brand')->getManufacturersByProducts($productIds, Mage::app()->getStore()->getId());

        if (empty($manufacturerIds))

        {

            return array();

        }

        $collection = Mage::getModel('brand/brand')->getCollection()

                                                                         ->addStoreFilter(Mage::app()->getStore()->getId())

                                                                         ->addStatusFilter()

                                                                         ->addSortOrder()

                                                                         ->addFieldToFilter('main_table.manufacturer_id', array('in' => $manufacturerIds));

        $this->_totalNum = $collection->count();

        $collection->clear();

        $collection->addLimit(Mage::helper('brand')->getBriefNum());

        $collection->load();

        return $collection->getItems();

    }

    

    public function getTotal()

    {

        return $this->_totalNum;

    }

}

