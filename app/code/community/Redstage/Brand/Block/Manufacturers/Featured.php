<?php
class Redstage_Brand_Block_Manufacturers_Featured extends Mage_Core_Block_Template

{

    public function getItems()

    {

        $collection = Mage::getModel('brand/brand')->getCollection()

            ->addStoreFilter(Mage::app()->getStore()->getId())

            ->addStatusFilter()

            ->addFeaturedFilter()

            ->addSortOrder();

        $collection->load();

        $items = $collection->getItems();

        foreach ($items as $i => $item)

        {

            $productIds = Mage::getModel('brand/brand')->getProductsByManufacturer($item->getManufacturerId(), Mage::app()->getStore()->getId());

            if ( empty($productIds) && Mage::getStoreConfig('catalog/brand/manufacturers_show_brands_withproducts_only') )

            {

                unset($items[$i]);

            }

        }

        return $items;

    }

}

