<?php
class Redstage_Brand_Block_Product_List extends Mage_Catalog_Block_Product_List //Mage_Catalog_Block_Product_Abstract

{

    /**

     * Product Collection

     *

     * @var Mage_Eav_Model_Entity_Collection_Abstract

     */

    protected $_productCollection;

    protected $_manufacturer;

    

    public function __construct(){

        $manufacturers = Mage::registry('brand_manufacturers');

        if (isset($manufacturers[$this->getRequest()->getParam('id')])){

            $this->_manufacturer = $manufacturers[$this->getRequest()->getParam('id')];

        }

        else {

            $this->_manufacturer = Mage::getModel('brand/brand')->load($this->getRequest()->getParam('id'))

                ->getManufacturerId();

            $manufacturers[$this->getRequest()->getParam('id')] = $this->_manufacturer;

            Mage::register('brand_manufacturers', $manufacturers);

        }

    }

    

    protected function _beforeToHtml()

    {

        parent::_beforeToHtml();

        $toolbar = $this->getToolbarBlock();

        $toolbar->removeOrderFromAvailableOrders('position');

        return $this;

    }



    /**

     * Retrieve loaded product collection

     *

     * @return Mage_Eav_Model_Entity_Collection_Abstract

     */

    protected function _getProductCollection()

    {

        if (is_null($this->_productCollection)) {

            $collection = Mage::getResourceModel('catalog/product_collection');

            $attributes = Mage::getSingleton('catalog/config')

                ->getProductAttributes();

            $collection->addAttributeToSelect($attributes)

                ->addMinimalPrice()

                ->addFinalPrice()

                ->addTaxPercents()

                ->addStoreFilter()

                ;



            $productIds = Mage::getModel('brand/brand')->getProductsByManufacturer($this->_manufacturer, Mage::app()->getStore()->getId());

            //$collection->addAttributeToFilter(Mage::helper('brand')->getAttributeCode(), array('eq' => $this->_manufacturer), 'left');

            $collection->addIdFilter($productIds);

            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);

            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

            $this->_productCollection = $collection;

        }

        return $this->_productCollection;

    }

    

    

    

    protected function _toHtml()

    {

        if ($this->_getProductCollection()->count()){

            return parent::_toHtml();

        }

        return '';

    }

}

