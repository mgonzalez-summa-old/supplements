<?php

class Redstage_Brand_Block_Adminhtml_Brand extends Mage_Adminhtml_Block_Template//Mage_Adminhtml_Block_Widget_Grid_Container

{

  public function __construct()

  {

    //$this->_controller = 'adminhtml_brand';

    //$this->_blockGroup = 'brand';

    //$this->_headerText = Mage::helper('brand')->__('Brands Pages Manager');

    //$this->_addButtonLabel = Mage::helper('brand')->__('Add Brand Page');



    //$this->_addButton('fillout', array(

    //        'label'     => Mage::helper('brand')->__('Fill Out Brands Pages'),

    //        'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/fillOut') .'\')',

    //        'class'     => '',

    //    ));

    parent::__construct();

  }

  

    protected function getStoreId()

    {

        return Mage::registry('store_id');

    }

  

    protected function _prepareLayout()

    {

        $this->setChild('add_new_button',

            $this->getLayout()->createBlock('adminhtml/widget_button')

                ->setData(array(

                    'label'     => Mage::helper('brand')->__('Add Brand Page'),

                    'onclick'   => "setLocation('".$this->getUrl('*/*/new', array('store' => $this->getStoreId()))."')",

                    'class'   => 'add'

                    ))

        );

        

        $this->setChild('fillout_button',

            $this->getLayout()->createBlock('adminhtml/widget_button')

                ->setData(array(

                    'label'     => Mage::helper('brand')->__('Fill Out Brands Pages'),

                    'onclick'   => "setLocation('".$this->getUrl('*/*/fillOut', array('store' => $this->getStoreId()))."')",

                    'class'   => ''

                    ))

        );

        $this->setChild('grid', $this->getLayout()->createBlock('brand/adminhtml_brand_grid', 'brand.grid'));

        return parent::_prepareLayout();

    }



    public function getAddNewButtonHtml()

    {

        return $this->getChildHtml('add_new_button');

    }



    public function getFillOutButtonHtml()

    {

        return $this->getChildHtml('fillout_button');

    }

    

    public function getGridHtml()

    {

        return $this->getChildHtml('grid');

    }



    public function isSingleStoreMode()

    {

        if (!Mage::app()->isSingleStoreMode()) {

               return false;

        }

        return true;

    }

}