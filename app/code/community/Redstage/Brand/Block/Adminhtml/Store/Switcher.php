<?php
class Redstage_Brand_Block_Adminhtml_Store_Switcher extends Mage_Adminhtml_Block_Store_Switcher //Mage_Adminhtml_Block_Template
{
    protected $_storeIds;

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('brand/store_switcher.phtml');
        $this->setUseConfirm(true);
        $this->setUseAjax(true);
        $this->setDefaultStoreName($this->__('All Store Views'));
    }
}