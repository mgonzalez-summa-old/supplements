<?php
class Redstage_Brand_Block_Adminhtml_Brand_Edit_Tab_Design extends Mage_Adminhtml_Block_Widget_Form

{

    protected function _prepareForm()

    {

        $form = new Varien_Data_Form();

        $this->setForm($form);

        $fieldset = $form->addFieldset('brand_display', array('legend'=>Mage::helper('brand')->__('Custom Design')));

        

        $layouts = array();

        foreach (Mage::getConfig()->getNode('global/brand/layouts')->children() as $layoutName=>$layoutConfig) {

            //if ('empty' == $layoutName) continue;

            $layouts[$layoutName] = (string)$layoutConfig->label;

        }

        $fieldset->addField('root_template', 'select', array(

            'name'      => 'root_template',

            'label'     => Mage::helper('brand')->__('Layout'),

            'required'  => true,

            'options'   => $layouts,

        ));

        

        $fieldset->addField('layout_update_xml', 'editor', array(

            'name'      => 'layout_update_xml',

            'label'     => Mage::helper('brand')->__('Layout Update XML'),

            'style'     => 'height:24em;'

        ));



     

      if ( Mage::getSingleton('adminhtml/session')->getBrandData() )

      {

          $form->setValues(Mage::getSingleton('adminhtml/session')->getBrandData());

          Mage::getSingleton('adminhtml/session')->setBrandData(null);

      } elseif ( Mage::registry('brand_data') ) {

          $form->setValues(Mage::registry('brand_data')->getData());

      }

      return parent::_prepareForm();

  }

}