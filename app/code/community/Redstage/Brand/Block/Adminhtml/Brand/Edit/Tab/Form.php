<?php
class Redstage_Brand_Block_Adminhtml_Brand_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('brand_form', array('legend'=>Mage::helper('brand')->__('Brand information')));
      $storeId = $this->getStoreId();
      
      $manufacturers = Mage::getModel('brand/brand');
      
	  if (Mage::registry('brand_data')->getId())
	  {
	      $fieldset->addField('manufacturer', 'text', array(
              'label'     => Mage::helper('brand')->__('Brand'),
              'required'  => false,
              'name'      => 'manufacturer',
              'style'     => 'width:500px;',
	          'readonly'  => true,
	          'disabled'  => true,
          ));
          $fieldset->addField('manufacturer_id', 'hidden', array(
              'name'      => 'manufacturer_id',
          ));
	  }
	  else
	  {
          $fieldset->addField('manufacturer_id', 'select', array(
              'label'     => Mage::helper('brand')->__('Brand'),
              'class'     => 'required-entry',
              'required'  => true,
              'name'      => 'manufacturer_id',
              'values'    => $manufacturers->toManufacturersOptionsArray($storeId),
          ));
	  }
      
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('brand')->__('Brand Page Title'),
          'required'  => false,
          'name'      => 'title',
          'style'     => 'width:500px;',
      ));
      
      /*if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('brand')->__('Store View'),
                'title'     => Mage::helper('brand')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            Mage::registry('brand_data')->setStoreId(Mage::app()->getStore(true)->getId());
        }*/
      if (!Mage::app()->isSingleStoreMode()) {
          $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => $storeId
            ));
            Mage::registry('brand_data')->setStoreId($storeId);
      }
      else {
          $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            Mage::registry('brand_data')->setStoreId(Mage::app()->getStore(true)->getId());
      }
      
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('brand')->__('Description'),
          'title'     => Mage::helper('brand')->__('Description'),
          'style'     => 'width:500px; height:200px;',
          'wysiwyg'   => false,
          'required'  => false,
      ));

      $fieldset->addField('small_logo', 'file', array(
          'label'     => Mage::helper('brand')->__('Small Logo'),
          'required'  => false,
          'name'      => 'small_logo',
		  'after_element_html' => (''!=Mage::registry('brand_data')->getData('small_logo')?'<p style="margin-top: 5px"><img src="'.Mage::getBaseUrl('media') . 'brand/logo/' . Mage::registry('brand_data')->getData('small_logo').'" width="60px" height="60px" /><br /><a href="'.$this->getUrl('*/*/*/', array('_current'=>true, 'delete'=>'small_logo')).'">'.Mage::helper('brand')->__('Delete Logo').'</a></p>':''),      	  
	  ));
	  
	  $fieldset->addField('small_logo_', 'hidden', array(
        'name'      => 'small_logo_',
      ));
      Mage::registry('brand_data')->setData('small_logo_', Mage::registry('brand_data')->getData('small_logo'));
	  
      $fieldset->addField('image', 'file', array(
          'label'     => Mage::helper('brand')->__('Image'),
          'required'  => false,
          'name'      => 'image',
		  'after_element_html' => (''!=Mage::registry('brand_data')->getData('image')?'<p style="margin-top: 5px"><img src="'.Mage::getBaseUrl('media') . 'brand/' . Mage::registry('brand_data')->getData('image').'" width="60px" height="60px" /><br /><a href="'.$this->getUrl('*/*/*/', array('_current'=>true, 'delete'=>'image')).'">'.Mage::helper('brand')->__('Delete Image').'</a></p>':''),      	  
	  ));
	  
	  $fieldset->addField('image_', 'hidden', array(
        'name'      => 'image_',
      ));
      Mage::registry('brand_data')->setData('image_', Mage::registry('brand_data')->getData('image'));
	  
      $fieldset->addField('meta_keywords', 'textarea', array(
          'name'      => 'meta_keywords',
          'label'     => Mage::helper('brand')->__('Meta Keywords'),
          'title'     => Mage::helper('brand')->__('Meta Keywords'),
          'style'     => 'width:500px; height:100px;',
          'required'  => false,
      ));
      
      $fieldset->addField('meta_description', 'textarea', array(
          'name'      => 'meta_description',
          'label'     => Mage::helper('brand')->__('Meta Description'),
          'title'     => Mage::helper('brand')->__('Meta Description'),
          'style'     => 'width:500px; height:100px;',
          'required'  => false,
      ));
		
      $fieldset->addField('url_key', 'text', array(
          'label'     => Mage::helper('brand')->__('URL key'),
          'required'  => false,
          'name'      => 'url_key',
          'after_element_html' => '<p class="nm"><small>' . Mage::helper('brand')->__('(eg: domain.com/<b>url-key</b>)') . '</small></p>',
      ));
      
      $fieldset->addField('featured', 'select', array(
          'label'     => Mage::helper('brand')->__('Featured'),
          'name'      => 'featured',
          'values'    => array(
              array(
                  'value'     => 0,
                  'label'     => Mage::helper('brand')->__('No'),
              ),

              array(
                  'value'     => 1,
                  'label'     => Mage::helper('brand')->__('Yes'),
              ),
          ),
      ));
      
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('brand')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('brand')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('brand')->__('Disabled'),
              ),
          ),
      ));
      
      $fieldset->addField('sort_order', 'text', array(
          'label'     => Mage::helper('brand')->__('Sort Order'),
          'required'  => false,
          'name'      => 'sort_order',
          'after_element_html' => '<p class="nm"><small>' . Mage::helper('brand')->__('for right sidebar block') . '</small></p>',
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getBrandData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getBrandData());
          Mage::getSingleton('adminhtml/session')->setBrandData(null);
      } elseif ( Mage::registry('brand_data') ) {
          $form->setValues(Mage::registry('brand_data')->getData());
      }
      return parent::_prepareForm();
  }
  
    protected function getStoreId()
    {
        return Mage::registry('store_id');
    }
}