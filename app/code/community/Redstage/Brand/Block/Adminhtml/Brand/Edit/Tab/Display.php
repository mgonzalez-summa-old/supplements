<?php
class Redstage_Brand_Block_Adminhtml_Brand_Edit_Tab_Display extends Mage_Adminhtml_Block_Widget_Form

{

  protected function _prepareForm()

  {

      $form = new Varien_Data_Form();

      $this->setForm($form);

      $fieldset = $form->addFieldset('brand_display', array('legend'=>Mage::helper('brand')->__('Display Settings')));

     

      $fieldset->addField('available_sort_by', 'multiselect', array(

                'name'      => 'available_sort_by[]',

                'label'     => Mage::helper('brand')->__('Available Product Listing Sort By'),

                'title'     => Mage::helper('brand')->__('Available Product Listing Sort By'),

                'required'  => true,

                'values'    => array(),

            ));



      if ( Mage::getSingleton('adminhtml/session')->getBrandData() )

      {

          $form->setValues(Mage::getSingleton('adminhtml/session')->getBrandData());

          Mage::getSingleton('adminhtml/session')->setBrandData(null);

      } elseif ( Mage::registry('brand_data') ) {

          $form->setValues(Mage::registry('brand_data')->getData());

      }

      return parent::_prepareForm();

  }

}