<?php

$installer = $this;



$installer->startSetup();



$installer->run("



ALTER TABLE {$this->getTable('brand_stores')} DROP FOREIGN KEY `FK_BRAND_STORES_MANUFACTURER`;



ALTER TABLE {$this->getTable('brand_stores')} CHANGE COLUMN `manufacturers_id` `id` INTEGER(11) UNSIGNED NOT NULL;



ALTER TABLE {$this->getTable('brand_stores')} ADD CONSTRAINT `FK_BRAND_STORES_MANUFACTURER` FOREIGN KEY (`id`)

	REFERENCES {$this->getTable('brand')} (`id`) ON DELETE CASCADE ON UPDATE CASCADE;



ALTER TABLE {$this->getTable('brand_stores')} ADD COLUMN `manufacturer_id` INTEGER(11) UNSIGNED NOT NULL AFTER `id`;



ALTER TABLE {$this->getTable('brand')} ADD COLUMN `small_logo` VARCHAR(255) NOT NULL AFTER `title`;



ALTER TABLE {$this->getTable('brand')} ADD COLUMN `featured` SMALLINT(2) UNSIGNED NOT NULL DEFAULT '0' AFTER `layout_update_xml`;



UPDATE  {$this->getTable('brand_stores')},  {$this->getTable('brand')} SET {$this->getTable('brand_stores')}.`manufacturer_id` = {$this->getTable('brand')}.`manufacturer_id`

    WHERE {$this->getTable('brand_stores')}.id = {$this->getTable('brand')}.`id`;

");



$installer->endSetup();