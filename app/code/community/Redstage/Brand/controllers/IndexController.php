<?php
class Redstage_Brand_IndexController extends Mage_Core_Controller_Front_Action

{

    public function indexAction()

    {

        $this->loadLayout(array('default', 'brand_index_list'));     

        $this->renderLayout();

    }

    

    public function viewAction(){

        $id = $this->getRequest()->getParam('id');

        if (!Mage::helper('brand/manufacturer')->renderPage($this, $id)) {

            $this->_forward('defaultIndex', 'index', 'cms');

        }

    }



}