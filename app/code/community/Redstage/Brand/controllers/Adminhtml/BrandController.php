<?php
class Redstage_Brand_Adminhtml_BrandController extends Mage_Adminhtml_Controller_action

{



    protected function _init()

    {

        if (!is_null($this->getRequest()->getParam('store'))){

	        Mage::getSingleton('adminhtml/session')->setData('brand_store', $this->_getStoreId());

	    }

	    elseif (!is_null(Mage::getSingleton('adminhtml/session')->getData('brand_store'))){

	        $this->getRequest()->setParam('store', Mage::getSingleton('adminhtml/session')->getData('brand_store'));

	    }

	    Mage::register('store_id', $this->_getStoreId());

    }

    

    protected function _initAction() {

		$this->loadLayout()

			->_setActiveMenu('catalog/brand')

			->_addBreadcrumb(Mage::helper('adminhtml')->__('Brands Pages Manager'), Mage::helper('adminhtml')->__('Brands Pages Manager'));

		

		return $this;

	}   

 

	public function indexAction() {

	    $this->_init();

		$this->_initAction()

			->renderLayout();

	}

	

	protected function _getStoreId()

	{

	    if (Mage::app()->isSingleStoreMode()) {

	        return Mage::app()->getStore(true)->getId();

	    }

	    return Mage::app()->getStore((int) $this->getRequest()->getParam('store', 0))->getId();

	}

	

	protected function _redirect($path, $arguments = array())

	{

	    $arguments['store'] = $this->_getStoreId();

	    parent::_redirect($path, $arguments);

	}

	

	public function fillOutAction(){

	    $this->_init();

	    $model  = Mage::getModel('brand/brand');

	    $storeId = $this->_getStoreId();

	    try {

    	    $model->fillOut($storeId);

    	    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('brand')->__('Brands Pages were successfully filled out'));

	    } catch (Exception $e) {

			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('brand')->__('There was an error during the process'));

			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());

		}

    	$this->_redirect('*/*/', array('store' => $storeId));

	}



	public function editAction() {

	    $this->_init();

		$id     = $this->getRequest()->getParam('id');

		$model  = Mage::getModel('brand/brand')->load($id);

		

		if ($delete = $this->getRequest()->getParam('delete')){

		    switch ($delete){

		        case 'small_logo':

		        case 'image':

		            $params = $this->getRequest()->getParams();

		            unset($params['delete']);

		            if ($model->getFeatured() && 'image' == $delete){

		                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('brand')->__('Image must be uploaded for Featured Brand'));

                        Mage::getSingleton('adminhtml/session')->setFormData($model->getData());

                        $this->_redirect('*/*/*/', $params);

                        return;

		            }

		            $filename = $model->getData($delete);

		            $path = Mage::getBaseDir('media') . DS . 'brand' . DS . ($delete == 'small_logo'?'logo'.DS:'');

		            if (file_exists($path.$filename)){

		                unlink($path.$filename);

		            }

		            $model->setData($delete, '');

		            $model->save();



		            $this->_redirect('*/*/*/', $params);

		            return;

		    }

		}



		if ($model->getId() || $id == 0) {

			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);

			if (!empty($data)) {

				$model->setData($data);

			}



			Mage::register('brand_data', $model);



			$this->loadLayout();

			$this->_setActiveMenu('catalog/brand');



			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Brands Pages Manager'), Mage::helper('adminhtml')->__('Brands Pages Manager'));

			//$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));



			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);



			$this->_addContent($this->getLayout()->createBlock('brand/adminhtml_brand_edit'))

				->_addLeft($this->getLayout()->createBlock('brand/adminhtml_brand_edit_tabs'));



			$this->renderLayout();

		} else {

			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('brand')->__('Brand does not exist'));

			$this->_redirect('*/*/');

		}

	}

 

	public function newAction() {

		$this->_forward('edit');

	}

 

	public function saveAction() {

	    $this->_init();

		if ($data = $this->getRequest()->getPost()) {

		    $model = Mage::getModel('brand/brand');

		    $manufacturer = $model->getManufacturerName($data['manufacturer_id']);

		    if (empty($data['title'])){

                $data['title'] = $manufacturer;

            }

            if (!empty($data['url_key'])){

                $urlKey = Mage::helper('brand')->toUrlKey($data['url_key']);

            }

            else {

                $urlKey = Mage::helper('brand')->toUrlKey($manufacturer);

            }

            $data['url_key'] = $urlKey;

            

			if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {

				try {	

					/* Starting upload */	

					$uploader = new Varien_File_Uploader('image');

					

					// Any extention would work

	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));

					$uploader->setAllowRenameFiles(false);

					

					// Set the file upload mode 

					// false -> get the file directly in the specified folder

					// true -> get the file in the product like folders 

					//	(file.jpg will go in something like /media/f/i/file.jpg)

					$uploader->setFilesDispersion(false);

							

					// We set media as the upload dir

					$path = Mage::getBaseDir('media') . DS . 'brand' . DS;

					//$uploader->save($path, $_FILES['image']['name'] );

					$imagename = md5($_FILES['image']['name'].time()) . '.' . substr(strrchr($_FILES['image']['name'], '.'), 1);

					//$uploader->resize(200);

					//$uploader->convert();

					//print_r($path.$filename);exit;

					$uploader->save($path, $imagename);

					/*$image = new Varien_Image($path . $filename);

					$image->resize(100);

					$image->keepFrame(true);

					$image->save();*/

					/*$processor = new Varien_Image($path.$newimage);

                    $processor->keepAspectRatio(true);

                    $processor->resize(200);

                    $processor->save();exit;*/

					

				} catch (Exception $e) {

				    

		        }

	        

		        if (isset($imagename)){

    		        //this way the name is saved in DB

    	  			$data['image'] = $imagename;

		        }

			}

			

		    if (isset($_FILES['small_logo']['name']) && $_FILES['small_logo']['name'] != '') {

				try {	

					$uploader = new Varien_File_Uploader('small_logo');

	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));

					$uploader->setAllowRenameFiles(false);

					$uploader->setFilesDispersion(false);

					$path = Mage::getBaseDir('media') . DS . 'brand' . DS . 'logo' . DS;

					$logoName = md5($_FILES['small_logo']['name'].time()) . '.' . substr(strrchr($_FILES['small_logo']['name'], '.'), 1);

					$uploader->save($path, $logoName);

					

				} catch (Exception $e) {

				    

		        }

	        

		        if (isset($logoName)){

    		        //this way the name is saved in DB

    	  			$data['small_logo'] = $logoName;

		        }

			}

	  			

			

			$model->setData($data)

				->setId($this->getRequest()->getParam('id'));

				

			try {

				$model->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('brand')->__('Brand Page was successfully saved'));

				Mage::getSingleton('adminhtml/session')->setFormData(false);



				if ($this->getRequest()->getParam('back')) {

					$this->_redirect('*/*/edit', array('id' => $model->getId()));

					return;

				}

				$this->_redirect('*/*/');

				return;

            } catch (Exception $e) {

                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());

                Mage::getSingleton('adminhtml/session')->setFormData($data);

                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));

                return;

            }

        }

        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('brand')->__('Unable to find Brand Page to save'));

        $this->_redirect('*/*/');

	}

 

	public function deleteAction() {

	    $this->_init();

		if( $this->getRequest()->getParam('id') > 0 ) {

			try {

				$model = Mage::getModel('brand/brand');

				 

				$model->setId($this->getRequest()->getParam('id'))

					->delete();

					 

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Brand Page was successfully deleted'));

				$this->_redirect('*/*/');

			} catch (Exception $e) {

				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());

				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));

			}

		}

		$this->_redirect('*/*/');

	}



    public function massDeleteAction() {

        $this->_init();

        $brandIds = $this->getRequest()->getParam('brand');

        if(!is_array($brandIds)) {

			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select Brands Page(s)'));

        } else {

            try {

                foreach ($brandIds as $brandId) {

                    $brand = Mage::getModel('brand/brand')->load($brandId);

                    $brand->delete();

                }

                Mage::getSingleton('adminhtml/session')->addSuccess(

                    Mage::helper('adminhtml')->__(

                        'Total of %d record(s) were successfully deleted', count($brandIds)

                    )

                );

            } catch (Exception $e) {

                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());

            }

        }

        $this->_redirect('*/*/index');

    }

	

    public function massStatusAction()

    {

        $this->_init();

        $brandIds = $this->getRequest()->getParam('brand');

        if(!is_array($brandIds)) {

            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select Brands Page(s)'));

        } else {

            try {

                foreach ($brandIds as $brandId) {

                    $brand = Mage::getSingleton('brand/brand')

                        ->load($brandId)

                        ->setStatus($this->getRequest()->getParam('status'))

                        ->setIsMassupdate(true)

                        ->save();

                }

                $this->_getSession()->addSuccess(

                    $this->__('Total of %d record(s) were successfully updated', count($brandIds))

                );

            } catch (Exception $e) {

                $this->_getSession()->addError($e->getMessage());

            }

        }

        $this->_redirect('*/*/index');

    }

  

    /*public function exportCsvAction()

    {

        $fileName   = 'brand.csv';

        $content    = $this->getLayout()->createBlock('brand/adminhtml_brand_grid')

            ->getCsv();



        $this->_sendUploadResponse($fileName, $content);

    }



    public function exportXmlAction()

    {

        $fileName   = 'brand.xml';

        $content    = $this->getLayout()->createBlock('brand/adminhtml_brand_grid')

            ->getXml();



        $this->_sendUploadResponse($fileName, $content);

    }*/



    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')

    {

        $response = $this->getResponse();

        $response->setHeader('HTTP/1.1 200 OK','');

        $response->setHeader('Pragma', 'public', true);

        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);

        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);

        $response->setHeader('Last-Modified', date('r'));

        $response->setHeader('Accept-Ranges', 'bytes');

        $response->setHeader('Content-Length', strlen($content));

        $response->setHeader('Content-type', $contentType);

        $response->setBody($content);

        $response->sendResponse();

        die;

    }

}