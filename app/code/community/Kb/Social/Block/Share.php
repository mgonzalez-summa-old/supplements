<!--
 * @package     Kb_Social
 * @author      Kris Brown <extensions@kab8609.com>
 * @license     GPL | Open Source
-->
<?php

class Kb_Social_Block_Share extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('kb/social/share.phtml');
    }
}
