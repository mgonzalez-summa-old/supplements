<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_AdvancedStock_Block_MassStockEditor_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('MassStockEditorGrid');
        $this->_parentTemplate = $this->getTemplate();
        $this->setEmptyText(Mage::helper('AdvancedStock')->__('No Items'));
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * 
     *
     * @return unknown
     */
    protected function _prepareCollection() {

        $collection = Mage::getModel('cataloginventory/stock_item')
                ->getCollection()
                ->join('catalog/product', 'product_id=`catalog/product`.entity_id')
                ->addFieldToFilter('`catalog/product`.type_id', array('in' => array('simple', 'virtual')))
                ->join('AdvancedStock/CatalogProductVarchar', '`catalog/product`.entity_id=`AdvancedStock/CatalogProductVarchar`.entity_id and `AdvancedStock/CatalogProductVarchar`.store_id = 0 and `AdvancedStock/CatalogProductVarchar`.attribute_id = ' . mage::getModel('AdvancedStock/Constant')->GetProductNameAttributeId())
        ;
        
        // Additional catalog_product attributes
        $collection->getSelect()->joinLeft(array('catalog_product_index_eav_brand' => 'catalog_product_index_eav'), '`catalog_product_index_eav_brand`.`entity_id` = `main_table`.`product_id` AND `catalog_product_index_eav_brand`.`attribute_id` IN (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "brand")', array('attribute_id'));
        $collection->getSelect()->joinLeft(array('eav_attribute_option_value_brand' => 'eav_attribute_option_value'), '`eav_attribute_option_value_brand`.`option_id` = `catalog_product_index_eav_brand`.`value`', array('brand' => 'value'));
        $collection->getSelect()->joinLeft(array('catalog_product_entity_varchar_size' => 'catalog_product_entity_varchar'), '`catalog_product_entity_varchar_size`.`attribute_id` = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "size") AND `catalog_product_entity_varchar_size`.`entity_id` = `catalog/product`.`entity_id`', array('size' => 'value'));
        $collection->getSelect()->joinLeft(array('catalog_product_index_eav_flavor' => 'catalog_product_index_eav'), '`catalog_product_index_eav_flavor`.`entity_id` = `main_table`.`product_id` AND `catalog_product_index_eav_flavor`.`attribute_id` IN (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "flavor")', array('attribute_id'));
        $collection->getSelect()->joinLeft(array('eav_attribute_option_value_flavor' => 'eav_attribute_option_value'), '`eav_attribute_option_value_flavor`.`option_id` = `catalog_product_index_eav_flavor`.`value`', array('flavor' => 'value'));
        $collection->getSelect()->joinLeft(array('catalog_product_entity_varchar_barcode' => 'catalog_product_entity_varchar'), '`catalog_product_entity_varchar_barcode`.`attribute_id` = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "barcode") AND `catalog_product_entity_varchar_barcode`.`entity_id` = `catalog/product`.`entity_id`', array('barcode' => 'value'));
        $collection->getSelect()->joinLeft(array('catalog_product_entity_varchar_promo_description' => 'catalog_product_entity_varchar'), '`catalog_product_entity_varchar_promo_description`.`attribute_id` = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "promo_description") AND `catalog_product_entity_varchar_promo_description`.`entity_id` = `catalog/product`.`entity_id`', array('promo_description' => 'value'));
        
         //zend_debug::dump($collection);
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * D�fini les colonnes du grid
     *
     * @return unknown
     */
    protected function _prepareColumns() {
        /*
        $this->addColumn('manufacturer', array(
            'header' => Mage::helper('AdvancedStock')->__('Manufacturer'),
            'index' => 'product_id',
            'type' => 'options',
            'renderer' => 'MDN_AdvancedStock_Block_MassStockEditor_Widget_Grid_Column_Renderer_Manufacturer',
            'filter' => 'MDN_AdvancedStock_Block_MassStockEditor_Widget_Grid_Column_Filter_Manufacturer'
        ));
        */

        $this->addColumn('sku', array(
            'header' => Mage::helper('AdvancedStock')->__('SKU'),
            'index' => 'sku',
            'filter_index' => '`catalog/product`.sku'
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('AdvancedStock')->__('Name'),
            'index' => 'value',
            'filter_index' => '`AdvancedStock/CatalogProductVarchar`.value'
        ));

        $this->addColumn('brand', array(
            'header' => Mage::helper('AdvancedStock')->__('Brand'),
            'index' => 'brand',
            'filter_index' => '`eav_attribute_option_value_brand`.`value`',
        ));

        $this->addColumn('flavor', array(
            'header' => Mage::helper('AdvancedStock')->__('Flavor'),
            'index' => 'flavor',
            'filter_index' => '`eav_attribute_option_value_flavor`.`value`',
        ));

        $this->addColumn('size', array(
            'header' => Mage::helper('AdvancedStock')->__('Size'),
            'index' => 'size',
            'filter_index' => '`eav_attribute_option_value_size`.`value`',
        ));

        $this->addColumn('barcode', array(
            'header' => Mage::helper('AdvancedStock')->__('Barcode'),
            'index' => 'barcode',
            'filter_index' => '`eav_attribute_option_value_barcode`.`value`',
        ));

        $this->addColumn('promo_description', array(
            'header' => Mage::helper('AdvancedStock')->__('Promo Desc.'),
            'index' => 'promo_description',
            'filter_index' => '`eav_attribute_option_value_promo_description`.`value`',
        ));

        Mage::helper('AdvancedStock/Product_ConfigurableAttributes')->addConfigurableAttributesColumn($this, 'product_id');


        $this->addColumn('stock_id', array(
            'header' => Mage::helper('AdvancedStock')->__('Warehouse'),
            'width' => '80',
            'index' => 'stock_id',
            'type' => 'options',
            'options' => Mage::getSingleton('AdvancedStock/System_Config_Source_Warehouse')->getListForFilter(),
        ));

        $this->addColumn('stock', array(
            'header' => Mage::helper('AdvancedStock')->__('Stock'),
            'index' => 'stock',
            'filter_index' => 'qty',
            'renderer' => 'MDN_AdvancedStock_Block_MassStockEditor_Widget_Grid_Column_Renderer_Stock',
            'align' => 'center',
            'type' => 'number'
        ));

        $this->addColumn('warning_stock_level', array(
            'header' => Mage::helper('AdvancedStock')->__('Warning stock level'),
            'filter' => false,
            'sortable' => false,
            'renderer' => 'MDN_AdvancedStock_Block_MassStockEditor_Widget_Grid_Column_Renderer_WarningStockLevel',
            'align' => 'center'
        ));

        $this->addColumn('ideal_stock_level', array(
            'header' => Mage::helper('AdvancedStock')->__('Ideal stock level'),
            'filter' => false,
            'sortable' => false,
            'renderer' => 'MDN_AdvancedStock_Block_MassStockEditor_Widget_Grid_Column_Renderer_IdealStockLevel',
            'align' => 'center'
        ));

        $this->addColumn('shelf_location', array(
            'header' => Mage::helper('AdvancedStock')->__('Location'),
            'index' => 'shelf_location',
            'renderer' => 'MDN_AdvancedStock_Block_MassStockEditor_Widget_Grid_Column_Renderer_StockLocation',
            'align' => 'center'
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('AdvancedStock')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getproduct_id',
            'actions' => array(
                array(
                    'caption' => Mage::helper('AdvancedStock')->__('View'),
                    'url' => array('base' => 'AdvancedStock/Products/Edit'),
                    'field' => 'product_id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));

        //raise event to allow other modules to add columns
        Mage::dispatchEvent('advancedstock_mass_stock_editor_grid_preparecolumns', array('grid' => $this));

        return parent::_prepareColumns();
    }

    public function getGridParentHtml() {
        $templateName = Mage::getDesign()->getTemplateFilename($this->_parentTemplate, array('_relative' => true));
        return $this->fetchView($templateName);
    }

    public function getGridUrl() {
        return $this->getUrl('AdvancedStock/Misc/MassStockEditorAjax', array('_current' => true));
    }

}
