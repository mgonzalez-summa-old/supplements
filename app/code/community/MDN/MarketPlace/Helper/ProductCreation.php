<?php

/**
 * Main product creation class
 */
class MDN_MarketPlace_Helper_ProductCreation extends MDN_MarketPlace_Helper_Feed {

    const kStatusCreated = "created";
    const kStatusPending = "pending";
    const kStatusNotCreated = "notCreated";
    const kStatusInError = "error";
    const kStatusIncomplete = 'incomplete';
    

    /**
     * Return status list
     *
     * @return array
     */
    public function getStatus(){

        $retour = array(
            self::kStatusCreated => self::kStatusCreated,
            self::kStatusNotCreated => self::kStatusNotCreated,
            self::kStatusPending => self::kStatusPending,
            self::kStatusInError =>self::kStatusInError,
            self::kStatusIncomplete => self::kStatusIncomplete
        );

        return $retour;

    }

    /**
     * Get export path name
     *
     * @return string
     *
     * @see getMarketPlaceName
     */
    public function getExportPath($marketplaceName){
        return Mage::app()->getConfig()->getTempVarDir().'/export/marketplace/'.$marketplaceName.'/';
    }

    /**
     * Getter status incomplete
     *
     * @return string
     */
    public function getStatusIncomplete(){
        return self::kStatusIncomplete;
    }

    /**
     * Getter status not created
     *
     * @return string
     */
    public function getStatusNotCreated(){
        return self::kStatusNotCreated;
    }

    /**
     * Getter status created
     *
     * @return string
     */
    public function getStatusCreated(){
        return self::kStatusCreated;
    }

    /**
     * Getter status pending
     *
     * @return string
     */
    public function getStatusPending(){
        return self::kStatusPending;
    }

    /**
     * Getter status in error
     *
     * @return string
     */
    public function getStatusInError(){
        return self::kStatusInError;
    }

    /**
     * Update product status
     *
     * @param request $request
     */
    public function updateStatus($request){

        $ids = $request->getPost('product_ids');
        $marketplace = ucfirst($request->getParam('mp'));
        $status = ($request->getParam('status') == $this->getStatusNotCreated()) ? new Zend_Db_Expr('null') : $request->getParam('status');

        Mage::register('country', $request->getParam('country'));
        Mage::getModel('MarketPlace/Data')->updateStatus($ids, $marketplace, $status);

    }

    /**
     * Is marketplace allow product feed generation ?
     *
     * @return boolean
     */
    public function allowGenerateProductFeed(){
        return false;
    }

    /**
     * Allow matching EAN
     *
     * @return boolean
     */
    public function allowMatchingEan(){
        return false;
    }

}
