<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN & Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_MarketPlace_Helper_Barcode extends Mage_Core_Helper_Abstract {

    /**
     * retrieve product barcode
     *
     * @param product $product
     * @return string
     */
    public function getBarcodeForProduct($product) {
        $barcodeAttributeCode = mage::getStoreConfig('marketplace/general/ean_attribute_name');
        if ($barcodeAttributeCode == '')
            return '';
        else
            return $product->getData($barcodeAttributeCode);
    }

    /**
     * Add barcode to collection
     * 
     * @param collection $collection
     * @return collection 
     */
    public function addBarcodeAttributeToProductCollection($collection) {
        $barcodeAttributeCode = mage::getStoreConfig('marketplace/general/ean_attribute_name');
        if ($barcodeAttributeCode)
            $collection->addAttributeToSelect($barcodeAttributeCode);

        return $collection;
    }

    /**
     * Retrieve barcode attribute name
     * 
     * @return string 
     */
    public function getCollectionBarcodeIndex() {

        return mage::getStoreConfig('marketplace/general/ean_attribute_name');
    }

}
