<?php

/**
 * Get and set param values
 */
class MDN_MarketPlace_Helper_Parameters extends Mage_Core_Helper_Abstract {

    /**
     * Getter
     *
     * @param string $path
     * @return string
     */
    public function getParamValue($path){
        return mage::getStoreConfig($path);
    }

    /**
     * Setter
     *
     * @param string $path
     * @param string $value
     */
    public function setParamValue($path, $value){
        $obj = new Mage_Core_Model_Config();
        $obj->saveConfig($path, $value);
    }

}
