<?php

class MDN_MarketPlace_Helper_Shippingcost extends Mage_Core_Helper_Abstract {

    const kDelayTnt = '1';
    const kDelayColissimo = '2';

    /**
     * Calculate shipping cost
     *
     * @param product $product
     * @param boolean $addTax
     * @return float
     */
    public function calculateShippingCost($product, $addTax) {

        $country = "FR";
        $retour = false;

        $storeId = mage::getStoreConfig('marketplace/general/default_store_id');
        $websiteId = Mage::app()->getStore()->getwebsite_id();
        $carrierCode = mage::getStoreConfig('marketplace/general/default_shipment_method');

        $code = explode("_", $carrierCode);

        $carrierObject = $this->getCarrierFromCode($code[0]);

        if ($carrierObject) {

            try {
                $request = Mage::getModel('shipping/rate_request');

                //force stock item load (ressource consumer but no choice regarding the way magento implements cost calculation)
                $product->setStockItem(mage::getModel('cataloginventory/stock_item')->loadByProduct($product));


                //set request item (build a quote)
                $quote = mage::getModel('sales/quote');
                $quoteItem = mage::getModel('sales/quote_item');
                $quoteItem->setQuote($quote);
                $quoteItem->setProduct($product);
                $quoteItem->setWeight($product->getweight());
                $quoteItem->setQty(1);
                $quote->addItem($quoteItem);
                $request->setAllItems($quote->getAllItems());

                //target address to calculate rates
                $request->setDestCountryId($country);
                $request->setPackageWeight($product->getweight());
                $request->setPackageQty(1);
                $request->setStoreId($storeId);
                $request->setWebsiteId($websiteId);
                $rates = $carrierObject->collectRates($request);
                $cheaperRate = $rates->getCheapestRate();

                if ($cheaperRate !== null && !$cheaperRate->getError()) {
                    $retour = $cheaperRate->getPrice();
                    if ($addTax === true) {
                        $tax = mage::getStoreConfig('marketplace/general/tax');
                        $retour = $retour * ( 1 + $tax / 100);
                    }
                }
            } catch (Exception $ex) {
                throw new Exception('Unable to collect shipping cost for product ' . $product->getName() . ':' . $ex->getMessage());
            }
        }
        else
            throw new Exception('Unable to find carrier with code = ' . $code[0], 17);

        return $retour;
    }

    /**
     *  Return carrier model from carrier code
     *
     * @param string
     * @return Model
     *
     */
    public function getCarrierFromCode($CarrierCode) {

        $config = Mage::getStoreConfig('carriers');
        foreach ($config as $code => $methodConfig) {
            if (Mage::getStoreConfigFlag('carriers/' . $code . '/active')) {

                if ($code == $CarrierCode) {

                    if (isset($methodConfig['model'])) {
                        $modelName = $methodConfig['model'];
                        $Model = Mage::getModel($modelName);
                        return $Model;
                    }
                }
            }
        }
    }

    public function getShippingCostByWeightRange($method, $weight, $ttc = true) {

        $shipping_cost = 0;

        switch (strtolower($method)) {

            case 'tnt':
                if ($weight < 2) {
                    $shipping_cost = ($ttc === true) ? 16.6 : 13.88;
                } elseif (2 <= $weight && $weight < 5) {
                    $shipping_cost = ($ttc === true) ? 18.8 : 15.72;
                } elseif (5 <= $weight && $weight < 10) {
                    $shipping_cost = ($ttc === true) ? 21.6 : 18.06;
                } elseif (10 <= $weight && $weight < 18) {
                    $shipping_cost = ($ttc === true) ? 26 : 21.74;
                } elseif (18 <= $weight && $weight < 30) {
                    $shipping_cost = ($ttc === true) ? 34.6 : 28.93;
                } elseif (30 <= $weight && $weight < 40) {
                    $shipping_cost = ($ttc === true) ? 41.99 : 35.11;
                } elseif (40 <= $weight && $weight < 50) {
                    $shipping_cost = ($ttc === true) ? 62 : 51.84;
                } elseif (50 <= $weight && $weight < 60) {
                    $shipping_cost = ($ttc === true) ? 80.01 : 66.90;
                } elseif (60 <= $weight && $weight < 80) {
                    $shipping_cost = ($ttc === true) ? 90 : 75.25;
                } elseif (80 <= $weight && $weight < 100) {
                    $shipping_cost = ($ttc === true) ? 104 : 86.96;
                } elseif (100 <= $weight && $weight < 130) {
                    $shipping_cost = ($ttc === true) ? 128 : 107.02;
                } else {
                    $shipping_cost = null;
}
                break;
            case 'colissimo':
                if ($weight < 0.25) {
                    $shipping_cost = ($ttc === true) ? 5.25 : 4.3896;
                } elseif (0.25 <= $weight && $weight < 0.5) {
                    $shipping_cost = ($ttc === true) ? 5.95 : 4.9749;
                } elseif (0.5 <= $weight && $weight < 0.75) {
                    $shipping_cost = ($ttc === true) ? 6.65 : 5.5602;
                } elseif (0.75 <= $weight && $weight < 1) {
                    $shipping_cost = ($ttc === true) ? 6.9 : 5.7692;
                } elseif (1 <= $weight && $weight < 2) {
                    $shipping_cost = ($ttc === true) ? 7.7 : 6.4381;
                } elseif (2 <= $weight && $weight < 3) {
                    $shipping_cost = ($ttc === true) ? 8.4 : 7.0234;
                } elseif (3 <= $weight && $weight < 4) {
                    $shipping_cost = ($ttc === true) ? 9.1 : 7.6087;
                } elseif (4 <= $weight && $weight < 5) {
                    $shipping_cost = ($ttc === true) ? 9.8 : 8.194;
                } elseif (5 <= $weight && $weight < 6) {
                    $shipping_cost = ($ttc === true) ? 10.5 : 8.7793;
                } elseif (6 <= $weight && $weight < 7) {
                    $shipping_cost = ($ttc === true) ? 11.2 : 9.3645;
                } elseif (7 <= $weight && $weight < 8) {
                    $shipping_cost = ($ttc === true) ? 11.9 : 9.9498;
                } elseif (8 <= $weight && $weight < 9) {
                    $shipping_cost = ($ttc === true) ? 12.6 : 10.5351;
                } elseif (9 <= $weight && $weight < 10) {
                    $shipping_cost = ($ttc === true) ? 13.4 : 11.204;
                } elseif (10 <= $weight && $weight < 15) {
                    $shipping_cost = ($ttc === true) ? 15.25 : 12.7508;
                } elseif (15 <= $weight && $weight < 30) {
                    $shipping_cost = ($ttc === true) ? 20.09 : 17.4749;
                } else {
                    $shipping_cost = null;
                }
                break;
        }

        return $shipping_cost;
    }

    public function getAvailableWeightRange($delivery_value, $ttc = true) {

        $retour = array();

        switch (strtolower($delivery_value)) {

            case 'tnt':

                $retour = array(
                    array(
                        'id' => 100,
                        'cost' => 16.6,
                        'delay' => self::kDelayTnt
                    ),
                    array(
                        'id' => 101,
                        'cost' => 18.8,
                        'delay' => self::kDelayTnt
                    ),
                    array(
                        'id' => 102,
                        'cost' => 21.6,
                        'delay' => self::kDelayTnt
                    ),
                    array(
                        'id' => 103,
                        'cost' => 26,
                        'delay' => self::kDelayTnt
                    ),
                    array(
                        'id' => 104,
                        'cost' => 34.6,
                        'delay' => self::kDelayTnt
                    ),
                    array(
                        'id' => 105,
                        'cost' => 41.99,
                        'delay' => self::kDelayTnt
                    ),
                    array(
                        'id' => 106,
                        'cost' => 62,
                        'delay' => self::kDelayTnt
                    ),
                    array(
                        'id' => 107,
                        'cost' => 80.01,
                        'delay' => self::kDelayTnt
                    ),
                    array(
                        'id' => 108,
                        'cost' => 90,
                        'delay' => self::kDelayTnt
                    ),
                    array(
                        'id' => 109,
                        'cost' => 104,
                        'delay' => self::kDelayTnt
                    ),
                    array(
                        'id' => 110,
                        'cost' => 128,
                        'delay' => self::kDelayTnt
                    )
                );
                break;
            case 'colissimo':
                $retour = array(
                    array(
                        'id' => 200,
                        'cost' => 5.25,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 201,
                        'cost' => 5.95,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 202,
                        'cost' => 6.65,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 203,
                        'cost' => 6.9,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 204,
                        'cost' => 7.7,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 205,
                        'cost' => 8.4,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 206,
                        'cost' => 9.1,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 207,
                        'cost' => 9.8,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 208,
                        'cost' => 10.5,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 209,
                        'cost' => 11.2,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 210,
                        'cost' => 11.9,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 211,
                        'cost' => 12.6,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 212,
                        'cost' => 13.4,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 213,
                        'cost' => 15.25,
                        'delay' => self::kDelayColissimo
                    ),
                    array(
                        'id' => 214,
                        'cost' => 20.9,
                        'delay' => self::kDelayColissimo
                    )
                );
                break;
        }

        return $retour;
    }

    public function getDeliveryIdByWeightRange($method, $weight) {

        $tmp = explode("_",$method);
        $method = $tmp[0];

        $id = 0;

        switch (strtolower($method)) {

            case 'tnt':
                if ($weight < 2) {
                    $id = 100;
                } elseif (2 <= $weight && $weight < 5) {
                    $id = 101;
                } elseif (5 <= $weight && $weight < 10) {
                    $id = 102;
                } elseif (10 <= $weight && $weight < 18) {
                    $id = 103;
                } elseif (18 <= $weight && $weight < 30) {
                    $id = 104;
                } elseif (30 <= $weight && $weight < 40) {
                    $id = 105;
                } elseif (40 <= $weight && $weight < 50) {
                    $id = 106;
                } elseif (50 <= $weight && $weight < 60) {
                    $id = 107;
                } elseif (60 <= $weight && $weight < 80) {
                    $id = 108;
                } elseif (80 <= $weight && $weight < 100) {
                    $id = 109;
                } elseif (100 <= $weight && $weight < 130) {
                    $id = 110;
                } else {
                    $id = null;
                }
                break;
            case 'colissimo':
                if ($weight < 0.25) {
                    $id = 200;
                } elseif (0.25 <= $weight && $weight < 0.5) {
                    $id = 201;
                } elseif (0.5 <= $weight && $weight < 0.75) {
                    $id = 202;
                } elseif (0.75 <= $weight && $weight < 1) {
                    $id = 203;
                } elseif (1 <= $weight && $weight < 2) {
                    $id = 204;
                } elseif (2 <= $weight && $weight < 3) {
                    $id = 205;
                } elseif (3 <= $weight && $weight < 4) {
                    $id = 206;
                } elseif (4 <= $weight && $weight < 5) {
                    $id = 207;
                } elseif (5 <= $weight && $weight < 6) {
                    $id = 208;
                } elseif (6 <= $weight && $weight < 7) {
                    $id = 209;
                } elseif (7 <= $weight && $weight < 8) {
                    $id = 210;
                } elseif (8 <= $weight && $weight < 9) {
                    $id = 211;
                } elseif (9 <= $weight && $weight < 10) {
                    $id = 212;
                } elseif (10 <= $weight && $weight < 15) {
                    $id = 213;
                } elseif (15 <= $weight && $weight < 30) {
                    $id = 214;
                } else {
                    $id = null;
                }
                break;
        }

        return $id;
    }

}
