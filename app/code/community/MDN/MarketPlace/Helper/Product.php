<?php

/**
 * Product methods
 */
class MDN_MarketPlace_Helper_Product extends Mage_Core_Helper_Abstract {

    /**
     * Get product stock availibility
     *
     * @param product $product
     * @return integer
     */
    public function getStockToExport($product, $mp = null) {

        if($product->getstatus() == 2){
            $stock = 0;
        }
        else{
            // TODO : find another method (trop lourd)
            $p = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            $stock = $p->getQty();
            //$stock = $product->getStockItem()->getqty();
            // check force quantity
            if ($product->getmp_force_qty() != '') {
                $stock = $product->getmp_force_qty();
            }
            if ($product->getmp_exclude() == 1 || $p->getis_in_stock() == 0) {
                $stock = 0;
            }
        }

        // margin filter
        if($mp !== null){
            $margin = Mage::getStoreConfig('marketplace/'.strtolower($mp).'/margin_min');

            if($margin == ''){
                $margin = Mage::getStoreConfig('marketplace/general/margin');
            }

        }else{
            $margin = Mage::getStoreConfig('marketplace/general/margin');
        }
        //$margin = ($mp !== null) ? Mage::getStoreConfig('marketplace/'.strtolower($mp).'/margin_min') : Mage::getStoreConfig('marketplace/general/margin');
        if($margin != ''){

            $price = str_replace(",",".",$this->getPriceToExport($product));
            $cost = str_replace(",",".",$product->getcost());

            $product_margin = round(($price - $cost) / $price * 100, 2);
            
            if($product_margin < $margin && $product->getmp_force_export() == 0){

                $stock = 0;

            }
        }

        return (int)$stock;
    }

    /**
     * get delay to export
     *
     * @param product $product
     * @return int
     */
    public function getDelayToExport($product) {
        $mp_delay = $product->getmp_delay();
        return ($mp_delay !== NULL && $mp_delay >= 0) ? $mp_delay : mage::getStoreConfig('marketplace/general/delay');
    }

    /**
     * Get product price (including tax)
     *
     * @param product $product
     * @return float
     *
     * @see getPrice()
     */
    public function getPriceToExport($product) {

        //compute price with taxes
        if (!mage::getStoreConfig('tax/calculation/price_includes_tax')) {
            $taxRate = mage::getStoreConfig('marketplace/general/tax');
            $price = $this->getPrice($product);
            $price = str_replace(",", ".", $price);
            $price = round($price, 2);
            $taxCoef = 1 + $taxRate / 100;
            $price = $price * $taxCoef;
            $price = round($price, 2);
            $price = str_replace(".", ",", $price);
        } else {
            $price = $this->getPrice($product);
            $price = str_replace(",", ".", $price);
            $price = round($price, 2);
            $price = str_replace(".", ",", $price);
        }

        return $price;
    }

    /**
     * Get price
     *
     * @param product $product
     * @return float
     */
    public function getPrice($product) {
        $price = $product->getprice();
        if ($product->getspecial_price() != '') {
            $applySpecialPrice = true;
            if ($product->getspecial_from_date() != '') {
                $fromTimeStamp = strtotime($product->getspecial_from_date());
                if ($fromTimeStamp > time())
                    $applySpecialPrice = false;
            }
            if ($product->getspecial_to_date() != '') {
                $toTimeStamp = strtotime($product->getspecial_to_date());
                if ($toTimeStamp < time())
                    $applySpecialPrice = false;
            }
            if ($applySpecialPrice)
                $price = $product->getspecial_price();
        }
        $price = number_format($price, '4', ',', '');
        return $price;
    }

    /**
     * Get product collection to export
     *
     * @param string $marketplace
     * @return collection
     */
    public function getProductsToExport($marketplace) {

        $path = 'marketplace/' . $marketplace . '/last_updated_product_date';

        //recupere date du dernier produit mis a jour : si pas renseign�, on met le 01/01/0001
        $date = mage::helper('MarketPlace/Parameters')->getParamValue($path);
        if ($date == "") {
            $date = date('Y-m-d H:i:s', 0);
        }

        $collection = $this->getProductCollection($marketplace, $date);

        //define max products to export
        $max = mage::getStoreConfig('marketplace/general/updated_products_limit');
        if ($max == "") {
            $max = 500;
        }

        $collection->getSelect()->limit($max);

        return $collection;
    }

    /**
     * Get products collection
     *
     * @param string $marketplace
     * @param date $date
     * @return collection
     */
    public function getProductCollection($marketplace, $date){

        $store_id = (Mage::getStoreConfig('marketplace/'.$marketplace.'/store_id') != "") ? Mage::getStoreConfig('marketplace/'.$marketplace.'/store_id') : Mage::getStoreConfig('marketplace/general/default_store_id');

        $collection = mage::getResourceModel('catalog/product_collection')
                        ->setStoreId($store_id)
                        ->addAttributeToSelect('price')
                        ->addAttributeToSelect('special_price')
                        ->addAttributeToSelect('special_from_date')
                        ->addAttributeToSelect('special_to_date')
                        ->addAttributeToSelect('reserved_qty')
                        ->addAttributeToSelect('weight')
                        ->addAttributeToSelect('name')
                        ->addAttributeToSelect('image')
                        ->addAttributeToSelect('small_image')
                        ->addAttributeToSelect('updated_at')
                        ->addAttributeToSelect('status')
                        ->addAttributeToSelect('cost')
                        ->addAttributeToFilter('updated_at', array('gt' => $date))
                        ->addAttributeToSort('updated_at', 'desc')
                        ->addFieldToFilter('type_id', 'simple')
                        ->joinTable(
                                'MarketPlace/Data',
                                'mp_product_id=entity_id',
                                array(
                                    'mp_reference' => 'mp_reference',
                                    'mp_exclude' => 'mp_exclude',
                                    'mp_force_qty' => 'mp_force_qty',
                                    'mp_delay' => 'mp_delay',
                                    'mp_free_shipping' => 'mp_free_shipping',
                                    'mp_marketplace_status' => 'mp_marketplace_status',
                                    'mp_force_export' => 'mp_force_export'
                                ),
                                "mp_marketplace_id='$marketplace' and mp_reference <> '' and mp_marketplace_status = '".MDN_MarketPlace_Helper_ProductCreation::kStatusCreated."'",
                                'inner'
                        );

        //add barcode attribute
        $collection = mage::helper('MarketPlace/Barcode')->addBarcodeAttributeToProductCollection($collection);

        return $collection;
    }

    /**
     * Stock the latest product sent to market place
     *
     * @param string $marketplace
     * @param object $product
     * @param string $type (stock, price)
     */
    public function stockLatestSentProduct($marketplace, $product) {

        $date = $product->getupdated_at();
        $path = 'marketplace/' . $marketplace . '/last_updated_product_date';
        mage::helper('MarketPlace/Parameters')->setParamValue($path, $date);
    }

    public function hasSpecialPrice($product) {

        $hasSpecialPrice = false;

        if ($product->getspecial_price() != '') {

            $hasSpecialPrice = true;

            $fromdate = $product->getspecial_from_date();

            if ($fromdate != '')
                if (strtotime($fromdate) > time())
                    $hasSpecialPrice = false;

            $todate = $product->getspecial_to_date();

            if ($todate != '')
                if (strtotime($todate) < time())
                    $hasSpecialPrice = false;
        }

        return $hasSpecialPrice;
    }

    public function formatExportedTxt($txt){
        $txt = strip_tags($txt);
        $txt = preg_replace('/"/',"'",$txt);
        return $txt;
    }

    public function getProductFromBarcode($ean){

        $p = Mage::getModel('catalog/product')->getCollection()
                    ->addAttributeToSelect(mage::getStoreConfig('marketplace/general/ean_attribute_name'))
                    ->addFieldToFilter(mage::getStoreConfig('marketplace/general/ean_attribute_name'), $ean);
                    
        return ($p->count() > 0) ? $p->getFirstItem() : null;

    }

}
