<?php

/**
 * Manage feeds
 */
class MDN_MarketPlace_Helper_Feed extends Mage_Core_Helper_Abstract {

    const kFeedSubmitted = "_SUBMITTED_";
    const kFeedDone = "_DONE_";
    const kFeedInProgress = "_IN_PROGRESS_";
    const kFeedError = "_ERROR_";
    const kFeedDebug = "_DEBUG_";

    const kFeedTypeProductCreation = "_PRODUCT_CREATION_";
    const kFeedTypeUpdateStockPrice = "_UPDATE_STOCK_PRICE_";
    const kFeedTypeImportOrders = "_IMPORT_ORDERS_";
    const kFeedTypeAcceptOrders = "_ACCEPT_ORDERS_";
    const kFeedTypeCancelOrders = "_CANCEL_ORDERS_";
    const kFeedTypeTracking = "_TRACKING_";
    const kFeedTypeMedia = "_MEDIA_";
    const kFeedTypeMatchingProducts = "_MATCHING_PRODUCTS_";
    const kFeedTypeFulfillment = "_FULFILLMENT_";
    const kFeedTypeMatchingEAN = "_MATCHING_EAN_";
    const kFeedTypeShippingCost = "_SHIPPING_COST_";
    const kFeedTypeSubmissionResultProductCreation = "_SUBMISSION_RESULT_PRODUCT_CREATION_";
    const kFeedTypeSubmissionResultProductUpdate = "_SUBMISSION_RESULT_PRODUCT_UPDATE_";
    const kFeedTypeSubmissionResultTracking = "_SUBMISSION_RESULT_TRACKING_";
    const kFeedTypeGetOrdersToShip = "_GET_ORDERS_TO_SHIP_";
    const kFeedTypeUpdateOrders = '_UPDATE_ORDER_';
    const kFeedTypeSellerInformation = '_SELLER_INFORMATION_';
    const kFeedTypeDivers = '_DIVERS_';

    /**
     * Get feed status
     *
     * @return array
     */
    public function getFeedStatusOptions(){

        return array(
            self::kFeedSubmitted => self::kFeedSubmitted,
            self::kFeedDone => self::kFeedDone,
            self::kFeedInProgress => self::kFeedInProgress,
            self::kFeedError => self::kFeedError,
			self::kFeedDebug => self::kFeedDebug
        );
    }

    /**
     * Get feed type
     *
     * @return array
     */
    public function getFeedTypeOptions(){

        return array(
            self::kFeedTypeProductCreation => self::kFeedTypeProductCreation,
            self::kFeedTypeUpdateStockPrice => self::kFeedTypeUpdateStockPrice,
            self::kFeedTypeImportOrders => self::kFeedTypeImportOrders,
            self::kFeedTypeAcceptOrders => self::kFeedTypeAcceptOrders,
            self::kFeedTypeCancelOrders => self::kFeedTypeCancelOrders,
            self::kFeedTypeGetOrdersToShip => self::kFeedTypeGetOrdersToShip,
            self::kFeedTypeTracking => self::kFeedTypeTracking,
            self::kFeedTypeMedia => self::kFeedTypeMedia,
            self::kFeedTypeMatchingProducts => self::kFeedTypeMatchingProducts,
            self::kFeedTypeFulfillment => self::kFeedTypeFulfillment,
            self::kFeedTypeMatchingEAN => self::kFeedTypeMatchingEAN,
            self::kFeedTypeShippingCost => self::kFeedTypeShippingCost,
            self::kFeedTypeSubmissionResultProductCreation => self::kFeedTypeSubmissionResultProductCreation,
            self::kFeedTypeSubmissionResultProductUpdate => self::kFeedTypeSubmissionResultProductUpdate,
            self::kFeedTypeSubmissionResultTracking => self::kFeedTypeSubmissionResultTracking,
            self::kFeedTypeUpdateOrders => self::kFeedTypeUpdateOrders,
            self::kFeedTypeSellerInformation => self::kFeedTypeSellerInformation,
            self::kFeedTypeDivers => self::kFeedTypeDivers
        );
    }

}
