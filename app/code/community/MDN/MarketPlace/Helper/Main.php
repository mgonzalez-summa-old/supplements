<?php

/*
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MDN_MarketPlace_Helper_Main extends Mage_Core_Helper_Abstract {

    /**
     * Retrieve marketplaces names
     *
     * @return string
     */
    public function getMarketPlaces() {

        return Mage::helper('MarketPlace')->getMarketplacesName();
    }

    /**
     * check if cron is activated
     *
     * @param string $mp (marketplace name)
     * @return boolean
     */
    public function isCronActivate($mp) {
        return (Mage::getStoreConfig('marketplace/' . strtolower($mp) . '/' . strtolower($mp) . '_enable_cron') == 1);
    }

    /**
     * Import orders for one marketplace
     *
     * @param string $mp (marketplace name)
     * @return string
     */
    public function importOrders($mp) {
        try {
            $start = microtime(true);

            $orders = mage::helper(ucfirst($mp))->getMarketplaceOrders();

            if (count($orders) > 0) {

                $result = Mage::helper(ucfirst($mp))->importMarketPlaceOrders($orders);

                $end = microtime(true);

                $executionTime = $end - $start;

                mage::getModel('MarketPlace/Logs')->addLog(
                        Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                        0,
                        'Orders imported.',
                        array('fileName' => NULL),
                        $executionTime
                );

                return $result;
            } else {

                $end = microtime(true);

                $executionTime = $end - $start;

                mage::getModel('MarketPlace/Logs')->addLog(
                        Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                        0,
                        'No order to import.',
                        array('fileName' => NULL),
                        $executionTime
                );

                return 'No order to import.';
            }
        } catch (Exception $e) {

            $errorCode = 0;

            if (preg_match('/^Notice/', $e->getMessage()))
                $errorCode = 98;
            elseif (preg_match('/^Warning/', $e->getMessage()))
                $errorCode = 99;
            else
                $errorCode = $e->getCode();

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                    $errorCode,
                    $e->getMessage(),
                    array('fileName' => NULL)
            );
        }
    }

    /**
     * Update stocks and prices for one marketplace
     *
     * @param string $mp (marketplace name)
     *
     */
    public function updateStocksAndPrices($mp) {

        try {
            $start = microtime(true);

            mage::helper(ucfirst($mp))->updateStocksAndPrices();

            $end = microtime(true);

            $executionTime = $end - $start;

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                    0,
                    'Prices & stocks exported.',
                    array('fileName' => NULL),
                    $executionTime
            );
        } catch (Exception $e) {

            $errorCode = 0;

            if (preg_match('/^Notice/', $e->getMessage()))
                $errorCode = 98;
            elseif (preg_match('/^Warning/', $e->getMessage()))
                $errorCode = 99;
            else
                $errorCode = $e->getCode();

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                    $errorCode,
                    $e->getMessage(),
                    array('fileName' => NULL)
            );
        }
    }

    /**
     * Send trackings for one marketplace
     *
     * @param string $mp (marketplace name)
     */
    public function sendTracking($mp) {
        try {

            $start = microtime(true);

            mage::helper(ucfirst($mp))->sendTracking();

            $end = microtime(true);

            $executionTime = $end - $start;

            // add log message
            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                    0,
                    'Tracking send.',
                    array('fileName' => NULL),
                    $executionTime
            );
        } catch (Exception $e) {

            $errorCode = 0;

            if (preg_match('/^Notice/', $e->getMessage()))
                $errorCode = 98;
            elseif (preg_match('/^Warning/', $e->getMessage()))
                $errorCode = 99;
            else
                $errorCode = $e->getCode();

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                    $errorCode,
                    $e->getMessage(),
                    array('fileName' => NULL)
            );
        }
    }

    /**
     * Check product creation for one marketplace
     *
     * @param string $mp
     */
    public function checkProductCreation($mp) {

        try {
            $start = microtime(true);

            mage::helper(ucfirst($mp))->checkProductCreation();

            $end = microtime(true);

            $executionTime = $end - $start;

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                    0,
                    'Product creation checked.',
                    array('fileName' => NULL),
                    $executionTime
            );
        } catch (Exception $e) {

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                    14,
                    'An error occures during attempt to check product creation : ' . $e->getMessage(),
                    array('fileName' => NULL)
            );
        }
    }

    /**
     * Import orders for all marketplaces (called by the cron)
     *
     * @return string
     */
    public function cronImportOrders() {

        try {
            foreach ($this->getMarketPlaces() as $mp) {

                $mp = strtolower($mp);

                if(Mage::helper(ucfirst($mp))->allowImportOrders()){
                    
                    if ($this->isCronActivate($mp)) {

                        $this->importOrders($mp);
                    } else {
                        mage::getModel('MarketPlace/Logs')->addLog(
                                Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                                0,
                                'Cron is disabled.',
                                array('fileName' => NULL)
                        );
                    }
                }
            }

            return 'Orders imported';

        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, "marketplace.log");
        }
    }

    /**
     * Update stocks and prices for all marketplaces (called by the cron)
     *
     * @return string
     */
    public function cronUpdateStocksAndPrices() {

        try {
            foreach ($this->getMarketPlaces() as $mp) {

                $mp = strtolower($mp);

                if(Mage::helper(ucfirst($mp))->allowUpdateStocksAndPrices()){

                    if ($this->isCronActivate($mp)) {

                        $this->updateStocksAndPrices($mp);

                    } else {
                        mage::getModel('MarketPlace/Logs')->addLog(
                                Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                                0,
                                'Cron is disabled.',
                                array('fileName' => NULL)
                        );
                    }
                }
            }

            return 'stocks & prices exported';

        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, "marketplace.log");
        }
    }

    /**
     * Send trackings for all marketplaces (called by the cron)
     *
     * @return string
     */
    public function cronSendTracking() {

        try {
            foreach ($this->getMarketPlaces() as $mp) {

                $mp = strtolower($mp);

                if(Mage::helper(ucfirst($mp))->allowTracking()){

                    if ($this->isCronActivate($mp)) {

                        $this->sendTracking($mp);
                    } else {
                        mage::getModel('MarketPlace/Logs')->addLog(
                                Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                                0,
                                'Cron is disabled.',
                                array('fileName' => NULL)
                        );
                    }
                }
            }

            return 'traking sent';

        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, "marketplace.log");
        }
    }

    /**
     * Check product creation for all marketplaces (called by the cron)
     *
     * @return string
     */
    public function cronCheckProductCreation() {

        try {
            foreach ($this->getMarketPlaces() as $mp) {

                $mp = strtolower($mp);

                if(Mage::helper(ucfirst($mp))->allowProductCreation()){
                    
                    if ($this->isCronActivate($mp)) {

                        $this->checkProductCreation($mp);
                    } else {
                        mage::getModel('MarketPlace/Logs')->addLog(
                                Mage::helper(ucfirst($mp))->getMarketPlaceName(),
                                0,
                                'Cron is disabled.',
                                array('fileName' => NULL)
                        );
                    }
                }
            }

            return 'product creation checked';

        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, "marketplace.log");
        }
    }

}
