<?php
/*
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

abstract class MDN_MarketPlace_Helper_ProductUpdate extends Mage_Core_Helper_Abstract {

    protected $_lastSentProduct = null;
    protected $_products = null;
    protected $_mp = null;

    /**
     * update stock and price
     */
    abstract public function update($request = null);

    /**
     * Get marketplace name
     */
    abstract function getMp();

    /**
     * Update mp_last_update field for each updated products
     *
     * @return int 0
     */
    public function updateLastUpdatedDate(){


            foreach($this->getProducts() as $product){

                $obj = Mage::getModel('MarketPlace/Data')->load($product->getmp_id());
                $obj->setmp_last_update(date('Y-m-d H:i:s'))
                        ->save();


            }


        return 0;

    }

    /**
     *
     * @return collection $products
     */
    public function getProducts($request = null){

        if($this->_products === null){

            $this->_products = $this->getProductsToExport();

        }

        return $this->_products;

    }

    /**
     * Retrieve products to update on amazon marketplace
     * - simple products
     * - visibility : search, catalog, nowhere, catalog/search
     * - mp_reference not null in market_place_data table
     *
     * @return collection
     */
    public function getProductsToExport() {

        return mage::helper('MarketPlace/Product')->getProductsToExport($this->getMp());

    }

    /**
     * Get export path name
     *
     * @return string
     *
     */
    public function getExportPath(){
        return Mage::app()->getConfig()->getTempVarDir().'/export/marketplace/'.$this->getMp().'/';
    }

}
