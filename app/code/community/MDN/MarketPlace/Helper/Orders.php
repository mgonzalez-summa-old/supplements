<?php
/* 
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

abstract class MDN_MarketPlace_Helper_Orders extends Mage_Core_Helper_Abstract {

    const kOrderAccepted = "Accepted";

    /**
     * Import orders manually from exported file
     *
     * @param string $path
     * @param string $file
     * @return string $debug
     *
     */
    public function importOrdersFromUploadedFile($path, $file) {

        $filePath = $path . $file;
        $lines = file($filePath);

        $ordersTab = $this->buildOrdersTab($lines);
        return $this->importMarketPlaceOrders($ordersTab);
    }

    /**
     * Import orders from marketplace
     *
     * @param array $ordersTab
     * @return string
     */
    public function importMarketPlaceOrders($ordersTab) {

        $debug = "";
        $nbImported = 0;
        $nbSkipped = 0;

        $successOrders = array();
        $errorOrders = array();
        $skippedOrders = array();
        
        foreach ($ordersTab as $order) {

            if ($this->orderAlreadyImported($order['mpOrderId'])) {
                $nbSkipped += 1;
                $skippedOrders[] = $order['mpOrderId'];
                continue;
            }

            $new_order = $this->importOrder($order);

            if ($new_order != null) {
                $nbImported += 1;
                $successOrders[] = $order['mpOrderId'];
            } else {
                $nbSkipped += 1;
                $errorOrders[] = $order['mpOrderId'];
            }
        }

        $this->setOrderImportationLog($successOrders, $skippedOrders, $errorOrders);
        $this->updateOrders($successOrders, $this->getOrderStatus(self::kOrderAccepted));

        $debug .= $nbImported . ' order(s) imported' . "<br>";
        $debug .= $nbSkipped . ' order(s) skipped' . "<br>";
        return $debug;
    }

    protected function updateOrders($orders, $status){
        // to implement in subclass
        return 0;
    }

    protected function getOrderStatus($status){
        return '';
    }

    /**
     * Order importation
     *
     * @param array $order
     * @return order
     */
    protected function importOrder($order) {
        
        $decrement_stock = (Mage::getStoreConfig('cataloginventory/options/can_subtract') == 1) ? true : false;

        // get taxRate
        $taxRate = mage::getStoreConfig('marketplace/general/tax');
        // use default value if core_config_data value is empty
        // $taxRate = (!$taxRate) ? 19.6 : $taxRate;
        if ($taxRate == "") {
            throw new Exception($this->__('Tax rate attribute not set in Sytem > Configuration > Marketplace'), 15);
        }

        $currency = mage::getStoreConfig('marketplace/general/default_currency');
        if ($currency == "") {
            throw new Exception($this->__('Currency attribute not set in Sytem > Configuration > Marketplace'), 15);
        }

        $payment_method = Mage::getStoreConfig('marketplace/general/default_payment_method');
        if ($payment_method == "") {
            throw new Exception($this->__('Payment method attribute not set in Sytem > Configuration > Marketplace'), 15);
        }

        //create customer
        $customer = $this->createOrReturnCustomer($order);

        //create order
        $new_order = Mage::getModel('sales/order');
        $new_order->reset();
        $new_order->setcustomer_id($customer->getId());
        $new_order->setCustomerGroupId($customer->getGroupId());
        $new_order->setCustomerFirstname($customer->getFirstname());
        $new_order->setCustomerLastname($customer->getLastname());
        $new_order->setCustomerIsGuest(0);
        $new_order->setCustomerEmail($customer->getemail());
        $new_order->setcreated_at(now());

        $new_order->setStore_id($this->getCurrentStoreID());
        $new_order->setorder_currency_code($currency);
        $new_order->setbase_currency_code($currency);
        $new_order->setstore_currency_code($currency);
        $new_order->setstore_to_base_rate(1);

        //shipping address
        $shipping_address = Mage::getModel('sales/order_address');
        $shipping_address->setOrder($new_order);
        $shipping_address->setId(null);
        $shipping_address->setentity_type_id(12);
        $shipping_address->setfirstname($order['shipping_adress']['firstname']);
        $shipping_address->setlastname('');

        $shipping_address->setStreet($order['shipping_adress']['street']);

        $shipping_address->setCity($order['shipping_adress']['city']);
        $shipping_address->setPostcode($order['shipping_adress']['zipCode']);
        $shipping_address->setcountry_id($order['shipping_adress']['country']);
        $shipping_address->setEmail($customer->getEmail());
        $shipping_address->setTelephone($order['shipping_adress']['phone']);
        $shipping_address->setcomments($order['shipping_adress']['comments']);
        $shipping_address->setcompany('');

        $new_order->setShippingAddress($shipping_address);

        //shipping address
        $billing_address = Mage::getModel('sales/order_address');
        $billing_address->setOrder($new_order);
        $billing_address->setId(null);

        // retrieve entity type id address, if current mage version does'nt use flat order then let it as null
        $entity_type_id_address = null;
        if (!Mage::helper('MarketPlace/FlatOrder')->isFlatOrder())
            $entity_type_id_address = Mage::getResourceModel("sales/order_address")->getTypeId();

        $billing_address->setentity_type_id($entity_type_id_address);
        $billing_address->setfirstname($order['billing_adress']['firstname']);
        $billing_address->setlastname('');
        $billing_address->setStreet($order['billing_adress']['street']);
        $billing_address->setCity($order['billing_adress']['city']);
        $billing_address->setPostcode($order['billing_adress']['zipCode']);
        $billing_address->setcountry_id($order['billing_adress']['country']);
        $billing_address->setEmail($customer->getEmail());
        $billing_address->setTelephone($order['billing_adress']['phone']);
        $billing_address->setcomments($order['billing_adress']['comments']);
        $billing_address->setcompany('');
        $new_order->setBillingAddress($billing_address);

        //Payment method
        $payment = Mage::getModel('sales/order_payment');
        $payment->setMethod($payment_method);
        $new_order->setPayment($payment);

        //shipping method
        $ShippingTaxAmount = $order['shipping_tax'];
        $ShippingAmount = $order['shipping_excl_tax'];
        $shipping_method = mage::getStoreConfig('marketplace/general/default_shipment_method');
        $shipping_method_title = $this->getShippingMethodTitle($shipping_method);

        $new_order->setshipping_method($shipping_method);
        $new_order->setshipping_description($shipping_method_title);
        $new_order->setshipping_amount((double) $ShippingAmount);
        $new_order->setbase_shipping_amount((double) $ShippingAmount);
        $new_order->setshipping_tax_amount((double) $ShippingTaxAmount);
        $new_order->setbase_shipping_tax_amount((double) $ShippingTaxAmount);

        //init order totals
        $new_order
                ->setGrandTotal($ShippingAmount + $ShippingTaxAmount)
                ->setBaseGrandTotal($ShippingAmount + $ShippingTaxAmount)
                ->setTaxAmount($ShippingTaxAmount)
                ->setBaseTaxAmount($ShippingTaxAmount);

        foreach ($order['products'] as $item) {

            //set price and tax
            $price_excl_tax = $item['price_excl_tax'];
            $price_incl_tax = $item['price_incl_tax'];
            $tax = $item['price_tax'];
            $qty = $item['quantity'];

            $taxTotal = $tax * $qty;
            $htTotal = $price_excl_tax * $qty;

            //add product
            $product = Mage::getModel('catalog/product')->load($item['id']);

            // check if product exists
            if (!$product->getId()) {
                return null;
            }

            $NewOrderItem = Mage::getModel('sales/order_item')
                            ->setProductId($product->getId())
                            ->setSku($product->getSku())
                            ->setName($product->getName())
                            ->setWeight($product->getWeight())
                            ->setTaxClassId($product->getTaxClassId())
                            ->setCost($product->getCost())
                            ->setOriginalPrice($price_excl_tax)
                            ->setbase_original_price($price_excl_tax)
                            ->setIsQtyDecimal(0)
                            ->setProduct($product)
                            ->setPrice((double) $price_excl_tax)
                            ->setBasePrice((double) $price_excl_tax)
                            ->setQtyOrdered($item['quantity'])
                            ->setmarketplace_item_id($item['mp_item_id'])
                            ->setTaxAmount($taxTotal)
                            ->setBaseTaxAmount($taxTotal)
                            ->setTaxPercent($taxRate)
                            ->setRowTotal($htTotal)
                            ->setBaseRowTotal($htTotal)
                            ->setRowWeight($product->getWeight() * $qty)
                            ->setbase_tax_before_discount($taxTotal)
                            ->settax_before_discount($taxTotal);
                            /*->setprice_incl_tax($price_incl_tax)
                            ->setrow_total_incl_tax($row_total_incl_tax);*/

            // update product stock (fix bug...)
            if($decrement_stock === true)
                    $this->_updateProductStock($product, $item['quantity']);

            //add product
            $new_order->addItem($NewOrderItem);
            $new_order
                    ->setSubtotal($new_order->getSubtotal() + $price_excl_tax * $qty)
                    ->setBaseSubtotal($new_order->getBaseSubtotal() + $price_excl_tax * $qty)
                    ->setGrandTotal($new_order->getGrandTotal() + (($tax + $price_excl_tax) * $qty))
                    ->setBaseGrandTotal($new_order->getBaseGrandTotal() + (($tax + $price_excl_tax) * $qty))
                    ->setTaxAmount($new_order->getTaxAmount() + $tax * $qty)
                    ->setBaseTaxAmount($new_order->getBaseTaxAmount() + $tax * $qty);
        }

        //save order
        $new_order->setstatus(Mage::getStoreConfig('marketplace/general/orders_state'));
        $new_order->setstate('new');
        $new_order->addStatusToHistory(
                                    'pending',
                                    'Commande ' . $order['marketplace'] . ' #' . $order['mpOrderId']
                                );
        $new_order->setcreated_at(date("Y-m-d H:i:s"), Mage::getModel('Core/Date')->timestamp());
        $new_order->setupdated_at(date("Y-m-d H:i:s"), Mage::getModel('Core/Date')->timestamp());
        $new_order->setfrom_site($order['marketplace']);
        $new_order->setmarketplace_order_id($order['mpOrderId']);
        $new_order->setinvoice_comments('Commande ' . $order['marketplace'] . ' #' . $order['mpOrderId']);
        $new_order->save();

        if(mage::getStoreConfig('marketplace/general/generate_invoice') == 1){

            Mage::helper('MarketPlace/Invoice')->createInvoice($new_order);
            $new_order->save();
        }

        return $new_order;
    }

    protected function _updateProductStock($product, $qty_to_substract){

        $stock_item = Mage::getModel('cataloginventory/stock_item');
        $current_qty = $stock_item->loadByProduct($product)->getQty();
        $qty = $current_qty - $qty_to_substract;

        $stock_item->setQty($qty)
                    ->save();

    }

    /**
     * Return shipment method title
     *
     * @param string $shipping_method
     * @return string
     */
    public function getShippingMethodTitle($shipping_method) {

        return mage::helper('MarketPlace')->getShippingMethodTitle($shipping_method);
    }

    /**
     * Create or return customer
     *
     * @param unknown_type $tFields
     * @return customer $customer
     */
    private function createOrReturnCustomer($order) {

        // retrieve store ID and webSiteId
        $storeId = $this->getCurrentStoreID();
        $webSiteId = Mage::getModel('core/store')->load($storeId)->getWebsiteId();

        //return customer if already exists
        $email = $order['email'];
        $fakeCustomer = mage::getModel('customer/customer');
        $fakeCustomer->setWebsiteId($webSiteId);
        $tName = explode(' ', $order['firstname']);
        $firstname = $tName[0];
        $lastname = '';
        for ($i = 1; $i < count($tName); $i++)
            $lastname .= $tName[$i] . ' ';

        $customer = Mage::getModel('customer/customer')
                        ->setWebsiteId($webSiteId)
                        ->loadByEmail($email);
        if ($customer->getId())
            return $customer;

        //create new customer
        $customer = mage::getModel('customer/customer');
        $customer->setWebsiteId($webSiteId);
        $customer->setstore_id($storeId);
        $customer->setFirstname($firstname);
        $customer->setLastname($lastname);
        $customer->setEmail($email);
        $customer_group_id = Mage::Helper('MarketPlace')->getCustomerGroupId(strtolower($this->getMarketPlaceName()));
        $customer->setgroup_id($customer_group_id);
        $customer->save();

        return $customer;
    }

    public function getCurrentStoreID(){

        $mp = strtolower($this->getMarketPlaceName());

	$store_id = Mage::getStoreConfig('marketplace/'.$mp.'/store_id');

        return ($store_id != "") ? $store_id : Mage::getStoreConfig('marketplace/general/default_store_id');

    }

    /**
     * Check if current order has not be imported yet
     *
     * @param string $marketplaceOrderId
     * @return boolean
     */
    protected function orderAlreadyImported($marketplaceOrderId) {

        return mage::helper('MarketPlace')->orderAlreadyImported($marketplaceOrderId);
    }

    /**
     * Set log for marketplace imporation orders
     *
     * @param array $successOrders
     * @param array $skippedOrders
     * @param array $errorOrders
     */
    public function setOrderImportationLog($successOrders, $skippedOrders, $errorOrders) {

        // Some error occured during order importation (invalid product)
        if (count($errorOrders) != 0) {

            // build error message
            $errorMsg = "An error occured during orders importation :";
            foreach ($errorOrders as $order) {
                $errorMsg .= ' ' . $order;
            }

            // add error log
            mage::getModel('MarketPlace/Logs')->addLog(
                    $this->getMarketPlaceName(),
                    3,
                    $errorMsg,
                    array('fileName' => NULL)
            );
        }

        $successMsg = "Imported orders :";
        // add information log
        if (count($successOrders) != 0) {
            // build success message
            foreach ($successOrders as $order) {
                $successMsg .= ' ' . $order;
            }
            $successMsg .= ".";
        } else {
            $successMsg .= ' none.';
        }

        $successMsg .= ' Skipped orders (already exist) :';
        if (count($skippedOrders) != 0) {
            foreach ($skippedOrders as $order) {
                $successMsg .= " " . $order;
            }
            $successMsg .= ".";
            $errorCode = 2;
        } else {
            $successMsg .= ' none.';
            $errorCode = 0;
        }

        mage::getModel('MarketPlace/Logs')->addLog(
                $this->getMarketPlaceName(),
                $errorCode,
                $successMsg,
                array('fileName' => NULL)
        );
    }

    abstract function getMarketPlaceOrders();

    abstract function checkFileVersion($lines);

    abstract function buildOrdersTab($str);

    abstract function isFileOk($lines);

    abstract function getMarketPlaceName();

}
