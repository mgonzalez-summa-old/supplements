<?php

/**
 * List global errors
 */
class MDN_MarketPlace_Helper_Errors extends Mage_Core_Helper_Abstract implements MDN_MarketPlace_Helper_Interface_Errors {

    protected $_errorsOptions = null;


    public function getBaseErrors(){

        $errors = array(
            '0' => mage::helper('MarketPlace')->__('No error'),
            '1' => mage::helper('MarketPlace')->__('Get orders from marketplace'),
            '2' => mage::helper('MarketPlace')->__('Order(s) already imported'),
            '3' => mage::helper('MarketPlace')->__('Invalid product'),
            '4' => mage::helper('MarketPlace')->__('Update marketplace stock'),
            '9' => mage::helper('MarketPlace')->__('Bad query'),
            '10' => mage::helper('MarketPlace')->__('Can\'t write file'),
            '11' => mage::helper('MarketPlace')->__('Invalid file'),
            '12' => mage::helper('MarketPlace')->__('Error Response'),
            '13' => mage::helper('MarketPlace')->__('No more available request'),
            '14' => mage::helper('MarketPlace')->__('Check product creation'),
            '15' => mage::helper('MarketPlace')->__('Undefined Attribute'),
	    '16' => mage::helper('MarketPlace')->__('Send tracking'),
            '17' => mage::helper('MarketPlace')->__('Unavailable carrier code'),
            '18' => mage::helper('MarketPlace')->__('Unable to retrieve some required informations'),
            '19' => mage::helper('MarketPlace')->__('Mass product creation'),
            '20' => mage::helper('MarketPlace')->__('URL called feed'),
            '21' => Mage::helper('MarketPlace')->__('Internationalization'),
            '22' => Mage::Helper('MarketPlace')->__('Matching EAN'),
            '98' => mage::helper('MarketPlace')->__('Notice'),
            '99' => mage::helper('MarketPlace')->__('Warning')
        );

        return $errors;

    }

    /**
     * Get errors tab
     *
     * @return array
     */
    public function getErrorsOptions(){

        $errors = $this->getBaseErrors();

        $helpers = Mage::helper('MarketPlace')->getHelpers();

        foreach($helpers as $helper){

            $helper = Mage::helper($helper);

            if($helper->hasSpecificErrors()){

                $tmp = Mage::helper(ucfirst($helper->getMarketPlaceName()).'/errors')->getErrorsOptions();

                foreach($tmp as $k => $v){

                    if(!array_key_exists($k, $errors))
                        $errors[$k] = $v;

                }

            }

        }

        return $errors;
    }

    public function getErrorsOptionsFor($mp){

        if($this->_errorsOptions === null){

            $this->_errorsOptions = $this->getBaseErrors();

            if(Mage::Helper(ucfirst($mp))->hasSpecificErrors()){

                $tmp = Mage::Helper(ucfirst($mp).'/errors')->getErrorsOptions();

                foreach($tmp as $k => $v){

                    if(!array_key_exists($k, $this->_errorsOptions))
                        $this->_errorsOptions[$k] = $v;

}

            }

        }

        return $this->_errorsOptions;

    }

    public function getErrorLabel($id, $mp){

        $retour = '';

        $options = $this->getErrorsOptionsFor($mp);

        if(array_key_exists($id, $options)){

            $retour = $options[$id];

        }

        return $retour;
    }

}
