<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_MarketPlace_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * Get Customer group ID
     * 
     * @param string $mp
     * @return int 
     */
    public function getCustomerGroupId($mp = ''){

        // default goup id
        $retour = (Mage::getStoreConfig('marketplace/general/default_customer_group_id') != '') ? Mage::getStoreConfig('marketplace/general/default_customer_group_id') : 1;

        if(!empty($mp))
            $retour = (Mage::getStoreConfig('marketplace/'.strtolower($mp).'/default_customer_group_id') != '') ? Mage::getStoreConfig('marketplace/'.strtolower($mp).'/default_customer_group_id') : $retour;

        return $retour;

    }

    /**
     * get available marketplaces from config.xml
     *
     * @return array $helpers
     */
    public function getHelpers() {

        $helpers = array();

        $marketplaces = Mage::getConfig()->getNode('marketplaces');
        foreach ($marketplaces->children() as $helper) {
            $helpers[] = (string) $helper;
        }

        return $helpers;
    }

    /**
     * get available marketplaces names
     *
     * @return array $retour
     */
    public function getMarketplacesName() {

        $retour = array();
        $helpers = $this->getHelpers();
        foreach ($helpers as $helper) {
            $tmp = explode("/", $helper);
            $retour[] = $tmp[0];
        }

        return $retour;
    }

    /**
     * get marketplaces for html select
     *
     * @return array $retour
     */
    public function getMarketPlaceOptions(){

        $retour = array();

        $helpers = $this->getHelpers();

        foreach($helpers as $helper){

            $tmp = explode("/", $helper);
            $market = $tmp[0];

            $market = strtolower($market);

            $retour[$market] = $market;

        }

        return $retour;
        
    }

    /**
     * Get helper according to marketplace name
     *
     * @param string $name
     * @return helper
     */
    public function getHelperByName($name) {
        foreach ($this->getHelpers() as $helper) {
            $helper = mage::helper($helper);
            if ($helper->getMarketPlaceName() == $name)
                return $helper;
        }

        throw new Exception('Unable to load market place helper for ' . $name);
    }

    /**
     * get shipping method title
     *
     * @param string $shipping_method
     * @return string
     */
    public function getShippingMethodTitle($shipping_method) {

        if ($shipping_method == "") {
            throw new Exception('Selected shipment method doesn\'t exists anymore. Please check your system configuration');
        }

        $tab = explode('_', $shipping_method);

        $options = array();
        $title = "";

        // get shipment methods
        $carriers = Mage::getStoreConfig('carriers', 0);

        foreach ($carriers as $carrierKey => $item) {
            if ($carrierKey == $tab[0]) {
                $title = mage::getModel($item['model'])->getConfigData('title');
            }
        }

        return $title;
    }

    /**
     * Check if current order has not be imported yet
     *
     * @param string $marketplaceOrderId
     * @return boolean
     */
    public function orderAlreadyImported($marketplaceOrderId) {
        $collection = mage::getModel('sales/order')
                        ->getCollection()
                        ->addAttributeToFilter('marketplace_order_id', $marketplaceOrderId);

        if ($collection->getSize() > 0)
            return true;
        else
            return false;
    }

    /**
     * Save grid
     *
     * @param request $request
     * @param string $mp
     */
    public function save($request, $mp) {

        $data = $request->getPost('data');

        foreach ($data as $productId => $value) {

            $status = NULL;

            // forec export field
            $value['mp_force_export'] = (!isset($value['mp_force_export'])) ? 0 : 1;

            // exclude ?
            if (!isset($value['mp_exclude'])) $value['mp_exclude'] = 0;

            //update information
            if ($value['mp_force_qty'] == '') $value['mp_force_qty'] = new Zend_Db_Expr('null');

            // has reference ?
            if ($value['mp_reference'] == '')
                $value['mp_reference'] = new Zend_Db_Expr('null');
            else
                $status = 'created';

            // delay
            if (!isset($value['mp_delay']) || $value['mp_delay'] == '')
                $value['mp_delay'] = new Zend_Db_Expr('null');

            // free shipping
            $free_shipping = (isset($value['mp_free_shipping'])) ? $value['mp_free_shipping'] : new Zend_Db_Expr('null');

            //try to load record
            $obj = mage::getModel('MarketPlace/Data')
                            ->getCollection()
                            ->addFieldToFilter('mp_marketplace_id', $mp)
                            ->addFieldToFilter('mp_product_id', $productId)
                            ->getFirstItem();   

            $obj->setmp_marketplace_id($mp)
                    ->setmp_product_id($productId)
                    ->setmp_exclude($value['mp_exclude'])
                    ->setmp_reference($value['mp_reference'])
                    ->setmp_force_qty($value['mp_force_qty'])
                    ->setmp_delay($value['mp_delay'])
                    ->setmp_marketplace_status($status)
                    ->setmp_free_shipping($free_shipping)
                    ->setmp_force_export($value['mp_force_export'])
                    ->save();

            // I18N
            if(Mage::Helper(ucfirst($mp))->allowInternationalization()){

                $country = Mage::Helper(ucfirst($mp).'/Internationalization')->getCurrentCountry($request);//($request->getParam('country') == '') ? 'FR' : $request->getParam('country');

                if($value['mps_delay'] == '')
                    $value['mps_delay'] = new Zend_Db_Expr('null');

                Mage::getModel('MarketPlace/Status')->update($obj, $country, $status, $value['mps_delay']);

            }
        }
    }

    /**
     * Rename uploaded file (when orders are manually imported)
     *
     * @param filename $uploadFile
     * @param pathname $path
     * @param string $marketplace
     * @return string $newName
     */
    public function renameUploadedFile($uploadFile, $path, $marketplace) {

        $extension = strrchr($uploadFile, '.');
        $newName = 'import' . ucfirst($marketplace) . '-' . date('Y-m-d_H:i:s') . $extension;
        rename($path . $uploadFile, $path . $newName);

        return $newName;
    }

    

}