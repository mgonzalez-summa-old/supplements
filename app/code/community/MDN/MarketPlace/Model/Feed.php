<?php

class MDN_MarketPlace_Model_Feed extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('MarketPlace/Feed');
    }

}