<?php

class MDN_MarketPlace_Model_Category extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('MarketPlace/Category');
    }

    /**
     * Update association in database
     */
    public function updateAssociation($marketPlace, $categoryId, $value) {
        //try to load the association
        $item = mage::getModel('MarketPlace/Category')
                        ->getCollection()
                        ->addFieldToFilter('mpc_marketplace_id', $marketPlace)
                        ->addFieldToFilter('mpc_category_id', $categoryId)
                        ->getFirstItem();

        //delete old item
        if ($item->getId())
            $item->delete();

        //insert record if value is set
        if ($value != '') {
            $category = mage::getModel('catalog/category')->load($categoryId);

            $newItem = mage::getModel('MarketPlace/Category')
                            ->setmpc_marketplace_id($marketPlace)
                            ->setmpc_category_id($categoryId)
                            ->setmpc_association_data($value)
                            ->setmpc_category_path($category->getPath())
                            ->save();
        }

        return true;
    }

    /**
     * Return association value between category & marketplace
     */
    public function getAssociationValue($categoryId, $marketPlace) {
        $item = mage::getModel('MarketPlace/Category')
                        ->getCollection()
                        ->addFieldToFilter('mpc_marketplace_id', $marketPlace)
                        ->addFieldToFilter('mpc_category_id', $categoryId)
                        ->getFirstItem();

        //unserialize
        $value = $item->getmpc_association_data();
        if ($value != '')
            $value = mage::helper('MarketPlace/Serializer')->unserializeObject($value);

        return $value;
    }

    public function getMagentoCategory($mp_cat_id, $mp) {

        $id = Mage::helper('MarketPlace/Serializer')->serializeObject($mp_cat_id);

        $item = mage::getModel('MarketPlace/Category')
                        ->getCollection()
                        ->addFieldToFilter('mpc_association_data', $id)
                        ->addFieldToFilter('mpc_marketplace_id', $mp)
                        ->getFirstItem();

        return ($item->getmpc_category_id() == "") ? null : $item->getmpc_category_id();
    }

}