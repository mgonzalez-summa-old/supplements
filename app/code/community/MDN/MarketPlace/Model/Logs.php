<?php

/**
 * Description of Logs
 *
 * @author Nicolas Mugnier
 */
class MDN_MarketPlace_Model_Logs extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('MarketPlace/Logs');
    }

    public function addLog($marketplace, $error, $message, $file, $executionTime = 0){

        $log = mage::getModel('MarketPlace/Logs');
        $log->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                ->setmp_marketplace($marketplace)
                ->setmp_is_error($error)
                ->setmp_message($message)
                ->setmp_execution_time($executionTime);

        $log->save();

        $this->limitRecords();

        // send an email for "bad" errors
        $notNotifyErrors = array('0','2');
        if(!in_array($error, $notNotifyErrors)){
            $dest = mage::getStoreConfig('marketplace/general/bug_report');
            if($dest != "")
                mail($dest, 'Error Marketplace ('.$marketplace.')', $message);
        }

        if($file['fileName'] != NULL){

            $content = $file['fileContent'];
            $filePath = Mage::app()->getConfig()->getTempVarDir();
            $filePath .= ($file['type'] == 'export') ? '/export/marketplace/'.$marketplace : '/import/marketplace/'.$marketplace;

            if(!file_exists($filePath))
                mkdir($filePath, 0755, true);

            $filePath .= '/'.$file['fileName'];

            $handle = @fopen($filePath, 'w');
            if(!$handle){
                throw new Exception('Error while attempt to write file '.$filePath, 10);
            }
            fputs($handle, $content);
            fclose($handle);
        }
    }

    /**
     * Limit logs records
     */
    public function limitRecords(){

        $prefix = Mage::getConfig()->getTablePrefix();

        $max = mage::getStoreConfig('marketplace/logs/max');

        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
        
        $sql = 'SELECT * FROM '.$prefix.'market_place_logs ORDER BY mp_id DESC';

        $res = $read->fetchAll($sql);

        if(count($res)>$max){

            $write = Mage::getSingleton('core/resource')->getConnection('core_write');

            $delete = array();

            for($i=$max; $i<count($res);$i++){

                $delete[] = $res[$i]['mp_id'];

            }

            $inClose = implode(",",$delete);

            $sql = 'DELETE FROM '.$prefix.'market_place_logs WHERE mp_id IN ('.$inClose.')';

            $write->query($sql);
        }

    }

    /**
     * Save file
     *
     * @param array $file
     * <ul>
     * <li>array $file['filenames']</li>
     * <li>string $file['fileContent']</li>
     * <li>string $file['marketplace']</li>
     * <li>string $file['type']</li>
     * </ul>
     */
    public function saveFile($file){

        $filenames = $file['filenames'];
        $content = $file['fileContent'];
        $marketplace = $file['marketplace'];
        $filePath = Mage::app()->getConfig()->getTempVarDir();
        $filePath .= ($file['type'] == 'export') ? '/export/marketplace/'.$marketplace : '/import/marketplace/'.$marketplace;
        if(array_key_exists('dir',$file)){
            $filePath .= '/'.$file['dir'];
        }

        if(!file_exists($filePath))
            mkdir($filePath, 0755, true);

        // update exported file and create history file
        foreach($filenames as $filename){
            $filePathTmp = $filePath.'/'.$filename;

            $handle = @fopen($filePathTmp, 'w+');
            if(!$handle)
                throw new Exception('Error while attempt to write file '.$filePathTmp, 10);

            fputs($handle, $content);
            fclose($handle);
        }

        // remove old files
        $handle = opendir($filePath);
        $files = array();
        while($file = readdir($handle)){
            if(!is_dir($filePath.'/'.$file) && !preg_match('/^\./', $file))
                $files[$file] = filemtime($filePath.'/'.$file);
        }

        arsort($files);
        $oldFiles = array_slice($files, 20, count($files));

        foreach($oldFiles as $k => $v){
            unlink($filePath.'/'.$k);
        }
    }
}
