<?php

class MDN_MarketPlace_Model_System_Config_Source_Storeid extends Mage_Core_Model_Abstract{

    public function getAllOptions(){

        if(!$this->_options){

            $tmp = array();
            $stores = mage::getModel('Core/Store')
                        ->getCollection();

            foreach($stores as $store){
                $tmp[] = array(
                    'value' => $store->getstore_id(),
                    'label' => $store->getname()
                );
                //$tmp[$store->getstore_id()] = $store->getname();
            }

            $this->_options = $tmp;

        }

        return $this->_options;

    }

    public function toOptionArray(){
        return $this->getAllOptions();
    }


}
