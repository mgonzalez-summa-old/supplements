<?php

class MDN_MarketPlace_Model_System_Config_Source_SellerProductId extends Mage_Core_Model_Abstract{

    public function getAllOptions(){

        if(!$this->_options){

            $tmp = array(
                array(
                    'value' => 'sku',
                    'label' => 'SKU'
                ),
                array(
                    'value' => 'id',
                    'label' => 'Product ID'
                )
            );

            $this->_options = $tmp;

        }

        return $this->_options;

    }

    public function toOptionArray(){
        return $this->getAllOptions();
    }


}
