<?php

class MDN_MarketPlace_Model_System_Config_Source_Paymentmethods extends Mage_Core_Model_Abstract{

    const XML_PATH_PAYMENT_METHODS = 'payment';
    
    public function getAllOptions(){
        
        if(!$this->_options){

            $payment_methods = $this->getPaymentMethods();
            
            foreach($payment_methods as $method){
                $tmp[$method->getcode()] = $method->getcode();
            }

            $this->_options = $tmp;
        }

        return $this->_options;
    }

    public function toOptionArray(){
        return $this->getAllOptions();
    }

    public function getPaymentMethods(){

        $methods = Mage::getStoreConfig(self::XML_PATH_PAYMENT_METHODS, null);
        $res = array();

        foreach ($methods as $code => $methodConfig) {
            $prefix = self::XML_PATH_PAYMENT_METHODS.'/'.$code.'/';

            if (!$model = Mage::getStoreConfig($prefix.'model', null)) {
                continue;
            }

            $res[] = Mage::getModel($model);
        }

        return $res;
    }

}
