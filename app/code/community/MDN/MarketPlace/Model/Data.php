<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_MarketPlace_Model_Data extends Mage_Core_Model_Abstract {

    /**
     * Construtor
     */
    public function _construct() {
        parent::_construct();
        $this->_init('MarketPlace/Data');
    }

    /**
     * Get available products number
     *
     * @param string $marketplace
     * @return int
     */
    public function getAvailableProductsNumber($marketplace) {

        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $prefix = Mage::getConfig()->getTablePrefix();

        $select = $read->select()
                        ->from($prefix . 'market_place_data', array('mp_product_id'))
                        ->where('mp_marketplace_id = ?', $marketplace)
                        ->where('mp_reference IS NOT NULL');

        $results = $select->query()->fetchAll();

        return count($results);
    }

    /**
     * Is product created
     *
     * @param object $product
     * @param string $marketplace
     * @return boolean
     */
    public function isCreated($product, $marketplace) {

        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
        $prefix = Mage::getConfig()->getTablePrefix();

        $select = $read->select()
                        ->from($prefix . 'market_place_data', array('mp_marketplace_status'))
                        ->where('mp_product_id = ?', $product->getid())
                        ->where('mp_marketplace_id = ?', $marketplace);

        $res = $select->query()->fetchAll();

        return (count($res) > 0) && ($res[0]['mp_marketplace_status'] == "created");
    }

    /**
     * Is product pending
     *
     * @param object $product
     * @param string $marketplace
     * @return boolean
     */
    public function isPending($product, $marketplace) {

        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
        $prefix = Mage::getConfig()->getTablePrefix();

        $select = $read->select()
                        ->from($prefix . 'market_place_data', array('mp_marketplace_status'))
                        ->where('mp_product_id = ?', $product->getid())
                        ->where('mp_marketplace_id = ?', $marketplace);

        $res = $select->query()->fetchAll();

        return (count($res) > 0) && ($res[0]['mp_marketplace_status'] == "pending");
    }

    /**
     * Is product not created
     *
     * @param object $product
     * @param string $marketplace
     * @return boolean
     */
    public function isNotCreated($product, $marketplace) {

        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
        $prefix = Mage::getConfig()->getTablePrefix();

        $select = $read->select()
                        ->from($prefix . 'market_place_data', array('mp_marketplace_status'))
                        ->where('mp_product_id = ?', $product->getid())
                        ->where('mp_marketplace_id = ?', $marketplace);

        $res = $select->query()->fetchAll();

        return (count($res) > 0) && ($res[0]['mp_marketplace_status'] == NULL);
    }

    /**
     * Update status
     *
     * @param mixed array | int $ids
     * @param string $marketplace
     * @param string $status
     * @param string $mp_reference
     */
    public function updateStatus($ids, $marketplace, $status, $mp_reference = null) {

        if (!is_array($ids))
            $ids = array($ids);
        
        foreach ($ids as $id) {

            if($id == '' || $id === null)
                continue;

            $status = ($status == Mage::helper('MarketPlace/ProductCreation')->getStatusNotCreated()) ? new Zend_Db_Expr('null') : $status;

            $p = mage::getResourceModel('MarketPlace/Data_collection')
                            ->addFieldToFilter('mp_product_id', $id)
                            ->addFieldToFilter('mp_marketplace_id', $marketplace)
                            ->getFirstItem();
            
            if ($p->getmp_id()) {

                $p->setmp_marketplace_status($status);

                if ($mp_reference !== null) {
                    $p->setmp_reference($mp_reference);
                }

                $p->save();
            } else {

                $p = mage::getModel('MarketPlace/Data');
                $p->setmp_product_id($id)
                        ->setmp_marketplace_status($status)
                        ->setmp_marketplace_id(strtolower($marketplace))
                        ->setmp_exclude(0);

                if($mp_reference !== null){
                    $p->setmp_reference($mp_reference);
                }

                $p->save();
            }

            if(Mage::Helper(ucfirst($marketplace))->allowInternationalization()){
                Mage::getModel('MarketPlace/Status')->update($p, Mage::registry('country'), $status);
            }

        }
    }

    /**
     * Is free shipping enable for product
     *
     * @param object $product
     * @param string $mp
     * @return boolean
     */
    public function hasFreeShipping($product, $mp) {

        $obj = $this->getCollection()
                ->addFieldToFilter('mp_marketplace_id', $mp)
                ->addFieldToFilter('mp_product_id', $product->getid())
                ->getFirstItem();

        return ($obj->getmp_free_shipping() == 1);
    }

    /**
     * Get available products
     *
     * @param string $marketplace
     * @return collection
     */
    public function getAvailableProducts($marketplace) {

        $products = $this->getCollection()
                ->addFieldToFilter('mp_marketplace_id', $marketplace)
                ->addFieldToFilter('mp_marketplace_status', MDN_MarketPlace_Helper_ProductCreation::kStatusCreated);

        return $products;
    }

    /**
     * Force export
     *
     * @param <type> $id
     * @param <type> $mp
     * @return <type>
     */
    public function forceExport($id, $mp){

       $obj = $this->getCollection()
               ->addFieldToFilter('mp_marketplace_id', $mp)
               ->addFieldToFilter('mp_product_id', $id)
               ->getFirstItem();

       if($obj->getmp_id()){

           $obj->setmp_force_export(1)
                   ->save();

       }else{

           $obj = $this->setmp_product_id($id)
                       ->setmp_marketplace_id($mp)
                       ->setmp_force_export(1)
                       ->save();

       }

       return 0;

    }

    /**
     * Add message
     *
     * @param int $product_id
     * @param string $message
     * @param string $mp
     * @return int 0
     */
    public function addMessage($product_id, $message, $mp){

        $item = Mage::getModel('MarketPlace/Data')->getCollection()
                        ->addFieldToFilter('mp_product_id', $product_id)
                        ->addFieldToFilter('mp_marketplace_id', $mp)
                        ->getFirstItem();

        if($item->getmp_id()){

            $item->setmp_message($message)
                    ->save();

        }

        return 0;

    }

}