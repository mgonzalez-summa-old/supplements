<?php

/**
 * Description of Logs
 *
 * @author Nicolas Mugnier
 */
class MDN_MarketPlace_Model_Mysql4_Logs extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('MarketPlace/Logs', 'id');
    }
}
