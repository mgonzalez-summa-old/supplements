<?php
/**
 * Description of Collection
 *
 * @author Nicolas Mugnier
 */
class MDN_MarketPlace_Model_Mysql4_Token_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('MarketPlace/Token');
    }

    public function addAttributeToSort($attribute, $dir='asc')
    {
        if (!is_string($attribute)) {
            return $this;
        }
        $this->setOrder($attribute, $dir);
        return $this;
    }

}
