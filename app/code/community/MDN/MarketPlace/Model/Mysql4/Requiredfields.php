<?php

/**
 * Description of Logs
 *
 * @author Nicolas Mugnier
 */
class MDN_MarketPlace_Model_Mysql4_Requiredfields extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('MarketPlace/Requiredfields', 'mp_id');
    }
}
