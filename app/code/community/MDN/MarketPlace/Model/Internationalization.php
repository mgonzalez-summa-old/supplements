<?php
/* 
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MDN_MarketPlace_Model_Internationalization extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('MarketPlace/Internationalization');
    }

    public function getAssociationValue($store_id, $mp_name){

        $obj = $this->getCollection()
                ->addFieldToFilter('mpi_store_id', $store_id)
                ->addFieldToFilter('mpi_marketplace_id', $mp_name)
                ->getFirstItem();

        return (!$obj->getmpi_id()) ? '' : $obj->getmpi_language();

    }

    public function updateAssociation($marketplace, $store_id, $language){

        $obj = $this->getCollection()
                ->addFieldToFilter('mpi_marketplace_id', $marketplace)
                ->addFieldToFilter('mpi_store_id', $store_id)
                ->getFirstItem();

        if($obj->getmpi_id())
                $obj->delete();

        if(count($language) > 0){

            $this->setmpi_marketplace_id($marketplace)
                    ->setmpi_store_id($store_id)
                    ->setmpi_language(implode(",",$language))
                    ->save();

        }

        return true;

    }

    public function getStoreId($marketplace, $country){

        $obj = null;

        $collection = $this->getCollection()
                        ->addFieldToFilter('mpi_marketplace_id', $marketplace);

        foreach($collection as $item){

            if(in_array($country, explode(",",$item->getmpi_language()))){

                $obj = $item;
                break;

            }

        }

        return ($obj !== null) ? $obj->getmpi_store_id() : null;

    }

}
