<?php

class MDN_MarketPlace_Model_Requiredfields extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('MarketPlace/Requiredfields');
    }

    public function loadValueForPath($path, $marketplace){

        $retour = "";

        $requiredfield = $this->getCollection()
                            ->addFieldToFilter('mp_marketplace_id', $marketplace)
                            ->addFieldToFilter('mp_path', $path)
                            ->getFirstItem();

        if($requiredfield->getmp_id()){
            $retour = $requiredfield->getmp_attribute_name();
        }

        return $retour;

    }

    public function loadDefaultForPath($path, $mp){

        $retour = "";

        $requiredField = $this->getCollection()
                                ->addFieldToFilter('mp_marketplace_id', $mp)
                                ->addFieldToFilter('mp_path', $path)
                                ->getFirstItem();

        if($requiredField->getmp_id()){
            $retour = $requiredField->getmp_default();
        }

        return $retour;

    }

}