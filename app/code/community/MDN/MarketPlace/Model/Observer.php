<?php

/**
 * Description of Observer
 *
 * @author Nicolas Mugnier
 */
class MDN_MarketPlace_Model_Observer {

    /**
     * Get marketplaces orders
     *
     */
    public function getOrders() {

        Mage::helper('MarketPlace/Main')->cronImportOrders();
        Mage::helper('MarketPlace/Main')->cronSendTracking();

    }

    /**
     * Update marketplaces stocks
     *
     */
    public function updateStocks() {

        Mage::helper('MarketPlace/Main')->cronUpdateStocksAndPrices();

    }

    /**
     * Check Product creation
     *
     */
    public function checkProductCreation(){

        Mage::helper('MarketPlace/Main')->cronCheckProductCreation();

    }

}
