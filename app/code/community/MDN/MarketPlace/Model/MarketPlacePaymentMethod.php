<?php

/**
 * Description of MarketPlacePaymentMethod
 *
 * @author Mugnier Nicolas
 */
class MDN_MarketPlace_Model_MarketPlacePaymentMethod extends Mage_Payment_Model_Method_Abstract {

    protected $_code  = 'MarketPlacePaymentMethod';
    protected $_formBlockType = 'MarketPlace/MarketPlacePaymentMethod_form';
    protected $_infoBlockType = 'MarketPlace/MarketPlacePaymentMethod_info';

    public function getInformation() {
        return $this->getConfigData('information');
    }

    public function getAddress() {
        return $this->getConfigData('address');
    }
}
