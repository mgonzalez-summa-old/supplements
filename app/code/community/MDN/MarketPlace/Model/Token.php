<?php

class MDN_MarketPlace_Model_Token extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('MarketPlace/Token');
    }

    public function _afterSave(){

        $collection = $this->getCollection()
                        ->addFieldToFilter('mp_marketplace_id', $this->getmp_id())
                        ->setOrder('mp_id', 'desc');

        $i = 0;
        foreach($collection as $item){

            if($i > 50){
                $item->delete();
            }

            $i++;

        }

        return 0;

    }

}