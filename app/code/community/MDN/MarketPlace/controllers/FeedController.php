<?php

class MDN_MarketPlace_FeedController extends Mage_Adminhtml_Controller_Action {

    public function indexAction(){

        $this->LoadLayout();
        $this->renderLayout();

    }

    public function downloadFeedAction(){
        try{

            $id = $this->getRequest()->getParam('id');
            $type = $this->getRequest()->getParam('type');

            $feed = Mage::getModel('MarketPlace/Feed')->load($id);
            
            switch($type){
                case 'response':
                    $content = $feed->getmp_response();
                    break;
                case 'content':
                    $content = $feed->getmp_content();
                    break;
            }

            $this->_prepareDownloadResponse($type.'.txt', $content, 'text/plain');
        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage().' : '.$e->getTraceAsString());
            $this->_redirectRefeferer();
        }

    }

}
