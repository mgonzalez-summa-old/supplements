<?php

class MDN_MarketPlace_CategoriesController extends Mage_Adminhtml_Controller_Action {

    /**
     * Main screen
     */
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Edit association
     */
    public function EditAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Save associations
     */
    public function SaveAction() {
        //get data
        $data = $this->getRequest()->getPost();
        $marketPlace = $data['marketplace'];
        $categoryId = $data['category_id'];
        $associationdata = $data['association_data'];

        //serialize associationData & save
        $associationdata = mage::helper('MarketPlace/Serializer')->serializeObject($associationdata);
        $model = mage::getModel('MarketPlace/Category');
        $model->updateAssociation($marketPlace, $categoryId, $associationdata);

        //confirm & redirect
        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Data saved'));
        $this->_redirect('MarketPlace/Categories/index');
    }

    /**
     * Show categories select according to univers and marketplace
     */
    public function showCategoriesComboAction() {

        try {

            $mp = ucfirst($this->getRequest()->getParam('mp'));
            $univers = $this->getRequest()->getParam('univers');
            $html = Mage::helper($mp.'/Category')->getCategoriesAsCombo($univers);

            $this->getResponse()->setBody($html);

        } catch (Exception $e) {
            $this->getResponse()->setBody($e->getMessage().' : '.$e->getTraceAsString());
        }
    }

    /**
     * show sub categories select according to category and marketplace
     */
    public function showSubCategoriesComboAction(){

        try{

            $mp = ucfirst($this->getRequest()->getParam('mp'));
            $cat = $this->getRequest()->getParam('category');
            $html = Mage::helper($mp.'/Category')->getSubCategoriesAsCombo($cat);

            $this->getResponse()->setBody($html);
        }catch(Exception $e){
            $this->getResponse()->setBody($e->getMessage().' : '.$e->getTraceAsString());
        }

    }

    /**
     * Retrieve current category's marketplace reference
     */
    public function retrieveReferenceAction(){
        try{
            $mp = ucfirst($this->getRequest()->getParam('mp'));
            $cat = $this->getRequest()->getParam('selected');
            $html = Mage::helper($mp.'/Category')->getReference($cat);
            $this->getResponse()->setBody($html);
        }catch(Exception $e){
            $this->getResponse()->setBody($e->getMessage().' : '.$e->getTraceAsString());
        }
    }

}