<?php

/**
 * Description of LogsController
 *
 * @author Nicolas Mugnier
 */
class MDN_MarketPlace_LogsController extends Mage_Adminhtml_Controller_Action {

    /**
     * Main screen 
     */
    public function indexAction(){
        $this->loadLayout();
        $this->renderLayout();
    }
}
