<?php

class MDN_MarketPlace_ProductsController extends Mage_Adminhtml_Controller_Action {

    public function MassAddProductsAction(){

        try{

            $marketplace = ucfirst($this->getRequest()->getParam('mp'));

            if(Mage::Helper($marketplace)->allowProductCreation()){

                $helper = mage::helper($marketplace.'/ProductCreation');

                $helper->massProductCreation($this->getRequest());

                Mage::getSingleton('adminhtml/session')->addSuccess('Products submitted to '.$marketplace);

            }else{

                Mage::getSingleton('adminhtml/session')->addError('Actually, '.$marketplace.' doesn\'t allow product creation, it will be available in next release, you can use matching EAN action');

            }
            $this->_redirectReferer();

        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage().' : '.$e->getTraceAsString());
            $this->_redirectReferer();
        }
       
    }

    public function MassUpdateStatusAction(){
        try{
            $helper = mage::helper('MarketPlace/ProductCreation');
            $helper->updateStatus($this->getRequest());
            Mage::getSingleton('adminhtml/session')->addSuccess('Status updated.');
            $this->_redirectReferer();
        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage().' : '.$e->getTraceAsString());
            $this->_redirectReferer();
        }
    }

    public function MassGenerateProductFeedAction(){
        try{

            $mp = ucfirst($this->getRequest()->getParam('mp'));

            $helper = Mage::helper($mp.'/ProductCreation');
            if($helper->allowGenerateProductFeed()){
                $content = $helper->generateProductFeed($this->getRequest());
                $type = $helper->getProductFileType();
                $this->_prepareDownloadResponse('productFeed.'.$type, $content, 'text/'.$type);
            }else{
                Mage::getSingleton('adminhtml/session')->addSuccess('Not allowed.');
                $this->_redirectReferer();
            }
        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage().' : '.$e->getTraceAsString());
            $this->_redirectReferer();
        }
    }

    public function MassUpdateFreeShippingAction(){
        try{

            $mp = $this->getRequest()->getParam('mp');
            $value = $this->getRequest()->getParam('value');
            $ids = $this->getRequest()->getPost('product_ids');

            $ids = implode(',',$ids);

            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $prefix = Mage::getConfig()->getTablePrefix();

            $sql = 'UPDATE `'.$prefix.'market_place_data` SET mp_free_shipping = '.$value.' WHERE mp_product_id IN ('.$ids.')';

            $write->query($sql);

            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Free shipping status updated'));
            $this->_redirectReferer();


        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage().' : '.$e->getTraceAsString());
            $this->_redirectReferer();
        }
    }

    public function gridAjaxAction(){
        try{
            $mp = $this->getRequest()->getParam('mp');
            $country = $this->getRequest()->getParam('country');

            //echo $country;die();

            $block = $this->getLayout()->createBlock('MarketPlace/Products');
            $block->setMp($mp);
            $block->setCountry($country);
            $this->getResponse()->setBody(
                $block->toHtml()
            );

            //echo $block->getCountry();die();

        }catch(Exception $e){
            $this->getResponse()->setBody($e->getMessage().' : '.$e->getTraceAsString());
        }

    }

    /**
     * Mass update stock and price
     */
    public function MassUpdateStockPriceAction() {
        try {

            $mp = $this->getRequest()->getParam('mp');

            $helper = Mage::Helper(ucfirst($mp) . '/ProductUpdate');
            $helper->update($this->getRequest());

            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Price & stock exported'));
            $this->_redirectReferer();

        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage() . ' : ' . $e->getTraceAsString());
            $this->_redirectReferer();
        }
    }

    /**
     * Mass update image
     */
    public function MassUpdateImageAction() {
        try {

            $mp = $this->getRequest()->getParam('mp');
            $ids = $this->getRequest()->getPost('product_ids');
            $message = '';
            $products = array();
            $helper = Mage::Helper(ucfirst($mp) . '/ProductUpdate');

            $helper->updateImageFromGrid($ids);

            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Images exported'));
            $this->_redirectReferer();

        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage() . ' : ' . $e->getTraceAsString());
            $this->_redirectReferer();
        }
    }

    /**
     * Mass matching EAN
     */
    public function MassMatchingEanAction() {

        try {

            $mp = $this->getRequest()->getParam('mp');
            $ids = $this->getRequest()->getPost('product_ids');
            $message = '';
            $products = array();
            $errors = array();
            $helper = Mage::Helper(ucfirst($mp) . '/Matching');

            if (Mage::Helper(ucfirst($mp) . '/ProductCreation')->allowMatchingEan()) {

                foreach ($ids as $id) {

                    // load product
                    $product = Mage::getModel('Catalog/Product')->load($id);
                    $barcode = Mage::Helper('MarketPlace/Barcode')->getBarcodeForProduct($product);

                    if (Mage::Helper('MarketPlace/Checkbarcode')->checkCode($barcode) === true) {

                        $products[] = $product;
                    }else{

                        $errors[] = $id;

                    }
                }

                // ok
                if(count($products) > 0){
                    $mathingArray = $helper->buildMatchingArray($products);
                    $helper->Match($mathingArray);

                    $message = $this->__('Matching processing');
                }

                // errors
                if(count($errors) > 0){
                    Mage::getModel('MarketPlace/Data')->updateStatus($errors, Mage::helper('Amazon')->getMarketPlaceName(), MDN_MarketPlace_Helper_ProductCreation::kStatusInError);
                    foreach($errors as $id){
                        Mage::getModel('MarketPlace/Data')->addMessage($id, 'Invalid EAN code', Mage::Helper('Amazon')->getMarketPlaceName());
                    }
                    $message = 'Some products can\'t be matched. Please check EAN codes';
                    // add log
                    $log = $message .= ' : '.implode(',',$errors);
                    mage::getModel('MarketPlace/Logs')->addLog(
                        Mage::helper('Amazon')->getMarketPlaceName(),
                        22,
                        $log,
                        array(
                            'fileName' => NULL
                        )
                    );
                }

            } else {

                $message = $this->__(ucfirst($mp) . ' is not allowed to process matching EAN');
            }

            // redirect
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__($message));
            $this->_redirectReferer();
        } catch (Exception $e) {
            // redirect
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage() . ' : ' . $e->getTraceAsString());
            $this->_redirectReferer();
        }
    }

}
