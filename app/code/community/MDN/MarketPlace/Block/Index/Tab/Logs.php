<?php
/* 
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MDN_MarketPlace_Block_Index_Tab_Logs extends Mage_Adminhtml_Block_Widget_Grid{

    protected $_mp;

    public function setMp($mp){
        $this->_mp = $mp;
    }

    public function __construct(){

        parent::__construct();
        $this->setId('logs_grid');
        $this->_parentTemplate = $this->getTemplate();
        $this->setEmptyText('Aucun elt');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);

    }

    /**
     * Load collection
     *
     * @return unknown
     */
    protected function _prepareCollection() {
        //charge
        $collection = mage::getModel('MarketPlace/Logs')
                                ->getCollection()
                                ->addFieldToFilter('mp_marketplace', $this->_mp)
                                ->addAttributeToSort('mp_id', 'desc');

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

     /**
     * Grid configuration
     *
     * @return unknown
     */
    protected function _prepareColumns() {

        $this->addColumn('mp_id', array(
                'header'=> Mage::helper('sales')->__('Id'),
                'index' => 'mp_id'
        ));

        $this->addColumn('mp_date', array(
                'header'=> Mage::helper('sales')->__('Date'),
                'index' => 'mp_date'
        ));

        $this->addColumn('mp_is_error', array(
                'header'=> Mage::helper('sales')->__('Error'),
                'index' => 'mp_is_error',
                'type' => 'options',
                'options' => mage::helper('MarketPlace/Errors')->getErrorsOptionsFor($this->_mp)
        ));

        $this->addColumn('mp_message', array(
                'header'=> Mage::helper('sales')->__('Message'),
                'index' => 'mp_message',
                'width' => '800',
                'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Log_Message'
        ));

        $this->addColumn('mp_execution_time', array(
                'header'=> Mage::helper('MarketPlace')->__('Execution time (micro sec)'),
                'index' => 'mp_execution_time'
        ));

        return parent::_prepareColumns();

    }

    public function getGridParentHtml() {
        $templateName = Mage::getDesign()->getTemplateFilename($this->_parentTemplate, array('_relative'=>true));
        return $this->fetchView($templateName);
    }

    public function getGridUrl() {
        return $this->getUrl('MarketPlace/Logs/gridAjax', array('mp' => $this->_mp));
    }

}
