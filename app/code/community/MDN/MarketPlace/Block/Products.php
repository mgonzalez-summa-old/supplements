<?php

class MDN_MarketPlace_Block_Products extends Mage_Adminhtml_Block_Widget_Grid{

    private $mp;
    private $country;

    public function setCountry($value){
        $this->country = $value;
    }

    public function getCountry(){
        return $this->country;
    }

    public function setMp($value){
        $this->mp = $value;
    }

    public function getMp(){
        return $this->mp;
    }

    public function __construct(){

        parent::__construct();
        $this->setId('ProductsGrid');
        $this->_parentTemplate = $this->getTemplate();
        $this->setEmptyText('Aucun elt');
        $this->setSaveParametersInSession(true);
        //$this->setUseAjax(true);

    }

    protected function _prepareCollection(){

        //load collection
        $collection = mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('price')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('status')
                ->addAttributeToSelect('visibility')
                ->addAttributeToSelect('cost')
                ->addAttributeToSelect(mage::getStoreConfig('marketplace/general/manufacturer_attribute_name'))
                ->addAttributeToSelect('special_price')
                ->addAttributeToSelect('special_from_date')
                ->addAttributeToSelect('special_to_date')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->joinTable(
                    'MarketPlace/Data',
                    'mp_product_id=entity_id',
                    array(
                        'mp_reference'          => 'mp_reference',
                        'mp_exclude'            => 'mp_exclude',
                        'mp_force_qty'          => 'mp_force_qty',
                        'mp_delay'              => 'mp_delay',
                        'mp_marketplace_id'     => 'mp_marketplace_id',
                        'mp_marketplace_status' => 'mp_marketplace_status',
                        'mp_free_shipping'      => 'mp_free_shipping',
                        'mp_force_export'       => 'mp_force_export',
                        'mp_product_id'         => 'mp_product_id',
                        'mp_message'            => 'mp_message',
                        'mp_id'                 => 'mp_id'
                    ),
                    "mp_marketplace_id='".$this->getMp()."'",
                    'left'
                );

        // check if marketplace allow configurable products
        if(Mage::Helper(ucfirst($this->mp))->allowConfigurableProduct()){
            $collection->addFieldToFilter('type_id', array('in', array('simple', 'configurable')));
        }else{
            $collection->addFieldToFilter('type_id', 'simple');
        }

        // check if marketplace allow internationalization
        if(Mage::Helper(ucfirst($this->mp))->allowInternationalization()){
            $collection->joinTable(
                    'MarketPlace/Status',
                    'mps_product_id=mp_product_id',
                    array(
                        'mps_product_id' => 'mps_product_id',
                        'mps_marketplace_id' => 'mps_marketplace_id',
                        'mps_status' => 'mps_status',
                        'mps_country' => 'mps_country',
                        'mps_delay' => 'mps_delay'
                    ),
                    "mps_country='".$this->getCountry()."' AND mps_marketplace_id='".$this->getMp()."'",
                    'left'
                );
        }

        $collection->addExpressionAttributeToSelect(
                                'margin',
                                'round(({{price}} - {{cost}}) / {{price}} * 100, 2)',
                                array('price', 'cost')
                        );


        //add barcode attribute
        $collection = mage::helper('MarketPlace/Barcode')->addBarcodeAttributeToProductCollection($collection);

        $this->setCollection($collection);
        return parent::_prepareCollection();

    }

    protected function _prepareColumns(){

        // DISPLAY PRODUCT ID
        if(Mage::getStoreConfig('marketplace/general/grid_show_id') == 1){
        $this->addColumn('id', array(
                'header'=> Mage::helper('MarketPlace')->__('ID'),
                'index' => 'entity_id',
        ));
        }

        // DISPLAY PICTURE
        /*$this->addColumn('picture', array(
                'header' => Mage::Helper('MarketPlace')->__('Picture'),
                'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Picture',
                'filter' => false,
                'sort' => false
        ));*/

        // DISPLAY MANUFACTURER
        $this->addColumn('manufacturer', array(
                'header'  => Mage::helper('MarketPlace')->__('Manufacturer'),
                'index'   => mage::getStoreConfig('marketplace/general/manufacturer_attribute_name'),
                'type'    => 'options',
                'options' => $this->getManufacturerOptions()
        ));

        // DISPLAY SKU
        $this->addColumn('sku', array(
                'header'=> Mage::helper('MarketPlace')->__('Sku'),
                'index' => 'sku',
        ));


        // DISPLAY EAN
        $this->addColumn('ean', array(
                'header'    => Mage::helper('MarketPlace')->__('Ean'),
                'renderer'  => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Barcode',
                'filter'    => 'MarketPlace/Widget_Grid_Column_Filter_Barcode',
                'index'     => Mage::helper('MarketPlace/Barcode')->getCollectionBarcodeIndex(),
                'type'	    => 'number',
                'align'	    => 'center',
                'sort'      => false
        ));


        // DISPLAY NAME
        $this->addColumn('name', array(
                'header'=> Mage::helper('MarketPlace')->__('Product'),
                'index' => 'name'
        ));

        // ATTRIBUTE SET
        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
            ->load()
            ->toOptionHash();

        $this->addColumn('set_name',
            array(
                'header'=> Mage::helper('catalog')->__('Attrib. Set Name'),
                'width' => '100px',
                'index' => 'attribute_set_id',
                'type'  => 'options',
                'options' => $sets,
        ));

        // DISPLAY PRICE
        $this->addColumn('price', array(
                'header'   => Mage::helper('MarketPlace')->__('Price excl tax'),
                'index'    => 'price',
                'type'	   => 'price',
                'align'    => 'right',
                'currency_code' => Mage::getStoreConfig('currency/options/base'),
                'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Price'
        ));

        // DISPLAY MARGIN
        $this->addColumn('margin', array(
                'header'   => Mage::helper('MarketPlace')->__('Margin'),
                'index'    => 'margin',
                'align'    => 'right',
                'type'	   => 'number',
                'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Margin',
        ));

        // DISPLAY VISIBILITY
        $this->addColumn('visibility',
                array(
                'header'  => Mage::helper('MarketPlace')->__('Visibility'),
                'width'   => '150px',
                'index'   => 'visibility',
                'type'    => 'options',
                'align'   => 'center',
                'options' => Mage::getModel('catalog/product_visibility')->getOptionArray(),
        ));

        // DISPLAY STATUS
        $this->addColumn('status',
                array(
                'header'  => Mage::helper('MarketPlace')->__('Enabled'),
                'width'   => '70px',
                'index'   => 'status',
                'type'    => 'options',
                'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        ));

        // DISPLAY REFERENCE
        $this->addColumn('mp_reference', array(
                'header'   => Mage::helper('MarketPlace')->__('Reference '.$this->getMp()),
                'index'    => 'mp_reference',
                'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_EditorReference',
                'align'    => 'center',
                'width'    => '30px'
        ));

        // DISPLAY EXCLUDE ?
        $this->addColumn('mp_exclude', array(
                'header'    => Mage::helper('MarketPlace')->__('Exclude ?'),
                'index'     => 'mp_exclude',
                'renderer'  => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_EditorExclude',
                'align'     => 'center',
                'type'      => 'options',
                'sortable'  => false,
                'options'   => $this->getYesNoOptions(),
        ));

        // DISPLAY FORCE QUANTITY
        if(Mage::getSToreConfig('marketplace/general/grid_show_force_qty') == 1){
        $this->addColumn('mp_force_qty', array(
                'header'   => Mage::helper('MarketPlace')->__('Force Qty'),
                'index'    => 'mp_force_qty',
                'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_EditorForceQty',
                'align'    => 'center',
                    'width'    => '80px',
                'class'    => 'validate-zero-or-greater'
        ));
        }

        // DISPLAY DELAY
        if(Mage::getStoreConfig('marketplace/general/grid_show_force_delay') == 1){
        $this->addColumn('mp_delay', array(
                'header'   => Mage::helper('MarketPlace')->__('Delay'),
                'index'    => 'mp_delay',
                'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_EditorDelay',
                'align'    => 'center',
                    'width'    => '80px',
                'class'    => 'validate-greater-than-zero'
        ));
        }

        // FREE SHIPPING
        if(Mage::helper(ucfirst($this->getMp()))->allowFreeShipping()){
            $this->addColumn('mp_free_shipping', array(
               'header' => Mage::helper('MarketPlace')->__('Free Shipping'),
               'index' => 'mp_free_shipping',
               'type' => 'options',
               'options' => $this->getYesNoOptions(),
               'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_FreeShipping',
            ));
        }

        // DISPLAY STOCKS
        $this->addColumn('Stocks', array(
                'header'   => Mage::helper('MarketPlace')->__('Stocks'),
                'sortable' => false,
                'filter'   => false,
                'width' => '10px',
                'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Stocks'
        ));


        // DISPLAY LINK TO PRODUCT INFORMATIONS
        $this->addColumn('action',
                array(
                'header'    => Mage::helper('MarketPlace')->__('Action'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                        array(
                                'caption' => Mage::helper('MarketPlace')->__('View'),
                                'url'     => array('base'=>'adminhtml/catalog_product/edit/'),
                                'field'   => 'id'
                        )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));

        // DISPLAY STATUS
        $this->addColumn('add',
                array(
                    'header'     => Mage::helper('MarketPlace')->__('Status'),
                    'index'      => 'mp_marketplace_status',
                    'renderer'   => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_ProductStatus' ,
                    'filter'     => 'MDN_MarketPlace_Block_Widget_Grid_Column_Filter_Status',
                    'sortable'   => true
        ));

        // EXPORT
        $this->addExportType('MarketPlace/Manual/exportCsv/marketplace/'.$this->getMp().'/country/'.$this->getCountry(), Mage::helper('customer')->__('CSV') );

        // allow other modules to add some columns
        //Mage::dispatchEvent('marketplace_products', array('grid'=>$this));

        return parent::_prepareColumns();

    }

    public function getGridParentHtml(){
        $templateName = Mage::getDesign()->getTemplateFilename($this->_parentTemplate, array('_relative'=>true));
        return $this->fetchView($templateName);
    }

    public function getManufacturerOptions(){

        $retour = array();

        $product = Mage::getModel('catalog/product');
        $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                  ->setEntityTypeFilter($product->getResource()->getTypeId())
                  ->addFieldToFilter('attribute_code', mage::getStoreConfig('marketplace/general/manufacturer_attribute_name'));
        $attribute = $attributes->getFirstItem()->setEntity($product->getResource());
        $manufacturers = $attribute->getSource()->getAllOptions(false);

        foreach($manufacturers as $manufacturer){
            $retour[$manufacturer['value']] = $manufacturer['label'];
        }

        return $retour;

    }

    public function getYesNoOptions(){
        return array('0'=>'No', '1'=>'Yes');
    }

    protected function _prepareMassaction(){

        $this->setMassactionIdField('mp_marketplace_id');
        $this->getMassactionBlock()->setFormFieldName('product_ids');

        if(Mage::helper(ucfirst($this->getMp()).'/ProductCreation')->allowGenerateProductFeed()){
            $this->getMassactionBlock()->addItem('generate_product_file', array(
                 'label'=> Mage::helper('MarketPlace')->__('Generate product feed'),
                 'url'  => $this->getUrl('MarketPlace/Products/MassGenerateProductFeed', array('mp' => $this->getMp(), 'country'=>$this->getCountry())),
            ));
        }

        if(Mage::helper(ucfirst($this->getMp()))->allowFreeShipping()){

            $this->getMassactionBlock()->addItem('add_free_shipping', array(
                 'label'=>Mage::helper('MarketPlace')->__('Enable free shipping'),
                 'url' => $this->getUrl('MarketPlace/Products/MassUpdateFreeShipping', array('mp'=>$this->getMp(), 'country'=> $this->getCountry(), 'value'=>'1'))
            ));

            $this->getMassactionBlock()->addItem('remove_free_shipping', array(
                 'label'=>Mage::helper('MarketPlace')->__('Disable free shipping'),
                 'url' => $this->getUrl('MarketPlace/Products/MassUpdateFreeShipping', array('mp'=>$this->getMp(), 'country'=>$this->getCountry(), 'value'=>'0'))
            ));
        }

        if(Mage::helper(ucfirst($this->getMp()))->allowManualUpdate()){
            $this->getMassactionBlock()->addItem('update_stock_price', array(
                'label' => Mage::helper('MarketPlace')->__('Update stock & price'),
                'url' => $this->getUrl('MarketPlace/Products/MassUpdateStockPrice', array('mp' => $this->getMp())),
                'confirm' => Mage::helper('MarketPlace')->__('Are you sure ?')
            ));

            $this->getMassactionBlock()->addItem('update_image', array(
                'label' => Mage::helper('MarketPlace')->__('Update image'),
                'url' => $this->getUrl('MarketPlace/Products/MassUpdateImage', array('mp' => $this->getMp())),
                'confirm' => Mage::helper('MarketPlace')->__('Are you sure ?')
            ));
        }

        if(Mage::helper(ucfirst($this->getMp()).'/ProductCreation')->allowMatchingEan()){
            $this->getMassactionBlock()->addItem('matching_ean', array(
               'label' => Mage::helper('MarketPlace')->__('Matching EAN'),
                'url' => $this->getUrl('MarketPlace/Products/MassMatchingEAN', array('mp' => $this->getMp()))
            ));
        }

        $this->getMassactionBlock()->addItem('add_selection', array(
             'label'=> Mage::helper('MarketPlace')->__('Add to marketplace'),
             'url'  => $this->getUrl('MarketPlace/Products/MassAddProducts', array('mp' => $this->getMp(), 'country' => $this->getCountry())),
        ));

        $this->getMassactionBlock()->addItem('set_to_notCreated', array(
             'label'=> Mage::helper('MarketPlace')->__('Set as not created'),
             'url'  => $this->getUrl('MarketPlace/Products/MassUpdateStatus', array('mp' => $this->getMp(), 'country'=>$this->getCountry(), 'status'=>Mage::helper('MarketPlace/ProductCreation')->getStatusNotCreated())),
             'confirm' => Mage::helper('MarketPlace')->__('Are you sure ?')
        ));

        $this->getMassactionBlock()->addItem('set_to_pending', array(
             'label'=> Mage::helper('MarketPlace')->__('Set as pending'),
             'url'  => $this->getUrl('MarketPlace/Products/MassUpdateStatus', array('mp' => $this->getMp(), 'country'=>$this->getCountry(), 'status'=>Mage::helper('MarketPlace/ProductCreation')->getStatusPending())),
             'confirm' => Mage::helper('MarketPlace')->__('Are you sure ?')
        ));

        $this->getMassactionBlock()->addItem('set_to_created', array(
             'label'=> Mage::helper('MarketPlace')->__('Set as created'),
             'url'  => $this->getUrl('MarketPlace/Products/MassUpdateStatus', array('mp' => $this->getMp(), 'country'=>$this->getCountry(), 'status'=>Mage::helper('MarketPlace/ProductCreation')->getStatusCreated())),
             'confirm' => Mage::helper('MarketPlace')->__('Are you sure ?')
        ));

        return $this;
    }

    /*public function getGridUrl()
    {
        return $this->getUrl('MarketPlace/Products/GridAjax', array('_current'=>true, 'mp'=>$this->getMp(), 'country'=>$this->getCountry()));

    }*/

}
