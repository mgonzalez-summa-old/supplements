<?php

class MDN_MarketPlace_Block_Orders extends Mage_Adminhtml_Block_Widget_Grid {

    /**
     * Constructor
     */
    public function __construct() {

        parent::__construct();
        $this->setId('OrdersGrid');
        $this->_parentTemplate = $this->getTemplate();
        $this->setEmptyText('Aucun elt');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare collection
     *
     * @return <type>
     */
    protected function _prepareCollection() {

        $collection = $this->getOrdersCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Get orders collection
     *
     * @return collection $collection
     */
    public function getOrdersCollection() {

        $marketplaces = array();
        $collection = null;
        
        $helpers = mage::helper('MarketPlace')->getHelpers();
        $prefix = Mage::getConfig()->getTablePrefix();

        if(count($helpers) > 0){

            for($i=0; $i<count($helpers);$i++){
                if(Mage::helper($helpers[$i])->isDisplayedInSalesOrderSummary()){
                    $marketplaces[$i] = strtolower(Mage::helper($helpers[$i])->getMarketPlaceName());
                }
                
            }

            if (Mage::helper('MarketPlace/FlatOrder')->isFlatOrder()) {

                $collection = mage::getModel('sales/order')
                                ->getCollection();
                $collection->addFieldToFilter('from_site', array('in', $marketplaces));
                $collection->getSelect()->joinLeft(array('s1' => $prefix.'sales_flat_order_address'), 'main_table.shipping_address_id = s1.entity_id', array('shipping_name'=>'firstname', 'lastname'));
                $collection->getSelect()->joinLeft(array('s2' => $prefix.'sales_flat_order_address'), 'main_table.billing_address_id = s2.entity_id', array('billing_name' => 'firstname', 'lastname'));
                //$collection->getSelect()->columns(new Zend_Db_Expr("CONCAT(s2.firstname, ' ',s2.lastname) AS billing_name"));
                //$collection->getSelect()->columns(new Zend_Db_Expr("CONCAT(s1.firstname, ' ',s1.lastname) AS shipping_name"));
            } else {

                $collection = mage::getModel('sales/order')
                        ->getCollection()
                        ->addAttributeToSelect('*')
                        ->addFieldToFilter('from_site', array('in', $marketplaces))
                        ->joinAttribute('billing_firstname', 'order_address/firstname', 'billing_address_id', null, 'left')
                        ->joinAttribute('billing_lastname', 'order_address/lastname', 'billing_address_id', null, 'left')
                        ->joinAttribute('billing_company', 'order_address/company', 'billing_address_id', null, 'left')
                        ->joinAttribute('shipping_firstname', 'order_address/firstname', 'shipping_address_id', null, 'left')
                        ->joinAttribute('shipping_lastname', 'order_address/lastname', 'shipping_address_id', null, 'left')
                        ->joinAttribute('shipping_company', 'order_address/company', 'shipping_address_id', null, 'left')
                        ->addExpressionAttributeToSelect('billing_name',
                                'CONCAT({{billing_firstname}}, " ", {{billing_lastname}}, " (", {{billing_company}}, ")")',
                                array('billing_firstname', 'billing_lastname', 'billing_company'))
                        ->addExpressionAttributeToSelect('shipping_name',
                                'CONCAT({{shipping_firstname}}, " ", {{shipping_lastname}}, " (", {{shipping_company}}, ")")',
                                array('shipping_firstname', 'shipping_lastname', 'shipping_company'));
            }

            $collection->addAttributeToSort('entity_id', 'desc');
        }
        
        return $collection;
    }

    /**
     * Prepare columns
     */
    protected function _prepareColumns() {

        // ORDER ID
        $this->addColumn('increment_id', array(
            'header' => mage::helper('sales')->__('Order #'),
            'index' => 'increment_id'
        ));

        // CREATED AT
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));

        // BILLING NAME
        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));

        // SHIPPING NAME
        $this->addColumn('shipping_name', array(
            'header' => Mage::helper('sales')->__('Ship to Name'),
            'index' => 'shipping_name',
        ));

        // GRAND TOTAL
        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type' => 'currency',
            'currency' => 'order_currency_code',
        ));

        // STATUS
        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        // FROM SITE
        $this->addColumn('from_site', array(
            'header' => Mage::helper('MarketPlace')->__('From site'),
            'index' => 'from_site',
            'type' => 'options',
            'options' => Mage::helper('MarketPlace')->getMarketPlaceOptions()
        ));

        // MARKETPLACE ORDER ID
        $this->addColumn('marketplace_order_id', array(
            'header' => Mage::helper('MarketPlace')->__('Marketplace order id'),
            'index' => 'marketplace_order_id',
        ));

        // DISPLAY LINK TO PRODUCT INFORMATIONS
        $this->addColumn('action',
                array(
                    'header' => Mage::helper('MarketPlace')->__('Action'),
                    'width' => '50px',
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('MarketPlace')->__('View'),
                            'url' => array('base' => 'adminhtml/sales_order/view/'),
                            'field' => 'order_id'
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
        ));
    }

    /**
     * Get grid parent html
     *
     * @return <type>
     */
    public function getGridParentHtml() {
        $templateName = Mage::getDesign()->getTemplateFilename($this->_parentTemplate, array('_relative' => true));
        return $this->fetchView($templateName);
    }

    /**
     * Get row url
     *
     * @param object $row
     * @return string
     */
    public function getRowUrl($row) {
        return $this->getUrl('adminhtml/sales_order/view', array('order_id' => $row->getId()));
    }

}
