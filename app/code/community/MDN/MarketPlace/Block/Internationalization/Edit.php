<?php
/* 
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MDN_MarketPlace_Block_Internationalization_Edit extends Mage_Adminhtml_Block_Widget_Form {

    private $_store = null;

    public function getInternationalizationForm(){

        try{

            $name = 'associated_data';
            $value = Mage::getModel('MarketPlace/Internationalization')->getAssociationValue($this->getStore()->getstore_id(), $this->getMarketPlace()->getMarketPlaceName());
            $html = $this->getMarketPlace()->getInternationalizationForm($name, $value);

        }catch(Exception $e){
            $html = '<span style="color:red;">'.$e->getMessage().'</span>';
        }

        return $html;

    }

    public function getMarketPlace(){
        $marketplace_id = $this->getRequest()->getParam('marketplace_id');
        return Mage::helper('MarketPlace')->getHelperByName($marketplace_id);
    }

    public function getStore(){

        if($this->_store == null){
            $store_id = $this->getRequest()->getParam('store_id');
            $this->_store = Mage::getModel('core/store')->load($store_id);
        }
        
        return $this->_store;
    }

}
