<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_MarketPlace_Block_Categories_Edit extends Mage_Adminhtml_Block_Widget_Form
{
	private $_category = null;

	/** 
	 * Return html controls to set up category association
	 */
	public function getCategoryForm()
	{
		$html = '';
		
		try
		{
			$name = 'association_data';
			$value = mage::getModel('MarketPlace/Category')->getAssociationValue($this->getCategory()->getId(), $this->getMarketPlace()->getMarketPlaceName());
			$html = $this->getMarketPlace()->getCategoryForm($name, $value);
		}
		catch(Exception $ex)
		{
			$html = '<font color="red">'.$ex->getMessage().'</font>';
		}

		return str_replace('value="Health-HealthMisc"', 'value="Health-HealthMisc" selected ', str_replace('10781141','10781141" selected ', $html));
	}
	
	/**
	 * Return current category
	 */
	public function getCategory()
	{
		if ($this->_category == null)
		{
			$categoryId = $this->getRequest()->getParam('category_id');
			$this->_category = mage::getModel('catalog/category')->load($categoryId);
		}
		return $this->_category;
	}
	
	/**
	 * Return current market place helper
	 */
	public function getMarketPlace()
	{
		$marketPlaceName = $this->getRequest()->getParam('marketplace');
		return mage::helper('MarketPlace')->getHelperByName($marketPlaceName);
	}
	
	/**
	 * Return category tree
	 */
	public function getCategoryInformation()
	{
		$html = '';
		$category = $this->getCategory();
		$t = explode('/', $category->getPath());
		$indent = 0;
		$isFirst = true;
		foreach ($t as $cat)
		{
			if ($isFirst)
			{
				$isFirst = false;
				continue;
			}
			$cat = mage::getModel('catalog/category')->load($cat);
			$indentText = '';
			for ($i=0;$i<=$indent;$i++)
				$indentText .= '&nbsp;';
			$html .= $indentText.'| '.$cat->getName().'<br>';
			$indent += 5;
		}
		return $html;
	}
}