<?php

/**
 * Description of LogsGrid
 *
 * @author Nicolas Mugnier
 */
class MDN_MarketPlace_Block_Feed_FeedGrid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {

        parent::__construct();
        $this->setId('Feeds');
        $this->_parentTemplate = $this->getTemplate();
        $this->setEmptyText('Aucun elt');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Load collection
     *
     * @return unknown
     */
    protected function _prepareCollection() {
        //charge
        $collection = mage::getModel('MarketPlace/Feed')->getCollection()->setOrder('mp_id', 'DESC');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

     /**
     * Grid configuration
     *
     * @return unknown
     */
    protected function _prepareColumns() {

        $this->addColumn('mp_id', array(
                'header'=> Mage::helper('sales')->__('Id'),
                'index' => 'mp_id'
        ));

        $this->addColumn('mp_feed_id', array(
                'header'=> Mage::helper('MarketPlace')->__('Feed id'),
                'index' => 'mp_feed_id'
        ));

        $this->addColumn('mp_marketplace_id', array(
                'header'=> Mage::helper('MarketPlace')->__('Marketplace'),
                'index' => 'mp_marketplace_id',
                'type'  => 'options',
                'options' => Mage::helper('MarketPlace')->getMarketPlaceOptions()
        ));

        $this->addColumn('mp_status', array(
                'header'=> Mage::helper('MarketPlace')->__('Status'),
                'index' => 'mp_status',
                'type' => 'options',
                'options' => Mage::helper('MarketPlace/Feed')->getFeedStatusOptions()
        ));

        $this->addColumn('mp_type', array(
                'header'=> Mage::helper('MarketPlace')->__('Type'),
                'index' => 'mp_type',
                'type' => 'options',
                'options' => Mage::helper('MarketPlace/Feed')->getFeedTypeOptions()
        ));

        $this->addColumn('mp_content', array(
                'header' => Mage::helper('MarketPlace')->__('Content'),
                'index' => 'mp_content',
                'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Feed_Content',
                'filter' => false
        ));

        $this->addColumn('mp_response', array(
                'header' => Mage::helper('MarketPlace')->__('Response'),
                'index' => 'mp_response',
                'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Feed_Response',
                'filter' => false
        ));

        $this->addColumn('mp_date', array(
                'header'=> Mage::helper('MarketPlace')->__('Date'),
                'index' => 'mp_date'
        ));

        /*$this->addColumn('mp_country', array(
                'header' => Mage::Helper('MarketPlace')->__('Country'),
                'index' => 'mp_country'
        ));*/

        return parent::_prepareColumns();

    }

    public function getGridParentHtml() {
        $templateName = Mage::getDesign()->getTemplateFilename($this->_parentTemplate, array('_relative'=>true));
        return $this->fetchView($templateName);
    }

}
