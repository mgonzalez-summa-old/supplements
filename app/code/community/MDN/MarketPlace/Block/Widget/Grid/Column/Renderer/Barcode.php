<?php

class MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Barcode extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{

    public function render(Varien_Object $row)
    {
        $productId = $row->getId();
        $retour = mage::helper('MarketPlace/Barcode')->getBarcodeForProduct($row);
        return $retour;
    }

    public function renderExport(Varien_Object $row){

    }
}
