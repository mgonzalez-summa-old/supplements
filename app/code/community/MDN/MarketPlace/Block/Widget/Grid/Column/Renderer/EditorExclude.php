<?php

class MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_EditorExclude
	extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
    	$name='data['.$row->getId().'][mp_exclude]';

        if($row->getmp_exclude() == "" || $row->getmp_reference() == ""){
            $html = '<select disabled name="'.$name.'" id="'.$name.'">';
            $html .= '<option value="0">No</option>';
            $html .= '<option value="1" selected >Yes</option>';
            $html .= '</select>';
        }
        else{
            $html = '<select name="'.$name.'" id="'.$name.'">';
            if($row->getmp_exclude() == 1){
                $html .= '<option value="0">No</option>';
                $html .= '<option value="1" selected >Yes</option>';
            }
            else{
                $html .= '<option value="0" selected >No</option>';
                $html .= '<option value="1">Yes</option>';
            }
            $html .= '</select>';
        }


        return $html;
    }

    public function renderExport(Varien_Object $row)
    {
    	return $row->getmp_exclude();
    }
}