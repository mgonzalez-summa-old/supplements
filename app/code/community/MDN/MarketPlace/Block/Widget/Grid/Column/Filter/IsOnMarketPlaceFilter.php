<?php
class MDN_MarketPlace_Block_Widget_Grid_Column_Filter_IsOnMarketPlaceFilter extends Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Select
{
    protected function _getOptions()
    {
    	$retour = array();
    	$retour[] = array('label' => '', 'value' => '');
    	$retour[] = array('label' => 'Yes', 'value' => '1');
        $retour[] = array('label' => 'No', 'value' => '0');
        return $retour;
    }

    public function getCondition()
    {
        if ($this->getValue()) {
            return array(
                'notnull'=> 0
                );
        }
        else{
            return array(
                'null' => 0
            );
        }
        
    }

}