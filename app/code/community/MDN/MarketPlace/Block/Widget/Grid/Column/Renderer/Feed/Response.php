<?php

class MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Feed_Response extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row){

        $html = "";

        $response = $row->getmp_response();
        if($response !== NULL && $response !== ""){
            $html .= '<a href="'.mage::helper('adminhtml')->getUrl('MarketPlace/Feed/downloadFeed', array('id'=>$row->getmp_id(), 'type'=>'response')).'">'.$this->__('Download').'</a>';
        }else{
            $html .= $this->__('Unavailable');
        }

        return $html;

    }

    public function renderExport(Varien_Object $row){

    }


}
