<?php

class MDN_MarketPlace_Block_Widget_Grid_Column_Filter_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Select{

    public function _getOptions(){

        $retour = array();
        $tmp = mage::helper('MarketPlace/ProductCreation')->getStatus();
        
        $retour[] = array('label'=>'', 'value'=>'');
        foreach($tmp as $k => $v){
            
            $retour[] = array('label'=>$this->__($k), 'value'=>$v);

        }
        
        return $retour;

    }

    public function getCondition(){

        if($this->getValue() == 'notCreated'){
            return array(
                    'null' => 0
                );
        }
        else{
            return array(
                    'eq' => $this->getValue()
                    );
        }

    }

}
