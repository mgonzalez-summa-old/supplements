<?php
/**
 * Description of Stocks
 *
 * @author Nicolas Mugnier
 */
class MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Stocks extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        // TODO : find another method (trop lourd)
        $qty = Mage::getModel('cataloginventory/stock_item')->loadByProduct($row)->getQty();
        return number_format($qty);
        
        //return $row->getStockItem()->getQty();
    }

    public function renderExport(Varien_Object $row) {

        $qty = Mage::getModel('cataloginventory/stock_item')->loadByProduct($row)->getQty();
        return number_format($qty);

        //return $row->getStockItem()->getQty();
    }

}
