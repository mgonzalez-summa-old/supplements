<?php
/**
 * Description of Message
 *
 * @author Nicolas Mugnier
 */
class MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Log_Message
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

       $color = ($row->getmp_is_error() == 0 || $row->getmp_is_error() == 2) ? 'green' : 'red';
       return '<span style="color:'.$color.'">'.$row->getmp_message().'</span>';
    }

    public function renderExport(Varien_Object $row) {
        return $row->getmp_message();
    }

}
