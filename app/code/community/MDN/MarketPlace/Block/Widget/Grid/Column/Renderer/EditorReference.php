<?php

class MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_EditorReference
	extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
    	$name='data['.$row->getId().'][mp_reference]';
    	$value = $row->getmp_reference();
        
        $html = '<input type="text" size="15" value="'.$value.'" name="'.$name.'" id="'.$name.'"/>';

        return $html;
    }

    public function renderExport(Varien_Object $row)
    {
    	return $row->getmp_reference();
    }

}