<?php
/**
 * Description of Stocks
 *
 * @author Nicolas Mugnier
 */
class MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_AddProductLink extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $html = "";

        $marketplace = ucfirst($row->getmp_marketplace_id());

        if($row->getmp_reference() == ""){

            $id = $row->getid();
            $url = Mage::helper('adminhtml')->getUrl('MarketPlace/Amazon/createProduct', array('id' => $id));
            $html .= '<a href="'.$url.'">Add</a>';

        }
        else
            $html .= "Already exists";

        return $html;

    }

    public function renderExport(Varien_Object $row) {
        return "";
    }

}
