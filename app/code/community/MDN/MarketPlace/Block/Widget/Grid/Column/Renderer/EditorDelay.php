<?php

class MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_EditorDelay
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $name='data['.$row->getId().'][mp_delay]';
        $value = $row->getmp_delay();
        $html = '<input class="validate-greater-than-zero" type="text" size="4" value="'.$value.'" name="'.$name.'" id="'.$name.'">';
        return $html;
    }

    public function renderExport(Varien_Object $row) {
        return $row->getmp_delay();
    }

}