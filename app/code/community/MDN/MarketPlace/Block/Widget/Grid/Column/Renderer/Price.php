<?php
/**
 * Description of Price
 *
 * @author Nicolas Mugnier
 */
class MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Price
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $html = "";

        $price = mage::helper('MarketPlace/Product')->getPrice($row);

        $price = str_replace(',', '.', $price);

        $color = ($price == $row->getprice()) ? 'black' : 'blue';

        $currency = mage::getStoreConfig('currency/options/base');

        $special = ($price == $row->getPrice()) ? false : true;

        $price = round($price, 2);

        if(!$special){
            $html = $price.' '.$currency;
        }
        else{
            $rowprice = round($row->getprice(), 2);
            $html = $rowprice.' '.$currency.'<br/><span style="color:'.$color.'"> ('.$price.' '.$currency.')</span>';
        }

        return $html;
    }

    public function renderExport(Varien_Object $row) {

        return mage::helper('MarketPlace/Product')->getPrice($row);

    }

}

