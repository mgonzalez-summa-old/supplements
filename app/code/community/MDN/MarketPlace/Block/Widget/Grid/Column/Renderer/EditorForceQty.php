<?php

class MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_EditorForceQty
	extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
    	$name='data['.$row->getId().'][mp_force_qty]';
    	$value = $row->getmp_force_qty();
    	$html = '<input type="text" size="4" value="'.$value.'" name="'.$name.'" id="'.$name.'">';
		return $html;
    }

    public function renderExport(Varien_Object $row)
    {
    	return $row->getmp_force_qty();
    }
}