<?php

class MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_SalesOrderIncrementId
	extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
		return $row->getincrement_id();
    }

    public function renderExport(Varien_Object $row){
        return $row->getincrement_id();
    }
}