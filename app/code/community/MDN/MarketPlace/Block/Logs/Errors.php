<?php
/* 
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MDN_MarketPlace_Block_Logs_Errors extends Mage_Adminhtml_Block_Widget{

    protected $_mp;

    public function setMp($mp){
        $this->_mp = $mp;
    }

    public function getErrors(){

        $timestamp = Mage::getModel('core/date')->timestamp() - 3600 * 24;
        
        //charge
        $collection = mage::getModel('MarketPlace/Logs')
                                ->getCollection()
                                ->addFieldToFilter('mp_is_error', array('nin'=>array(0,1,2)))
                                ->addFieldToFilter('mp_marketplace', $this->_mp)
                                ->addAttributeToSort('mp_id', 'desc')
                                ->addFieldToFilter('mp_date', array('gt'=>date('Y-m-d H:i:s', $timestamp)));

        $collection->getSelect()->limit(5);

        return $collection;

    }

}
