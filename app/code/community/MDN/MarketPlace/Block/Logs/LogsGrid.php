<?php

/**
 * Description of LogsGrid
 *
 * @author Nicolas Mugnier
 */
class MDN_MarketPlace_Block_Logs_LogsGrid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('Logs');
        $this->_parentTemplate = $this->getTemplate();
        $this->setEmptyText('Aucun elt');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Load collection
     *
     * @return unknown
     */
    protected function _prepareCollection() {
        //charge
        $collection = mage::getModel('MarketPlace/Logs')->getCollection()->addAttributeToSort('mp_id', 'desc');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

     /**
     * Grid configuration
     *
     * @return unknown
     */
    protected function _prepareColumns() {

        $this->addColumn('mp_id', array(
                'header'=> Mage::helper('sales')->__('Id'),
                'index' => 'mp_id'
        ));

        $this->addColumn('mp_date', array(
                'header'=> Mage::helper('sales')->__('Date'),
                'index' => 'mp_date'
        ));

        $this->addColumn('mp_marketplace', array(
                'header'=> Mage::helper('sales')->__('Marketplace'),
                'index' => 'mp_marketplace',
                'type'  => 'options',
                'options' => Mage::helper('MarketPlace')->getMarketPlaceOptions()
        ));

        $this->addColumn('mp_is_error', array(
                'header'=> Mage::helper('sales')->__('Error'),
                'index' => 'mp_is_error',
                'type' => 'options',
                'options' => mage::helper('MarketPlace/Errors')->getErrorsOptions()
        ));

        $this->addColumn('mp_message', array(
                'header'=> Mage::helper('sales')->__('Message'),
                'index' => 'mp_message',
                'width' => '800',
                'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Log_Message'
        ));

        $this->addColumn('mp_execution_time', array(
                'header'=> Mage::helper('MarketPlace')->__('Execution time'),
                'index' => 'mp_execution_time'
        ));

        return parent::_prepareColumns();

    }

    public function getGridParentHtml() {
        $templateName = Mage::getDesign()->getTemplateFilename($this->_parentTemplate, array('_relative'=>true));
        return $this->fetchView($templateName);
    }

}
