<?php

class MDN_Amazon_Block_Index_Tab_General extends Mage_Adminhtml_Block_Widget {

    public function __construct(){

        parent::__construct();
        $this->setHtmlId('general');
        $this->setTemplate('Amazon/Index/Tab/General.phtml');

    }

    protected function _prepareLayout()
    {

        // product grid
        $block = $this->getLayout()->createBlock('MarketPlace/Products');
        $block->setMp('amazon');
        $block->setTemplate('MarketPlace/Products.phtml');

        $this->setChild('marketplace_products',
                $block
        );

        // last logs table
        $lastErrorsBlock = $this->getLayout()->createBlock('MarketPlace/Logs_Errors');
        $lastErrorsBlock->setMp('amazon');
        $lastErrorsBlock->setTemplate('MarketPlace/Logs/Errors.phtml');
        $this->setChild('marketplace_last_errors',$lastErrorsBlock);

    }

}
