<?php

class MDN_Amazon_Block_Index_Tab_Debug extends Mage_Adminhtml_Block_Widget {

    public function __construct(){

        parent::__construct();
        $this->setHtmlId('debug');
        $this->setTemplate('Amazon/Index/Tab/Debug.phtml');

    }

}

?>
