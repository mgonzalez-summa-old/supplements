<?php

require_once ('MDN/Marketplace/Amazon/Parser/parser/ParseAmazonRequiredFields.class.php');

class MDN_Amazon_Block_Index_Tab_RequiredFields extends Mage_Adminhtml_Block_Widget{

    public function __construct(){

        parent::__construct();
        $this->setHtmlId('requiredFields');
        $this->setTemplate('Amazon/Index/Tab/RequiredFields.phtml');
        $this->categories = Mage::helper('Amazon/XSD')->getAllCategories(true);

    }

    protected function _prepareLayout()
    {


    }

}

?>
