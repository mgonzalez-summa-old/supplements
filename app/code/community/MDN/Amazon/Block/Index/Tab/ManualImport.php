<?php
/* 
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MDN_Amazon_Block_Index_Tab_ManualImport extends Mage_Adminhtml_Block_Widget {

    public function __construct(){

        parent::__construct();
        $this->setHtmlId('manual_import');
        $this->setTemplate('Amazon/Index/Tab/ManualImport.phtml');

    }

}
