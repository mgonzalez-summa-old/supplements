<?php

class MDN_Amazon_Block_Index_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {

        parent::__construct();
        $this->setId('amazon_index_tab');
        $this->setDestElementId('amazon_index_tab_content');
        $this->setTemplate('widget/tabshoriz.phtml');

    }

    protected function _beforeToHtml(){

        $this->addTab('general', array(
            'label' => Mage::helper('MarketPlace')->__('General'),
            'content' => $this->getLayout()->createBlock('Amazon/Index_Tab_General')->toHtml(),
            'active' => true
        ));

         $this->addTab('manual_import', array(
           'label' => Mage::Helper('MarketPlace')->__('Manual import'),
            'content' => $this->getLayout()->createBlock('Amazon/Index_Tab_ManualImport')->toHtml()
        ));

        $logsBlock = $this->getLayout()->createBlock('MarketPlace/Index_Tab_logs');
        $logsBlock->setmp('amazon');
        $this->addTab('logs_grid', array(
            'label' => Mage::helper('MarketPlace')->__('Logs'),
            'content' => $logsBlock->toHtml()
        ));

        $this->addTab('requiredFields', array(
            'label' => Mage::helper('MarketPlace')->__('Required Fields'),
            'content' => $this->getLayout()->createBlock('Amazon/Index_Tab_RequiredFields')->toHtml()
        ));

        if (mage::getStoreConfig('marketplace/general/debug_mode')){
            $this->addTab('debug', array(
                'label' => Mage::helper('MarketPlace')->__('Debug'),
                'content' => $this->getLayout()->createBlock('Amazon/Index_Tab_Debug')->toHtml()
            ));
        }

        return parent::_beforeToHtml();
    }

}