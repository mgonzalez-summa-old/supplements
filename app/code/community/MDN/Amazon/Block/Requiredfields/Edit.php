<?php

class MDN_Amazon_Block_Requiredfields_Edit extends Mage_Adminhtml_Block_Widget_Form {

    public function __construct(){

        $this->category = $this->getRequest()->getParam('category');
        $this->marketplace = Mage::helper('Amazon')->getMarketPlaceName();
        $this->fields = $this->getFields($this->category);
        $this->attributeTab = Mage::getModel('MarketPlace/System_Config_ProductAttribute')->getAllOptions();

    }

    public function getFields($category){

        $retour = array();
       
        $filename = Mage::helper('Amazon/XSD')->retrieveFilename($category);
        $retour = Mage::helper('Amazon/ProductCreation')->getRequiredFields($filename);
                
        return $retour;

    }

    public function GetBackUrl() {
        return $this->getUrl('Amazon/Main/index', array());
    }

    public function countMainCategoryFields($fields){

        $i = 0;

        foreach($fields as $field){
            if(!preg_match('/producttype/i', $field['parentNode'])) $i++;
        }

        return $i;

    }

    public function getRestrictionList($field){

        $html = "<ul>";
        
        if(array_key_exists('restriction', $field)){
            
            $type = $field['restriction'];

            $type = preg_replace('/xsd:/','',$type);

            foreach($field[$type]['enumeration'] as $k => $v){

                $html .= '<li>'.$v.'</li>';

            }

        }

        if(array_key_exists('constraint', $field) && !is_string($field['constraint'])){

            $type = $field['constraint'][1]['restriction'];

            $type = preg_replace('/xsd:/','',$type);

            foreach($field['constraint'][1][$type]['enumeration'] as $k => $v){

                $html .= '<li>'.$v.'</li>';

            }

        }

        $html .= "</ul>";

        return $html;

    }

    public function getType($field){

        $retour = "";

        if(array_key_exists('constraint', $field)){

            
            $retour =  (is_string($field['constraint'])) ? $field['constraint'] : $field['constraint'][1]['name'];


        }

        return $retour;

    }
    

}

?>
