<?php

/**
 * Description of AmazonController
 *
 * @author Nicolas Mugnier
 */
class MDN_Amazon_MainController extends Mage_Adminhtml_Controller_Action {

    /**
     * Main screen
     */
    public function indexAction() {
        Mage::register('current_marketplace', 'Amazon');

        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Save products datas
     */
    public function saveAction() {

        try {

            Mage::helper('MarketPlace')->save($this->getRequest(), Mage::helper('Amazon')->getMarketPlaceName());

            //confirm & redirect
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Data saved'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper('Amazon')->getMarketPlaceName(),
                    $e->getCode(),
                    $e->getMessage(),
                    array('fileName' => NULL)
            );
        }

        $this->_redirect('Amazon/Main/index');
    }

    /**
     * Format string
     *
     * @param string $value
     * @return string $value
     */
    public function formatUcFirst($value) {

        $value = strtolower($value);
        $value = ucfirst($value);

        return $value;
    }

    public function checkConnexionAction(){
        try{

            $connexion = mage::helper('Amazon/CheckConnexion')->connexion();
            if($connexion === true){

                Mage::getSingleton('adminhtml/session')->addSuccess('Connexion OK.');

            }else{

                Mage::getSingleton('adminhtml/session')->addError('Connexion FAILED : '.$connexion);

            }

            $this->_redirectReferer();

        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            $this->_redirectReferer();
        }

    }

}