<?php

class MDN_Amazon_Debug_AmazonFeedController extends Mage_Adminhtml_Controller_Action{

    public function getFeedSubmissionListAction() {

        try{
            $helper = mage::helper('Amazon/Feed');
            $response = $helper->getFeedSubmissionList();
            $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
        
    }

    public function getFeedSubmissionResultAction() {

        try{
            $helper = mage::helper('Amazon/Feed');
            $id = $this->getRequest()->getParam('feedSubmissionId');
            if ($id != "") {
                $response = $helper->getFeedSubmissionResult($id);
                $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
            }else{
                mage::getSingleton('adminhtml/session')->addError('Empty request id');
                $this->_redirectReferer();
            }
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }

    }

    public function getFeedSubmissionCountAction() {

        try{
            $helper = mage::helper('Amazon/Feed');
            $response = $helper->getFeedSubmissionCount();
            $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
        
    }

}

?>
