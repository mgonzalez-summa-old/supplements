<?php

class MDN_Amazon_Debug_AmazonTrackingController extends Mage_Adminhtml_Controller_Action{


    public function cronSendTrackingAction() {

        try {
            Mage::helper('MarketPlace/Main')->sendTracking(Mage::helper('Amazon')->getMarketplaceName());
            mage::getSingleton('adminhtml/session')->addSuccess('Tracking successfully send.');
            $this->_redirectReferer();
        } catch (Exception $e) {

            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirectReferer();
        }
    }

    public function getShippingConfirmationFeedAction() {

        try {

            $boolean = mage::helper('Amazon/Tracking')->buildTrackingFile();

            if($boolean === true){

                $message = 'Shipping confirmation feed has been saved. Ready for send to Amazon.';

            }
            else{

                $message = 'No Actionable orders data requested yet !';

            }

            mage::getSingleton('adminhtml/session')->addSuccess($message);
            $this->_redirectReferer();

        } catch (Exception $e) {

            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirectReferer();
        }
    }

    public function sendTrackingAction() {

        try {

            $result = mage::helper('Amazon/Tracking')->sendTracking();
            if($result === true){
                mage::getSingleton('adminhtml/session')->addSuccess('Tracking successfully send.');   
            }
            else{
                mage::getSingleton('adminhtml/session')->addSuccess('File to export doesn\'t exists yet !');
            }
            $this->_redirectReferer();
        } catch (Exception $e) {

            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirectReferer();
        }
    }

    public function requestActionableOrderDataAction() {

        try {

            $requestId = mage::helper('Amazon/Tracking')->getOrdersToUpdate();
            mage::getSingleton('adminhtml/session')->addSuccess('Request sent to Amazon (request ID : '.$requestId.')');
            $this->_redirectReferer();
        } catch (Exception $e) {

            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirectReferer();
        }
    }

}

?>
