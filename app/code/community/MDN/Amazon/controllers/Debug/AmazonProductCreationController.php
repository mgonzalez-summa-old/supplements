<?php

class MDN_Amazon_Debug_AmazonProductCreationController extends Mage_Adminhtml_Controller_Action{

    public function cronCheckProductCreationAction() {

        try{
            //mage::helper('MarketPlace/Amazon')->cronCheckProductCreation();
            mage::helper('MarketPlace/Main')->checkProductCreation(Mage::helper('Amazon')->getMarketPlaceName());
            $this->_redirectReferer();
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
    }

    public function requestListingProductsAction() {
        try{
            $retour = mage::helper('Amazon/ProductCreation')->requestListingProducts();
            $this->_prepareDownloadResponse('response.xml', $retour, 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
    }

    public function showListingProductsAction(){
        try{

            $content = Mage::Helper('Amazon/ProductCreation')->getLastListingProducts();

            $this->_prepareDownloadResponse('listingProducts.txt', $content, 'text/plain');

        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
    }

}

?>
