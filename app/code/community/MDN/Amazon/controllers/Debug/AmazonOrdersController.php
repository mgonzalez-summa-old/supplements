<?php

class MDN_Amazon_Debug_AmazonOrdersController extends Mage_Adminhtml_Controller_Action{


    public function cronImportOrdersAction(){
        try{
            //$debug = mage::helper('MarketPlace/Amazon')->cronImportOrders();
            $debug = mage::helper('MarketPlace/Main')->importOrders(Mage::helper('Amazon')->getMarketPlaceName());
            mage::getSingleton('adminhtml/session')->addSuccess($debug);
            $this->_redirectReferer();
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
    }
    
    /**
     * Récupération du fichier de commandes Amazon (fichier plat)
     * Affichage du contenu
     */
    public function getMarketPlaceOrdersAction() {

        try{

            $helper = mage::helper('Amazon/Orders');
            $orders = $helper->getMarketPlaceOrders();
            
            $newOrders = array();

            foreach($orders as $ordersTab){
                foreach($ordersTab as $order){
                    if(!mage::helper('MarketPlace')->orderAlreadyImported($order['mpOrderId'])){
                        $newOrders[] = $order;
                    }
                }
            }
            
            ob_start();
            var_dump($newOrders);
            $debug = ob_get_contents();
            ob_end_clean();
            $debug = '<pre>'.$debug.'</pre>';

            mage::getSingleton('adminhtml/session')->addSuccess('Orders tab : '.$debug);
            $this->_redirectReferer();
            
        }catch(Exception $e){

            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
            
        }
    }

    /*public function getOrderAcknowledgmentFeedAction() {

        try {
            $xml = mage::helper('MarketPlace/Amazon')->getOrderAcknowledgmentFeed();
            $this->_prepareDownloadResponse('orderAckonwledgmentFeed.xml', $xml, 'text/xml');
        } catch (Exception $e) {
            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirectReferer();
        }
    }*/

}

?>
