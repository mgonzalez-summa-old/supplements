<?php

class MDN_Amazon_Debug_AmazonEcsController extends Mage_Adminhtml_Controller_Action {

    public function ecsSearchAction() {

        try{
            $item_id = $this->getRequest()->getParam('item_id');
            $id_type = $this->getRequest()->getParam('id_type');
            $retour = mage::helper('Amazon/ECS')->ecsRequest($item_id, $id_type);
            $this->_prepareDownloadResponse('ecsResponse.xml', $retour->getBody(), 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }

    }

}

?>
