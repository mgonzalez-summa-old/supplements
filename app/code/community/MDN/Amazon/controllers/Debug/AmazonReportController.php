<?php

class MDN_Amazon_Debug_AmazonReportController extends Mage_Adminhtml_Controller_Action {

    public function getReportCountAction() {

        try{
            $helper = mage::helper('Amazon/Report');
            $response = $helper->getReportCount();
            $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }

    }

    public function getReportAction() {

        try{
            $helper = mage::helper('Amazon/Report');
            $reportId = $this->getRequest()->getParam('reportId');
            $response = $helper->getReport($reportId);
            $this->_prepareDownloadResponse('orders.txt', $response, 'text/plain');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }

    }

    public function getReportListAction() {

        try{
            $helper = mage::helper('Amazon/Report');
            $response = $helper->getReportList();
            $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }

    }

    public function getReportRequestListAction() {

        try{
            $helper = mage::helper('Amazon/Report');
            $response = $helper->getReportRequestList('_GET_FLAT_FILE_ORDERS_DATA_');
            $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }

    }

}

?>
