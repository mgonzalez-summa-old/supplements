<?php

class MDN_Amazon_Debug_AmazonProductUpdateController extends Mage_Adminhtml_Controller_Action{

    public function cronExportStockAndPriceAction(){
        try{
            //mage::helper('MarketPlace/Amazon')->cronExportStocksAndPrices();
            Mage::helper('MarketPlace/Main')->updateStocksAndPrices(Mage::helper('Amazon')->getmarketPlaceName());
            $this->_redirectReferer();
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
    }

    /**
     * Construction du fichier inventoryFeed.xml (mise à jour du stock)
     * Affichage du fichier
     */
    public function getInventoryFeedAction() {

        try{
            $helper = mage::helper('Amazon/ProductUpdate');

            $products = $helper->getProductsToExport();
    
            $filename = $helper->getInventoryFeedName();
            $helper->setInventoryFeed($products, $filename);

            $filePath = Mage::app()->getConfig()->getTempVarDir();
            $filePath .= '/export/marketplace/amazon';

            $content = file_get_contents($filePath . '/inventoryFeed.xml');
            
            $this->_prepareDownloadResponse('testInventoryFeed.xml', $content, 'text/xml');
            
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
    }

    /**
     * Construction du fichier priceFeed.xml (mise à jour des prix)
     * Affichage du fichier
     */
    public function getPriceFeedAction() {

        try{
            $helper = mage::helper('Amazon/ProductUpdate');

            $products = $helper->getProductsToExport();
            $filename = $helper->getPriceFeedName();
            $helper->setPriceFeed($products, $filename);

            $filePath = Mage::app()->getConfig()->getTempVarDir();
            $filePath .= '/export/marketplace/amazon';

            $content = file_get_contents($filePath . '/priceFeed.xml');

            $this->_prepareDownloadResponse('testPriceFeed.xml', $content, 'text/xml');
            
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
    }

    public function sendInventoryFeedAction() {
        try{
            $helper = mage::helper('Amazon/ProductUpdate');
            $filename = $helper->getInventoryFeedName();
            $response = $helper->updateMarketPlaceStock($filename);
            $this->_prepareDownloadResponse('response.xml', $response->getBody(), 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
    }

    public function sendPriceFeedAction() {
        try{
            $helper = mage::helper('Amazon/ProductUpdate');
            $filename = $helper->getPriceFeedName();
            $response = $helper->updateMarketPlacePrices($filename);
            $this->_prepareDownloadResponse('response.xml', $response->getBody(), 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
    }

    public function getImageFeedAction(){
        try{
            $helper = mage::helper('Amazon/ProductUpdate');

            $products = $helper->getProductsToExport();
            $filename = $helper->getImageFeedName();
            $helper->setImageFeed($products, $filename);

            $filePath = Mage::app()->getConfig()->getTempVarDir();
            $filePath .= '/export/marketplace/amazon';

            $content = file_get_contents($filePath . '/imageFeed.xml');

            $this->_prepareDownloadResponse('imageFeed.xml', $content, 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
    }

    public function sendImageFeedAction(){
        try{
            $helper = mage::helper('Amazon/ProductUpdate');
            $filename = $helper->getImageFeedName();
            $response = $helper->updateImage($filename);
            $this->_prepareDownloadResponse('response.xml', $response->getBody(), 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirectReferer();
        }
    }

}

?>
