<?php

class MDN_Amazon_RequiredfieldsController extends Mage_Adminhtml_Controller_Action {

    public function EditAction(){
        
        $this->loadLayout();
        $this->renderLayout();
        
    }

    public function saveAction(){

        try{

            $marketplace = strtolower(Mage::helper('Amazon')->getMarketPlaceName());

            $tmp = $this->getRequest()->getPost();

            foreach($tmp as $path => $value){

                $requiredField = Mage::getModel('MarketPlace/Requiredfields')
                                ->getCollection()
                                ->addFieldToFilter('mp_path',$path)
                                ->addFieldToFilter('mp_marketplace_id', $marketplace)
                                ->getFirstItem();

                if($requiredField->getmp_id()){
                    $requiredField->setmp_attribute_name($value)
                            ->save();
                }else{
                    $requiredField = Mage::getModel('MarketPlace/Requiredfields')
                            ->setmp_path($path)
                            ->setmp_marketplace_id($marketplace)
                            ->setmp_attribute_name($value)
                            ->save();
                }

            }

            Mage::getSingleton('adminhtml/session')->addSuccess('Data saved');
            $this->_redirectReferer();

        }catch(Exception $e){

            Mage::getSingleton('adminhtml/session')->addError($e->getMessage().' : '.$e->getTraceAsString());
            $this->_redirectReferer();
            
        }

    }

}

?>
