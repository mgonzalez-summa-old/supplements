<?php

require_once('MDN/Marketplace/Amazon/Client/MWSClient.php');

class MDN_Amazon_Helper_Feed extends Mage_Core_Helper_Abstract {

    /**
     * Get feed submission ids name
     *
     * @return string
     */
    public function getFeedSubmissionIdsName() {

        return Mage::getConfig()->getTempVarDir() . '/export/marketplace/tmp/amazonFeedSubmissionIds.txt';
    }

    /**
     * Get feed submission list
     *
     * @return string
     */
    public function getFeedSubmissionList() {

        $params = array(
            'Action' => 'GetFeedSubmissionList',
            'FeedTypeList.Type.1' => '_POST_PRODUCT_DATA_',
            'FeedTypeList.Type.2' => '_POST_PRODUCT_PRICING_DATA_',
            'FeedTypeList.Type.3' => '_POST_INVENTORY_AVAILABILITY_DATA_'
        );

        $mws = new MWSClient($params);
        return $mws->sendRequest()->getBody();
    }

    /**
     * Get feed submission result
     *
     * @param int $feedSubmissionId
     * @return string
     */
    public function getFeedSubmissionResult($feedSubmissionId) {

        $params = array(
            'Action' => 'GetFeedSubmissionResult',
            'FeedSubmissionId' => $feedSubmissionId
        );

        $mws = new MWSClient($params);
        return $mws->sendRequest()->getBody();
    }

    /**
     * Get feed submission count
     *
     * @return string
     */
    public function getFeedSubmissionCount() {

        $params = array(
            'Action' => 'GetFeedSubmissionCount',
            'FeedTypeList.Type.1' => '_POST_PRODUCT_DATA_',
            'FeedTypeList.Type.2' => '_POST_PRODUCT_PRICING_DATA_',
            'FeedTypeList.Type.3' => '_POST_INVENTORY_AVAILABILITY_DATA_',
            'FeedProcessingStatus.Status.1' => '_DONE_',
            'FeedProcessingStatus.Status.2' => '_SUBMITTED_'
        );

        $mws = new MWSClient($params);
        return $mws->sendRequest()->getBody();
    }

    /**
     * Get current feed submission ids
     *
     * @return array $ids
     */
    public function getFeedSubmissionIds() {

        $ids = array();

        $file = $this->getFeedSubmissionIdsName();

        if(file_exists($file)) {

            $ids = file($file);
        }

        return $ids;
    }

    /**
     * Set feed submission ids
     *
     * @param array $ids
     */
    public function setFeedSubmissionIds($ids) {

        $str = "";

        $file = $this->getFeedSubmissionIdsName();

        foreach ($ids as $id) {

            $str .= trim($id) . "\n";
        }

        file_put_contents($file, $str);
    }

}