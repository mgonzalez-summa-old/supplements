<?php
/* 
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

require_once('MDN/Marketplace/Amazon/Client/MWSClient.php');

class MDN_Amazon_Helper_Matching extends MDN_MarketPlace_Helper_Matching {

    protected $_matched = array();

    /**
     * Match products with EAN
     *
     * @param array $products
     * @return int 0
     */
    public function Match($products){

        $ecs = Mage::Helper('Amazon/ECS');
        $i = 0;

        foreach($products as $k => $product){

            if(array_key_exists('ean', $product)){

                $product_id = Mage::getModel('catalog/product')->getIdBySku($product['sku']);

                $response = $ecs->request($product[MDN_MarketPlace_Helper_Matching::kEAN], MDN_Amazon_Helper_ECS::kTypeEAN);

                $res = $ecs->parseResponse($response->getBody());

                $status = ($res['error'] === true) ? MDN_MarketPlace_Helper_Feed::kFeedError : MDN_MarketPlace_Helper_Feed::kFeedDone;

                if($res['error'] === true){

                    if(isset($res['message']))
                        Mage::getModel('MarketPlace/Data')->addMessage($product_id, $res['message']);

                    Mage::getModel('MarketPlace/Data')->updateStatus(array($product_id), Mage::Helper('Amazon')->getMarketPlaceName(), MDN_MarketPlace_Helper_ProductCreation::kStatusInError);
                }else{

                    Mage::getModel('MarketPlace/Data')->updateStatus(array($product_id), Mage::Helper('Amazon')->getMarketPlaceName(), MDN_MarketPlace_Helper_ProductCreation::kStatusPending);

                }
                
                $this->_addFeed(Mage::Helper('Amazon')->getMarketPlaceName(), '', $response, $res['requestId'], $status);

                if($res['data'] !== null){

                    $this->_matched[$i] = $product;
                    $this->_matched[$i]['asin'] = $res['data']['ASIN'];
                    $i++;

                }

            }else
                throw new Exception('Key ean not defined in $product tab');

        }

        if(count($this->_matched) > 0)
            $this->_addProducts();

        return 0;

    }

    /**
     * Get product feed name
     *
     * @return string
     */
    protected function getProductFeedName(){
        return 'supplierAmazonProductCreation.xml';
    }

    /**
     * Add products
     *
     * @return int 0
     */
    protected function _addProducts(){

        // build xml file
        $content = $this->_buildProductFeed();

        // save file to export
        $fileToExport = $this->getProductFeedName();
        $history = 'productFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $fileToExport),
                    'fileContent' => $content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );

        $this->_sendFeed();

        return 0;

    }

    /**
     * Build product feed
     *
     * @return string
     */
    protected function _buildProductFeed(){

        $merchantId = mage::getStoreConfig('marketplace/amazon/amazon_merchant_id');

        // add declaration
        $xml = new DOMDocument('1.0', 'utf-8');

        // create AmazonEnvelope
        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        // add attributes
        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        // add Header node
        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        // add document version to header node
        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        // add merchant identifier to header node
        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        // add Message type node to AmazonEnvelope
        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('Product');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        // add purge and replace node to AmazonEnvelope
        $purgeAndReplace = $xml->createElement('PurgeAndReplace', '');
        $purgeAndReplace->appendChild($xml->createTextNode('false'));
        $amazonEnvelope->appendChild($purgeAndReplace);

        // add new products to feed
        $id = 0;
        foreach ($this->_matched as $k => $v) {

            $id++;

            // add message node
            $message = $xml->createElement('Message', '');
            $messageId = $xml->createElement('MessageID', '');
            $messageId->appendChild($xml->createTextNode($id));
            $message->appendChild($messageId);

            // add operation type node
            $operationType = $xml->createElement('OperationType', '');
            $operationType->appendChild($xml->createTextNode('Update'));
            $message->appendChild($operationType);

            // add product node
            $product = $xml->createElement('Product', '');

            $sku = $xml->createElement('SKU','');
            $sku->appendChild($xml->createTextNode($v['sku']));
            $product->appendChild($sku);

            $standardProductId = $xml->createElement('StandardProductID','');

            $type = $xml->createElement('Type','');
            $type->appendChild($xml->createTextNode('ASIN'));
            $standardProductId->appendChild($type);

            $value = $xml->createElement('Value', '');
            $value->appendChild($xml->createTextNode($v['asin']));
            $standardProductId->appendChild($value);

            $product->appendChild($standardProductId);

            $descriptionData = $xml->createElement('DescriptionData', '');

            // title
            $title = $xml->createElement('Title');
            $title->appendChild($xml->createTextNode($v['name']));
            $descriptionData->appendChild($title);

            // description
            $description = $xml->createElement('Description', '');
            $description->appendChild($xml->createTextNode($v['name']));
            $descriptionData->appendChild($description);

            if(isset($v[MDN_MarketPlace_Helper_Matching::kPackageWeight]) && $v[MDN_MarketPlace_Helper_Matching::kPackageWeight] != 0){
                //package weight
                $packageWeight = $xml->createElement('PackageWeight', '');
                $unit = $xml->createAttribute('unitOfMeasure');
                $unit->appendChild($xml->createTextNode('KG'));
                $packageWeight->appendChild($unit);
                $packageWeight->appendChild($xml->createTextNode($v[MDN_MarketPlace_Helper_Matching::kPackageWeight]));
                $descriptionData->appendChild($packageWeight);
            }

            if(isset($v[MDN_MarketPlace_Helper_Matching::kShippingWeight]) && $v[MDN_MarketPlace_Helper_Matching::kShippingWeight] != 0){
                // shipping weight
                $shippingWeight = $xml->createElement('ShippingWeight', '');
                $unit = $xml->createAttribute('unitOfMeasure');
                $unit->appendChild($xml->createTextNode('KG'));
                $shippingWeight->appendChild($unit);
                $shippingWeight->appendChild($xml->createTextNode($v[MDN_MarketPlace_Helper_Matching::kShippingWeight]));
                $descriptionData->appendChild($shippingWeight);
            }

            $product->appendChild($descriptionData);

            $message->appendChild($product);
            $amazonEnvelope->appendChild($message);
        }

        return $xml->saveXML();

    }

    /**
     * Send product feed
     *
     * @return int 0
     */
    protected function _sendFeed(){

        // add feed
        $filename = Mage::app()->getConfig()->getTempVarDir().'/export/marketplace/'.Mage::Helper('Amazon')->getMarketPlaceName().'/'.$this->getProductFeedName();
        $content = file_get_contents($filename);

        $feed = Mage::getModel('MarketPlace/Feed')
                    ->setmp_content($content)
                    ->setmp_marketplace_id(Mage::Helper(ucfirst('Amazon'))->getMarketPlaceName())
                    ->setmp_date(date('Y-m-d H:i:s'), Mage::getModel('core/date')->timestamp())
                    ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeProductCreation)
                    ->save();

        $params = array(
            'Action' => 'SubmitFeed',
            'FeedType' => '_POST_PRODUCT_DATA_'
        );

        $mws = new MWSClient($params);
        $response = $mws->sendFeed($filename);

        if ($response->getStatus() == 200) {

            $tabIds = array();

            $xml = new DomDocument();
            $report = $response->getBody();
            $xml->loadXML($report);

            $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

            // update product status
            foreach($this->_matched as $matched){

                $productId = Mage::getModel('Catalog/Product')->getIdBySku($matched['sku']);

                if(!$productId)
                    $productId = Mage::Helper('MarketPlace/UnexistingProduct')->process($matched['ean']);

                $tabIds[] = $productId;

            }

            Mage::getModel('MarketPlace/Data')->updateStatus($tabIds, strtolower(Mage::helper('Amazon')->getMarketPlaceName()), MDN_MarketPlace_Helper_ProductCreation::kStatusPending);

            $ids = implode(',',$tabIds);

            $feed->setmp_response($response->getBody())
                    ->setmp_feed_id($feedSubmissionId)
                    ->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedSubmitted)
                    ->setmp_ids($ids)
                    ->save();


        }else{

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                    ->save();


        }

        return 0;

    }

}
