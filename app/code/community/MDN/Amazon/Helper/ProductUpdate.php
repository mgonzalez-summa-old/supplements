<?php

require_once('MDN/Marketplace/Amazon/Client/MWSClient.php');
require_once('MDN/Marketplace/Amazon/Client/MWSFeed.php');

class MDN_Amazon_Helper_ProductUpdate extends Mage_Core_Helper_Abstract implements MDN_MarketPlace_Helper_Interface_ProductUpdate {

    protected $_lastSentProductStock = null;
    protected $_lastSentProductPrice = null;
    
    /**
     * Get export path name
     *
     * @return string
     *
     */
    public function getExportPath(){
        return Mage::app()->getConfig()->getTempVarDir().'/export/marketplace/'.Mage::helper('Amazon')->getMarketPlaceName().'/';
    }

    /**
     * Retrieve products to update on amazon marketplace
     * - simple products
     * - visibility : search, catalog, nowhere, catalog/search
     * - mp_reference not null in market_place_data table
     *
     * @return collection
     */
    public function getProductsToExport() {

        return mage::helper('MarketPlace/Product')->getProductsToExport(Mage::helper('Amazon')->getMarketPlaceName());

    }

    /**
     * Update stock and price
     */
    public function update(){

        $this->exportStocks();
        $this->exportPrices();

        //store last sent product
        if($this->_lastSentProductStock !== null)
            mage::helper('MarketPlace/Product')->stockLatestSentProduct('amazon', $this->_lastSentProductStock);

    }

    /**
     * Export stocks (CRON)
     *
     */
    public function exportStocks() {
        try {

            $products = $this->getProductsToExport();
            $filename = $this->getInventoryFeedName();
            $this->setInventoryFeed($products, $filename);
            $this->updateMarketPlaceStock($filename);
            
        } catch (Exception $e) {

            $errorCode = 0;

            if (preg_match('/^Notice/', $e->getMessage()))
                $errorCode = 98;
            elseif (preg_match('/^Warning/', $e->getMessage()))
                $errorCode = 99;
            else
                $errorCode = $e->getCode();

            throw new Exception($e->getMessage(), $errorCode);

        }
    }

    /**
     * Retrieve inventory feed name
     *
     * @return string
     */
    public function getInventoryFeedName() {
        return 'inventoryFeed.xml';
    }

    /**
     * Build & save inventory feed
     *
     * @param collection $products
     * @param string $filename
     *
     */
    public function setInventoryFeed($products, $filename) {

        $tab = array();
        $id = mage::getStoreConfig('marketplace/amazon/amazon_merchant_id');
        $i = 0;
        foreach ($products as $product) {

            $tab[$i]['sku'] = $product->getsku();
            $tab[$i]['stock'] = mage::helper('MarketPlace/Product')->getStockToExport($product, Mage::Helper('Amazon')->getMarketPlaceName());
            $tab[$i]['delay'] = mage::helper('MarketPlace/Product')->getDelayToExport($product);

            // save last send product
            $this->_lastSentProductStock = $product;

            $i++;
        }

        

        $content = MWSFeed::getInstance()->buildInventoryFeed($id, $tab);

        $history = 'inventoryFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $filename),
                    'fileContent' => $content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );

    }

    /**
     * Update marketplace stock
     *
     * @param string $filename
     *
     */
    public function updateMarketPlaceStock($filename) {

        $file = $filename;
        $path = $this->getExportPath();
        $filename = $path . $file;
        $content = file_get_contents($filename);

        $params = array(
            'Action' => 'SubmitFeed',
            'FeedType' => '_POST_INVENTORY_AVAILABILITY_DATA_'
        );

        $mws = new MWSClient($params);
        $response = $mws->sendFeed($filename);

        $feed = Mage::getModel('MarketPlace/Feed')
                    ->setmp_content($content)
                    ->setmp_response($response->getBody())
                    ->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                    ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeUpdateStockPrice)
                    ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName());

        if ($response->getStatus() == 200) {

            $xml = new DomDocument();
            $report = $response->getBody();
            $xml->loadXML($report);
            $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedDone)
                    ->setmp_feed_id($feedSubmissionId)
                    ->save();
        }
        else{

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                    ->setmp_feed_id(' - ')
                    ->save();

            throw new Exception($response->getMessage(), 4);
        }

        return $response;
    }

    /**
     * Export prices (CRON)
     *
     */
    public function exportPrices() {
        try {
            $products = $this->getProductsToExport();
            $filename = $this->getPriceFeedName();
            $this->setPriceFeed($products, $filename);
            $this->updateMarketPlacePrices($filename);
        } catch (Exception $e) {

            $errorCode = 0;

            if (preg_match('/^Notice/', $e->getMessage()))
                $errorCode = 98;
            elseif (preg_match('/^Warning/', $e->getMessage()))
                $errorCode = 99;
            else
                $errorCode = $e->getCode();

            throw new Exception($e->getMessage(), $errorCode);

        }
    }

    /**
     * Retrieve price feed name
     *
     * @return string
     */
    public function getPriceFeedName() {
        return 'priceFeed.xml';
    }

    /**
     * Build & save price feed
     *
     * @param collection $products
     * @param string $filename
     *
     */
    public function setPriceFeed($products, $filename) {

        $tab = array();
        $id = mage::getStoreConfig('marketplace/amazon/amazon_merchant_id');
        $i = 0;
        foreach ($products as $product) {

            $tab[$i]['sku'] = $product->getsku();
            $productPrice = mage::helper('MarketPlace/Product')->getPriceToExport($product);

            $productPrice = str_replace(",", ".", $productPrice);

            // add marketplace coef
            $productPrice = (Mage::getStoreConfig('marketplace/amazon/coef_price') == "") ? $productPrice : round($productPrice * Mage::getStoreConfig('marketplace/amazon/coef_price'), 2);
            
            $tab[$i]['price'] = $productPrice;

            // save last send product
            //$this->_lastSentProductPrice = $product;

            $i++;
        }

        $content = MWSFeed::getInstance()->buildPriceFeed($id, $tab);

        $history = 'priceFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $filename),
                    'fileContent' => $content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );
    }

    /**
     * Update marketplace prices
     *
     * @param string $filename
     *
     */
    public function updateMarketPlacePrices($filename) {

        $file = $filename;
        $path = $this->getExportPath();
        $filename = $path . $file;
        $content = file_get_contents($filename);

        $params = array(
            'Action' => 'SubmitFeed',
            'FeedType' => '_POST_PRODUCT_PRICING_DATA_'
        );

        $mws = new MWSClient($params);
        $response = $mws->sendFeed($filename);

        $feed = Mage::getModel('MarketPlace/Feed')
                    ->setmp_content($content)
                    ->setmp_response($response->getBody())
                    ->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                    ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeUpdateStockPrice)
                    ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName());

        if ($response->getStatus() == 200) {

            $xml = new DomDocument();
            $report = $response->getBody();
            $xml->loadXML($report);
            $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedDone)
                    ->setmp_feed_id($feedSubmissionId)
                    ->save();

            return $response;
        }
        else{

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                    ->setmp_feed_id(' - ')
                    ->save();

            throw new Exception($response->getMessage(), 4);
        }
    }

    /**
     * Export image
     */
    public function exportImage(){
        try {
            $products = $this->getProductsToExport();
            $filename = $this->getImageFeedName();
            $this->setImageFeed($products, $filename);
            $this->updateImage($filename);
        } catch (Exception $e) {

            $errorCode = 0;

            if (preg_match('/^Notice/', $e->getMessage()))
                $errorCode = 98;
            elseif (preg_match('/^Warning/', $e->getMessage()))
                $errorCode = 99;
            else
                $errorCode = $e->getCode();

            throw new Exception($e->getMessage(), $errorCode);
            
        }
    }

    /**
     * Get image feed name
     *
     * @return string
     */
    public function getImageFeedName(){
        return 'imageFeed.xml';
    }

    /**
     * Set image feed
     *
     * @param array $products
     * @param string $filename
     */
    public function setImageFeed($products, $filename){

        $tab = array();
        $id = mage::getStoreConfig('marketplace/amazon/amazon_merchant_id');

        $content = MWSFeed::getInstance()->buildImageFeed($id, $products);

        $history = 'imageFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $filename),
                    'fileContent' => $content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );

    }

    /**
     * Update image
     *
     * @param string $filename
     * @return object $response
     */
    public function updateImage($filename){

        $file = $filename;
        $path = $this->getExportPath();
        $filename = $path . $file;
        $content = file_get_contents($filename);

        $params = array(
            'Action' => 'SubmitFeed',
            'FeedType' => '_POST_PRODUCT_IMAGE_DATA_'
        );

        $mws = new MWSClient($params);
        $response = $mws->sendFeed($filename);

        $feed = Mage::getModel('MarketPlace/Feed')
                    ->setmp_content($content)
                    ->setmp_response($response->getBody())
                    ->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                    ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeMedia)
                    ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName());

        if ($response->getStatus() == 200) {

            $xml = new DomDocument();
            $report = $response->getBody();
            $xml->loadXML($report);
            $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedDone)
                    ->setmp_feed_id($feedSubmissionId)
                    ->save();
            
            return $response;
        }
        else{

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                    ->setmp_feed_id(' - ')
                    ->save();

            throw new Exception($response->getMessage(), 4);
        }

    }

    /**
     * Update stock and price for ids $ids
     *
     * @param array $ids
     * @return int 0
     */
    public function updateStockPriceFromGrid($ids){

        $t_stock = array();
        $t_price = array();
        $error = false;
        $message = '';
        $errorMsg = '';
        $merchant_id = mage::getStoreConfig('marketplace/amazon/amazon_merchant_id');
        $store_id = (Mage::getStoreConfig('marketplace/amazon/store_id') != "") ? Mage::getStoreConfig('marketplace/amazon/store_id') : Mage::getStoreConfig('marketplace/general/default_store_id');

        $i = 0;
        foreach($ids as $id){

            $product = Mage::getModel('Catalog/Product')->setStoreId($store_id)->load($id);

            // stock
            $t_stock[$i]['sku'] = $product->getsku();
            $t_stock[$i]['stock'] = mage::helper('MarketPlace/Product')->getStockToExport($product, Mage::Helper('Amazon')->getMarketPlaceName());
            $t_stock[$i]['delay'] = mage::helper('MarketPlace/Product')->getDelayToExport($product);

            // price
            $t_price[$i]['sku'] = $product->getsku();
            $productPrice = mage::helper('MarketPlace/Product')->getPriceToExport($product);
            $productPrice = str_replace(",", ".", $productPrice);
            // add marketplace coef
            $productPrice = (Mage::getStoreConfig('marketplace/amazon/coef_price') == "") ? $productPrice : round($productPrice * Mage::getStoreConfig('marketplace/amazon/coef_price'), 2);
            $t_price[$i]['price'] = $productPrice;

            $i++;
        }

        // save stock file
        $stock_content = MWSFeed::getInstance()->buildInventoryFeed($merchant_id, $t_stock);

        $history = 'inventoryFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $this->getInventoryFeedName()),
                    'fileContent' => $stock_content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );

        // send stock file
        try{

            $this->updateMarketPlaceStock($this->getInventoryFeedName());
            
        }catch(Exception $e){

            $error = true;
            $errorMsg .= 'Update Stock form grid : '.$e->getMessage();

        }

        // save price file
        $price_content = MWSFeed::getInstance()->buildPriceFeed($merchant_id, $t_price);

        $history = 'priceFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $this->getPriceFeedName()),
                    'fileContent' => $price_content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );

        // send price file
        try{

            $this->updateMarketPlacePrices($this->getPriceFeedName());
            
        }catch(Exception $e){

            $error = true;
            $errorMsg .= 'Update Price form grid : '.$e->getMessage();

        }

        if($error){
            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper('Amazon')->getMarketPlaceName(),
                    4,
                    $errorMsg,
                    array('fileName' => NULL)
            );
            $message = $errorMsg;

            throw new Exception($errorMsg);
        }else{

            $t_ids = array();

            //update incomplete status
            $collection = Mage::getModel('MarketPlace/Data')->getCollection()
                                ->addFieldToFilter('mp_marketplace_id', 'amazon')
                                ->addFieldToFIlter('mp_marketplace_status', MDN_MarketPlace_Helper_ProductCreation::kStatusIncomplete)
                                ->addFieldToFilter('mp_product_id', array('in', $ids));

            foreach($collection as $item){

                $item->setmp_marketplace_status(MDN_MarketPlace_Helper_ProductCreation::kStatusCreated)
                        ->save();

                $t_ids[] = $item->getmp_product_id();

            }

            // send image feed for incomplete products
            if(count($t_ids) > 0){
                $this->updateImageFromGrid($t_ids);
            }

        }

        return 0;

    }

    /**
     * Update image for ids $ids
     *
     * @param array $ids
     * @return int 0
     */
    public function updateImageFromGrid($ids){

        $products = array();
        $merchant_id = mage::getStoreConfig('marketplace/amazon/amazon_merchant_id');

        foreach($ids as $id){

            $products[] = Mage::getModel('Catalog/Product')->load($id);

        }

        $content = MWSFeed::getInstance()->buildImageFeed($merchant_id, $products);

        $history = 'imageFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $this->getImageFeedName()),
                    'fileContent' => $content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );

        $this->updateImage($this->getImageFeedName());

        return 0;

    }

}