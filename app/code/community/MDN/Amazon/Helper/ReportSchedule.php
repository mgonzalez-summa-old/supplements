<?php
/* 
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

require_once('MDN/Marketplace/Amazon/Client/MWSClient.php');

class ReportSchedule extends Mage_Core_Helper_Abstract {

    const k_15_Minutes = "_15_MINUTES_";
    const k_30_Minutes = "_30_MINUTES_";

    const k_1_Hour = "_1_HOUR_";
    const k_2_Hours ="_2_HOURS_";
    const k_4_Hours ="_4_HOURS_";
    const k_8_Hours ="_8_HOURS_";
    const k_12_Hours = "_12_HOURS_";

    const k_1_Day = "_1_DAY_";
    const k_2_Days = "_2_DAYS_";
    const k_3_Days = "_72_HOURS_";
    const k_7_Days = "_7_DAYS_";
    const k_14_Days = "_14_DAYS_";
    const k_15_Days = "_15_DAYS_";
    const k_30_Days = "_30_DAYS_";

    const kNever = "_NEVER_";

    /**
     * Get scheduled values
     *
     * @return array $retour
     */
    public function getScheduledValues(){
        
        $retour = array(
            self::k_15_Minutes,
            self::k_30_Minutes,
            self::k_1_Hour,
            self::k_2_Hours,
            self::k_4_Hours,
            self::k_8_Hours,
            self::k_12_Hours,
            self::k_1_Day,
            self::k_2_Days,
            self::k_3_Days,
            self::k_7_Days,
            self::k_14_Days,
            self::k_15_Days,
            self::k_30_Days,
            self::kNever
        );

        return $retour;
    }

    /**
     * Manage reports
     *
     * @param string $reportType
     * @param string $schedule
     * @param string $date
     * @return string $retour
     */
    public function manageReport($reportType, $schedule, $date = ''){

        $retour = '';

        if(in_array($schedule, $this->getScheduledValues())){

            $params = array(
                'ReportType' => $reportType,
                'Schedule' => $schedule
            );

            if($date != '')
                $params['ScheduledDate'] = $date;

            $client = new MWSClient($params);
            $retour =  $client->sendRequest()->getBody();

        }

        return $retour;

    }

    /**
     * Get report list
     *
     * @param string> $typeList
     * @return string $retour
     */
    public function getReportList($typeList){

        $retour = '';

        $params = array(
            'Action' => 'GetReportScheduleList',
            'reportTypeList' => '_GET_ORDERS_DATA_'
        );

        $client = new MWSClient($params);
        $retour =  $client->sendRequest()->getBody();

        return $retour;

    }

    /**
     * Get report list by next token
     *
     * @param string $token
     * @return string
     */
    public function getReportListByNextToken($token){

        $retour = '';

        $params = array(
            'Action' => 'GetReportScheduleListByNextToken',
            'NextToken' => $token
        );

        $client = new MWSClient($params);
        $retour =  $client->sendRequest()->getBody();

        return $retour;

    }

    /**
     * Ge treport count
     *
     * @return string $retour
     */
    public function getReportCount(){

        $retour = '';

        $params = array(
            'Action' => 'GetReportScheduleCount',
            'ReportTypeList.Type.1' => '_GET_ORDERS_DATA_'
        );

        $client = new MWSClient($params);
        $retour =  $client->sendRequest()->getBody();

        return $retour;

    }


}
