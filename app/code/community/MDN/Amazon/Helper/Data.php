<?php

require_once('MDN/Marketplace/Amazon/Parser/parser/ParseAmazon.class.php');

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN & Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_Amazon_Helper_Data extends MDN_MarketPlace_Helper_Abstract {

    /**
     * Create tracking file
     *
     * @param unknown_type $orderIds
     * @return string
     */
    public function createFlatTrackingFile($orderIds) {

        $lineReturn = "\r\n";
        $retour = 'order-id	order-item-id	quantity	ship-date	carrier-code	carrier-name	tracking-number	ship-method' . $lineReturn;

        if (!$orderIds)
            return $retour;

        foreach ($orderIds as $orderId) {
            //init vars
            $lineModel = 'order_ref	order_item_id	qty	ship_date	carrier_code	carrier_name	tracking	ship_method';
            $order = mage::getModel('sales/order')->load($orderId);

            //retrieve shipment
            $shipment = null;
            foreach ($order->getShipmentsCollection() as $item) {
                $shipment = $item;
                break;
            }

            if ($shipment == null) {
                continue;
            }

            //add items
            foreach ($order->getAllItems() as $item) {
                $line = $lineModel;
                $line = str_replace('order_ref', $order->getmarketplace_order_id(), $line);
                $line = str_replace('order_item_id', $item->getmarketplace_item_id(), $line);
                $line = str_replace('qty', (int) $item->getqty_ordered(), $line);

                $shipDate = date('Y-m-d', strtotime($shipment->getcreated_at()));
                $line = str_replace('ship_date', $shipDate, $line);

                $line = str_replace('carrier_code', 'Other', $line);
                $line = str_replace('carrier_name', 'La Poste', $line);

                $tracking = '';
                foreach ($order->getTracksCollection() as $track) {
                    if (is_object($track->getNumberDetail()))
                        $tracking = $track->getNumberDetail()->gettracking();
                }

                $line = str_replace('tracking', $tracking, $line);
                $line = str_replace('ship_method', 'Colissimo', $line);

                $retour .= $line . $lineReturn;
            }
        }

        return $retour;
    }

    /**
     * Return product export file content
     *
     * @param string $filename
     * @return string
     *
     */
    public function getProductFile() {

        $content = '';
        $content .= "sku\tprice\tquantity\titem-condition\r\n";

        $fileName = 'export' . $this->getMarketPlaceName() . '-' . date('Y-m-d_H:i:s') . '.csv';

        $collection = mage::helper('Amazon/ProductUpdate')->getProductsToExport();

        if ($collection->count() == 0)
            throw new Exception('No product to export.', 0);

        foreach ($collection as $product) {
            
            //collect datas
            $sku = $product->getSku();

            $price = mage::helper('MarketPlace/Product')->getPriceToExport($product);
            $stock = mage::helper('MarketPlace/Product')->getStockToExport($product);

            $content .= $sku . "\t" . $price . "\t" . $stock . "\t11\r\n";
        }

        mage::getModel('MarketPlace/Logs')->saveFile(array(
            'filenames' => array(
                $fileName
            ),
            'fileContent' => $content,
            'type' => 'export',
            'marketplace' => $this->getMarketPlaceName()
        ));

        return $content;
    }

    /**
     * Parse XSD
     *
     * @param string $value
     * @param string $parentNode
     * @return array
     */
    public function parseXSD($value, $parentNode) {

        $filename = Mage::app()->getConfig()->getOptions()->getLibDir() . '/MDN/Marketplace/Amazon/Parser/xsd/' . $value . '.xsd';
        $parseAmazon = new ParseAmazon($filename);
        $parsed = $parseAmazon->toArray($parentNode);
        return $parsed;
    }

    /**
     * Define if the market place need custom association with categories
     *
     * @return boolean
     */
    public function needCategoryAssociation() {
        return true;
    }

    /**
     * Form controls to associate category
     *
     * @param string $name
     * @param string $value
     *
     * @return string $html
     */
    public function getCategoryForm($name, $value) {

        $defaultCat = $value['cat'];
        $defaultBrowseNode = $value['browse_node'];

        //add category & sub category combo
        $html = '<select name="' . $name . '[cat]" id="' . $name . '[cat]">';
        $browseNode = mage::helper('Amazon/Gua')->getAllCategoriesSubCategories('');
        foreach ($browseNode as $itemKey => $itemName) {
            $selected = '';
            if ($itemKey == $defaultCat)
                $selected = ' selected ';
            $html .= '<option value="' . $itemKey . '" ' . $selected . '>' . $itemName . '</option>';
        }
        $html .= '</select>';

        //add browse node combo
        $html .= '<br><select name="' . $name . '[browse_node]" id="' . $name . '[browse_node]">';
        $browseNode = mage::helper('Amazon/Gua')->getBrowseNodes('all');
        foreach ($browseNode as $itemKey => $itemName) {
            $selected = '';
            if ($itemKey == $defaultBrowseNode)
                $selected = ' selected ';
            $html .= '<option value="' . $itemKey . '" ' . $selected . '>' . $itemName . '</option>';
        }

        $html .= '</select>';
        return $html;
    }

    /**
     * Get marketplace name
     *
     * @return string
     */
    public function  getMarketPlaceName() {
        return 'amazon';
    }

    /**
     * Get marketplace orders
     *
     * @return array
     */
    public function getMarketPlaceOrders(){
        return Mage::helper('Amazon/Orders')->getMarketPlaceOrders();
    }

    /**
     * Import marketplace orders
     *
     * @param array $orders
     * @return <type>
     */
    public function importMarketPlaceOrders($orders){
        return Mage::helper('Amazon/Orders')->importMarketPlaceOrders($orders);
    }

    /**
     * Update stock and prices
     */
    public function updateStocksAndPrices(){
        Mage::helper('Amazon/ProductUpdate')->update();
    }

    /**
     * Send tracking
     */
    public function sendTracking(){
        Mage::Helper('Amazon/Tracking')->sendTracking();
    }

    /**
     * Check product creation
     */
    public function checkProductCreation(){
        Mage::helper('Amazon/ProductCreation')->importCreatedProducts();
    }

    /**
     * Is marketplace allow manual update
     *
     * @return boolean
     */
    public function allowManualUpdate(){
        return false;
    }

}