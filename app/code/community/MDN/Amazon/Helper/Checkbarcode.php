<?php
/* 
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MDN_Amazon_Helper_Checkbarcode extends MDN_MarketPlace_Helper_Checkbarcode {

    const kASIN = 'ASIN';

    /**
     * Retrieve barcode type
     *
     * @param int $code
     * @return string
     */
    public function getType($code){

        $length = strlen($code);
        $type = null;

        switch($length){

            case '12':
                $type = self::kUPC;
                break;
            case '13':
            case '14':
            case '8':
                $type = self::kEAN;
                break;
            case '10':
                $type = self::kASIN;
                break;
            default:
                $type = null;
                break;

        }

        return $type;

    }

}
