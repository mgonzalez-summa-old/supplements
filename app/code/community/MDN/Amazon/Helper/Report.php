<?php

require_once('MDN/Marketplace/Amazon/Client/MWSClient.php');

class MDN_Amazon_Helper_Report extends Mage_Core_Helper_Abstract {
    
    /**
     * Request a report
     *
     * @param string $reportType
     * @param date $startDate
     * @param date $endDate
     *
     * @return string
     *
     */
    public function requestReport($reportType, $startDate, $endDate) {

        $params = array(
            'Action' => 'RequestReport',
            'ReportType' => $reportType
        );

        if ($startDate != null) {

            $params['StartDate'] = $startDate;
        }

        if ($endDate != null) {

            $params['EndDate'] = $endDate;
        }

        $MWSOrders = new MWSClient($params);
        return $MWSOrders->sendRequest()->getBody();
    }

    /**
     * Request report count
     *
     * @return string
     *
     */
    public function getReportCount() {

        $params = array(
            'Action' => 'GetReportCount'
        );

        $MWSOrders = new MWSClient($params);
        return $MWSOrders->sendRequest()->getBody();
    }

    /**
     * Get report by id
     *
     * @param int $reportId
     * @return string
     *
     */
    public function getReport($reportId) {

        $params = array(
            'Action' => 'GetReport',
            'ReportId' => $reportId
        );

        $MWSOrders = new MWSClient($params);
        return $MWSOrders->sendRequest()->getBody();
    }

    /**
     * Get report list
     *
     * @return string
     */
    public function getReportList() {

        $params = array(
            'Action' => 'GetReportList',
            'ReportTypeList.Type.1' => '_GET_ORDERS_DATA_',
            'ReportTypeList.Type.2' => '_GET_MERCHANT_LISTINGS_DATA_',
            'ReportTypeList.Type.3' => '_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_',
            'ReportTypeList.Type.4' => '_GET_FLAT_FILE_ORDERS_DATA_'
        );

        $MWSOrders = new MWSClient($params);
        return $MWSOrders->sendRequest()->getBody();
    }

    /**
     * Get report request list
     *
     * @param string $type
     * @return string
     */
    public function getReportRequestList($type) {

        $params = array(
            'Action' => 'GetReportRequestList',
            'ReportTypeList' => $type
        );

        $MWSOrders = new MWSClient($params);
        return $MWSOrders->sendRequest()->getBody();
    }

    /**
     * Get report after retrive id in request list
     *
     * @param int $requestId
     * @param string $type
     * @return string
     * 
     */
    public function getReportById($requestId, $type) {

        $reportId = "";
        $status = "";
        $xml = new DomDocument();

        $reportList = $this->getReportRequestList($type);

        $xml->loadXML($reportList);

        $reportRequestInfos = $xml->getElementsByTagName('ReportRequestInfo');

        foreach ($reportRequestInfos as $request) {
            if ($request->getElementsByTagName('ReportRequestId')->item(0)->nodeValue == $requestId) {

                $status = $request->getElementsByTagName('ReportProcessingStatus')->item(0)->nodeValue;
                break;
            }
        }

        if ($status == "") {
            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper('Amazon')->getMarketPlaceName(),
                    13,
                    'Unable to find report that corresponding to request id : ' . $requestId,
                    array('fileName' => NULL)
            );
        }

        switch ($status) {

            case "_DONE_":
                $reportId = $request->getElementsByTagName('GeneratedReportId')->item(0)->nodeValue;
                break;
            case "_SUBMITTED_":
            case "_IN_PROGRESS_":
                $reportId = "unvailable";
                break;
            default:
                $reportId = "none";
                break;
        }

        return ($reportId != "none" && $reportId != "unvailable") ? $this->getReport($reportId) : $reportId;
    }

}