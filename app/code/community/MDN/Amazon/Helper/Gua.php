<?php

require_once('MDN/Marketplace/Amazon/Parser/parser/ParseAmazon.class.php');

class MDN_Amazon_Helper_Gua extends Mage_Core_Helper_Abstract {

    private $_cache = array();
    protected $_country = null;

    /**
     * Get configured country
     */
    public function getCountry(){

        if($this->_country === null){
            $this->_country = Mage::getStoreConfig('marketplace/amazon/web_service_url');
        }

        return $this->_country;

    }

    /**
     * Get browse nodes for tree $tree
     *
     * @param string $tree
     * @return array $retour
     */
    public function getBrowseNodes($tree) {

        $retour = array('' => '');

        //search in cache
        $cacheKey = $tree;
        if (isset($this->_cache[$cacheKey])){

            $retour = $this->_cache[$cacheKey];

        }else{

            $trees = $this->getTrees();

            // show all browse nodes
            if ($tree == 'all') {
                foreach ($trees as $tree) {
                    $options = $this->parseGUA($tree);

                    $retour = $retour + $options;
                }
            } else {
                // show selected browse node
                if (in_array($tree, $trees)) {

                    $retour = $this->parseGUA($tree);
                }
            }

            //save in cache
            $this->_cache[$cacheKey] = $retour;

        }

        return $retour;
    }

    /**
     * Parse csv files
     *
     * @param string $tree
     * @return array
     */
    public function parseGUA($tree) {

        $options = array();
        $filename = Mage::app()->getConfig()->getOptions()->getLibDir() . '/MDN/Marketplace/Amazon/GUA/'.$this->getCountry() . '/' .strtolower($this->getCountry()).'_'. strtolower($tree) . '_browse_tree_guide.csv';

        if(file_exists($filename)){
            $tmp = file($filename);

            for ($i = 0; $i < count($tmp) - 1; $i++) {

                if ($i == 0)
                    continue;

                $option = explode(';', $tmp[$i]);
                $node = preg_replace('/"/', "", $option[1]);
                $node = trim($node);
                $code = trim($option[0]);
                $options[$code] = $node;
            }

        }

        return $options;
    }

    /**
     * Get trees avaliable for country $country
     *
     * @param string $country
     * @return array
     */
    public function getTrees() {

        $retour = array();

        $nodes = array(
            'CA' => array(
                '927726' => 'Books',
                '962454' => 'Classical',
                '14113311' => 'DVD',
                '677211011' => 'Electronics',
                '927726' => 'ForeignBooks',
                '2206275011' => 'Kitchen',
                '962454' => 'Music',
                '3234171' => 'Software',
                '3323751' => 'SoftwareVideoGames',
                '962072' => 'VHS',
                '962454' => 'Video',
                '110218011' => 'VideoGames'
            ),
            'CN' => array(
                '2016156051' => 'Apparel',
                '80207071' => 'Appliances',
                '1947899051' => 'Automotive',
                '42692071' => 'Baby',
                '746776051' => 'Beauty',
                '658390051' => 'Books',
                '2016116051' => 'Electronics',
                '2127215051' => 'Grocery',
                '852803051' => 'HealthPersonalCare',
                '2016126051' => 'Home',
                '1952920051' => 'HomeImprovement',
                '816482051' => 'Jewelry',
                '899280051' => 'Miscellaneous',
                '754386051' => 'Music',
                '2127221051' => 'OfficeProducts',
                '755653051' => 'Photo',
                '2029189051' => 'Shoes',
                '863872051' => 'Software',
                '836312051' => 'SportingGoods',
                '647070051' => 'Toys',
                '2016136051' => 'Video',
                '897415051' => 'VideoGames',
                '1953164051' => 'Watches'
            ),
            'DE' => array(
                '78689031' => 'Apparel',
                '78194031' => 'Automotive',
                '357577011' => 'Baby',
                '64257031' => 'Beauty',
                '541686' => 'Books',
                '542676' => 'Classical',
                '547664' => 'DVD',
                '569604' => 'Electronics',
                '54071011' => 'ForeignBooks',
                '340846031' => 'Grocery',
                '64257031' => 'HealthPersonalCare',
                '10925241' => 'HomeGarden',
                '327473011' => 'Jewelry',
                '530484031' => 'KindleStore',
                '3169011' => 'Kitchen',
                '213083031' => 'Lighting',
                '1198526' => 'Magazines',
                '77256031' => 'MP3Downloads',
                '542676' => 'Music',
                '11965861' => 'MusicalInstruments',
                '16291311' => 'OfficeProducts',
                '10925051' => 'OutdoorLiving',
                '569604' => 'PCHardware',
                '569604' => 'Photo',
                '542064' => 'Software',
                '541708' => 'SoftwareVideoGames',
                '16435121' => 'SportingGoods',
                '12950661' => 'Toys',
                '547082' => 'DVD',
                '547664' => 'Video',
                '541708' => 'VideoGames',
                '193708031' => 'Watches'
            ),
            'ES' => array(
                '599364031' => 'Books',
                '599379031' => 'DVD',
                '667049031' => 'Electronics',
                '599367031' => 'ForeignBooks',
                '599391031' => 'Kitchen',
                '599373031' => 'Music',
                '599376031' => 'Software',
                '599385031' => 'Toys',
                '599382031' => 'VideoGames',
                '599388031' => 'Watches'
            ),
            'FR' => array(
                '340855031' => 'Apparel',
                '206617031' => 'Baby',
                '197858031' => 'Beauty',
                '468256' => 'Books',
                '537366' => 'Classical',
                '578608' => 'DVD',
                '1058082' => 'Electronics',
                '69633011' => 'ForeignBooks',
                '197861031' => 'HealthPersonalCare',
                '193711031' => 'Jewelry',
                '57686031' => 'Kitchen',
                '213080031' => 'Lighting',
                '206442031' => 'MP3Downloads',
                '537366' => 'Music',
                '11965861' => 'MusicalInstruments',
                '192420031' => 'OfficeProducts',
                '215934031' => 'Shoes',
                '548012' => 'Software',
                '548014' => 'SoftwareVideoGames',
                '548014' => 'Toys',
                '578610' => 'VHS',
                '578608' => 'Video',
                '548014' => 'VideoGames',
                '60937031' => 'Watches'
            ),
            'IT' => array(
                '411663031' => 'Books',
                '412606031' => 'DVD',
                '412609031' => 'Electronics',
                '433842031' => 'ForeignBooks',
                '635016031' => 'Garden',
                '524015031' => 'Kitchen',
                '412600031' => 'Music',
                '524006031' => 'Shoes',
                '412612031' => 'Software',
                '523997031' => 'Toys',
                '412603031' => 'VideoGames',
                '524009031' => 'Watches'
            ),
            'JP' => array(
                '361299011' => 'Apparel',
                '2017304051' => 'Automotive',
                '13331821' => 'Baby',
                '52391051' => 'Beauty',
                '465610' => 'Books',
                '562032' => 'Classical',
                '562002' => 'DVD',
                '3210991' => 'Electronics',
                '388316011' => 'ForeignBooks',
                '57239051' => 'Grocery',
                '161669011' => 'HealthPersonalCare',
                '13331821' => 'Hobbies',
                '85896051' => 'Jewelry',
                '3839151' => 'Kitchen',
                '2128134051' => 'MP3Downloads',
                '562032' => 'Music',
                '11965861' => 'MusicalInstruments',
                '2016926051' => 'Shoes',
                '637630' => 'Software',
                '14315361' => 'SportingGoods',
                '13331821' => 'Toys',
                '2130989051' => 'VHS',
                '561972' => 'Video',
                '637872' => 'VideoGames',
                '14315361' => 'Watches'
            ),
            'UK' => array(
                '83451031' => 'Apparel',
                '248877031' => 'Automotive',
                '60032031' => 'Baby',
                '66280031' => 'Beauty',
                '1025612' => 'Books',
                '505510' => 'Classical',
                '283926' => 'DVD',
                '560800' => 'Electronics',
                '340834031' => 'Grocery',
                '66280031' => 'HealthPersonalCare',
                '11052591' => 'HomeGarden',
                '2016929051' => 'HomeImprovement',
                '193717031' => 'Jewelry',
                '341677031' => 'KindleStore',
                '11052591' => 'Kitchen',
                '213077031' => 'Lighting',
                '77198031' => 'MP3Downloads',
                '505510' => 'Music',
                '11965861' => 'MusicalInstruments',
                '560800' => 'OfficeProducts',
                '11052591' => 'OutdoorLiving',
                '1025614' => 'Software',
                '1025616' => 'SoftwareVideoGames',
                '319530011' => 'SportingGoods',
                '11052591' => 'Tools',
                '712832' => 'Toys',
                '283926' => 'VHS',
                '283926' => 'Video',
                '1025616' => 'VideoGames',
                '595312' => 'Watches'
            ),
            'US' => array(
                '1036592' => 'Apparel',
                '2619525011' => 'Appliances',
                '2617941011' => 'ArtsAndCrafts',
                '15690151' => 'Automotive',
                '165796011' => 'Baby',
                '11055981' => 'Beauty',
                '1000' => 'Books',
                '301668' => 'Classical',
                '195208011' => 'DigitalMusic',
                '2625373011' => 'DVD',
                '493964' => 'Electronics',
                '3580501' => 'GourmetFood',
                '16310101' => 'Grocery',
                '3760931' => 'HealthPersonalCare',
                '285080' => 'HomeGarden',
                '228239' => 'Industrial',
                '3880591' => 'Jewelry',
                '133141011' => 'KindleStore',
                '1063498' => 'Kitchen',
                '599872' => 'Magazines',
                '10304191' => 'Miscellaneous',
                '2350149011' => 'MobileApps',
                '195211011' => 'MP3Downloads',
                '301668' => 'Music',
                '11965861' => 'MusicalInstruments',
                '1084128' => 'OfficeProducts',
                '1063498' => 'OutdoorLiving',
                '493964' => 'PCHardware',
                '1063498' => 'PetSupplies',
                '493964' => 'Photo',
                '409488' => 'Software',
                '1079730' => 'SportingGoods',
                '468240' => 'Tools',
                '493964' => 'Toys',
                '404272' => 'VHS',
                '130' => 'Video',
                '493964' => 'VideoGames',
                '1079730' => 'Watches',
                '508494' => 'Wireless',
                '13900851' => 'WirelessAccessories'
            )
        );

        if(array_key_exists($this->getCountry(), $nodes)){
            $retour = $nodes[$this->getCountry()];
        }

        return $retour;
    }

    /**
     * Get sub categories
     *
     * @return array $retour
     */
    public function getAllCategoriesSubCategories() {

        $retour = array();

        //check cache
        $cacheKey = 'all_category_and_subcategories';
        if (isset($this->_cache[$cacheKey])){

            $retour = $this->_cache[$cacheKey];

        }else{

            $content = utf8_encode(file_get_contents(Mage::app()->getConfig()->getOptions()->getLibDir() . '/MDN/Marketplace/Amazon/Parser/xsd/Product.xsd'));
            $xml = new DomDocument();
            $xml->loadXML($content);

            $schema = $xml->documentElement;

            foreach ($this->getCategories() as $category) {

                if ($category == "")
                    continue;

                //add subcategory
                $hasSubCategory = false;
                foreach ($this->getSubCategory($category) as $subCategory) {
                    $key = $category . '-' . $subCategory;
                    $name = $category . ' / ' . $subCategory;
                    $retour[$key] = $name;
                    $hasSubCategory = true;
                }
                if (!$hasSubCategory) {
                    $key = $category . '-';
                    $name = $category;
                    $retour[$key] = $name;
                    $hasSubCategory = true;
                }
            }

            //save in cache
            $this->_cache[$cacheKey] = $retour;

        }

        return $retour;
    }

    /**
     * Return product categories
     */
    public function getCategories() {

        $retour = array();

        //check cache
        $cacheKey = 'all_categories';
        if (isset($this->_cache[$cacheKey])){

            $retour = $this->_cache[$cacheKey];

        }else{

            $content = utf8_encode(file_get_contents(Mage::app()->getConfig()->getOptions()->getLibDir() . '/MDN/Marketplace/Amazon/Parser/xsd/Product.xsd'));
            $xml = new DomDocument();
            $xml->loadXML($content);

            $schema = $xml->documentElement;

            foreach ($schema->getElementsByTagName('include') as $include) {

                //parse category
                $schemaLocation = $include->getAttribute('schemaLocation');
                $tmp = explode(".", $schemaLocation);
                $categorie = $tmp[0];
                if ($categorie == "amzn-base")
                    $categorie = "";

                $retour[$categorie] = $categorie;
            }

            //save in cache
            $this->_cache[$cacheKey] = $retour;

        }

        return $retour;
    }

    /**
     * Return sub categories for one category
     */
    public function getSubCategory($category) {

        $subcategories = array();

        $filename = Mage::app()->getConfig()->getOptions()->getLibDir() . '/MDN/Marketplace/Amazon/Parser/xsd/' . $category . '.xsd';
        $parseAmazon = new ParseAmazon($filename);
        $subcategories = $parseAmazon->getSubcategories();

        return $subcategories;
    }

}
