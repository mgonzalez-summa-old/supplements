<?php

require_once('MDN/Marketplace/Amazon/Client/MWSClient.php');

class MDN_Amazon_Helper_CheckConnexion extends Mage_Core_Helper_Abstract implements MDN_MarketPlace_Helper_Interface_CheckConnection {

    /**
     * Check connexion, try login/pass
     *
     * @return mixed
     */
    public function connexion() {

        $params = array(
            'Action' => 'GetReportRequestList',
        );

        $MWSOrders = new MWSClient($params);
        $response = $MWSOrders->sendRequest();

        if ($response->getStatus() == 200)
            return true;

        $xml = new DomDocument();
        $content = $response->getBody();

        $xml->loadXML($content);
        $message = $xml->getElementsByTagName('Error')->item(0)->getElementsByTagName('Message')->item(0)->nodeValue;
        return $message;

    }

}
