<?php
/* 
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MDN_Amazon_Helper_Url extends Mage_Core_Helper_Abstract {


    public function getExt() {

        $value = Mage::getStoreConfig('marketplace/amazon/web_service_url');
        
        if(!$value){
            throw new Exception($this->__('Check webservice url configuration in System > Configuration > Marketplace'));
        }

        $retour = '';

        $ext = array(
            'CA' => '.ca',
            'CN' => '.com.cn',
            'DE' => '.de',
            'ES' => '.es',
            'FR' => '.fr',
            'IT' => '.it',
            'JP' => '.jp',
            'UK' => '.co.uk',
            'US' => '.com'
        );

        $retour = array_key_exists($value, $ext) ? $ext[$value] : '.fr';

        return $retour;
    }

}
