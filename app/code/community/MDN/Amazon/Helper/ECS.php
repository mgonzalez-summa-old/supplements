<?php

class MDN_Amazon_Helper_ECS extends Mage_Core_Helper_Abstract {

    const kTypeSKU = 'SKU';
    const kTypeASIN = 'ASIN';
    const kTypeUPC = 'UPC';
    const kTypeEAN = 'EAN';

    /**
     * Get types
     *
     * @return array
     */
    public function getTypes(){

        return array(
            self::kTypeSKU,
            self::kTypeASIN,
            self::kTypeUPC,
            self::kTypeEAN
        );

    }

    /**
     * Request
     *
     * @param int $itemId
     * @param int $idType
     * @return object $response
     */
    public function request($itemId, $idType){

        // init
        $host = "ecs.amazonaws".Mage::Helper('Amazon/Url')->getExt();
        $path = "/onca/xml";

        $AWSAccessKey = Mage::getStoreConfig('marketplace/amazon/productadvertising_access_key_id');
        $secretAccessKey = Mage::getStoreConfig('marketplace/amazon/productadvertising_secret_key');
        $tag = Mage::getStoreConfig('marketplace/amazon/productadvertising_associateTag');

        $timestamp = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time() + 3600 * 2);

        $idType = (in_array($idType, $this->getTypes())) ? $idType : 'ASIN';

        // set params
        $params = array(
            'Service'         => 'AWSECommerceService',
            'AWSAccessKeyId'  => $AWSAccessKey,
            'Operation'       => 'ItemLookup',
            'ItemId'          => $itemId,
            'IdType'          => $idType,
            'Timestamp'       => $timestamp,
            'MerchanId'       => 'All',
            'Condition'       => 'All',
            'Version'         => '2011-08-01',
            'AssociateTag'    => $tag,
            'ResponseGroup'   => 'Large'
        );

        if($idType != 'ASIN')
            $params['SearchIndex'] = "All";

        if($idType == 'SKU'){
            $merchantId = mage::getStoreConfig('marketplace/amazon/amazon_merchant_id');
            $params['MerchantId'] = $merchantId;
        }

        // sort parameters
        uksort($params, 'strcmp');

        // encode params
        foreach($params as $param => $value){
            $param = str_replace('%7E', '~', rawurlencode($param));
            if ($param != 'Timestamp')  //do not encode timestamp !!!
                $value = str_replace('%7E', '~', rawurlencode($value));
        }

       // calculate signature
       $signature = $this->calculSignature($params, $secretAccessKey, $host, $path);

       // add signature to query parameters
       $params['Signature'] = $signature;

       // send request
       $client = new Zend_Http_Client();
       $client->setUri('http://'.$host.$path);

       $client->setConfig(array(
               'maxredirects' => 0,
               'timeout'      => 30
           )
       );

       $client->setParameterGet($params);
       
       $response = $client->request();

       return $response;

    }

    /**
     *
     * @param object $response
     * @return array
     */
    public function parseResponse($response){

        $retour = array(
            'error' => false,
            'requestId' => '',
            'data' => null
        );

        $xml = new DomDocument();
        $xml->loadXML($response);

        $retour['requestId'] = $xml->getElementsByTagName('RequestId')->item(0)->nodeValue;

        if($xml->getElementsByTagName('Errors')->item(0))
                $retour['error'] = true;

                //load first error message
                if($xml->getElementsByTagName('Error')->item(0)){
                    $retour['message'] = $xml->getElementsByTagName('Error')->item(0)->nodeValue;
                }

        else{
            if($xml->getElementsByTagName('Items')->item(0)){

                $items = $xml->getElementsByTagName('Items')->item(0);

                $typeId = $items->getElementsByTagName('IdType')->item(0)->nodeValue;
                $itemId = $items->getElementsByTagName('ItemId')->item(0)->nodeValue;
                $asin = $items->getElementsByTagName('ASIN')->item(0)->nodeValue;

                $retour['data'] = array(
                    'IdType' => $typeId,
                    'ItemId' => $itemId,
                    'ASIN' => $asin
                );

            }
        }

        return $retour;

    }

    /**
     * calcul signature
     *
     * @param <type> $params
     * @param <type> $secretAccessKey
     * @param <type> $host
     * @param <type> $path
     * @return <type>
     */
    public function calculSignature($params, $secretAccessKey, $host, $path){

        $tmp = array();
        $signature = "";

        // url encode parameters names and values
        foreach($params as $param => $value){

            if ($param == 'Timestamp')  //now, encode timestamp
                $value = str_replace('%7E', '~', rawurlencode($value));

            $tmp[] = $param."=".$value;
        }

        // construct query string
        $query = implode('&', $tmp);

        // construct signToString
        $signToString = "GET"."\n";
        $signToString .= $host."\n";
        $signToString .= $path."\n";
        $signToString .= $query;

        // HMAC $signToString with $secretAccessKey and convert result to base64
        $signature = base64_encode(hash_hmac("sha256", $signToString, $secretAccessKey, true));

        return $signature;
    }

}