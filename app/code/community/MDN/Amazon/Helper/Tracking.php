<?php

require_once('MDN/Marketplace/Amazon/Client/MWSFeed.php');
require_once('MDN/Marketplace/Amazon/Client/MWSClient.php');

class MDN_Amazon_Helper_Tracking extends Mage_Core_Helper_Abstract implements MDN_MarketPlace_Helper_Interface_Tracking {

    /**
     * Build and send send tracking file
     */
    public function sendTracking() {

        if ($this->buildTrackingFile()) {

            $this->send();

        }
        else{
            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper('Amazon')->getMarketPlaceName(),
                    0,
                    'No tracking to send.',
                    array('fileName' => NULL)
            );
        }

        $this->getOrdersToUpdate();
    }

    
    /**
     * Retrieve waiting shipment orders
     * 
     * @return boolean $retour
     */
    public function buildTrackingFile() {

        // init
        $reportRequestId = "";
        $retour = false;
        $unchippedOrders = array();
        $ordersIds = array();

        $path = Mage::app()->getConfig()->getTempVarDir() . '/export/marketplace/tmp';
        if (file_exists($path.'/_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_ids.txt')) {
            $content = file_get_contents($path.'/_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_ids.txt');
            $reportRequestId = trim($content);
        }

        if ($reportRequestId != "") {

            $report = mage::helper('Amazon/Report')->getReportById($reportRequestId, '_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_');

            if ($report != "none") {
                if ($report != "unvailable") {

                    // get orders ids
                    $lines = explode("\n", $report);

                    for ($i = 0; $i < count($lines) - 1; $i++) {

                        if ($i == 0)
                            continue;

                        if ($lines[$i] != "") {

                            $tmp = explode("\t", utf8_encode($lines[$i]));
                            $unchippedOrders[] = $tmp[0];
                        }
                    }

                    if (count($unchippedOrders) > 0) {

                        $orders = mage::getModel('sales/order')
                                        ->getCollection()
                                        ->addAttributeToSelect('*')
                                        ->addAttributeToFilter('from_site', 'amazon')
                                        ->addAttributeToFilter('marketplace_order_id', $unchippedOrders)
                                        ->addAttributeToFilter('status', 'complete');

                        foreach ($orders as $order) {

                            $ordersIds[] = $order->getentity_id();
                        }

                        $merchantId = mage::getStoreConfig('marketplace/amazon/amazon_merchant_id');

                        $mwsFeed = MWSFeed::getInstance();
                        $xml = $mwsFeed->buildXMLTrackingFile($ordersIds, $merchantId);

                        // save file in order to be send
                        mage::getModel('MarketPlace/Logs')->saveFile(
                                array(
                                    'filenames' => array('exportTracking_' . date('Y-m-d_H:i:s') . '.xml', 'exportTracking.xml'),
                                    'fileContent' => $xml,
                                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                                    'type' => 'export'
                                )
                        );

                        $retour = true;
                    }
                }
            }

            file_put_contents($path . '/_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_ids.txt', "");
        }

        return $retour;
    }

    /**
     * Send tracking file
     *
     * @return mixed boolean | string
     */
    public function send() {

        $filename = Mage::getConfig()->getTempVarDir() . '/export/marketplace/amazon/exportTracking.xml';

        if(file_exists($filename)){

            $content = file_get_contents($filename);

            $params = array(
                'Action' => 'SubmitFeed',
                'FeedType' => '_POST_ORDER_FULFILLMENT_DATA_'
            );

            $mws = new MWSClient($params);
            $response = $mws->sendFeed($filename);

            $feed = Mage::getModel('MarketPlace/Feed')
                    ->setmp_content($content)
                    ->setmp_response($response->getBody())
                    ->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                    ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeTracking)
                    ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName());

            if ($response->getStatus() == 200) {

                $xml = new DomDocument();
                $report = $response->getBody();
                $xml->loadXML($report);
                $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

                $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedDone)
                        ->setmp_feed_id($feedSubmissionId)
                        ->save();

                return $response;
            }
            else{

                $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                        ->setmp_feed_id(' - ')
                        ->save();

                throw new Exception($response->getMessage(), 16);
            }

            /////////////////////////////////////////////////////////////////////

            /*if ($response->getStatus() == 200) {

                // save file in order to be send
                mage::getModel('MarketPlace/Logs')->saveFile(
                        array(
                            'filenames' => array('exportTrackingResponse_' . date('Y-m-d_H:i:s') . '.xml'),
                            'fileContent' => $response->getBody(),
                            'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                            'type' => 'export'
                        )
                );

                // add log message
                /*mage::getModel('MarketPlace/Logs')->addLog(
                        Mage::helper('Amazon')->getMarketPlaceName(),
                        0,
                        'Tracking send.',
                        array('fileName' => NULL)
                );*/

                /*return true;

            } else {
                throw new Exception($response->getMessage(), 16);
            }*/

            /////////////////////////////////////////////////////////////////////
        }
        else{
            return false;
        }
    }

    /**
     * Get orders to update (waiting for tracking)
     *
     * @return int $requestId
     */
    public function getOrdersToUpdate() {

        // request report
        $response = mage::helper('Amazon/Report')->requestReport('_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_', null, null);

        $xml = new DomDocument();
        $xml->loadXML($response);

        // check if some error occures
        if ($xml->getElementsByTagName('ErrorResponse')->item(0)) {

            $type = $xml->getElementsByTagName('Type')->item(0)->nodeValue;
            $code = $xml->getElementsByTagName('Code')->item(0)->nodeValue;
            $message = $xml->getElementsByTagName('Message')->item(0)->nodeValue;

            $errorMessage = " Some error occured while attempt to request order report : <ul><li>Type : $type</li><li>Code : $code</li><li>Message : $message</li></ul>";

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper('Amazon')->getMarketPlaceName(),
                    12,
                    $errorMessage,
                    array('fileName' => NULL)
            );

            $requestId = "";
        } else {
            // read request id
            $requestId = $xml->getElementsByTagName('ReportRequestInfo')->item(0)->getElementsByTagName('ReportRequestId')->item(0)->nodeValue;
        }

        // save request id
        $path = Mage::app()->getConfig()->getTempVarDir() . '/export/marketplace/tmp';
        $filename = $path . '/_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_ids.txt';

        if (!file_exists($path))
            mkdir($path, 0755, true);

        file_put_contents($filename, $requestId);

        // debug mode
        return $requestId;

    }

}