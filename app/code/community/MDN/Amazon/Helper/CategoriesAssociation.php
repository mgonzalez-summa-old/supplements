<?php

class MDN_Amazon_Helper_CategoriesAssociation extends Mage_Core_Helper_Abstract {

    /**
     * Return default browse node according to product's categories association
     */
    public function getDefaultBrowseNode($product) {

        $data = $this->getDefaultCategoryData($product);
        if (isset($data['browse_node']))
            return $data['browse_node'];
        else
            return null;
    }

    /**
     * Return default amazon category depending of product's categories
     */
    public function getDefaultCategory($product) {
        $data = $this->getDefaultCategoryData($product);
        if ($data != '') {
            $data = $data['cat'];
            $t = explode('-', $data);
            if (count($t == 2))
                return $t[0];
        }
        else
            return null;
    }

    /**
     * Get default product type
     *
     * @param object $product
     * @return mixed null | string
     */
    public function getDefaultProductType($product) {

        $data = $this->getDefaultCategoryData($product);
        if ($data != '') {
            $data = $data['cat'];
            $t = explode('-', $data);
            if (count($t == 2))
                return $t[1];
        }
        else
            return null;
    }

    /**
     * Return datas association to product / categorires / marketplace
     */
    private function getDefaultCategoryData($product) {
        return mage::helper('MarketPlace/Categories')->getCategoryDataForProduct($product, mage::helper('Amazon')->getMarketPlaceName());
    }

}

?>
