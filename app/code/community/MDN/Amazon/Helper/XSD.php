<?php

class MDN_Amazon_Helper_XSD extends Mage_Core_Helper_Abstract {

    /**
     * Retrieve main node
     *
     * @param string $category
     * @return string $retour
     */
    public function retrieveMainNode($category) {

        $retour = "";

        if ($category == "SWVG") {

            $retour = "SoftwareVideoGames";
        } elseif ($category == "ProductClothing") {

            $retour = "Clothing";
        }
        else
            $retour = $category;

        return $retour;
    }

    /**
     * Retrieve filename
     *
     * @param string $category
     * @return string $retour
     */
    public function retrieveFilename($category) {

        $retour = "";

        switch ($category) {
            case 'SoftwareVideoGames':
                $retour = 'SWVG';
                break;
            case 'Clothing':
                $retour = 'ProductClothing';
                break;
            default:
                $retour = $category;
        }

        return $retour;
    }

    /**
     * Retrieve filename and main node
     *
     * @param string $category
     * @return array $retour
     */
    public function retrieveFilenameAndMainNode($category) {

        $retour = array();

        $retour['filename'] = $category;

        $retour['category'] = $this->retrieveMainNode($category);

        return $retour;
    }

    /**
     * Get xsd filenames
     *
     * @return array $xsd
     */
    public function getFilenames() {

        $xsd = array(
            //array('name' => 'ClothingAccessories.xsd', 'requiredfields' => true),
            array('name' => 'ProductClothing.xsd', 'requiredfields' => true),
            //array('name' => 'Miscellaneous.xsd', 'requiredfields' => false),
            array('name' => 'CameraPhoto.xsd', 'requiredfields' => false),
            array('name' => 'Home.xsd', 'requiredfields' => true),
            array('name' => 'Sports.xsd', 'requiredfields' => false),
            array('name' => 'HomeImprovement.xsd', 'requiredfields' => false),
            array('name' => 'Tools.xsd', 'requiredfields' => false),
            array('name' => 'FoodAndBeverages.xsd', 'requiredfields' => false),
            array('name' => 'Gourmet.xsd', 'requiredfields' => false),
            array('name' => 'Jewelry.xsd', 'requiredfields' => false),
            array('name' => 'Health.xsd', 'requiredfields' => false),
            array('name' => 'CE.xsd', 'requiredfields' => true),
            array('name' => 'SWVG.xsd', 'requiredfields' => true),
            array('name' => 'Wireless.xsd', 'requiredfields' => false),
            array('name' => 'Beauty.xsd', 'requiredfields' => false),
            array('name' => 'Office.xsd', 'requiredfields' => false),
            array('name' => 'MusicalInstruments.xsd', 'requiredfields' => false),
            array('name' => 'AutoAccessory.xsd', 'requiredfields' => true),
            array('name' => 'PetSupplies.xsd', 'requiredfields' => false),
            array('name' => 'ToysBaby.xsd', 'requiredfields' => false),
            array('name' => 'TiresAndWheels.xsd', 'requiredfields' => true),
            array('name' => 'Music.xsd', 'requiredfields' => true),
            array('name' => 'Video.xsd', 'requiredfields' => false),
            array('name' => 'Lighting.xsd', 'requiredfields' => false),
        );

        return $xsd;
    }

    /**
     * Get all categories
     *
     * @param boolean $required
     * @return array $retour
     */
    public function getAllCategories($required = false) {

        $retour = array();

        $filenames = $this->getFilenames();

        foreach ($filenames as $filename) {
            if($required){
                if($filename['requiredfields']){
                    $tmp = explode(".", $filename['name']);
                    $retour[] = $this->retrieveMainNode($tmp[0]);
                }
            }else{
                $tmp = explode(".", $filename['name']);
                $retour[] = $this->retrieveMainNode($tmp[0]);
            }
            
        }

        return $retour;
    }

    /**
     * Get unit of measure for required fields
     *
     * @return array $retour
     */
    public function getUnitOfMeasureForRequiredFields(){

        $retour = array(
            'PackageWeight' => 'KG',
            'ShippingWeight' => 'KG',
            'RAMSize' => 'GO',
            'HardDriveSize' => 'GO',
            'ProcessorSpeed' => 'GHz',
            'PitchCircleDiameter' => 'M'
        );

        return $retour;

    }
    
}
