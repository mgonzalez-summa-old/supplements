<?php

require_once('MDN/Marketplace/Amazon/Client/MWSFeed.php');
require_once('MDN/Marketplace/Amazon/Client/MWSClient.php');
require_once('MDN/Marketplace/Amazon/Parser/parser/ParseAmazonRequiredFields.class.php');

class MDN_Amazon_Helper_ProductCreation extends MDN_MarketPlace_Helper_ProductCreation implements MDN_MarketPlace_Helper_Interface_ProductCreation {

    /**
     * Get product feed name
     *
     * @return string
     */
    public function getProductFeedName() {
        return 'productFeed.xml';
    }

    /**
     * Get file type
     *
     * @return string
     */
    public function getFileType() {
        return 'xml';
    }

    /**
     * Add product (useless)
     *
     * @param $request
     * @param string $marketplaceName
     */
    public function addProduct($request) {

        $this->setProductFeed($request);
        $mp_product_id = Mage::getModel('Catalog/Product')->getIdBySku($request->getParam('product_sku'));
        $this->sendProductFile($mp_product_id);
    }

    /**
     * Build & save product feed (useless)
     *
     * @param $request
     */
    public function setProductFeed($request) {

        $multiplesNodes = array(
            'bullet_point' => array(),
            'search_terms' => array(),
            'platinum_keywords' => array(),
            'used_for' => array(),
            'other_item_attributes' => array(),
            'target_audience' => array(),
            'subject_content' => array()
        );

        // get nbr occurs for multiples nodes
        foreach ($multiplesNodes as $k => $value) {

            $nbr = $request->getParam('product_nbr_' . $k);
            for ($i = 0; $i <= $nbr; $i++) {

                $tmp = $request->getParam('product_' . $k . '_' . $i);
                if ($tmp != "")
                    $multiplesNodes[$k][$i] = $tmp;
            }
        }

        // get merchant id
        $id = mage::getStoreConfig('marketplace/amazon/amazon_merchant_id');

        // general informations node
        $new_product[0]['general'] = array(
            'SKU' => $request->getParam('product_sku'),
            'ProductTaxCode' => $request->getParam('product_tax_code'),
            'LaunchDate' => $request->getParam('product_launch_date'),
            'ReleaseDate' => $request->getParam('product_release_date'),
            'Condition' => $request->getParam('product_condition'),
            //'Rebate'            => array(),
            'ItemPackageQuantity' => $request->getParam('product_item_package_quantity'),
            'NumberOfItems' => $request->getParam('product_number_of_items'),
        );

        $productData = $request->getParam('product_data');
        $details = array();

        foreach ($request->getPost() as $k => $v) {

            // find ProductData elt, check if input is not empty and if select is not null
            if (preg_match('#^ProductData_#', $k) && $v != "" && $v != "0") {

                $details[] = $this->buildProductDataArray($k, $v);
            }
        }

        // product data node
        $new_product[0]['ProductData'] = array(
            'name' => $productData,
            'details' => $details
        );

        // standard product id node
        $code = $request->getParam('product_ean');
        $codeType = Mage::helper('MarketPlace/Checkbarcode')->getType($code);
        $new_product[0]['StandardProductID'] = array(
            'Type' => $codeType,
            'Value' => $code
        );

        // description data node
        $new_product[0]['descriptionData'] = array(
            'Title' => $request->getParam('product_title'),
            'Brand' => $request->getParam('product_brand'),
            'Designer' => $request->getParam('product_designer'),
            'Description' => $request->getParam('product_description'),
            'BulletPoint' => $multiplesNodes['bullet_point'],
            'ItemDimensions' => $request->getParam('product_item_dimensions'),
            'PackageDimensions' => $request->getParam('product_package_dimensions'),
            'PackageWeight' => $request->getParam('product_package_weight'),
            'ShippingWeight' => $request->getParam('product_shipping_weight'),
            'MerchantCatalogNumber' => $request->getParam('product_merchant_catalog_number'),
            'MSRP' => $request->getParam('product_msrp'), // add attribute currency !!
            'MaxOrderQuantity' => $request->getParam('product_max_order_quantity'),
            'SerialNumberRequired' => $request->getParam('product_serial_number_required'),
            'Prop65' => $request->getParam('product_prop65'),
            'LegalDisclaimer' => $request->getParam('product_legal_disclaimer'),
            'Manufacturer' => $request->getParam('product_manufacturer'),
            'MfrPartNumber' => $request->getParam('product_manufacturer_part_number'),
            'SearchTerms' => $multiplesNodes['search_terms'],
            'PlatinumKeywords' => $multiplesNodes['platinum_keywords'],
            'RecommendedBrowseNode' => $request->getParam('product_recommended_browse_node'),
            'Memorabilia' => $request->getParam('product_memorabilia'),
            'Autographed' => $request->getParam('product_autographed'),
            'UsedFor' => $multiplesNodes['used_for'],
            'ItemType' => $request->getParam('product_item_type'),
            'TargetAudience' => $multiplesNodes['target_audience'],
            'SubjectContent' => $multiplesNodes['subject_content'],
            'IsGiftWrapAvailable' => $request->getParam('product_is_gift_wrap_available'),
            'IsGiftMessageAvailable' => $request->getParam('product_is_gift_message_available'),
            'IsDiscontinuedByManufacturer' => $request->getParam('product_is_discontinued_by_manufacturer'),
            'MaxAggregateShipQuantity' => $request->getParam('product_max_aggregate_ship_quantity')
        );

        // build xml file
        $content = MWSFeed::getInstance()->buildProductFeed($id, $new_product);

        // save file to export
        $fileToExport = $this->getProductFeedName();
        $history = 'productFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $fileToExport),
                    'fileContent' => $content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );
    }

    /**
     * Mass product creation
     *
     * @param request $request
     */
    public function massProductCreation($request) {

        $retour = $this->buildMassProductFile($request);
        if ($retour == 0)
            $this->sendProductFile();
    }

    /**
     * Build mass product file
     *
     * @param request $request
     * @return int
     */
    public function buildMassProductFile($request) {

        $ids = $request->__get('product_ids');
        $asins = $request->__get('product_asins');
        $matchWithAsin = (count($asins) == count($ids)) ? true : false;
		
        $content = "";
        $products = array();
        $invalidCodes = array();
        $unassociatedCategories = array();
        $errorMessage = "";
        $alreadyCreated = array();

        $manufacturerAttribute = mage::getStoreConfig('marketplace/general/manufacturer_attribute_name');

        if ($manufacturerAttribute == "") {

            // TODO : move log !!
            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper('Amazon')->getMarketPlaceName(),
                    0,
                    'Manufacturer attribute haven\'t been setted in System > Configuration > MarketPlace',
                    array(
                        'fileName' => NULL
                    )
            );
        } else {

            $i = 0;
            foreach ($ids as $id) {

                // retrieve product
                $product = Mage::getModel('catalog/product')->load($id);

                // check if it is not created yet
                $is_created = Mage::getModel('MarketPlace/Data')->isCreated($product, Mage::helper('Amazon')->getMarketPlaceName());

                if ($is_created) {
                    $alreadyCreated[] = array('id' => $id);
                    continue;
                }

                // retrieve informations
                $category = Mage::helper('Amazon/CategoriesAssociation')->getDefaultCategory($product);
                $productType = Mage::helper('Amazon/CategoriesAssociation')->getDefaultProductType($product);
                $recommendedBrowseNode = Mage::helper('Amazon/CategoriesAssociation')->getDefaultBrowseNode($product);
                $code = Mage::helper('MarketPlace/Barcode')->getBarcodeForProduct($product);

                // check barcode
                if (Mage::helper('MarketPlace/Checkbarcode')->checkCode($code) !== true) {
                    $invalidCodes[] = $product->getentity_id();
                    continue;
                }

                // GENERAL
                $products[$i]['general'] = array(
                    'SKU' => $product->getsku()
                );

                // PRODUCT TYPE
                if ($category !== NULL && $category != ""){

                    // retrieve required fields
                    $parsed = $this->getRequiredFields($category);

                    // required fields for main category
                    $requiredMainCat = $parsed['mainCat']; 

                    $mainCatFields = array();
                    foreach ($requiredMainCat as $k => $v) {

                        if($v['parentNode'] != 'ProductType'){

                            $name = $category .'_'. $v['parentNode'];

                            $attributeName = Mage::getModel('MarketPlace/Requiredfields')->loadValueForPath($name, 'amazon');

                            if($attributeName == NULL || $attributeName == ""){
                                //throw new Exception('You must configure required attributes for category : '.$category);
								continue;
                            }
                            
                            $value = $product->getData($attributeName);

                            $mainCatFields[$v['parentNode']] = $value;
                        }
                        
                    }

                    $details = array();
                    $cptDetails = 0;

                    if ($productType !== NULL && $productType != "") {

                        // required fields for subcategory $productType
                        $requiredSubCat = $parsed['subCat'][$productType]; 
                        
                        // retrieve values for subcategory required fields
                        $subCatFields = array();
                        foreach ($requiredSubCat as $k => $v) {

                            $name = $category . '_' . $productType .'_'. $v['parentNode'];
                            $attributeName = Mage::getModel('MarketPlace/Requiredfields')->loadValueForPath($name, 'amazon');

                            if($attributeName == NULL || $attributeName == ""){
                                //throw new Exception('You must configure required attributes for category : '.$category);
								continue;
                            }
                            
                            $value = $product->getData($attributeName);

                            $subCatFields[$productType .'_'. $v['parentNode']] = $value;
                        }
                        
                        $details[$cptDetails]['ProductType'] = $productType;

                        foreach ($subCatFields as $k => $v) {
                            $cptDetails++;
                            $details[$cptDetails] = $this->buildProductDataArray($k, $v);
                        }
                        
                    }
                    else{
                        // TODO : improve this part !
                        // set index 0 see MWSFeed class line 193
                        $details[$cptDetails]['ProductType'] = null;
                    }

                    foreach ($mainCatFields as $k => $v) {
                        $cptDetails++;
                        $details[$cptDetails] = $this->buildProductDataArray($k, $v);
                    }

                    // array which will be transformed into a node
                    $products[$i]['ProductData'] = array(
                        'name' => Mage::helper('Amazon/XSD')->retrieveMainNode($category),
                        'details' => $details
                    );
                }else{
                    $unassociatedCategories[] = $id;
                }

                // STANDARD PRODUCT ID
                $code = ($matchWithAsin === true) ? $asins[$i] : $code; // check if trying to match with asin
                $barcodeType = Mage::helper('Amazon/Checkbarcode')->getType($code);
                $products[$i]['StandardProductID'] = array(
                    'Type' => $barcodeType,
                    'Value' => $code
                );

                // DESCRIPTION DATA
                $products[$i]['descriptionData'] = array(
                    'Title' => Mage::helper('MarketPlace/Product')->formatExportedTxt($product->getname()),
                    'Brand' => $product->getAttributeText(Mage::getStoreConfig('marketplace/general/brand_attribute_name')),
                    'Description' => Mage::helper('MarketPlace/Product')->formatExportedTxt($product->getshort_description())
                );

                $weight = $product->getweight();
                if($weight !== "" && $weight != 0){
                    $products[$i]['descriptionData']['PackageWeight'] = round($weight,2);
                    $products[$i]['descriptionData']['ShippingWeight'] = round($weight,2);
                }

                $products[$i]['descriptionData']['Manufacturer'] = $product->getAttributeText($manufacturerAttribute);
                $products[$i]['descriptionData']['MfrPartNumber'] = $code;

                // add recommended browse node if it is defined
                if ($recommendedBrowseNode !== NULL && $recommendedBrowseNode != "") {
                    $products[$i]['descriptionData']['RecommendedBrowseNode'] = $recommendedBrowseNode;
                }

                $i++;
            }

            if (count($unassociatedCategories) > 0) {
                Mage::getModel('MarketPlace/Data')->updateStatus($unassociatedCategories, Mage::helper('Amazon')->getMarketPlaceName(), self::kStatusInError);
                $errorMessage .= "Check categories association for products : ";
                foreach ($unassociatedCategories as $k => $v) {
                    $errorMessage .= $v . ' ';
                    Mage::getModel('MarketPlace/Data')->addMessage($v, 'Check categories association', Mage::Helper('Amazon')->getMarketPlaceName());
                }
                $errorMessage .= '<br/>';
            }

            if (count($invalidCodes) > 0) {
                Mage::getModel('MarketPlace/Data')->updateStatus($invalidCodes, Mage::helper('Amazon')->getMarketPlaceName(), self::kStatusInError);
                $errorMessage .= 'Invalid EAN code for products : ';
                foreach ($invalidCodes as $k => $v) {
                    $errorMessage .= $v . ' ';
                    Mage::getModel('MarketPlace/Data')->addMessage($v, 'Invalid EAN code', Mage::Helper('Amazon')->getMarketPlaceName());
                }
                $errorMessage .= '<br/>';
            }

            if (count($alreadyCreated) > 0) {
                $errorMessage .= "Already created : ";
                foreach ($alreadyCreated as $created) {
                    $errorMessage .= $created['id'] . ' ';
                }
                $errorMessage .= '<br/>';
            }

            if ($errorMessage != "") {
                // TODO : move log !
                mage::getModel('MarketPlace/Logs')->addLog(
                        Mage::helper('Amazon')->getMarketPlaceName(),
                        19,
                        $errorMessage,
                        array(
                            'fileName' => NULL
                        )
                );
            }

            if (count($products) > 0) {

                // get merchant id
                $merchant_id = mage::getStoreConfig('marketplace/amazon/amazon_merchant_id');

                // build xml file
                $content = MWSFeed::getInstance()->buildProductFeed($merchant_id, $products);

                // save file to export
                $fileToExport = $this->getProductFeedName();
                $history = 'productFeed_' . date('Y-m-d_H:i:s') . '.xml';

                mage::getModel('MarketPlace/Logs')->saveFile(
                        array(
                            'filenames' => array($history, $fileToExport),
                            'fileContent' => $content,
                            'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                            'type' => 'export'
                        )
                );

                return 0;
            } else {
                // TODO : move log !
                mage::getModel('MarketPlace/Logs')->addLog(
                        Mage::helper('Amazon')->getMarketPlaceName(),
                        0,
                        'No product to export.',
                        array(
                            'fileName' => NULL
                        )
                );

                return 1;
            }
        }
    }

    /**
     * Get required fields
     *
     * @param string $category
     * @return array $parsed
     */
    public function getRequiredFields($category) {

        $parsed = array();

        $xsdTab = Mage::helper('Amazon/XSD')->retrieveFilenameAndMainNode($category);

        $filename = Mage::app()->getConfig()->getOptions()->getLibDir() . '/MDN/Marketplace/Amazon/Parser/xsd/' . $xsdTab['filename'] . '.xsd';

        $parseAmazon = new ParseAmazonRequiredFields($filename);
        $parsed = $parseAmazon->getRequiredFields($xsdTab['category']);

        return $parsed;
    }

    /**
     * Build product data array
     *
     * @param string $k
     * @param string $v
     * @return array $retour
     * 
     */
    public function buildProductDataArray($k, $v) {

        // init
        $retour = array();

        // get sub nodes
        $tmp = explode("_", $k);

        // remove first item if this value egals "ProductData", redundant information
        if ($tmp[0] == "ProductData")
            $tmp = array_slice($tmp, 1, count($tmp) - 1);

        if (count($tmp) > 1) {

            // get first key
            $firstKey = $tmp[0];

            // build new array, remove first elt
            $newTmp = array_slice($tmp, 1, count($tmp) - 1);

            // build new key
            $newKey = implode("_", $newTmp);

            // recursive call
            $retour[$firstKey] = $this->buildProductDataArray($newKey, $v);
        } else {
            // add value
            $retour[$tmp[0]] = Mage::helper('MarketPlace/Product')->formatExportedTxt($v);
        }

        return $retour;
    }

    /**
     * Send product feed
     *
     * @param int $mp_product_id
     * @param array $identifiants
     *
     */
    public function sendProductFile($mp_product_id = null) {

        $file = $this->getProductFeedName();
        $path = $this->getExportPath(Mage::helper('Amazon')->getMarketPlaceName());
        $filename = $path . $file;

        $params = array(
            'Action' => 'SubmitFeed',
            'FeedType' => '_POST_PRODUCT_DATA_'
        );

        $mws = new MWSClient($params);
        $response = $mws->sendFeed($filename);

        if ($response->getStatus() == 200) {

            $xml = new DomDocument();
            $report = $response->getBody();
            $xml->loadXML($report);
            $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper('Amazon')->getMarketPlaceName(),
                    0,
                    'Product has been created. (feed submission id : ' . $feedSubmissionId . ')',
                    array(
                        'fileName' => "amazonProductFeedReport-" . $feedSubmissionId . ".xml",
                        'fileContent' => $response->getBody(),
                        'type' => 'export'
                    )
            );

            $xml = new DomDocument('1.0', 'utf8');
            $xml->load($filename);
            $tabIds = array();

            foreach ($xml->getElementsByTagName('SKU') as $skuElt) {


                $current_sku = $skuElt->nodeValue;

                $mp_product_id = Mage::getModel('Catalog/Product')->getIdBySku($current_sku);

                $tabIds[] = $mp_product_id;

            }

            // update product status
            Mage::getModel('MarketPlace/Data')->updateStatus($tabIds, strtolower(Mage::helper('Amazon')->getMarketPlaceName()), self::kStatusPending);

            $ids = implode(',', $tabIds);

            $feed_content = file_get_contents($filename);
            $feed = Mage::getModel('MarketPlace/Feed')
                            ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName())
                            ->setmp_feed_id($feedSubmissionId)
                            ->setmp_status(self::kFeedSubmitted)
                            ->setmp_ids($ids)
                            ->setmp_content($feed_content)
                            ->setmp_response('')
                            ->setmp_type(self::kFeedTypeProductCreation)
                            ->setmp_date(date('Y-m-d H:i:s'))
                            ->save();
        }
        else
            throw new Exception($response->getMessage(), 4);
    }

    /**
     * Import created products, call by the CRON
     *
     * @param string $marketplaceName
     * @see checkProductCreation()
     * @see importUnmatchedProducts()
     */
    public function importCreatedProducts() {

        $this->checkProductCreation();
        $this->importUnmatchedProducts();
    }

    /**
     * Check product creation
     *
     * @param string $marketplaceName
     */
    public function checkProductCreation() {

        // get request errors
        $errors = array();
        $success = array();
        //$unvailable = array();

        $addedProducts = array();

        // load feed collection
        $collection = Mage::getModel('MarketPlace/Feed')
                        ->getCollection()
                        ->addFieldToFilter('mp_marketplace_id', Mage::helper('Amazon')->getMarketPlaceName())
                        ->addFieldToFilter('mp_type', self::kFeedTypeProductCreation)
                        ->addFieldToFilter(
                                'mp_status',
                                array(
                                    'in',
                                    array(
                                        self::kFeedSubmitted,
                                        self::kFeedInProgress
                                    )
                                )
        );

        // check each lines
        foreach ($collection as $feedSubmission) {

            $id = $feedSubmission->getmp_feed_id();

            // request result to amazon
            $response = mage::helper('Amazon/Feed')->getFeedSubmissionResult($id);

            // parse response
            $xml = new DomDocument('1.0', 'utf8');
            $xml->loadXML($response);
            $racine = $xml->documentElement;

            /*******************************************************************
             *
             *                    GLOBAL ERRORS
             *
             ******************************************************************/
            // check if response contains some error (GLOBAL ERRORS)
            if ($racine->getElementsByTagName('Error')->item(0)) {

                $code = $racine->getElementsByTagName('Error')->item(0)->getElementsByTagName('Code')->item(0)->nodeValue;
                
                if ($code == "FeedProcessingResultNotReady" || $code == "RequestThrottled") {

                    $feedSubmission->setmp_status(self::kFeedInProgress)
                            ->save();
                } else {
                    $log = "(id : $id)";

                    foreach ($racine->getElementsByTagName('Error') as $error) {

                        $code = $error->getElementsByTagName('Code')->item(0)->nodeValue;
                        $message = $error->getElementsByTagName('Message')->item(0)->nodeValue;

                        $log .= 'Error code : ' . $code . '. Description : ' . $message . "<br/>";
                    }

                    // TODO : move log !
                    // add log
                    mage::getModel('MarketPlace/Logs')->addLog(
                            Mage::helper('Amazon')->getMarketPlaceName(),
                            14,
                            $log,
                            array('fileName' => NULL)
                    );

                    $feedSubmission->setmp_status(self::kFeedError)
                            ->setmp_response($response)
                            ->save();

                    $productsIds = $feedSubmission->getmp_ids();
                    $productsTab = explode(",",$productsIds);
                    Mage::getModel('MarketPlace/Data')->updateStatus($productsTab, Mage::helper('Amazon')->getMarketPlaceName(), self::kStatusNotCreated);
                }
            } else {
                // get summary
                $processingSummary = $racine->getElementsByTagName('ProcessingSummary')->item(0);
                $messagesProcessed = $processingSummary->getElementsByTagName('MessagesProcessed')->item(0)->nodeValue;
                $messagesSuccessful = $processingSummary->getElementsByTagName('MessagesSuccessful')->item(0)->nodeValue;
                $messagesWithError = $processingSummary->getElementsByTagName('MessagesWithError')->item(0)->nodeValue;
                $messagesWithWarning = $processingSummary->getElementsByTagName('MessagesWithWarning')->item(0)->nodeValue;

                // check if submitted feed was correctly formated
                // if not, log the first error message
                if ($messagesWithError > 0) {

                    $traitedResults = array();

                    // load product feed
                    $productFeed = new DomDocument('1.0', 'utf8');
                    $productFeed->loadXML($feedSubmission->getmp_content());

                    $results = $racine->getElementsByTagName('Result');
                    
                    // tab which contain matched products but some of the information submitted does not match the product information that is already in the Amazon catalog
                    $t_match = array();

                    // tab which contain unauthorized products
                    $t_unauthorized = array();

                    /***************************************************************************************
                     *
                     *                                    PARSE RESULT
                     *
                     ***************************************************************************************/
                    $cpt = 0;
                    foreach ($results as $result) {

                        // init
                        $sku = "";
                        $productID = "";
                        // retrieve message ID
                        $currentMessageID = $result->getElementsByTagName('MessageID')->item(0)->nodeValue;

                        // check if message ID already processed
                        if($currentMessageID != 0 && !in_array($currentMessageID, $traitedResults)){

                            // retrieve SKU
                            $messages = $productFeed->getElementsByTagName('Message');

                            foreach($messages as $message){
                                if($message->getElementsByTagName('MessageID')->item(0)->nodeValue == $currentMessageID){
                                    $sku = $message->getElementsByTagName('SKU')->item(0)->nodeValue;
                                    break;
                                }
                            }

                            // if SKU is defined in feed, then get product ID and add it in errors tab
                            if($sku !== ""){
                                $productID = Mage::getModel('Catalog/product')->getIdBySku($sku);
                                $errors[] = $productID;
                            }

                            // save current message ID as processed
                            $traitedResults[] = $currentMessageID;
                        }

                        // find result code
                        $resultCode = $result->getElementsByTagName('ResultCode')->item(0)->nodeValue;

                        // if Error
                        if ($resultCode == "Error") {

                            // find error code and description
                            $errorCode = $result->getElementsByTagName('ResultMessageCode')->item(0)->nodeValue;
                            $errorDescription = $result->getElementsByTagName('ResultDescription')->item(0)->nodeValue;

                            switch($errorCode){
                                // Seller is not authorized to list products in this category
                                case '8026':

                                    // retrieve SKU
                                    $errorSku = $result->getElementsByTagName('AdditionalInfo')->item(0)->getElementsByTagName('SKU')->item(0)->nodeValue;
                                    $errorId = Mage::getModel('catalog/product')->getIdBySku($errorSku);
                                    $t_unauthorized[] = $errorId;

                                    break;
                                // if error 8041 (match� mais certaines informations divergent)
                                case '8041':

                                    // retrieve SKU and ASIN
                                    $matched_sku = $result->getElementsByTagName('AdditionalInfo')->item(0)->getElementsByTagName('SKU')->item(0)->nodeValue;
                                    preg_match('#ASIN (B\w+)#', $errorDescription, $match_asin);
                                    $errorSku = $result->getElementsByTagName('AdditionalInfo')->item(0)->getElementsByTagName('SKU')->item(0)->nodeValue;
                                    $errorId = Mage::getModel('catalog/product')->getIdBySku($errorSku);
                                    if(isset($match_asin[1])){
                                        $t_match[] = array(
                                                'id' => $errorId,
                                                'asin' => $match_asin[1]
                                        );
                                    }

                                    break;

                            }

                            // add error message in database
                            if($productID != ""){
                                Mage::getModel('MarketPlace/Data')->addMessage($productID, $errorDescription, Mage::Helper('Amazon')->getMarketPlaceName());
                            }

                            // TODO : move log !!
                            // add log
                            mage::getModel('MarketPlace/Logs')->addLog(
                                    Mage::helper('Amazon')->getMarketPlaceName(),
                                    14,
                                    'Error code : ' . $errorCode . '. Description : ' . $errorDescription . ' (id : ' . $id . ')',
                                    array('fileName' => NULL)
                            );

                        }
                    }

                    // save feed response
                    $feedSubmission->setmp_status(self::kFeedError)
                            ->setmp_response($response)
                            ->save();

                    // update created products
                    $pendingIds = $feedSubmission->getmp_ids();
                    $pendingIdsTab = explode(",", $pendingIds);

                    // if no error, it seems that all messageID = 0 (bad feed)
                    if(count($errors) > 0){
                        // update marketplace status
                        foreach ($pendingIdsTab as $pendingId) {

                            if (in_array($pendingId, $errors)) {
                                // set product as in error
                                Mage::getModel('MarketPlace/Data')->updateStatus(array($pendingId), Mage::helper('Amazon')->getMarketPlaceName(), self::kStatusInError);
                            } else {
                                // set product as created
                                Mage::getModel('MarketPlace/Data')->updateStatus(array($pendingId), Mage::helper('Amazon')->getMarketPlaceName(), self::kStatusCreated);
                                $addedProducts[] = $pendingId;
                            }
                        }
                    }else{
                        // set all products as not created
                        Mage::getModel('MarketPlace/Data')->updateStatus($pendingIdsTab, Mage::helper('Amazon')->getMarketPlaceName(), self::kStatusInError);
                    }

                    // set unauthorized products as created, error 8026
                    if(count($t_unauthorized) > 0){
                        foreach($t_unauthorized as $id){
                            Mage::getModel('MarketPlace/Data')->updateStatus(array($id), Mage::helper('Amazon')->getMarketPlaceName(), self::kStatusIncomplete);
                            $addedProducts[] = $id;
                        }
                    }

                    // try to match with ASIN, error n°8041
                    if(count($t_match) > 0){
                        $this->tryToMatchWithASIN($t_match);
                    }
                    
                } else {

                    // NO ERROR
                    $current_ids = $feedSubmission->getmp_ids();

                    $idsTab = explode(',', $current_ids);

                    foreach ($idsTab as $addedProductId) {

                        // update marketplace status
                        Mage::getModel('MarketPlace/Data')->updateStatus($addedProductId, Mage::helper('Amazon')->getMarketPlaceName(), self::kStatusCreated);

                        $addedProducts[] = $addedProductId;
                    }


                    // update feed status
                    $feedSubmission->setmp_status(self::kFeedDone)
                            ->setmp_response($response)
                            ->save();

                    // TODO : move log !
                    // add log which notice that product is created on amazon
                    mage::getModel('MarketPlace/Logs')->addLog(
                            Mage::helper('Amazon')->getMarketPlaceName(),
                            0,
                            'Product added to marketplace. (product id : ' . $current_ids . ')',
                            array('fileName' => NULL)
                    );
                }
            }

            // save response
            mage::getModel('MarketPlace/Logs')->saveFile(
                    array(
                        'filenames' => array('addProductResponse.xml'),
                        'fileContent' => $response,
                        'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                        'type' => 'export'
                    )
            );
        }

        if (count($addedProducts) > 0) {
            // send price and inventory feed for new products, if not => incomplete
            $this->UpdateStockAndPriceForNewProducts($addedProducts);
        }
    }

    /**
     * Product already exists on Amazon and merchant informations are differents so
     * match product with ASIN
     *
     * @param array $matched
     * @return int 0
     */
    public function tryToMatchWithASIN($matched){

        $ids = array();
        $asins = array();

        foreach($matched as $match){
            $ids[] = $match['id'];
            $asins[] = $match['asin'];
        }

        // build request
        $request = new Zend_Controller_Request_Http();
        $request->setParam('product_ids', $ids);
        $request->setParam('product_asins', $asins);

        // add products
        $this->massProductCreation($request);

        // update stock and price for matched products
        $this->UpdateStockAndPriceForNewProducts($ids);

        return 0;

    }

    /**
     * Update database
     *
     * @param array $newProducts
     * 
     */
    public function UpdateStockAndPriceForNewProducts($newProducts) {

        $products = mage::getResourceModel('catalog/product_collection')
                        ->addAttributeToSelect('price')
                        ->addAttributeToSelect('special_price')
                        ->addAttributeToSelect('special_from_date')
                        ->addAttributeToSelect('special_to_date')
                        ->addAttributeToSelect('reserved_qty')
                        ->addAttributeToSelect('weight')
                        ->addAttributeToSelect('name')
                        ->addAttributeToSelect('image')
                        ->addAttributeToSelect('small_image')
                        ->addFieldToFilter('type_id', 'simple')
                        ->addFieldToFilter('entity_id', array('in', $newProducts))
                        ->joinTable(
                                'MarketPlace/Data',
                                'mp_product_id=entity_id',
                                array(
                                    'mp_exclude' => 'mp_exclude',
                                    'mp_force_qty' => 'mp_force_qty',
                                    'mp_delay' => 'mp_delay'
                                ),
                                "mp_marketplace_id='amazon'",
                                'inner'
        );

        $inventoryFeedName = 'newProductsInventoryFeed.xml';
        $priceFeedName = 'newProductsPriceFeed.xml';
        $imageFeedName = 'newProductsImageFeed.xml';

        mage::helper('Amazon/ProductUpdate')->setInventoryFeed($products, $inventoryFeedName);
        mage::helper('Amazon/ProductUpdate')->updateMarketPlaceStock($inventoryFeedName);
        mage::helper('Amazon/ProductUpdate')->setPriceFeed($products, $priceFeedName);
        mage::helper('Amazon/ProductUpdate')->updateMarketPlacePrices($priceFeedName);
        mage::helper('Amazon/ProductUpdate')->setImageFeed($products, $imageFeedName);
        mage::helper('Amazon/ProductUpdate')->updateImage($imageFeedName);
    }

    /**
     * Import new products
     * 
     * @return int
     */
    public function importUnmatchedProducts() {

        $retour = 0;

        $feeds = Mage::getModel('MarketPlace/Feed')->getCollection()
                    ->addFieldToFilter('mp_type', MDN_MarketPlace_Helper_Feed::kFeedTypeMatchingProducts)
                    ->addFieldToFilter('mp_marketplace_id', Mage::helper('Amazon')->getMarketPlaceName())
                    ->addFieldToFilter('mp_status', array(MDN_MarketPlace_Helper_Feed::kFeedSubmitted, MDN_MarketPlace_Helper_Feed::kFeedInProgress));

        if ($feeds->getFirstItem()) {

            foreach($feeds as $feed){

                // retrieve request id
                $requestId = $feed->getmp_feed_id();

                // check if not empty
                if ($requestId != "") {

                    // clean up
                    $requestId = trim($requestId);

                    // get report corresponding to request id
                    $report = mage::helper('Amazon/Report')->getReportById($requestId, '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_');

                    // not yet available, retry next time !!
                    if ($report != 'unvailable') {

                        // no longer available...
                        if ($report != 'none') {

                            $lines = explode("\n", $report);
                            $nbr = $this->importProducts($lines);

                            $feed->setmp_response($report)
                                    ->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedDone)
                                    ->save();

                            $retour = $nbr;
                        } else {

                            $feed->setmp_response('No longer available')
                                    ->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                                    ->save();
                        }
                    } else {

                        $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedInProgress)
                                ->save();

                    }
                }
            }
        }

        // request product listing
        $this->requestListingProducts();

        return $retour;
    }

    /**
     * Import existing product into Magento
     * Used for matching new product (retrieve ASIN)
     *
     * @param array $lines
     * @return int $nbr (number of imported products)
     */
    public function importProducts($lines) {

        $nbr = 0;
        $errorMessage = "";
        $error = false;
        $unavailableProducts = array();

        for ($i = 0; $i < count($lines); $i++) {

            try {
                if ($i == 0 || $lines[$i] == "")
                    continue;

                $infos = explode("\t", $lines[$i]); // TODO : check if EAN already used for another product !

                $id = mage::getModel('catalog/product')->getIdBySku($infos[0]);

                if ($id === false) {
                    $unavailableProducts[] = $infos[0];
                    continue;
                }

                $add = mage::getModel('MarketPlace/Data')->getCollection()
                                ->addFieldToFilter('mp_marketplace_id', 'amazon')
                                ->addFieldToFilter('mp_product_id', $id)
                                ->getFirstItem();

                // check if product exists in database
                if ($add->getmp_id()) {

                    //check mp_reference
                    if ($add->getmp_reference() === NULL) {

                        $add->setmp_reference($infos[1])
                                ->setmp_marketplace_status(self::kStatusCreated)
                                ->setmp_exclude('0')
                                ->save();

                    } else {

                        $add->setmp_reference($infos[1])
                            ->setmp_marketplace_status(self::kStatusCreated)
                            ->save();
                       
                    }
                } else {

                    // if not add it !
                    $add = mage::getModel('MarketPlace/Data');
                    $add->setmp_marketplace_id('amazon');
                    $add->setmp_exclude('0');
                    $add->setmp_product_id($id);
                    $add->setmp_reference($infos[1]);
                    $add->setmp_marketplace_status(self::kStatusCreated);
                    $add->save();
                    
                }

                $nbr++;

            } catch (Exception $e) {
                $error = true;
                $errorMessage .= $e->getMessage() . '<br/>';
            }
        }

        $str = "";
        $i = 0;
        foreach ($unavailableProducts as $unavailable) {
            $i++;
            $str .= $unavailable;
            if ($i < count($unavailableProducts))
                $str .= ", ";
        }

        $message = "";
        if (count($unavailableProducts) == 0) {
            $errorCode = 0;
            $message = $nbr . ' products have been matched.';
        } else {
            $errorCode = 3;
            $message = $nbr . ' products have been matched. Unvalaible products : ' . $str;
        }

        mage::getModel('MarketPlace/Logs')->addLog(
                Mage::helper('Amazon')->getMarketPlaceName(),
                $errorCode,
                $message,
                array('fileName' => NULL)
        );

        if ($error) {
            throw new Exception($errorMessage, 14);
        }

        return $nbr;
    }

    /**
     * Request stock file
     * Save request id in _GET_FLAT_FILE_OPEN_LISTINGS_DATA_.txt
     *
     * @return boolean
     */
    public function requestListingProducts() {

        // send the report request
        $reportType = '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_';
        $retour = mage::helper('Amazon/Report')->requestReport($reportType, null, null);

        // save report request id by parsing response
        $retourXML = new DomDocument();
        $retourXML->loadXML($retour);
        $requestReportResult = $retourXML->getElementsByTagName('RequestReportResponse')->item(0)->getElementsByTagName('RequestReportResult')->item(0);
        $reportRequestInfo = $requestReportResult->getElementsByTagName('ReportRequestInfo')->item(0);
        $reportRequestId = $reportRequestInfo->getElementsByTagName('ReportRequestId')->item(0)->nodeValue;

        $feed = Mage::getModel('MarketPlace/Feed')
                ->setmp_feed_id($reportRequestId)
                ->setmp_type(MDN_MarketPlace_Helper_feed::kFeedTypeMatchingProducts)
                ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName())
                ->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedSubmitted)
                ->setmp_content($retour)
                ->setmp_date(date('Y-m-d H:i:s'))
                ->save();

        return $retour;
    }

    /**
     * Check if file seem's to be ok
     *
     * @param string $lines
     * @return boolean
     */
    public function isProductFileOk($lines) {

        $tmp = explode("\t", $lines[0]);
        if (trim($tmp[0]) == "sku" && trim($tmp[1]) == "asin" && trim($tmp[2]) == "price" && trim($tmp[3]) == "quantity")
            return true;

        return false;
    }

    /**
     * Get last Lisitng products
     *
     * @return string
     */
    public function getLastListingProducts(){

        $feed = Mage::getModel('MarketPlace/Feed')->getCollection()
                    ->addFieldToFilter('mp_type', MDN_MarketPlace_Helper_Feed::kFeedTypeMatchingProducts)
                    ->addFieldToFilter('mp_marketplace_id', Mage::helper('Amazon')->getMarketPlaceName())
                    ->addFieldToFilter('mp_status', MDN_MarketPlace_Helper_Feed::kFeedDone)
                    ->setOrder('mp_feed_id', 'DESC')
                    ->getFirstItem();

        return $feed->getmp_response();
    }
    
    /**
     * Allow product creation
     * 
     * @return boolean
     */
    public function allowMatchingEan(){
        return true;
    }

}