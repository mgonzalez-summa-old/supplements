<?php

class MDN_Amazon_Helper_Orders extends MDN_MarketPlace_Helper_Orders {
    
    private static $filename = "_GET_FLAT_FILE_ORDERS_DATA_ids.txt";

    /**
     * Get marketplace name
     *
     * @return string
     */
    public function getMarketPlaceName(){
        return Mage::helper('Amazon')->getMarketPlaceName();
    }

    /**
     * Get marketplace orders
     * 
     * <ul>
     * <li>1 - get previous report using saved id (read request_id.txt file) and import orders</li>
     * <li>2 - request new report and save request id in request_id.txt file</li>
     * </ul>
     *
     * @return array $orders
     * 
     */
    public function getMarketplaceOrders() {

        // init
        $reportId = 0;
        $report = "";
        $requestTab = array();
        $orders = array();

        $feeds = Mage::getModel('MarketPlace/Feed')->getCollection()
                    ->addFieldToFilter('mp_marketplace_id', Mage::helper('Amazon')->getMarketPlaceName())
                    ->addFieldToFilter('mp_type', MDN_MarketPlace_Helper_Feed::kFeedTypeImportOrders)
                    ->addFieldToFilter('mp_status', array(MDN_MarketPlace_Helper_Feed::kFeedSubmitted, MDN_MarketPlace_Helper_Feed::kFeedInProgress));

        if($feeds->getFirstItem()){

            foreach($feeds as $feed){

                $id = $feed->getmp_feed_id();
                
                // try to get the order report
                $report = ($id != "") ? mage::helper('Amazon/Report')->getReportById($id, '_GET_FLAT_FILE_ORDERS_DATA_') : "none";

                // check if there are some orders to import
                if ($report != "none") {
                    if ($report != "unvailable") {

                        $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedDone)
                                ->setmp_response($report)
                                ->save();

                        // import orders
                        $lines = explode("\n", $report);

                        // remove empty line at the end of file
                        if ($lines[count($lines) - 1] == "")
                            array_pop($lines);

                        $orders[] = $this->buildOrdersTab($lines);
                    }else {

                        $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedInProgress)
                                ->save();
                    }
                } else {
                    // add log indicated that there is no order
                    mage::getModel('MarketPlace/Logs')->addLog(
                            Mage::helper('Amazon')->getMarketPlaceName(),
                            0,
                            'No order to import.',
                            array('fileName' => NULL)
                    );

                    $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedDone)
                            ->setmp_response('No order to import')
                            ->save();
                }
            }

        }

        $timestamp = Mage::getModel('core/date')->timestamp();
        $startDate = date("Y-m-d\TH:i:s.\\0\\0\\0\\Z", $timestamp - 3600 * 24 * 7);
        $endDate = date("Y-m-d\TH:i:s.\\0\\0\\0\\Z", $timestamp);

        $response = mage::helper('Amazon/Report')->requestReport('_GET_FLAT_FILE_ORDERS_DATA_', $startDate, $endDate);
        $xml = new DomDocument();
        $xml->loadXML($response);

        // check if some error occures
        if ($xml->getElementsByTagName('ErrorResponse')->item(0)) {

            $type = $xml->getElementsByTagName('Type')->item(0)->nodeValue;
            $code = $xml->getElementsByTagName('Code')->item(0)->nodeValue;
            $message = $xml->getElementsByTagName('Message')->item(0)->nodeValue;

            $errorMessage = " Some error occured while attempt to request order report : <ul><li>Type : $type</li><li>Code : $code</li><li>Message : $message</li></ul>";

            // TODO : move log !
            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper('Amazon')->getMarketPlaceName(),
                    12,
                    $errorMessage,
                    array('fileName' => NULL)
            );

        } else {
            // read request id
            $requestId = $xml->getElementsByTagName('ReportRequestInfo')->item(0)->getElementsByTagName('ReportRequestId')->item(0)->nodeValue;

            $feed = Mage::getModel('MarketPlace/Feed')
                ->setmp_feed_id($requestId)
                ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName())
                ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeImportOrders)
                ->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedSubmitted)
                ->setmp_content($response)
                ->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                ->save();

        }   

        return $orders;
    }

    /**
     * Import marketplace orders
     *
     * @param array $ordersTab
     * @return string $retour
     */
    public function importMarketPlaceOrders($ordersTab){
        
        $retour = "";

        if(is_array($ordersTab) && count($ordersTab) > 0){

            foreach($ordersTab as $tab){
                if(array_key_exists('mpOrderId',$tab)){
                    $retour .= parent::importMarketPlaceOrders($ordersTab);
                    break;
                }

                $retour .= parent::importMarketPlaceOrders($tab)."\n";
            }
        }else{
            // fichier non reconnu
            $retour = $ordersTab;
        }
        return $retour;
    }

    /**
     * Check file version
     *
     * @param array $lines
     * @return int $v
     */
    public function checkFileVersion($lines){

        $v = 0;
        $tmp = explode("\t", $lines[0]);

        if(count($tmp) == 45) $v = 2;

        if(count($tmp) == 30) $v = 1;

        if(count($tmp) == 34) $v = 3;

        return $v;
    }

    /**
     * Imports marketplace orders from file
     *
     * @param string $lines
     * @return array $ordersTab
     *
     */
    public function buildOrdersTab($lines) {

		$index = array();

        if (!$this->isFileOk($lines)) {
            return "This file doesn't seems to be a valid Amazon order file. Please check it.";
        }

        // get indexes
        $header = explode("\t", $lines[0]);
        $index = array_flip($header);

        $i = 0;
        $ordersTab = array();
        $tax = mage::getStoreConfig('marketplace/general/tax');
        $currentOrder = "";

        // build orders array
        for ($i = 0; $i < count($lines); $i++) {

            // skip first line
            if ($i == 0)
                continue;

            // retrive fields
            $tFields = explode("\t", utf8_encode($lines[$i]));

            foreach ($tFields as $k => $v)
                $tFields[$k] = trim($v);

            if (!array_key_exists($tFields[$index['order-id']], $ordersTab)) {

                if ($currentOrder != "") {
                    $ordersTab[$currentOrder]['shipping_incl_tax'] = round($ordersTab[$currentOrder]['shipping_incl_tax'], 2);
                    $ordersTab[$currentOrder]['shipping_excl_tax'] = round($ordersTab[$currentOrder]['shipping_incl_tax'] / (1 + $tax / 100), 2);
                    $ordersTab[$currentOrder]['shipping_tax'] = round(($ordersTab[$currentOrder]['shipping_incl_tax'] - $ordersTab[$currentOrder]['shipping_excl_tax']), 2);
                    $currentOrder = $tFields[$index['order-id']];
                }
                else
                    $currentOrder = $tFields[$index['order-id']];

                // create order tab
                $ordersTab[$tFields[$index['order-id']]] = array(
                    'mpOrderId' => $tFields[$index['order-id']],
                    'email' => $tFields[$index['buyer-email']],
                    'marketplace' => 'amazon',
                    'phone' => $tFields[$index['buyer-phone-number']],
                    'firstname' => $tFields[$index['buyer-name']],
                    'lastname' => '',
                    'date' => $tFields[$index['purchase-date']],
                    'currency' => $tFields[$index['currency']],
                    'shipping_excl_tax' => 0,
                    'shipping_tax' => '',
                    'shipping_incl_tax' => 0,
                    'shipping_adress' => array(
                        'firstname' => $tFields[$index['recipient-name']],
                        'lastname' => '',
                        'zipCode' => $tFields[$index['ship-postal-code']],
                        'country' => $tFields[$index['ship-country']],
                        'state' => $tFields[$index['ship-state']],
                        'city' => $tFields[$index['ship-city']],
                        'comments' => $tFields[$index['delivery-Instructions']],
                        'company' => '',
                        'email' => $tFields[$index['buyer-email']],
                        'phone' => $tFields[$index['ship-phone-number']],
                        'street' => array(
                            'adress1' => $tFields[$index['ship-address-1']],
                            'adress2' => $tFields[$index['ship-address-2']],
                            'adress3' => $tFields[$index['ship-address-3']]
                        )
                    ),
                    'billing_adress' => array(
                        'firstname' => $tFields[$index['buyer-name']],
                        'lastname' => '',
                        'zipCode' => (array_key_exists('bill-postal-code', $index) && !empty($index['bill-postal-code'])) ? $tFields[$index['bill-postal-code']] : $tFields[$index['ship-postal-code']],
                        'country' => (array_key_exists('bill-country', $index) && !empty($index['bill-country'])) ? $tFields[$index['bill-country']] : $tFields[$index['ship-country']],
                        'state' => (array_key_exists('bill-state', $index) && !empty($index['bill-state'])) ? $tFields[$index['bill-state']] : $tFields[$index['ship-state']],
                        'city' => (array_key_exists('bill-city', $index) && !empty($index['bill-city'])) ? $tFields[$index['bill-city']] : $tFields[$index['ship-city']],
                        'comments' => $tFields[$index['delivery-Instructions']],
                        'company' => '',
                        'email' => $tFields[$index['buyer-email']],
                        'phone' => $tFields[$index['buyer-phone-number']],
                        'street' => array(
                            'adress1' => (array_key_exists('bill-address-1', $index) && !empty($index['bill-address-1'])) ? $tFields[$index['bill-address-1']] : $tFields[$index['ship-address-1']],
                            'adress2' => (array_key_exists('bill-address-2', $index) && !empty($index['bill-address-2'])) ? $tFields[$index['bill-address-2']] : $tFields[$index['ship-address-2']],
                            'adress3' => (array_key_exists('bill-address-3', $index) && !empty($index['bill-address-3'])) ? $tFields[$index['bill-address-3']] : $tFields[$index['ship-address-3']]
                        )
                    ),
                    'products' => array()
                );
            }

            $productId = mage::getModel('catalog/product')->getIdBySku($tFields[$index['sku']]);
            $price_incl_tax = $tFields[$index['item-price']] / $tFields[$index['quantity-purchased']];
            $price_excl_tax = $price_incl_tax / ( 1 + $tax / 100);
            $price_tax = $price_incl_tax - $price_excl_tax;

            // TODO : add product type for sales_flat_order_item
            $ordersTab[$tFields[$index['order-id']]]['products'][] = array(
                'id' => $productId,
                'mp_item_id' => $tFields[$index['order-item-id']],
                'price_excl_tax' => $price_excl_tax,
                'price_tax' => $price_tax,
                'price_incl_tax' => $price_incl_tax,
                'quantity' => $tFields[$index['quantity-purchased']]
            );

            // add tax shipping
            $ordersTab[$tFields[$index['order-id']]]['shipping_incl_tax'] += $tFields[$index['shipping-price']] + $tFields[$index['shipping-tax']];

            $ordersTab[$currentOrder]['shipping_incl_tax'] = round($ordersTab[$currentOrder]['shipping_incl_tax'], 2);
            $ordersTab[$currentOrder]['shipping_excl_tax'] = round($ordersTab[$currentOrder]['shipping_incl_tax'] / (1 + $tax / 100), 2);
            $ordersTab[$currentOrder]['shipping_tax'] = round(($ordersTab[$currentOrder]['shipping_incl_tax'] - $ordersTab[$currentOrder]['shipping_excl_tax']), 2);
        }

        return $ordersTab;

    }

    /**
     * Check if the order importation file is valid
     *
     * @param array $lines
     * @return boolean
     */
    public function isFileOk($lines) {
        $fields = explode("\t", $lines[0]);
        
        if ($fields[0] != "order-id")
            return false;

        return true;
    }

    /**
     * Remove empty lines from order file
     *
     * @param array $lines
     * @return array $retour
     */
    public function cleanOrdersArray($lines) {

        $retour = array();

        foreach ($lines as $line) {

            //$line = trim($line);

            if ($line != "" && $line != "\n") {
                $retour[] = $line;
            }
        }

        return $retour;
    }

}