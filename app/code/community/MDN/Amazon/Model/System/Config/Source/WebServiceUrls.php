<?php

/*
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MDN_Amazon_Model_System_Config_Source_WebServiceUrls extends Mage_Core_Model_Abstract {

    protected $countryCode = array(
        'CA' => 'CA',
        'CN' => 'CN',
        'DE' => 'DE',
        'ES' => 'ES',
        'FR' => 'FR',
        'IT' => 'IT',
        'JP' => 'JP',
        'UK' => 'UK',
        'US' => 'US'
    );

    public function getAllOptions() {

        if (!$this->_options) {

            $options = array();

            foreach ($this->countryCode as $value => $label) {

                $options[] = array(
                    'value' => $value,
                    'label' => $label
                );

                $this->_options = $options;
            }
        }

        return $this->_options;
    }

    public function toOptionArray() {
        return $this->getAllOptions();
    }

}
