<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
require_once 'MDN/Scanner/controllers/InventoryController.php';
 
class Pd_Inventory_InventoryController extends MDN_Scanner_InventoryController {


    /**
     * Process search
     *
     */
    public function processSearchAction() {
    
//    header("Content-Type: text/xml");
//die(Mage::app()->getConfig()->getNode()->asXML());


        $this->loadLayout();

        $query = $this->getRequest()->getPost('query');

        $resultBlock = $this->getLayout()->getBlock('scanner_inventory_result');
        $resultBlock->initResult($query);
        
        // SK Always redirecting to the search page.
        //if ($resultBlock->hasOnlyOneResult()) {
        //    $this->_redirect('Scanner/Inventory/ProductInformation', array('product_id' => $resultBlock->getOnlyProduct()->getId()));
        //} else {
            $this->renderLayout();
        //}
    }

}