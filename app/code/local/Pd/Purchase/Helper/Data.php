<?php
class Pd_Purchase_Helper_Data
    extends Mage_Core_Helper_Abstract
{
    protected $_productAttributes = array(
        'Europa'     => 'varchar',
        'Thresh'     => 'varchar',
        'USA'        => 'varchar',
        'Super'      => 'varchar',
        'USA'        => 'varchar',
        'Direct'     => 'varchar',
        'Resultz'    => 'varchar',
        'BetterBody' => 'varchar',
        'Pharma'     => 'varchar',
        'BrandNew'   => 'varchar',
        'JD'         => 'varchar',
        'LoneStar'   => 'varchar',
        'PAF'        => 'varchar',
        'MiscB'      => 'varchar',
        'Barcode'    => 'text',
        'UPC2'       => 'varchar',
        'Size'       => 'int'
    );

    public function getReportProductAttributes()
    {
        return $this->_productAttributes;
    }

    public function getVendorCollection()
    {
        $invoiceItems = Mage::getModel('sales/order_invoice_item')->getCollection();
        foreach($this->_productAttributes as $key => $type) {
            $invoiceItems = $this->_addProductAttribute($invoiceItems, $key, $type);
        }
        $invoiceItems->getSelect()->joinLeft(
            array('invoice' => 'sales_flat_invoice'),
            'main_table.parent_id = invoice.entity_id',
            array('order_id' => 'order_id')
        );
        return $invoiceItems;
    }

    protected function _addProductAttribute($collection, $attributeCode, $type)
    {
        $collection->getSelect()->joinLeft(
            array($attributeCode . '_table' => 'catalog_product_entity_' . $type),
            'main_table.product_id = ' . $attributeCode . '_table.entity_id AND ' . $attributeCode . '_table.attribute_id IN ('
                . 'select attribute_id as ' . $attributeCode . '_id from eav_attribute where attribute_code = "' . $attributeCode . '")',
            array($attributeCode => 'value')
        );
        return $collection;
    }
}
