<?php
class Pd_Purchase_Adminhtml_ReportController
    extends Mage_Adminhtml_Controller_Action
{
    public function invoiceAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName = 'Vendor Sheet.csv';
        $content  = $this->getLayout()->createBlock('pd_purchase/adminhtml_report_grid')->getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'Vendor Sheet.xml';
        $content  = $this->getLayout()->createBlock('pd_purchase/adminhtml_report_grid')->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }
}