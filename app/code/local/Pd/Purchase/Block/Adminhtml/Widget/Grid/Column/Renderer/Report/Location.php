<?php
class Pd_Purchase_Block_Adminhtml_Widget_Grid_Column_Renderer_Report_Location
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text
{
    public function render(Varien_Object $row)
    {
        $location            = array();
        $stockItemCollection = Mage::getModel('cataloginventory/stock_item')->getCollection();
        $stockItemCollection->getSelect()->joinLeft(
            array('warehouse' => Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock')),
            'warehouse.stock_id = main_table.stock_id',
            array('stock_name' => 'stock_name')
        );

        $stockItemCollection->addFieldToFilter('product_id', $row->getProductId());

        foreach ($stockItemCollection as $row) {
            if ($row->getShelfLocation()) {
                $location[] = $row->getStockName() . ' - ' . $row->getShelfLocation();
            }
        }

        $location = implode(',', $location);

        return $location;
    }
}