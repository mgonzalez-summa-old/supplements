<?php
class Pd_Purchase_Block_Adminhtml_Report_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('entity_id');
        $this->setDefaultSort('parent_id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::helper('pd_purchase')->getVendorCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepares the columns to be shown in the grid.
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     *
     */
    protected function _prepareColumns()
    {
        $this->addExportType('*/*/exportCsv', Mage::helper('pd_purchase')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('pd_purchase')->__('XML'));

        /**
         * @var $helper Pd_Purchase_Helper_Data
         */
        $helper = Mage::helper('pd_purchase');

        $productAttributes = $helper->getReportProductAttributes();

        $this->addColumn('order_id', array(
            'header' => $helper->__('#Order'),
            'index'  => 'order_id',
            'filter_condition_callback' => array($this, '_orderIdFilterIndexCallback'),
        ));

        foreach($productAttributes as $attribute => $type) {
            $this->addColumn($attribute, array(
                'header'       => $helper->__($attribute),
                'index'        => $attribute,
                'filter_index' => $attribute .'_table.value'
            ));
        }

        $this->addColumn('brand_name', array(
            'header' => $helper->__('Brand'),
            'index'  => 'brand_name'
        ));

        $this->addColumn('name', array(
            'header' => $helper->__('Product name'),
            'index'  => 'name'
        ));

        $this->addColumn('qty', array(
            'header' => $helper->__('Qty'),
            'index'  => 'qty'
        ));

        $this->addColumn('sku', array(
            'header' => $helper->__('SKU'),
            'index'  => 'sku'
        ));

        $this->addColumn('location', array(
            'header'   => $helper->__('Location'),
            'renderer' => 'pd_purchase/adminhtml_widget_grid_column_renderer_report_location',
            'index'    => 'product_id',
            'filter'   => false
        ));

        $this->addColumn('product_notes', array(
            'header' => $helper->__('Product Notes'),
            'index'  => 'product_notes'
        ));

        $this->addColumn('parent_id', array(
            'header' => $helper->__('Invoice'),
            'index'  => 'parent_id'
        ));

        $this->addColumn('product_id', array(
            'header' => $helper->__('idProduct'),
            'index'  => 'product_id'
        ));

        return parent::_prepareColumns();
    }

    protected function _orderIdFilterIndexCallback($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        if (!$value) {
            return $this;
        }
        $this->getCollection()->getSelect()->where('invoice.order_id IN (' . $value . ')');
        return $this;
    }
}