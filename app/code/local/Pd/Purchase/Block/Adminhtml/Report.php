<?php

class Pd_Purchase_Block_Adminhtml_Report
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'pd_purchase';
        $this->_controller = 'adminhtml_report';
        $this->_headerText = Mage::helper('purchase')->__('SW Purchase Report');
    }
}