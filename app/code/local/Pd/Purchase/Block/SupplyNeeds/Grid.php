<?php


class Pd_Purchase_Block_SupplyNeeds_Grid extends MDN_Purchase_Block_SupplyNeeds_Grid {

    protected function _prepareColumns() {
        parent::_prepareColumns();
        $this->getColumn("manufacturer_id")
            ->setData(array(
            'header' => Mage::helper('purchase')->__('Brand'),
            'index' => 'manufacturer_id',
            'type' => 'options',
            'options' => $this->getManufacturersAsArray(),));
    }

}