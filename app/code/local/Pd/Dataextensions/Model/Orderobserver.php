<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 * 
 * NOTICE OF LICENSE
 * 
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE 
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING 
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA") 
 * is a legal agreement between you (either an individual or a single entity) and Precision 
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above 
 * which may include associated software components, media, printed materials, and "online" 
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying, 
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the 
 * terms of this EULA. This license agreement represents the entire agreement concerning 
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If 
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international 
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget 
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE. 
 *  The ExactTarget Integration for Magento is licensed as follows: 
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget 
 *  Integration for Magento on your development, staging and production servers running a validly 
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for 
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to 
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento, 
 *  except and only to the extent that such activity is expressly permitted by applicable law 
 *  notwithstanding this limitation. 
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget 
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as 
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento 
 *  and subject to the terms and conditions of this EULA. 
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION 
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail 
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies 
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento 
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual 
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration 
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright 
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content. 
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento. 
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of 
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness 
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy 
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget 
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused 
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision 
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any 
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation, 
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or 
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or 
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract, 
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the 
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions 
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, 
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Dataextensions_Model_Orderobserver
{
    public function __construct(){}
    
    /**
     * This method batches the desired information from Magento and writes it to a CSV file which then
     * can be imported into ExactTarget
     */
    public function batchCSV()
    {
    	//Verify the exension is active
    	if (Mage::getStoreConfig('et_dataextensions/order_de/active') == 1):

	    	try{
	    	
				Mage::helper('exacttarget/exacttarget')->verifyDirectory(Mage::getBaseDir('var').'/export');
				
	    		$order_header_file  = 'orderHeader.csv';
	    		$order_address_file = 'orderAddress.csv';
	    		$order_items_file 	= 'orderItems.csv';
	    		$order_header_path  = Mage::getBaseDir('var').DS.'export'.DS.$order_header_file; //file path of the CSV file in which the data to be saved
	    		$order_address_path = Mage::getBaseDir('var').DS.'export'.DS.$order_address_file; //file path of the CSV file in which the data to be saved
	    		$order_items_path   = Mage::getBaseDir('var').DS.'export'.DS.$order_items_file; //file path of the CSV file in which the data to be saved
	    		 
		   		$connection                =    Mage::getSingleton('core/resource')->getConnection('core_write');
		   		$headerBatchId             =    Mage::helper('exacttarget/exacttarget')->getBatchId('header');
		   		$itemsBatchId              =    Mage::helper('exacttarget/exacttarget')->getBatchId('items');	   		
		   		$addressBatchId            =    Mage::helper('exacttarget/exacttarget')->getBatchId('address');	 

		   		$orderIds                  =    Mage::getResourceModel('sales/order_collection')->getAllIds();
                $date_modified             =    date('Y-m-d H:i:s');
                
                foreach($orderIds as $order):
                	$baseAttributes           =    Mage::helper('dataextensions/order')->getActiveAttributes('base',0);
                	$itemAttributes           =    Mage::helper('dataextensions/order')->getActiveAttributes('items',0);
                	$addressesAttributes      =    Mage::helper('dataextensions/order')->getActiveAttributes('addresses',0);
                	break;
                endforeach;
                
                //Order Headers
                $header['date_modified']		=	'date_modified';
                $header['store_id']				=	'store_id';
                $header['store_name']			=	'store_name';
                
                //Get all the header column values
                foreach($baseAttributes as $orderAttribute):
                	$header[$orderAttribute['mapped_field']]					=	$orderAttribute['mapped_field'];
                endforeach;
                
                //Item Headers
                $itemHeader			  =	   array();
                
                $itemHeader['row_number']			   =	'row_number';
                $itemHeader['magento_order_number']	   =	'magento_order_number';
                $itemHeader['order_number']			   =	'order_number';
                $itemHeader['email']			   	   =	'email';
                $itemHeader['purchase_date']		   =	'purchase_date';
                $itemHeader['date_modified']		   =	'date_modified';
                
                foreach($itemAttributes as $itemAttribute):
                	$itemHeader[$itemAttribute['mapped_field']]			=	$itemAttribute['mapped_field'];
                endforeach;
                
                //Address Headers
                $addressHeader		  =	   array();
                
                $addressHeader['row_number']			   =	'row_number';
                $addressHeader['magento_order_number']	   =	'magento_order_number';
                $addressHeader['order_number']			   =	'order_number';
                $addressHeader['customer_email']	   	   =	'customer_email';
                $addressHeader['address_type']		   	   =	'address_type';
                $addressHeader['date_modified']		   	   =	'date_modified';
                
                foreach($addressesAttributes as $addressAttribute):
                	$addressHeader[$addressAttribute['mapped_field']]			=	$addressAttribute['mapped_field'];
                endforeach;
                
                //Looping through each order object
                foreach($orderIds as $orderId):
                
                    $order                    =    Mage::getModel('sales/order')->setData(array())->load($orderId);
                
                    $baseAttributes           =    Mage::helper('dataextensions/order')->getActiveAttributes('base',$order->getStoreId());
                    $itemAttributes           =    Mage::helper('dataextensions/order')->getActiveAttributes('items',$order->getStoreId());
                    $addressesAttributes      =    Mage::helper('dataextensions/order')->getActiveAttributes('addresses',$order->getStoreId());
                    
                    //Headers
					$data					  =	   array();
                
                    $data['date_modified']					   =	$date_modified;	
                    $data['store_id']					   	   =	$order->getStoreId();	
                    $data['store_name']					   	   =	Mage::app()->getStore($order->getStoreId())->getName();;	
                    //Loops through all the active attributes and assigns them accordingly to the proper data extension
                    foreach($baseAttributes as $orderAttribute):
                        $data[$orderAttribute['mapped_field']] =    $order->$orderAttribute['object_reference']();
                    endforeach;
                    
			   		$batchData['batch_data']             =    serialize($data);
			   		$batchData['type']                   =    'header';
			   		$batchData['batch_id']               =    $headerBatchId;
			   		$batchData['created_at']             =    now();
			   		
			   		$connection->insert('exacttarget_dataextension_batch_export',$batchData);
			   		unset($batchData);
			   		unset($data);
                    
                    //Get the line items of each order
                    $order_detail         =    Mage::getModel('sales/order')->load($order->getId());
                    $items                =    $order_detail->getAllVisibleItems();
                    $itemData			  =	   array();
                    
                    //Loop through each line item; attributes outside of the loop are data extension columns created that
                    //are not managed in the extension but are used to create the association of order base, order lines and order addresses together.
                    foreach($items as $item):
                        $itemData['row_number']            =    $item->getItemId();
                        $itemData['magento_order_number']  =    $item->getOrderId();
                        $itemData['order_number']          =    $order->getIncrementId();
                        $itemData['email']                 =    $order->getCustomerEmail();
                        $itemData['purchase_date']         =    $order->getCreatedAt();
                        $itemData['date_modified']         =    $date_modified;
                        
                        //Loops through all the active attributes and assigns them accordingly to the proper data extension
                        foreach($itemAttributes as $itemAttribute):
                        
                            if($itemAttribute['key'] == 'brand'):
                                $product            =    Mage::getModel('catalog/product')->load($item->getProductId());
                                $itemData[$itemAttribute['mapped_field']] =    $product->getAttributeText($itemAttribute['object_reference']);
                            else:
                                $itemData[$itemAttribute['mapped_field']] =    $item->$itemAttribute['object_reference']();
                                
                                //Protecting against NULL price or weight values that will error out when sending over to ET.
                                if($itemAttribute['input_type'] == 'decimal'):
                                    if(is_null($item->$itemAttribute['object_reference']())):
                                        $itemData[$itemAttribute['mapped_field']]    =    (float)0.000;
                                    endif;
                                endif;
                            endif;
                        endforeach;
                        
                        $itemBatchData['batch_data']             =    serialize($itemData);
                        $itemBatchData['type']                   =    'items';
                        $itemBatchData['batch_id']               =    $itemsBatchId;
                        $itemBatchData['created_at']             =    now();
                        
                        $connection->insert('exacttarget_dataextension_batch_export',$itemBatchData);
                        unset($itemBatchData);
                        unset($itemData);
                    endforeach;
                    unset($items);
                    unset($itemAttributes);
                    
                    //Get the address information for each order
                    $orderAddresses                     =    Mage::getResourceModel('sales/order_address_collection')->setOrderFilter($order->getId());
                    
                    $addressData		  =	   array();

                    //Loop through each address associated to the order and assign the values as set in the confg.
                    //options outside the address attribute loop are needed to be sent over for order association purposes to the order base, etc
                    foreach($orderAddresses as $address):
                        $addressData['row_number']            =    $address->getEntityId();    
                        $addressData['magento_order_number']  =    $address->getParentId();    
                       	$addressData['order_number']          =    $order->getIncrementId(); 
                        $addressData['customer_email']        =    $order->getCustomerEmail();    
                        $addressData['address_type']          =    $address->getAddressType();    
                        $addressData['date_modified']         =    $date_modified; 
                        
                        //Loops through all the active attributes and assigns them accordingly to the proper data extension
                        foreach($addressesAttributes as $addressAttribute):
                            $addressData[$addressAttribute['mapped_field']] =    $address->$addressAttribute['object_reference']();
                        endforeach;
                        //Mage::helper('exacttarget/exacttarget')->writeCsvRow($addressData,$address_fh);
                        $addressBatchData['batch_data']             =    serialize($addressData);
                        $addressBatchData['type']                   =    'address';
                        $addressBatchData['batch_id']               =    $addressBatchId;
                        $addressBatchData['created_at']             =    now();
                        
                        $connection->insert('exacttarget_dataextension_batch_export',$addressBatchData);
                        unset($addressBatchData);
                        unset($addressData);
                    endforeach;
                    
                    $order->clearInstance();
                    unset($order);
                    unset($orderAddresses);
                    unset($order_detail);
                    unset($baseAttributes);
                    unset($addressAttributes);
	    		endforeach;
	    		
	    		$csv           =    Mage::helper('exacttarget/exacttarget')->writeCsv($headerBatchId,$header,$order_header_path);
	    		$itemCsv       =    Mage::helper('exacttarget/exacttarget')->writeCsv($itemsBatchId,$itemHeader,$order_items_path);
	    		$addressCsv    =    Mage::helper('exacttarget/exacttarget')->writeCsv($addressBatchId,$addressHeader,$order_address_path);
	    		
	    		if($csv && $itemCsv && $addressCsv):
    	    		return array(
    	    		            array(
            	    		        'type'  => 'filename',
            	    		        'value' => $order_header_file,
            	    		        'rm'    => true // can delete file after use
            	    		    ),
    	    		        array(
    	    		                'type'  => 'filename',
    	    		                'value' => $order_address_file,
    	    		                'rm'    => true // can delete file after use
    	    		        ),
    	    		        array(
    	    		                'type'  => 'filename',
    	    		                'value' => $order_items_file,
    	    		                'rm'    => true // can delete file after use
    	    		        ),
    	    		);
    	         endif;
	    		
	    	}catch(Exception $e) {
	    		Mage::logException($e);
	    	}
    	endif;
    }
    
    
    public function singleOrder($observer)
    {
        //Only run if the order data extension is active
        if(Mage::getStoreConfig('et_dataextensions/order_de/active',$observer->getOrder()->getStoreId()) == 1):
            try {
                
                $order                                            =    $observer->getOrder();
                
                $orderObjectArray['order']['base']                =    array();
                $orderObjectArray['order']['items']               =    array();
                $orderObjectArray['order']['addresses']           =    array();
                
                $orderObjectArray['order']['base']['key']         =    Mage::helper('dataextensions/order')->getDataExtensionKey('base',$observer->getOrder()->getStoreId());
                $orderObjectArray['order']['items']['key']        =    Mage::helper('dataextensions/order')->getDataExtensionKey('items',$observer->getOrder()->getStoreId());
                $orderObjectArray['order']['addresses']['key']    =    Mage::helper('dataextensions/order')->getDataExtensionKey('addresses',$observer->getOrder()->getStoreId());
                
                $baseAttributes                                   =    Mage::helper('dataextensions/order')->getActiveAttributes('base',$observer->getOrder()->getStoreId());
                $itemAttributes                                   =    Mage::helper('dataextensions/order')->getActiveAttributes('items',$observer->getOrder()->getStoreId());
                $addressesAttributes                              =    Mage::helper('dataextensions/order')->getActiveAttributes('addresses',$observer->getOrder()->getStoreId());
                
                $orderObjectArray['order']['storeId']             =    $observer->getOrder()->getStoreId();
                
                $date_modified                                    =    date('Y-m-d H:i:s');
                    
                //Loops through all the active attributes and assigns them accordingly to the proper data extension
                foreach($baseAttributes as $orderAttribute):
                    $orderObjectArray['order']['base']['attributes'][$orderAttribute['mapped_field']]    =    $order->$orderAttribute['object_reference']();
                endforeach;
                $orderObjectArray['order']['base']['attributes']['date_modified'] = $date_modified;
                $orderObjectArray['order']['base']['attributes']['store_id']	  = $observer->getOrder()->getStoreId();
                $orderObjectArray['order']['base']['attributes']['store_name'] 	  = $order->getStoreName();
                
                //Get the line items of each order
                $items                =    $order->getAllVisibleItems();
                    
                //Loop through each line item; attributes outside of the loop are data extension columns created that
                //are not managed in the extension but are used to create the association of order base, order lines and order addresses together.
                $j = 0;
                foreach($items as $item):
                
                    $orderObjectArray['order']['items']['attributes'][$j]['row_number']            =    $item->getItemId();
                    $orderObjectArray['order']['items']['attributes'][$j]['magento_order_number']  =    $item->getOrderId();
                    $orderObjectArray['order']['items']['attributes'][$j]['order_number']          =    $order->getIncrementId();
                    $orderObjectArray['order']['items']['attributes'][$j]['email']                 =    $order->getCustomerEmail();
                    $orderObjectArray['order']['items']['attributes'][$j]['purchase_date']         =    $order->getCreatedAt();
                    $orderObjectArray['order']['items']['attributes'][$j]['date_modified']         =    $date_modified;
                    //Loops through all the active attributes and assigns them accordingly to the proper data extension
                    foreach($itemAttributes as $itemAttribute):
                        $orderObjectArray['order']['items']['attributes'][$j][$itemAttribute['mapped_field']]  =    $item->$itemAttribute['object_reference']();
                        
                        //Protecting against NULL price or weight values that will error out when sending over to ET.
                        if($itemAttribute['input_type'] == 'decimal'):
                            if(is_null($item->$itemAttribute['object_reference']())):
                                $orderObjectArray['order']['items']['attributes'][$j][$itemAttribute['mapped_field']]    =    (float)0.000;
                            endif;
                        endif;
                    endforeach;
                    $j++;
                endforeach;
                
                //Get the address information for each order
                $orderAddresses                     =    Mage::getResourceModel('sales/order_address_collection')->setOrderFilter($order->getId());
                    
                //Loop through each address associated to the order and assign the values as set in the confg.
                //options outside the address attribute loop are needed to be sent over for order association purposes to the order base, etc

                $i = 0;
                foreach($orderAddresses as $address):
                
                    $orderObjectArray['order']['addresses']['attributes'][$i]['row_number']            =    $address->getEntityId();    
                    $orderObjectArray['order']['addresses']['attributes'][$i]['magento_order_number']  =    $address->getParentId();    
                    $orderObjectArray['order']['addresses']['attributes'][$i]['order_number']          =    $order->getIncrementId(); 
                    $orderObjectArray['order']['addresses']['attributes'][$i]['customer_email']        =    $order->getCustomerEmail();    
                    $orderObjectArray['order']['addresses']['attributes'][$i]['address_type']          =    $address->getAddressType();    
                    $orderObjectArray['order']['addresses']['attributes'][$i]['date_modified']         =    $date_modified;    
                    
                    //Loops through all the active attributes and assigns them accordingly to the proper data extension
                    foreach($addressesAttributes as $addressAttribute):
                        $orderObjectArray['order']['addresses']['attributes'][$i][$addressAttribute['mapped_field']]    =    $address->$addressAttribute['object_reference']();
                    endforeach;
                    $i++;
                endforeach;
                
                $data                             =    array();
                $data['object_array']             =    serialize($orderObjectArray);
                $data['send_type']                =    'order';
                
                $model    =    Mage::getModel('dataextensions/dataextensionlog');
                $model->addData($data);
                $model->save();

            
            } catch (Exception $e) {
                 Mage::logException($e);
            }
        endif; 
    }
}