<?php

$installer = $this;
$installer->startSetup();

$installer->run("DELETE FROM `{$installer->getTable('core_url_rewrite')}` WHERE  `id_path` LIKE  '%brands/%'");

$brands = Mage::getModel('brand/brand')->getCollection()->load();

foreach($brands->getItems() as $k => $brand) {

    $redirectModel = Mage::getModel('enterprise_urlrewrite/redirect');
    $urlKey = $brand->getUrlKey();
    if(empty($urlKey)) {
        $urlKey = Mage::helper('brand')->toUrlKey($brand->getTitle());
        $brand->setData('url_key', $urlKey)->save();
    }
    $redirectModel->setData('identifier', $brand->getUrlKey() );
    $redirectModel->setData('target_path', 'brands/index/view/id/' . $brand->getId());
    $bError = false;
    $iAttempt = 1;
    do {
        try {
            $redirectModel->save();
            /** @var $client Enterprise_Mview_Model_Client */
            $client = Mage::getSingleton('core/factory')->getModel('enterprise_mview/client')->init('enterprise_url_rewrite_redirect');
            $client->execute('enterprise_urlrewrite/index_action_url_rewrite_redirect_refresh_row', array(
                'redirect_id' => $redirectModel->getId()
            ));
        } catch (Exception $e) {
            $redirectModel->setData('identifier', $brand->getUrlKey() . '-' . $iAttempt);
            $bError = true;
            $iAttempt++;
            continue;
        }
        if ($iAttempt > 1) {
            $brand->setUrlKey($redirectModel->getData('identifier'));
            $brand->save();
        }
        $bError = false;
    } while ($bError && $iAttempt < 30);

}

$installer->endSetup();
