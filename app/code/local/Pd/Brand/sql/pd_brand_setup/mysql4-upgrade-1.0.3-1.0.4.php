<?php

$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('brand')} ADD COLUMN `subtitle` VARCHAR(255) NOT NULL AFTER `title`;
");

$installer->endSetup();