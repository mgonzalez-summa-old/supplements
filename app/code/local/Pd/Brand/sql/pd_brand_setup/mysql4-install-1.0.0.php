<?php

$installer = $this;



$installer->startSetup();



$installer->run("

ALTER TABLE {$this->getTable('brand')} ADD COLUMN `brand_banner` VARCHAR(255) NOT NULL AFTER `small_logo`;
ALTER TABLE {$this->getTable('brand')} ADD COLUMN `brand_promo_banner` VARCHAR(255) NOT NULL AFTER `brand_banner`;

");



$installer->endSetup();