<?php

$installer = $this;
$installer->startSetup();

Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$staticBlock = array(
    'title' => 'Brand Page Promo Banner',
    'identifier' => 'brand-page-promo-banner',
    'content' => '<img src="{{skin url=\'brand/images/brand-page-promo-banner.jpg\'}}" alt="" border="0" />',
    'is_active' => 1,
    'stores' => array(0)
);

Mage::getModel('cms/block')->setData($staticBlock)->save();


$installer->endSetup();