<?php

$installer = $this;
$installer->startSetup();

Mage::getModel('core/config')->saveConfig(Redstage_Brand_Helper_Data::XML_CONFIG_SHOW_LOGO, false);

$installer->endSetup();