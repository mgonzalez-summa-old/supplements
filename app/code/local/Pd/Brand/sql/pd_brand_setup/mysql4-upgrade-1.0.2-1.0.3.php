<?php

$installer = $this;
$installer->startSetup();

Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$staticBlock = array(
    'title' => 'Brand Promo Right',
    'identifier' => 'brand-promo-right',
    'content' => '
        <div class="brand-promo-right">
            <img src="{{skin url="images/brand/brand_promo_right.jpg"}}" alt="" border="0" />
            <a href="#" title="Learn More">Learn More &rsaquo;</a>
        </div>',
    'is_active' => 1,
    'stores' => array(0)
);

Mage::getModel('cms/block')->setData($staticBlock)->save();


$installer->endSetup();