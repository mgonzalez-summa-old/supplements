<?php
class Pd_Brand_Helper_Image extends Redstage_Brand_Helper_Image
{
    /**
     * Return Banner resized image url
     *
     * @param $image
     * @param string $type
     * @return string
     */
    public function getResizedUrl($image, $type = 'brand_banner')
    {
        $imageSrc = $this->getUrl('') . $type . DS . 'resized' . DS . $image;
        if(file_exists(Mage::getBaseDir('media') . DS . 'brand' . DS . $type . DS . 'resized' . DS . $image)) {
            return $imageSrc;
        }
        return $this->getUrl('') . $type . DS . $image;
    }

}
