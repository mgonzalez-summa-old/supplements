<?php
class Pd_Brand_Block_Adminhtml_Brand_Edit_Tab_Form extends Redstage_Brand_Block_Adminhtml_Brand_Edit_Tab_Form
{
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $form = $this->getForm();
        $fieldset = $form->getElement('brand_form');

        $fieldset->addField(
            'subtitle',
            'text',
            array(
                'label'     => Mage::helper('brand')->__('Brand Subtitle'),
                'required'  => false,
                'name'      => 'subtitle',
                'style'     => 'width:500px;',
                'index'     => 'subtitle',
                'value'     => Mage::registry('brand_data')->getData('subtitle')
            ),
            'title'
        );

        $fieldset->addField(
            'brand_banner',
            'file',
            array(
                'label' => Mage::helper('brand')->__('Brand Banner'),
                'required' => false,
                'name' => 'brand_banner',
                'after_element_html' => '<p>Recommended size 540x264px</p>'.
                    ('' != Mage::registry('brand_data')->getData('brand_banner') ?
                    '<p style="margin-top: 5px"><img src="' . Mage::getBaseUrl('media') . 'brand/brand_banner/' .
                    Mage::registry('brand_data')->getData('brand_banner') . '" width="180px" height="60px" /><br />
                    <a href="' . $this->getUrl('*/*/*/', array('_current' => true, 'delete' => 'brand_banner')) . '">' .
                    Mage::helper('brand')->__('Delete Image') . '</a></p>' : ''
                )
            ),
            'image'
        );

        $fieldset->addField('brand_banner_', 'hidden', array(
            'name' => 'brand_banner_',
        ));
        Mage::registry('brand_data')->setData('brand_banner_', Mage::registry('brand_data')->getData('brand_banner'));

        $fieldset->addField(
            'brand_promo_banner',
            'file',
            array(
                'label' => Mage::helper('brand')->__('Brand Promo Banner'),
                'required' => false,
                'name' => 'brand_promo_banner',
                'after_element_html' => '<p>Recommended size 540x100px</p>'.
                    ('' != Mage::registry('brand_data')->getData('brand_promo_banner') ?
                    '<p style="margin-top: 5px"><img src="' . Mage::getBaseUrl('media') . 'brand/brand_promo_banner/' .
                    Mage::registry('brand_data')->getData('brand_promo_banner') . '" width="180px" height="60px" /><br />
                    <a href="' . $this->getUrl('*/*/*/', array('_current' => true, 'delete' => 'brand_promo_banner')) . '">' .
                    Mage::helper('brand')->__('Delete Image') . '</a></p>' : ''
                )
            ),
            'brand_banner'
        );

        $fieldset->addField('brand_promo_banner_', 'hidden', array(
            'name' => 'brand_promo_banner_',
        ));
        Mage::registry('brand_data')->setData('brand_promo_banner_', Mage::registry('brand_data')->getData('brand_promo_banner'));

        $this->setForm($form);
        return $this;
    }

}
