<?php
class Pd_Brand_Model_Mysql4_Brand extends Redstage_Brand_Model_Mysql4_Brand

{
    /**
     * Process page data before saving
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Pd_Brand_Model_Mysql4_Brand
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        parent::_beforeSave($object);

        if (!$object->getBrandBanner() && $object->getData('brand_banner_')){
            $object->setBrandBanner($object->getData('brand_banner_'));
        }
        if (!$object->getBrandPromoBanner() && $object->getData('brand_promo_banner_')){

            $object->setBrandPromoBanner($object->getData('brand_promo_banner_'));
        }
        return $this;
    }
    
    /**
     * Process page data after saving
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Core_Model_Resource_Db_Abstract
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        if (!Mage::registry('brand_fillout_inprogress')) {
            if ($brandBanner = $object->getData('brand_banner')) {
                $path = Mage::getBaseDir('media') . DS . 'brand' . DS . 'brand_banner' . DS;
                if (file_exists($path.$brandBanner)){
                    $nweBrandBanner = $object->getId().'.'.substr(strrchr($brandBanner, '.'), 1);
                    if ($brandBanner != $nweBrandBanner){
                        if (file_exists($path.$nweBrandBanner)) {
                            unlink($path.$nweBrandBanner);
                        }

                        rename($path.$brandBanner, $path.$nweBrandBanner);
                        // actual path of image
                        $imageUrl = $path . $nweBrandBanner;
                        $imageResized = $path . 'resized' . DS . $nweBrandBanner;
                        if(file_exists($imageResized)) {
                            unlink($imageResized);
                        }

                        $this->_getWriteAdapter()->update(
                            $this->getMainTable(),
                            array('brand_banner' => $nweBrandBanner),
                            $this->_getWriteAdapter()->quoteInto(
                                $this->getIdFieldName().'=?', $object->getId()
                            )
                        );
                    }
                }
            }

            if ($image = $object->getData('brand_promo_banner')) {
                $path = Mage::getBaseDir('media') . DS . 'brand' . DS . 'brand_promo_banner' . DS;
                if (file_exists($path.$image)) {
                    $newImage = $object->getId().'.'.substr(strrchr($image, '.'), 1);
                    if ($image != $newImage){
                        if (file_exists($path.$newImage)) {
                            unlink($path.$newImage);
                        }

                        rename($path.$image, $path.$newImage);
                        // actual path of image
                        $imageUrl = $path . $newImage;
                        $imageResized = $path . 'resized' . DS . $newImage;
                        if(file_exists($imageResized)) {
                            unlink($imageResized);
                        }

                        $this->_getWriteAdapter()->update(
                            $this->getMainTable(),
                            array('brand_promo_banner'=>$newImage),
                            $this->_getWriteAdapter()->quoteInto(
                                $this->getIdFieldName().'=?', $object->getId()
                            )
                        );
                    }
                }
            }
        }

        $stores = (array)$object->getData('stores');

        if (!empty($stores)) {
            $condition = $this->_getWriteAdapter()->quoteInto('manufacturer_id = ? AND ', $object->getManufacturerId()).
                $this->_getWriteAdapter()->quoteInto('store_id IN (?)', $stores);

            $this->_getWriteAdapter()->delete($this->getTable('brand/brand_stores'), $condition);


            foreach ($stores as $store) {
                $storeArray = array();
                $storeArray['id'] = $object->getId();
                $storeArray['manufacturer_id'] = $object->getManufacturerId();
                $storeArray['store_id'] = $store;
                $this->_getWriteAdapter()->insert($this->getTable('brand/brand_stores'), $storeArray);
            }
        }



        if (!Mage::registry('brand_fillout_inprogress')) {
            if ($logo = $object->getData('small_logo')) {
                $path = Mage::getBaseDir('media') . DS . 'brand' . DS . 'logo' . DS;
                if (file_exists($path.$logo)) {
                    $newlogo = $object->getId().'.'.substr(strrchr($logo, '.'), 1);
                    if ($logo != $newlogo) {
                        if (file_exists($path.$newlogo)) {
                            unlink($path.$newlogo);
                        }
                        rename($path.$logo, $path.$newlogo);
                        $this->_getWriteAdapter()->update($this->getMainTable(), array('small_logo'=>$newlogo), $this->_getWriteAdapter()->quoteInto($this->getIdFieldName().'=?', $object->getId()));
                    }
                }
            }

            if ($image = $object->getData('image')) {
                $path = Mage::getBaseDir('media') . DS . 'brand' . DS;
                if (file_exists($path.$image)) {
                    $newimage = $object->getId().'.'.substr(strrchr($image, '.'), 1);
                    if ($image != $newimage) {
                        if (file_exists($path.$newimage)) {
                            unlink($path.$newimage);
                        }
                        rename($path.$image, $path.$newimage);
                        $this->_getWriteAdapter()->update($this->getMainTable(), array('image'=>$newimage), $this->_getWriteAdapter()->quoteInto($this->getIdFieldName().'=?', $object->getId()));
                    }
                }
            }
        }
        if ($subtitle = $object->getData('subtitle')) {
            $this->_getWriteAdapter()->update($this->getMainTable(), array('subtitle'=>$subtitle), $this->_getWriteAdapter()->quoteInto($this->getIdFieldName().'=?', $object->getId()));
        }

        /**

         * @desc adding url key to core/url_rewrite

         */

        $redirectModel = Mage::getModel('enterprise_urlrewrite/redirect');
        $redirect = $redirectModel->load('brands/index/view/id/' . $object->getId(), 'target_path');

        if($redirect->getId()) {
            if($redirect->getIdentifier() != $object->getUrlKey()) {
                $redirect->setData('identifier', $object->getUrlKey());
                $redirect->setData('target_path', 'brands/index/view/id/' . $object->getId());
                $redirect->save();
            }
        } else {
            try {
                $redirectModel->setData('identifier', $object->getUrlKey());
                $redirectModel->setData('target_path', 'brands/index/view/id/' . $object->getId());
                $redirectModel->save();
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('brand')->__('Request path %s is already used. Please select some other url key.', $redirectModel->getData('identifier')));
                throw new Exception;
            }
        }

    }

    /**
     * Perform actions after object delete
     *
     * @param Varien_Object $object
     * @return Mage_Core_Model_Resource_Db_Abstract
     */
    protected function _afterDelete(Mage_Core_Model_Abstract $object)
    {
        $redirectModel = Mage::getModel('enterprise_urlrewrite/redirect');
        $redirect = $redirectModel->load('brands/index/view/id/' . $object->getId(), 'target_path');

        if($redirect->getId()) {
            $redirect->delete();
        }

        parent::_afterDelete($object);
    }
}