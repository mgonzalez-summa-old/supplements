<?php
require_once 'Redstage/Brand/controllers/Adminhtml/BrandController.php';
class Pd_Brand_Adminhtml_BrandController extends Redstage_Brand_Adminhtml_BrandController
{

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('brand/brand')->load($id);

        if ($delete = $this->getRequest()->getParam('delete')) {
            switch ($delete) {
                case 'brand_banner':
                case 'brand_promo_banner':
                    $params = $this->getRequest()->getParams();
                    unset($params['delete']);
                    $filename = $model->getData($delete);
                    $path = Mage::getBaseDir('media') . DS . 'brand' . DS . $delete  . DS;
                    if (file_exists($path . $filename)) {
                        unlink($path . $filename);
                    }
                    $model->setData($delete, '');
                    $model->save();
                    $this->_redirect('*/*/*/', $params);
                    return;
            }
        }
        parent::editAction();
    }

    public function saveAction() {

        $this->_init();

        if ($data = $this->getRequest()->getPost()) {

            $model = Mage::getModel('brand/brand');
            $manufacturer = $model->getManufacturerName($data['manufacturer_id']);

            if (empty($data['title'])){
                $data['title'] = $manufacturer;
            }

            if (!empty($data['url_key'])){
                $urlKey = Mage::helper('brand')->toUrlKey($data['url_key']);
            } else {
                $urlKey = Mage::helper('brand')->toUrlKey($manufacturer);
            }
            $data['url_key'] = $urlKey;

            if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {

                try {
                    $uploader = new Varien_File_Uploader('image');
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);

                    $path = Mage::getBaseDir('media') . DS . 'brand' . DS;
                    $imagename = md5($_FILES['image']['name'].time()) . '.'
                        . substr(strrchr($_FILES['image']['name'], '.'), 1);

                    $uploader->save($path, $imagename);
                } catch (Exception $e) {

                }

                if (isset($imagename)){
                    $data['image'] = $imagename;
                }
            }

            if (isset($_FILES['small_logo']['name']) && $_FILES['small_logo']['name'] != '') {
                try {
                    $uploader = new Varien_File_Uploader('small_logo');
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);

                    $path = Mage::getBaseDir('media') . DS . 'brand' . DS . 'logo' . DS;
                    $logoName = md5($_FILES['small_logo']['name'].time()) . '.'
                        . substr(strrchr($_FILES['small_logo']['name'], '.'), 1);

                    $uploader->save($path, $logoName);
                } catch (Exception $e) {

                }
                if (isset($logoName)){
                    $data['small_logo'] = $logoName;
                }
            }

            /* New fields added */
            if (isset($_FILES['brand_banner']['name']) && $_FILES['brand_banner']['name'] != '') {
                try {
                    $uploader = new Varien_File_Uploader('brand_banner');
                    $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);

                    $path = Mage::getBaseDir('media') . DS . 'brand' . DS . 'brand_banner' . DS;
                    $imageName = md5($_FILES['brand_banner']['name'] . time()) . '.'
                        . substr(strrchr($_FILES['brand_banner']['name'], '.'), 1);

                    $uploader->save($path, $imageName);
                } catch (Exception $e) {

                }
                if (isset($imageName)) {
                    $data['brand_banner'] = $imageName;
                }
            }

            if (isset($_FILES['brand_promo_banner']['name']) && $_FILES['brand_promo_banner']['name'] != '') {

                try {
                    $uploader = new Varien_File_Uploader('brand_promo_banner');
                    $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);

                    $path = Mage::getBaseDir('media') . DS . 'brand' . DS . 'brand_promo_banner' . DS;
                    $imageName = md5($_FILES['brand_promo_banner']['name'] . time()) . '.'
                        . substr(strrchr($_FILES['brand_promo_banner']['name'], '.'), 1);

                    $uploader->save($path, $imageName);
                } catch (Exception $e) {

                }
                if (isset($imageName)) {
                    $data['brand_promo_banner'] = $imageName;
                }
            }

            $model->setData($data)
                ->setId($this->getRequest()->getParam('id'));

            try {
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('brand')->__('Brand Page was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }

                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }

        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('brand')->__('Unable to find Brand Page to save'));
        $this->_redirect('*/*/');
    }

}
