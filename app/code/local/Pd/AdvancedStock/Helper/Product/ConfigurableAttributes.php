<?php
class Pd_AdvancedStock_Helper_Product_ConfigurableAttributes
    extends MDN_AdvancedStock_Helper_Product_ConfigurableAttributes
{

    /**
     * Return header text in the following format Name, Brand, Promo Description, Size, Flavour, SKU
     *
     * @param int $productId
     * @return string
     */
    public function getHeaderText($productId,$useSku = false) {
        $description = '';
        if($useSku) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku',$productId);
        } else {
            $product = Mage::getModel('catalog/product')->load($productId);
        }
        if($product->getId()) {
            $description .= $this->validateAttribute($product->getAttributeText('brand'));
            $description .= $this->validateAttribute($product->getName());
            $description .= $this->validateAttribute($product->getAttributeText('flavor'));
            $description .= $this->validateAttribute($product->getPromoDescription());
            $description .= $this->validateAttribute($product->getSize(), true);

        }

        return $description;
    }

    public function validateAttribute($attribute, $last = false){
        if($attribute && $attribute != 'NULL' && $attribute != 'NO') {
            return $attribute . ((!$last)?' | ':'');
        }
        return '';
    }

}