<?php
class Pd_AdvancedStock_Block_StockError_Grid extends MDN_AdvancedStock_Block_StockError_Grid
{

    protected function _prepareCollection()
    {
        parent::_prepareCollection();
        /* @var $helper Pd_AdvancedStock_Helper_Product_ConfigurableAttributes */
        $helper = mage::helper('AdvancedStock/Product_ConfigurableAttributes');
        $collection = $this->getCollection();
        foreach ($collection->getItems() as $item) {
            $item->setValue($helper->getHeaderText($item->getEntityId()));
        }
        return $this->getCollection();
    }

}
