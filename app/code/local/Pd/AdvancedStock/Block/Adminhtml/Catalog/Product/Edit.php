<?php
class Pd_AdvancedStock_Block_Adminhtml_Catalog_Product_Edit
    extends Mage_Adminhtml_Block_Catalog_Product_Edit
{
    public function getHeader()
    {
        $header = '';
        if ($this->getProduct()->getId()) {
            $header = mage::helper('AdvancedStock/Product_ConfigurableAttributes')->getHeaderText($this->getProduct()->getId());
            $header .= ' (' . $this->getProduct()->getsku() . ')';
        }
        else {
            $header = Mage::helper('catalog')->__('New Product');
        }

        return $header;
    }
}
