<?php
class Pd_AdvancedStock_Block_Product_Edit extends MDN_AdvancedStock_Block_Product_Edit
{
    public function getHeaderText()
    {
        $text = mage::helper('AdvancedStock/Product_ConfigurableAttributes')->getHeaderText($this->getProduct()->getId());
        $text .= ' (' . $this->getProduct()->getsku() . ')';

        return $text;
    }
}
