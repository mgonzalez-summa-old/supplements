<?php
class Pd_AdvancedStock_Block_AdvancedStock_MassStockEditor_Grid
    extends MDN_AdvancedStock_Block_MassStockEditor_Grid
{
    protected function _prepareCollection()
    {

        $collection = Mage::getModel('cataloginventory/stock_item')
            ->getCollection()
            ->join('catalog/product', 'product_id=`catalog/product`.entity_id')
            ->addFieldToFilter('`catalog/product`.type_id', array('in' => array('simple', 'virtual')))
            ->join('AdvancedStock/CatalogProductVarchar',
                '`catalog/product`.entity_id=`AdvancedStock/CatalogProductVarchar`.entity_id'
                    . ' and `AdvancedStock/CatalogProductVarchar`.store_id = 0'
                    . ' and `AdvancedStock/CatalogProductVarchar`.attribute_id = '
                    . mage::getModel('AdvancedStock/Constant')->GetProductNameAttributeId()
            );

        // Additional catalog_product attributes
        $collection->getSelect()->joinLeft(
            array('catalog_product_index_eav_brand' => 'catalog_product_index_eav'),
            '`catalog_product_index_eav_brand`.`entity_id` = `main_table`.`product_id`'
                . ' AND `catalog_product_index_eav_brand`.`attribute_id` IN'
                . ' (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "brand")',
            array('attribute_id')
        );

        $collection->getSelect()->joinLeft(
            array('eav_attribute_option_value_brand' => 'eav_attribute_option_value'),
            '`eav_attribute_option_value_brand`.`option_id` = `catalog_product_index_eav_brand`.`value`',
            array('brand' => 'value')
        );

        $collection->getSelect()->joinLeft(
            array('catalog_product_entity_varchar_size' => 'catalog_product_entity_varchar'),
            '`catalog_product_entity_varchar_size`.`attribute_id` ='
                . ' (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "size")'
                . ' AND `catalog_product_entity_varchar_size`.`entity_id` = `catalog/product`.`entity_id`',
            array('size' => 'value')
        );

        $collection->getSelect()->joinLeft(
            array('catalog_product_index_eav_flavor' => 'catalog_product_index_eav'),
            '`catalog_product_index_eav_flavor`.`entity_id` = `main_table`.`product_id`'
                . ' AND `catalog_product_index_eav_flavor`.`attribute_id` IN'
                . ' (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "flavor")',
            array('attribute_id')
        );

        $collection->getSelect()->joinLeft(array('eav_attribute_option_value_flavor' => 'eav_attribute_option_value'),
            '`eav_attribute_option_value_flavor`.`option_id` = `catalog_product_index_eav_flavor`.`value`',
            array('flavor' => 'value')
        );

        $collection->getSelect()->joinLeft(
            array('catalog_product_entity_varchar_barcode' => 'catalog_product_entity_varchar'),
            '`catalog_product_entity_varchar_barcode`.`attribute_id` ='
                . ' (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "barcode")'
                . ' AND `catalog_product_entity_varchar_barcode`.`entity_id` = `catalog/product`.`entity_id`',
            array('barcode' => 'value')
        );

        $collection->getSelect()->joinLeft(
            array('catalog_product_entity_varchar_promo_description' => 'catalog_product_entity_varchar'),
            '`catalog_product_entity_varchar_promo_description`.`attribute_id` ='
                . ' (SELECT attribute_id FROM eav_attribute WHERE attribute_code = "promo_description")'
                . ' AND `catalog_product_entity_varchar_promo_description`.`entity_id` = `catalog/product`.`entity_id`',
            array('promo_description' => 'value')
        );

        $collection->getSelect()->distinct(true);
        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }
}