<?php
class Pd_AdvancedStock_Block_Transfer_Edit_Tabs_AddProducts
    extends MDN_AdvancedStock_Block_Transfer_Edit_Tabs_AddProducts
{

    protected function _prepareCollection()
    {
        parent::_prepareCollection();
        /* @var $helper Pd_AdvancedStock_Helper_Product_ConfigurableAttributes */
        $helper = mage::helper('AdvancedStock/Product_ConfigurableAttributes');
        $collection = $this->getCollection();
        foreach ($collection->getItems() as $item) {
            $item->setName($helper->getHeaderText($item->getEntityId()));
        }
        return $this->getCollection();
    }
    
}
