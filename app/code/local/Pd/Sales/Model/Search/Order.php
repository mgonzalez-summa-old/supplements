<?php


class Pd_Sales_Model_Search_Order extends Mage_Adminhtml_Model_Search_Order
{
    /**
     * Load search results
     *
     * @return Mage_Adminhtml_Model_Search_Order
     */
    public function load()
    {
        $arr = array();

        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }

        $query = $this->getQuery();
        //TODO: add full name logic
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToSearchFilter(array(
                array('attribute' => 'main_table.increment_id',       'like'=>$query.'%'),
                array('attribute' => 'billing_firstname',  'like'=>$query.'%'),
                array('attribute' => 'billing_lastname',   'like'=>$query.'%'),
                array('attribute' => 'billing_telephone',  'like'=>$query.'%'),
                array('attribute' => 'billing_postcode',   'like'=>$query.'%'),

                array('attribute' => 'shipping_firstname', 'like'=>$query.'%'),
                array('attribute' => 'shipping_lastname',  'like'=>$query.'%'),
                array('attribute' => 'shipping_telephone', 'like'=>$query.'%'),
                array('attribute' => 'shipping_postcode',  'like'=>$query.'%'),
            ));

        $this->addAmazonAndTrackingFields($collection,$query);


        $collection
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();

        foreach ($collection as $order) {
            $additional_data_uns = unserialize($order->getAdditionalData());
            $amazon_number = '';
            if(isset($additional_data_uns['channel_order_id'])){
               $amazon_number = $additional_data_uns['channel_order_id'];
            }

            $arr[] = array(
                'id'                => 'order/1/'.$order->getId(),
                'type'              => Mage::helper('adminhtml')->__('Order'),
                'name'              => Mage::helper('adminhtml')->__('Order Number: %s', $order->getIncrementId()),
                'amazon_number'   => Mage::helper('adminhtml')->__('Amazon Number: %s', $amazon_number),
                'ship_to'   => Mage::helper('adminhtml')->__('Ship To: %s', $order->getShippingLastname().', '.$order->getShippingFirstname()),
                'bill_to'   => Mage::helper('adminhtml')->__('Bill To: %s', $order->getBillingLastname().', '.$order->getBillingFirstname()),
                'carrier'   => Mage::helper('adminhtml')->__('Carrier: %s', $order->getTitle().', '.$order->getTrackNumber()),
                'form_panel_title'  => Mage::helper('adminhtml')->__('Order #%s (%s)', $order->getIncrementId(), $order->getBillingFirstname().' '.$order->getBillingLastname()),
                'url' => Mage::helper('adminhtml')->getUrl('*/sales_order/view', array('order_id'=>$order->getId())),
            );
        }

        $this->setResults($arr);

        return $this;
    }

    /**
     * Join table sales_flat_shipment_track and sales_flat_order_payment to select amazon order id and tracking number order.
     *
     * @return Mage_Sales_Model_Resource_Order_Collection
     */
    protected function addAmazonAndTrackingFields($collection,$query)
    {
        $paymentAliasName = 'payment_o_a';
        $joinTablePayment = $collection->getTable('sales/order_payment');
        $shipmentAliasName = 'shipment_o_a';
        $joinTableShipment = $collection->getTable('sales/shipment_track');


        $collection
            ->addFilterToMap('payment_amazon_order_id', $paymentAliasName . '.additional_data')

            ->addFilterToMap('shipment_tracking_id', $shipmentAliasName . '.track_number')
            ->addFilterToMap('shipment_tracking_id', $shipmentAliasName . '.title');

        $collection
            ->getSelect()
            ->joinLeft(
                array($paymentAliasName => $joinTablePayment),
                "(main_table.entity_id = {$paymentAliasName}.parent_id)",
                array(
                    $paymentAliasName . '.additional_data'
                )
            )
            ->joinLeft(
                array($shipmentAliasName => $joinTableShipment),
                "(main_table.entity_id = {$shipmentAliasName}.order_id)",
                array(
                    $shipmentAliasName . '.track_number',
                    $shipmentAliasName . '.title'
                )
            );

        Mage::getResourceHelper('core')->prepareColumnsList($collection->getSelect());

        $collection->addFieldToSearchFilter($shipmentAliasName.'.track_number', array('attribute' => 'track_number',       'like'=>$query.'%'));

        $collection->addFieldToSearchFilter($paymentAliasName.'.additional_data', array('attribute' => 'additional_data', 'like'=>'%'.$query.'%'));


        return $collection;
    }
}
