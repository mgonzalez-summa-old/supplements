<?php
class Pd_Friendlystatus_Model_System_Config_Source_Order_Status
{
    // set null to enable all possible
    protected $_stateStatuses = array(
            Mage_Sales_Model_Order::STATE_NEW,
            //        Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
            Mage_Sales_Model_Order::STATE_PROCESSING,
            Mage_Sales_Model_Order::STATE_COMPLETE,
            Mage_Sales_Model_Order::STATE_CLOSED,
            Mage_Sales_Model_Order::STATE_CANCELED,
            Mage_Sales_Model_Order::STATE_HOLDED,
            Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW,
            //Mage_Sales_Model_Order::STATUS_FRAUD,
    );

    public function toOptionArray()
    {
        if ($this->_stateStatuses) {
            $statuses = Mage::getSingleton('sales/order_config')->getStateStatuses($this->_stateStatuses);
        }
        else {
            $statuses = Mage::getSingleton('sales/order_config')->getStatuses();
        }
        
        //Limit status down only to those we are showing on the sales order grid
        $statuses    =    Mage::getModel('amoaction/command_status')->filterStatusResults($statuses);
        
        $options = array();
        $options[] = array(
                'value' => '',
                'label' => Mage::helper('adminhtml')->__('-- Please Select --')
        );
        foreach ($statuses as $code=>$label) {

            $options[] = array(
                    'value' => $code,
                    'label' => $label
            );
        }
        return $options;
    }
}