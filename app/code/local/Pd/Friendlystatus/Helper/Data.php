<?php
class Pd_Friendlystatus_Helper_Data extends Mage_Core_Helper_Abstract
{
    
    public function getProperLabel($statusLabel)
    {
        //Get status code based off label
        $collection        =    Mage::getModel('sales/order_status')->getCollection()->addFieldToFilter('label',$statusLabel);
        $badStatusArray    =    explode(',',Mage::getStoreConfig('friendlystatus/mapping/order_statuses'));
        
        foreach($collection as $status):
            if(in_array($status->getStatus(),$badStatusArray)):
                $statusLabel   =    Mage::getStoreConfig('friendlystatus/mapping/customer_label');
            endif;
        endforeach;
        
        return $statusLabel;
    }
}