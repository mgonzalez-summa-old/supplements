<?php
class Pd_Reports_Block_Report_Renderer_Flavor extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $eavAttributeModel = Mage::getModel('eav/entity_attribute');
        $attributeModel = $eavAttributeModel->loadByCode('catalog_product', 'flavor');
        $value =  $row->getData($this->getColumn()->getIndex());
        $value = $attributeModel->getSource()->getOptionText($value);
        return $value;
    }
}
