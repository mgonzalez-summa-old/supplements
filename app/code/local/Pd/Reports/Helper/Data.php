<?php

class Pd_Reports_Helper_Data extends Mage_Core_Helper_Abstract{

    /**
     * returns the min created date of any order with a greater Id than given
     * @param $orderId
     * @return string
     */

    public function getMinDateByOrderId($orderId)
    {
        $orderId = (int) $orderId;
        $minDateOrder = Mage::getModel("sales/order")
            ->getCollection()
            ->addFieldToFilter('increment_id', array('from' => $orderId))
            ->setOrder('created_at', 'asc')
            ->setPageSize(1)
            ->load()
            ->getFirstItem();

        if ($minDateOrder->getId()) {
            $unformatedDate = $minDateOrder->getCreatedAt();
            $time = strtotime($unformatedDate);
            $formatedDate = date("m/d/Y",$time);
            return $formatedDate;
        }

        return null;

    }

    /**
     * returns the max created date of any order with a lesser Id than given
     * @param $orderId
     * @return string
     */

    public function getMaxDateByOrderId($orderId)
    {
        $orderId = (int) $orderId;
        $maxDateOrder = Mage::getModel("sales/order")
            ->getCollection()
            ->addFieldToFilter('increment_id', array('to' => $orderId))
            ->setOrder('created_at', 'desc')
            ->setPageSize(1)
            ->load()
            ->getFirstItem();

        if ($maxDateOrder->getId()) {
            $unformatedDate = $maxDateOrder->getCreatedAt();
            $time = strtotime($unformatedDate);
            $formatedDate = date("m/d/Y",$time);
            return $formatedDate;
        }

        return null;

    }

}