<?php
/**
 * Adminhtml sales order column renderer
 *
 * @category   Pd
 * @package    Pd_Adminhtml
 */
class Pd_Adminhtml_Block_Sales_Items_Column_Name extends Mage_Adminhtml_Block_Sales_Items_Column_Name
{
    public function getItem()
    {
        /* @var $helper Pd_AdvancedStock_Helper_Product_ConfigurableAttributes */
        $helper = mage::helper('AdvancedStock/Product_ConfigurableAttributes');
        if ($this->_getData('item') instanceof Mage_Sales_Model_Order_Item) {
            $item = $this->_getData('item');
        } else {
            $item = $this->_getData('item')->getOrderItem();
        }
        $customName = $helper->getHeaderText($item->getProductId());
        $item->setName($customName);
        return $item;
    }

}
