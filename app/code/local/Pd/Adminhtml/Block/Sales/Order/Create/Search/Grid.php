<?php

class Pd_Adminhtml_Block_Sales_Order_Create_Search_Grid
    extends Mage_Adminhtml_Block_Sales_Order_Create_Search_Grid
{
    /**
     * Prepare the columns.
     *
     * @return Pd_Adminhtml_Block_Sales_Order_Create_Search_Grid
     */
    protected function _prepareColumns()
    {
        $grid = parent::_prepareColumns();
        $grid->getColumn('price')
            ->setData('renderer', 'pd_adminhtml/sales_order_create_search_grid_renderer_price')
            ->setFilterConditionCallback(array($this, '_applyTierPriceFilter'));

        return $grid;
    }

    /**
     * Custom filter for tier prices.
     *
     * @param Varien_Data_Collection_Db $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     */
    public function _applyTierPriceFilter(Varien_Data_Collection_Db $collection, Mage_Adminhtml_Block_Widget_Grid_Column $column)
    {
        $select = $collection
            ->getSelect()
            ->joinLeft(array('tp' => 'catalog_product_entity_tier_price'), 'e.entity_id = tp.entity_id')
            ->group('e.entity_id');

        $value = $column->getFilter()->getValue();
        if (isset($value['from']) && isset($value['to'])) {
            $priceCondition = "tp.value BETWEEN {$value['from']} AND {$value['to']}";

        } elseif (isset($value['from'])) {
            $priceCondition = "tp.value >= {$value['from']}";

        } elseif (isset($value['to'])) {
            $priceCondition = "tp.value <= {$value['to']}";
        }
        $select->where($priceCondition);
    }
}
