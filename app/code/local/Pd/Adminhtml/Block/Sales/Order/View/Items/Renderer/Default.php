<?php
/**
 * Adminhtml sales order item renderer
 *
 * @category   Pd
 * @package    Pd_Adminhtml
 */
class Pd_Adminhtml_Block_Sales_Order_View_Items_Renderer_Default
    extends Mage_Adminhtml_Block_Sales_Order_View_Items_Renderer_Default
{
    public function getItem()
    {
        /* @var $helper Pd_AdvancedStock_Helper_Product_ConfigurableAttributes */
        $helper = mage::helper('AdvancedStock/Product_ConfigurableAttributes');
        $item = $this->_getData('item');
        $customName = $helper->getHeaderText($item->getItemId());
        $item->setName($customName);
        return $item ;
    }

}
