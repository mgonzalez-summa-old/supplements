<?php

class Pd_Adminhtml_Block_Sales_Order_Create_Search_Grid_Renderer_Price
    extends Mage_Adminhtml_Block_Sales_Order_Create_Search_Grid_Renderer_Price
{
    /**
     * Renders grid column of products to add in the new order.
     *
     * @param Varien_Object $row
     *
     * @return string The HTML for the cell.
     */
    public function render(Varien_Object $row)
    {
        $product = $row;
        $renderedPrice = parent::render($product);

        $currencyCode = $this->_getCurrencyCode($product);
        $currency = Mage::app()->getLocale()->currency($currencyCode);

        // Prices by customer group
        $product->load($product->getId());
        $prices = $product->getData('tier_price');

        if (!empty($prices)) {
            $renderedPrice = '<em>' . $this->__('Base') . ": </em> <strong>$renderedPrice</strong>";
            foreach ($prices as $price) {
                $priceValue = sprintf("%f", $price['price']);
                $priceValue = $currency->toCurrency($priceValue); 

                // @todo Cache customer group names
                $group = Mage::getModel('customer/group')->load($price['cust_group']);
                $renderedPrice .= '<br/><em>' . $group->getCustomerGroupCode() . ':</em> ' . $priceValue;
            }
        }

        return $renderedPrice;
    }
}