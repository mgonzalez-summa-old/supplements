<?php
class Pd_Adminhtml_Block_Sales_Order_View_Info
    extends Mage_Adminhtml_Block_Sales_Order_View_Info
{
    public function __construct()
    {
        parent::__construct();
        Mage::getDesign()->setTheme('custom');
    }


    public function getAdminUsername($_order)
    {
        $adminUserId = $_order->getCreatorUserId();

        $adminUser = Mage::getModel('admin/user')->load($adminUserId);

        if ($adminUser->getId()) {
            return $adminUser->getName();
        } else {
            return $this->__('System');
        }
    }

}