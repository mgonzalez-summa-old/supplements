<?php
/**
 * Adminhtml sales order item renderer
 *
 * @category   Pd
 * @package    Pd_Adminhtml
 */
class Pd_Adminhtml_Block_Customer_Edit_Tab_View_Grid_Renderer_Item extends Mage_Adminhtml_Block_Customer_Edit_Tab_View_Grid_Renderer_Item
{

    /*
     * Returns product associated with this block
     *
     * @param Mage_Catalog_Model_Product $product
     * @return string
     */
    public function getProduct()
    {
        /* @var $helper Pd_AdvancedStock_Helper_Product_ConfigurableAttributes */
        $helper = mage::helper('AdvancedStock/Product_ConfigurableAttributes');
        $product = $this->getItem()->getProduct();
        $product->setName($helper->getHeaderText($product->getId()));
        return $product;
    }
}
