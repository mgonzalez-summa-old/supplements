<?php
/**
 * Adminhtml bestsellers report grid block
 *
 * @category   Pd
 * @package    Pd_Adminhtml
 */
class Pd_Adminhtml_Block_Report_Sales_Bestsellers_Grid
    extends Mage_Adminhtml_Block_Report_Sales_Bestsellers_Grid
{

    public function _prepareCollection()
    {
        parent::_prepareCollection();
        /* @var $helper Pd_AdvancedStock_Helper_Product_ConfigurableAttributes */
        $helper = mage::helper('AdvancedStock/Product_ConfigurableAttributes');
        $collection = $this->getCollection();
        foreach ($collection->getItems() as $period) {
            $period->setProductName($helper->getHeaderText($period->getProductId()));
            foreach ($period->getChildren() as $item) {
                $item->setProductName($helper->getHeaderText($item->getProductId()));
            }
        }
        return $this->getCollection();
    }
}
