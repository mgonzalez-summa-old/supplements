<?php
/**
 * Adminhtml bestsellers report grid block
 *
 * @category   Pd
 * @package    Pd_Adminhtml
 */
class Pd_Adminhtml_Block_Report_Review_Product_Grid extends Mage_Adminhtml_Block_Report_Review_Product_Grid
{

    protected function _prepareCollection()
    {
        parent::_prepareCollection();
        /* @var $helper Pd_AdvancedStock_Helper_Product_ConfigurableAttributes */
        $helper = mage::helper('AdvancedStock/Product_ConfigurableAttributes');
        $collection = $this->getCollection();
        foreach ($collection->getItems() as $item) {
            $item->setName($helper->getHeaderText($item->getEntityId()));
        }
        return $this->getCollection();
    }
}
