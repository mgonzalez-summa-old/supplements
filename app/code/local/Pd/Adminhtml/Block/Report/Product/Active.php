<?php

class Pd_Adminhtml_Block_Report_Product_Active extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Initialize container block settings
     *
     */
    public function __construct()
    {
        $this->_blockGroup = 'pd_adminhtml';
        $this->_controller = 'report_product_active';
        $this->_headerText = Mage::helper('reports')->__('Active Products Ordered');
        parent::__construct();
        $this->_removeButton('add');
    }
}
