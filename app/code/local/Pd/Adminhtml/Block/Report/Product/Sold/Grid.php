<?php
/**
 * Adminhtml bestsellers report grid block
 *
 * @category   Pd
 * @package    Pd_Adminhtml
 */
class Pd_Adminhtml_Block_Report_Product_Sold_Grid extends Mage_Adminhtml_Block_Report_Product_Sold_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('Pd/Report/Grid.phtml');
    }

    protected function _prepareCollection()
    {
        $filter = $this->getParam($this->getVarNameFilter(), null);

        if (is_null($filter)) {
            $filter = $this->_defaultFilter;
        }

        if (is_string($filter)) {
            $data = array();
            $filter = base64_decode($filter);
            parse_str(urldecode($filter), $data);

            if (!isset($data['report_from'])) {
                // getting all reports from 2001 year
                $date = new Zend_Date(mktime(0,0,0,1,1,2001));
                $data['report_from'] = $date->toString($this->getLocale()->getDateFormat('short'));
            }

            if (!isset($data['report_to'])) {
                // getting all reports from 2001 year
                $date = new Zend_Date();
                $data['report_to'] = $date->toString($this->getLocale()->getDateFormat('short'));
            }

            $this->_setFilterValues($data);
        } else if ($filter && is_array($filter)) {
            $this->_setFilterValues($filter);
        } else if(0 !== sizeof($this->_defaultFilter)) {
            $this->_setFilterValues($this->_defaultFilter);
        }
        /** @var $collection Mage_Reports_Model_Resource_Report_Collection */
        $collection = Mage::getResourceModel('reports/report_collection');


        $collection->setPeriod($this->getFilter('report_period'));


        $order_id_from = (int) $this->getFilter('report_from');
        $order_id_to =  (int) $this->getFilter('report_to');
        $orderRangeRegitry = array('from' => $order_id_from,'to' => $order_id_to);
        Mage::register("orderRangeRegistry",$orderRangeRegitry);

        if ($order_id_from && $order_id_to) {
            try {
                $helper = Mage::helper('pd_reports');
                $dateFrom = $helper->getMinDateByOrderId($order_id_from);
                $dateTo = $helper->getMaxDateByOrderId($order_id_to);
                if ($dateFrom && $dateTo) {
                    $from = $this->getLocale()->date($dateFrom, Zend_Date::DATE_SHORT, null, false);
                    $to   = $this->getLocale()->date($dateTo, Zend_Date::DATE_SHORT, null, false);

                    $collection->setInterval($from, $to);
                }
            }
            catch (Exception $e) {
                $this->_errors[] = Mage::helper('reports')->__('Invalid date specified.');
            }
        }

        /**
         * Getting and saving store ids for website & group
         */
        $storeIds = array();
        if ($this->getRequest()->getParam('store')) {
            $storeIds = array($this->getParam('store'));
        } elseif ($this->getRequest()->getParam('website')){
            $storeIds = Mage::app()->getWebsite($this->getRequest()->getParam('website'))->getStoreIds();
        } elseif ($this->getRequest()->getParam('group')){
            $storeIds = Mage::app()->getGroup($this->getRequest()->getParam('group'))->getStoreIds();
        }

        // By default storeIds array contains only allowed stores
        $allowedStoreIds = array_keys(Mage::app()->getStores());
        // And then array_intersect with post data for prevent unauthorized stores reports
        $storeIds = array_intersect($allowedStoreIds, $storeIds);
        // If selected all websites or unauthorized stores use only allowed
        if (empty($storeIds)) {
            $storeIds = $allowedStoreIds;
        }
        // reset array keys
        $storeIds = array_values($storeIds);


        $collection->setStoreIds($storeIds);

        if ($this->getSubReportSize() !== null) {
            $collection->setPageSize($this->getSubReportSize());
        }

        $this->setCollection($collection);

        Mage::dispatchEvent('adminhtml_widget_grid_filter_collection',
            array('collection' => $this->getCollection(), 'filter_values' => $this->_filterValues)
        );

        $this->getCollection()
            ->initReport('reports/product_sold_collection');
    }

    /**
     * Prepare Grid columns
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareColumns()
    {
 /*       $this->addColumn(
            'order_item_id',
            array(
                'header' => Mage::helper('reports')->__('Order'),
                'index' => 'order_number',
                //'renderer' => 'pd_reports/report_renderer_brand'
            )
        );
*/

        $this->addColumn(
            'brand',
            array(
                'header' => Mage::helper('reports')->__('Brand'),
                'index' => 'brand',
                'renderer' => 'pd_reports/report_renderer_brand'
            )
        );

        $this->addColumn(
            'name',
            array(
                'header' => Mage::helper('reports')->__('Product Name'),
                'index' => 'order_items_name'
            )
        );

        $this->addColumn(
            'flavor',
            array(
                'header' => Mage::helper('reports')->__('Flavor'),
                'index' => 'flavor',
                'renderer' => 'pd_reports/report_renderer_flavor'
            )
        );

        $this->addColumn(
            'promo_description',
            array(
                'header' => Mage::helper('reports')->__('Promo Description'),
                'index' => 'promo_description',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'size',
            array(
                'header' => Mage::helper('reports')->__('Size'),
                'index' => 'size',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'ordered_qty',
            array(
                'header' => Mage::helper('reports')->__('Quantity Ordered'),
                'width' => '120px',
                'align' => 'right',
                'index' => 'ordered_qty',
                'total' => 'sum',
                'type' => 'number'
            )
        );

        $this->addColumn(
            'europa',
            array(
                'header' => Mage::helper('reports')->__('Europa'),
                'index' => 'europa',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'thresh',
            array(
                'header' => Mage::helper('reports')->__('Thresh'),
                'index' => 'thresh',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'usasport',
            array(
                'header' => Mage::helper('reports')->__('Usa Sport'),
                'index' => 'usasport',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'supnat',
            array(
                'header' => Mage::helper('reports')->__('SupNat'),
                'index' => 'supnat',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'selectnut',
            array(
                'header' => Mage::helper('reports')->__('SelectNut'),
                'index' => 'selectnut',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'direct',
            array(
                'header' => Mage::helper('reports')->__('Direct'),
                'index' => 'direct',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'resultz',
            array(
                'header' => Mage::helper('reports')->__('Resultz'),
                'index' => 'resultz',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'baf',
            array(
                'header' => Mage::helper('reports')->__('BAF'),
                'index' => 'baf',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'betterbody',
            array(
                'header' => Mage::helper('reports')->__('BetterBody'),
                'index' => 'betterbody',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'pharmadirect',
            array(
                'header' => Mage::helper('reports')->__('PharmaDirect'),
                'index' => 'pharmadirect',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'brandnew',
            array(
                'header' => Mage::helper('reports')->__('BrandNew'),
                'index' => 'brandnew',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'jd',
            array(
                'header' => Mage::helper('reports')->__('JD'),
                'index' => 'jd',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'lonestar',
            array(
                'header' => Mage::helper('reports')->__('LoneStar'),
                'index' => 'lonestar',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'paf',
            array(
                'header' => Mage::helper('reports')->__('PAF'),
                'index' => 'paf',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'treelife',
            array(
                'header' => Mage::helper('reports')->__('TreeLife'),
                'index' => 'treelife',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'countrylife',
            array(
                'header' => Mage::helper('reports')->__('CountryLife'),
                'index' => 'countrylife',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'upc2',
            array(
                'header' => Mage::helper('reports')->__('UPC2'),
                'index' => 'upc2',
                'type' => 'text',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'sku',
            array(
                'header' => Mage::helper('reports')->__('SKU'),
                'index' => 'sku',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        return parent::_prepareColumns();
    }


}
