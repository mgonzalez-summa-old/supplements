<?php
/**
 * Adminhtml bestsellers report grid block
 *
 * @category   Pd
 * @package    Pd_Adminhtml
 */
class Pd_Adminhtml_Block_Report_Product_Viewed_Grid extends Mage_Adminhtml_Block_Report_Product_Viewed_Grid
{

    public function getReport($from, $to)
    {
        $report = parent::getReport($from, $to);
        /* @var $helper Pd_AdvancedStock_Helper_Product_ConfigurableAttributes */
        $helper = mage::helper('AdvancedStock/Product_ConfigurableAttributes');
        foreach ($report->getItems() as $item) {
            $item->setName($helper->getHeaderText($item->getEntityId()));
        }
        return $report;
    }

}
