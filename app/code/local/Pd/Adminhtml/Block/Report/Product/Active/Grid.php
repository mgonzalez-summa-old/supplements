<?php
/**
 * Adminhtml bestsellers report grid block
 *
 * @category   Pd
 * @package    Pd_Adminhtml
 */
class Pd_Adminhtml_Block_Report_Product_Active_Grid extends Mage_Adminhtml_Block_Report_Grid
{

    /**
     * Sub report size
     *
     * @var int
     */
    protected $_subReportSize = 0;

    /**
     * Initialize Grid settings
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('gridProductsActive');
    }

    /**
     * Prepare collection object for grid
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareCollection()
    {
        parent::_prepareCollection();
        $this->getCollection()
            ->initReport('pd_reports/product_active_collection');
        return $this;
    }

    /**
     * Prepare Grid columns
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'brand',
            array(
                'header' => Mage::helper('reports')->__('Brand'),
                'index' => 'brand',
                'renderer' => 'pd_reports/report_renderer_brand'
            )
        );

        $this->addColumn(
            'name',
            array(
                'header' => Mage::helper('reports')->__('Product Name'),
                'index' => 'order_items_name'
            )
        );

        $this->addColumn(
            'flavor',
            array(
                'header' => Mage::helper('reports')->__('Flavor'),
                'index' => 'flavor',
                'renderer' => 'pd_reports/report_renderer_flavor'
            )
        );

        $this->addColumn(
            'promo_description',
            array(
                'header' => Mage::helper('reports')->__('Promo Description'),
                'index' => 'promo_description',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'size',
            array(
                'header' => Mage::helper('reports')->__('Size'),
                'index' => 'size',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'ordered_qty',
            array(
                'header' => Mage::helper('reports')->__('Quantity Ordered'),
                'width' => '120px',
                'align' => 'right',
                'index' => 'ordered_qty',
                'total' => 'sum',
                'type' => 'number'
            )
        );

        $this->addColumn(
            'europa',
            array(
                'header' => Mage::helper('reports')->__('Europa'),
                'index' => 'europa',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'thresh',
            array(
                'header' => Mage::helper('reports')->__('Thresh'),
                'index' => 'thresh',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'usasport',
            array(
                'header' => Mage::helper('reports')->__('Usa Sport'),
                'index' => 'usasport',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'supnat',
            array(
                'header' => Mage::helper('reports')->__('SupNat'),
                'index' => 'supnat',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'selectnut',
            array(
                'header' => Mage::helper('reports')->__('SelectNut'),
                'index' => 'selectnut',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'direct',
            array(
                'header' => Mage::helper('reports')->__('Direct'),
                'index' => 'direct',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'resultz',
            array(
                'header' => Mage::helper('reports')->__('Resultz'),
                'index' => 'resultz',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'baf',
            array(
                'header' => Mage::helper('reports')->__('BAF'),
                'index' => 'baf',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'betterbody',
            array(
                'header' => Mage::helper('reports')->__('BetterBody'),
                'index' => 'betterbody',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'pharmadirect',
            array(
                'header' => Mage::helper('reports')->__('PharmaDirect'),
                'index' => 'pharmadirect',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'brandnew',
            array(
                'header' => Mage::helper('reports')->__('BrandNew'),
                'index' => 'brandnew',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'jd',
            array(
                'header' => Mage::helper('reports')->__('JD'),
                'index' => 'jd',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'lonestar',
            array(
                'header' => Mage::helper('reports')->__('LoneStar'),
                'index' => 'lonestar',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'paf',
            array(
                'header' => Mage::helper('reports')->__('PAF'),
                'index' => 'paf',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'treelife',
            array(
                'header' => Mage::helper('reports')->__('TreeLife'),
                'index' => 'treelife',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'countrylife',
            array(
                'header' => Mage::helper('reports')->__('CountryLife'),
                'index' => 'countrylife',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'upc2',
            array(
                'header' => Mage::helper('reports')->__('UPC2'),
                'index' => 'upc2',
                'type' => 'text',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addColumn(
            'sku',
            array(
                'header' => Mage::helper('reports')->__('SKU'),
                'index' => 'sku',
                'renderer' => 'pd_reports/report_renderer_null'
            )
        );

        $this->addExportType('*/*/exportActiveCsv', Mage::helper('reports')->__('CSV'));
        $this->addExportType('*/*/exportActiveExcel', Mage::helper('reports')->__('Excel XML'));

        return parent::_prepareColumns();
    }
}
