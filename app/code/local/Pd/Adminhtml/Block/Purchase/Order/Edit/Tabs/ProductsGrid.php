<?php

class Pd_Adminhtml_Block_Purchase_Order_Edit_Tabs_ProductsGrid extends MDN_Purchase_Block_Order_Edit_Tabs_ProductsGrid
{

    protected function _prepareCollection()
    {
        parent::_prepareCollection();
        /* @var $helper Pd_AdvancedStock_Helper_Product_ConfigurableAttributes */
        $helper = mage::helper('AdvancedStock/Product_ConfigurableAttributes');
        $collection = $this->getCollection();
        foreach ($collection->getItems() as $item) {
            $item->setPopProductName($helper->getHeaderText($item->getSku()), true);
        }
        return $this->getCollection();
    }

}
