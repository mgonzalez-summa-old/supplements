<?php
/**
 * Product in category grid
 *
 * @category   Pd
 * @package    Pd_Adminhtml
 */
class Pd_Adminhtml_Block_Catalog_Category_Tab_Product extends Mage_Adminhtml_Block_Catalog_Category_Tab_Product
{

    protected function _prepareCollection()
    {
        parent::_prepareCollection();
        /* @var $helper Pd_AdvancedStock_Helper_Product_ConfigurableAttributes */
        $helper = mage::helper('AdvancedStock/Product_ConfigurableAttributes');
        $collection = $this->getCollection();
        foreach ($collection->getItems() as $k => $item){
            $item->setName($helper->getHeaderText($k));
        }
        $this->setCollection($collection);
        return $this;
    }

}

