<?php
require_once 'Mage/Adminhtml/controllers/Report/ProductController.php';
class Pd_Adminhtml_Report_ProductController extends Mage_Adminhtml_Report_ProductController
{

    /**
     * Active Products Report Action
     *
     */
    public function activeAction()
    {
        $this->_title($this->__('Reports'))
             ->_title($this->__('Products'))
             ->_title($this->__('Active Products Ordered'));

        $this->_initAction()
            ->_setActiveMenu('report/product/active')
            ->_addBreadcrumb(Mage::helper('reports')->__('Active Products Ordered'), Mage::helper('reports')->__('Active Products Ordered'))
            ->_addContent($this->getLayout()->createBlock('pd_adminhtml/report_product_active'), 'report_product_active')
            ->renderLayout();
    }

    /**
     * Export Sold Products report to CSV format action
     *
     */
    public function exportActiveCsvAction()
    {
        $fileName   = 'active_products_ordered.csv';
        $content    = $this->getLayout()
            ->createBlock('pd_adminhtml/report_product_active_grid')
            ->getCsv();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export Sold Products report to XML format action
     *
     */
    public function exportActiveExcelAction()
    {
        $fileName   = 'active_products_ordered.xml';
        $content    = $this->getLayout()
            ->createBlock('pd_adminhtml/report_product_active_grid')
            ->getExcel($fileName);

        $this->_prepareDownloadResponse($fileName, $content);
    }

}
