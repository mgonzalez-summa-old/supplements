<?php
$installer = $this;
$installer->startSetup();

Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$content = <<<STR
<div>To change this content just edit the static block with id static-cross-sell-items</div>
<div>The widget used here (configured with example data) is "Static Cross Sell Cart Items", which you can reconfigure as needed.</div>
{{widget type="pd_staticcrosssell/crosssell_cart_items" template="pd/staticcrosssell/cart/items.phtml" cross_sell_item_a="product/18" cross_sell_item_b="product/24" cross_sell_category_items="category/125"}}
STR;

$staticBlock = array(
    'title' => 'Static Cross Sell Items',
    'identifier' => 'static-cross-sell-items',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);

Mage::getModel('cms/block')->setData($staticBlock)->save();

$installer->endSetup();