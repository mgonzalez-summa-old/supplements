<?php
class Pd_StaticCrossSell_Helper_Data
    extends Mage_Core_Helper_Abstract
{

    public function isInCart($id)
    {
        if(!$id) {
            return false;
        }
        $cart = Mage::helper('checkout/cart')->getCart();
        $ids = $cart->getQuoteProductIds();
        return in_array($id, $ids);
    }
}