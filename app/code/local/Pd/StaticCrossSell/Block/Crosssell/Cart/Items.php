<?php
class Pd_StaticCrossSell_Block_Crosssell_Cart_Items
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{

    var $_topItems = null;
    var $_bottomItems = null;

    /**
     * Internal constructor
     *
     */
    protected function _construct()
    {
        $this->setTemplate('pd/staticcrosssell/cart/items.phtml');
        parent::_construct();
    }

    public function getTopItems()
    {
        if (is_null($this->_topItems)) {
            $this->_topItems = array();

            foreach (array('a','b') as $itemIndex) {
                $crossSellItem = $this->getData('cross_sell_item_'.$itemIndex);
                $crossSellItem = explode('/', $crossSellItem);
                $crossSellItem = end($crossSellItem);

                $prod = Mage::getModel('catalog/product')->load($crossSellItem);
                if ($prod && $prod->getId()) {
                    $this->_topItems[] = $prod;
                }
            }

        }

        return $this->_topItems;
    }

    public function getBottomItems()
    {
        if (is_null($this->_bottomItems)) {
            $categoryId = $this->getData('cross_sell_category_items');
            $categoryId = explode('/', $categoryId);
            $categoryId = end($categoryId);

            $category = Mage::getModel('catalog/category')->load($categoryId);


            $productCollection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(array('name','promo_description'))
                ->addCategoryFilter($category)
                ->setPageSize(9);

            if ($productCollection->count() > 0) {
                $this->_bottomItems = $productCollection;
            }
        }
        return $this->_bottomItems;
    }
}