<?php
$installer = $this;

$installer->startSetup();

$installer->run("ALTER TABLE  {$this->getTable('sales/quote')} ADD  `creator_user_id` INT(10) UNSIGNED NULL;");
$installer->run("ALTER TABLE  {$this->getTable('sales/order')} ADD  `creator_user_id` INT(10) UNSIGNED NULL;");

$installer->endSetup();