<?php
/**
 * Sales Order observer
 *
 * @category    Pd
 * @package     Pd_Order
 */
class Pd_Order_Model_Observer
{
    /**
     * Register an event trigger after the order has been saved
     *
     * Is triggered by sales_order_place_after event
     *
     */
    public function hookSalesOrderPlaceAfter ($observer)
    {
        /* @var $order Mage_Sales_Model_Order */
        $order = $observer->getOrder(); // returns the Mage_Sales_Model_Order Object
        if($order->getGrandTotal() >= 100 && !$this->validateOrderAddresses($order)) {
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID); // Magento can only save attributes in admin mode
            $order->setStatus('fraud')->save();
        }

        return $this;
    }

    public function validateOrderAddresses($order)
    {
        /* @var $shipAddress Mage_Sales_Model_Order_Address */
        $shipAddress = $order->getShippingAddress();
        $billAddress = $order->getBillingAddress();
        if ($shipAddress->getStreet() != $billAddress->getStreet())
            return false;
        if ($shipAddress->getCity() != $billAddress->getCity())
            return false;
        if ($shipAddress->getRegionId() != $billAddress->getRegionId())
            return false;
        if ($shipAddress->getPostcode() != $billAddress->getPostcode())
            return false;
        if ($shipAddress->getCountryId() != $billAddress->getCountryId())
            return false;
        return true;
    }
    
    /**
     * If the invoice is printed - if the status was Amazon (new amazon order) or print_queue (Print Queue) - update the status to processing.
     * @param unknown_type $observer
     * @return boolean
     */
    public function hookPrintInvoice($observer)
    {
        $order    =    $observer->getOrder();
        
        if($order->getStatus() == 'amazon'):
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID); // Magento can only save attributes in admin mode
            $order->setStatus('processing')->save();
            return true;
        elseif($order->getStatus() == 'print_queue'):
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID); // Magento can only save attributes in admin mode
            $order->setStatus('processing')->save();
            return true;
        endif;
        
        return false;
    }
}
