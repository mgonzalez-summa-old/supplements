<?php
$installer = $this;

$installer->startSetup();

Mage::getModel('core/config')->saveConfig('catalog/price_match/show_pdp_price_match_button', false);

$installer->endSetup();