<?php
$installer = $this;

$installer->startSetup();

Mage::getModel('core/config')->saveConfig('catalog/price_match/show_product_list_price_match_button', false);
Mage::getModel('core/config')->saveConfig('catalog/price_match/show_compare_list_price_match_button', false);

$installer->endSetup();