<?php

/**
 * Queue observer
 */
class Pd_Queue_Model_Observer
    extends Enterprise_Queue_Model_Observer
{
    /**
     * Delete all completed/failed tasks
     */
    public function deleteUnnecessaryMessages()
    {
        $model = $this->_queueTaskModel ? $this->_queueTaskModel : Mage::getModel('enterprise_queue/queue_task');
        $model->deleteTasks(array(), array(
            Enterprise_Queue_Model_Queue_Task::TASK_STATUS_PENDING,
            Enterprise_Queue_Model_Queue_Task::TASK_STATUS_IN_PROGRESS
        ));
    }

    /**
     * Execute queue tasks
     */
    public function runTasks()
    {
        $helper = $this->_queueHelper ? $this->_queueHelper : Mage::helper('enterprise_queue');
        $helper->getReceiver()->work();
    }
}