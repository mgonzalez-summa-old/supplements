<?php
/**
 * @category    Pd
 * @package     Pd_Exacttarget
 * @author		Darren Brink
 * @copyright   Copyright (c) 2012 Precision Dialogue Marketing (www.precisiondialogue.com)
 *
 * NOTICE OF LICENSE
 *
 * END-USER LICENSE AGREEMENT FOR ExactTarget Integration for Magento IMPORTANT PLEASE
 * READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING
 * WITH THIS PROGRAM INSTALL: Precision Dialogue Marketing End-User License Agreement ("EULA")
 * is a legal agreement between you (either an individual or a single entity) and Precision
 * Dialogue Marketing. For the Precision Dialogue Marketing software product(s) identified above
 * which may include associated software components, media, printed materials, and "online"
 * or electronic documentation ("ExactTarget Integration for Magento"). By installing, copying,
 * or otherwise using the ExactTarget Integration for Magento, you agree to be bound by the
 * terms of this EULA. This license agreement represents the entire agreement concerning
 * the program between you and Precision Dialogue Marketing, (referred to as "licenser"), and it
 *  supersedes any prior proposal, representation, or understanding between the parties. If
 *  you do not agree to the terms of this EULA, do not install or use the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is protected by copyright laws and international
 *  copyright treaties, as well as other intellectual property laws and treaties. The ExactTarget
 *  Integration for Magento is licensed, not sold.
 *  1. GRANT OF LICENSE.
 *  The ExactTarget Integration for Magento is licensed as follows:
 *  (a) Installation and Use.
 *  Precision Dialogue Marketing grants you the right to install and use copies of the ExactTarget
 *  Integration for Magento on your development, staging and production servers running a validly
 *  licensed copy of the operating system for which the ExactTarget Integration for Magento was designed.
 *  (b) Backup Copies.
 *  You may also make copies of the ExactTarget Integration for Magento as may be necessary for
 *  backup and archival purposes.
 *  2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.
 *  (a) Maintenance of Copyright Notices.
 *  You must not remove or alter any copyright notices on any and all copies of the ExactTarget Integration for Magento.
 *  (b) Distribution.
 *  You may not distribute registered copies of the ExactTarget Integration for Magento to
 *  third parties. (c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.
 *  You may not reverse engineer, decompile, or disassemble the ExactTarget Integration for Magento,
 *  except and only to the extent that such activity is expressly permitted by applicable law
 *  notwithstanding this limitation.
 *  (d) Rental.
 *  You may not rent, lease, or lend the ExactTarget Integration for Magento.
 *  (e) Support Services.
 *  Precision Dialogue Marketing may provide you with support services related to the ExactTarget
 *  Integration for Magento ("Support Services"). Any supplemental software code provided to you as
 *  part of the Support Services shall be considered part of the ExactTarget Integration for Magento
 *  and subject to the terms and conditions of this EULA.
 *  (f) Compliance with Applicable Laws.
 *  You must comply with all applicable laws regarding use of the ExactTarget Integration for Magento.
 *  3. TERMINATION
 *  Without prejudice to any other rights, Precision Dialogue Marketing may terminate this EULA if you fail
 *  to comply with the terms and conditions of this EULA. In such event, you must destroy all copies
 *  of the ExactTarget Integration for Magento in your possession.
 *  4. COPYRIGHT
 *  All title, including but not limited to copyrights, in and to the ExactTarget Integration for Magento
 *  and any copies thereof are owned by Precision Dialogue Marketing or its suppliers. All title and intellectual
 *  property rights in and to the content which may be accessed through use of the ExactTarget Integration
 *  for Magento is the property of the respective content owner and may be protected by applicable copyright
 *  or other intellectual property laws and treaties. This EULA grants you no rights to use such content.
 *  All rights not expressly granted are reserved by Precision Dialogue Marketing.
 *  5. NO WARRANTIES
 *  Precision Dialogue Marketing expressly disclaims any warranty for the ExactTarget Integration for Magento.
 *  The ExactTarget Integration for Magento is provided 'As Is' without any express or implied warranty of
 *  any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness
 *  of a particular purpose. Precision Dialogue Marketing does not warrant or assume responsibility for the accuracy
 *  or completeness of any information, text, graphics, links or other items contained within the ExactTarget
 *  Integration for Magento. Precision Dialogue Marketing makes no warranties respecting any harm that may be caused
 *  by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. Precision
 *  Dialogue Marketing further expressly disclaims any warranty or representation to Authorized Users or to any
 *  third party.
 *  6. LIMITATION OF LIABILITY
 *  In no event shall Precision Dialogue Marketing be liable for any damages (including, without limitation,
 *  lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or
 *  inability to use the ExactTarget Integration for Magento, even if Precision Dialogue Marketing has been advised o
 *  f the possibility of such damages. In no event will Precision Dialogue Marketing be liable for loss of data or
 *  for indirect, special, incidental, consequential (including lost profit), or other damages based in contract,
 *  tort or otherwise. Precision Dialogue Marketing shall have no liability with respect to the content of the
 *  ExactTarget Integration for Magento or any part thereof, including but not limited to errors or omissions
 *  contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption,
 *  personal injury, loss of privacy, moral rights or the disclosure of confidential information.
 */
class Pd_Initialsetup_Model_Observer{
	
    /**
     * Process is kicked off by a button click in the config - only needs to be ran once since no matter what environment it was ran in it will created the data
     * extensions in ExactTarget.
     */
	public function createDataExtensions()
	{
		$isActive		=	Mage::getStoreConfig('exacttarget/setup/active');
		$categoryId		=	Mage::getStoreConfig('et_dataextensions/folder/categoryId');
		$createEnabled	=	Mage::getStoreConfig('exacttarget/dataextensions/create');
			
		if($isActive == 1 && $createEnabled == 1 && !is_null($categoryId)):
			
			$dataExtensionList		=	Mage::helper('initialsetup')->getExtensionInformation();
			
			$stores = Mage::app()->getStores();
			
			foreach($stores as $store):
				foreach($dataExtensionList as $dataExtension):
					$this->createExtension($dataExtension['name'],$dataExtension['customerKey'],$categoryId,$dataExtension['description'],$dataExtension['sendable'],$dataExtension['sendableField'],$dataExtension['fields'],$store->getStoreId());
				endforeach;
				
				Mage::getModel('core/config')->saveConfig('exacttarget/dataextensions/create',0,'websites',$store->getStoreId());
				Mage::getModel('core/config')->saveConfig('et_dataextensions/folder/categoryId',null,'websites',$store->getStoreId());
			endforeach;
			
			Mage::getModel('core/config')->saveConfig('exacttarget/dataextensions/create',0,'default',0);
			Mage::getModel('core/config')->saveConfig('et_dataextensions/folder/categoryId',null,'default',0);
		endif;
	}
	
	/**
	 * This method is responsible for creating the data extensions in ExactTarget
	 * @param string $name
	 * @param string $customerKey
	 * @param integer $categoryId
	 * @param string $description
	 * @param boolean $sendable
	 * @param mixed $fieldArray
	 */
	public function createExtension($name,$customerKey,$categoryId,$description,$sendable,$sendField,$fieldArray,$storeId)
	{
		require_once(Mage::helper('exacttarget')->getRequiredFiles());
		
		$client_de                 =    new ExactTargetSoapClient(Mage::helper('exacttarget')->getWsdl(), array('trace'=>1));
		$client_de->username       =    Mage::helper('exacttarget')->getApiUser($storeId);
		$client_de->password       =    Mage::helper('exacttarget')->getApiPassword($storeId);
		
		$rr = new ExactTarget_RetrieveRequest();
		$rr->ObjectType = 'DataExtension';
		
		//Set the properties to return
		$props = array("ObjectID", "CustomerKey", "Name", "IsSendable", "SendableSubscriberField.Name");
		$rr->Properties = $props;
		
		//Setup account filtering, to look for a given account MID
		$filterPart = new ExactTarget_SimpleFilterPart();
		$filterPart->Property = 'CustomerKey';
		$values = array($customerKey);
		$filterPart->Value = $values;
		$filterPart->SimpleOperator = ExactTarget_SimpleOperators::equals;
		
		//Encode the SOAP package
		$filterPart = new SoapVar($filterPart, SOAP_ENC_OBJECT,'SimpleFilterPart', "http://exacttarget.com/wsdl/partnerAPI");
		
		//Set the filter to NULL to return all MIDs, otherwise set to filter object
		//$rr->Filter = NULL;
		$rr->Filter = $filterPart;
		
		//Setup and execute request
		$rrm = new ExactTarget_RetrieveRequestMsg();
		$rrm->RetrieveRequest = $rr;
		$results = $client_de->Retrieve($rrm);
		
		//Check to see if the data extension already exist in ET
		if(!property_exists($results,'Results')):
		
    		$create_de                 =    new ExactTarget_DataExtension();
    		$create_de->CategoryID     =    $categoryId;
    		$create_de->Name           =    $name;
    		$create_de->CustomerKey    =    $customerKey;
    		
    		$create_de->IsTestable     =    false;
    		$create_de->IsSendable     =    $sendable;
    		$create_de->Description    =    $description;
    		
    		if($sendable && !empty($sendField)):
    			$create_de->SendableDataExtensionField = new ExactTarget_DataExtensionFieldType();
    			$create_de->SendableDataExtensionField->FieldType    =    ExactTarget_DataExtensionFieldType::EmailAddress;
    			$create_de->SendableDataExtensionField->Name         =    $sendField;
    			
    			$create_de->SendableSubscriberField = new ExactTarget_Attribute();
    			$create_de->SendableSubscriberField->Name = "Email Address";
    			$create_de->SendableSubscriberField->Name = Mage::getStoreConfig('et_dataextensions/subscriberField/name');
    		endif;
    		
    		$tempArray							=	array();
    		foreach($fieldArray as $fieldList):
    			foreach($fieldList as $field):
    				$genericObj						=	new ExactTarget_DataExtensionField();
    				$genericObj->Name				=	$field['name'];
    				$genericObj->FieldType			=	$field['type'];
    				$genericObj->IsPrimaryKey		=	$field['primary'];
    				$genericObj->IsRequired			=	$field['required'];
    				
    				if(array_key_exists('max_length', $field) && !empty($field['max_length'])):
    					$genericObj->MaxLength        	=    $field['max_length'];
    					$genericObj->MaxLengthSpecified	=    $field['max_length_specified'];
    				endif;
    				
    				if(array_key_exists('default',$field)):
    					$genericObj->DefaultValue		=	$field['default'];
    				endif;
    				
    				if(array_key_exists('precision',$field)):
    					$genericObj->Precision			=	$field['precision'];
    				endif;
    				
    				if(array_key_exists('scale',$field)):
    					$genericObj->Scale				=	$field['scale'];
    				endif;
    				
    				
    				array_push($tempArray,$genericObj);
    			endforeach;
    		endforeach;
    		
    		$create_de->Fields		  =	$tempArray;
    		
    		$de_object                =    new SoapVar($create_de,SOAP_ENC_OBJECT,'DataExtension','http://exacttarget.com/wsdl/partnerAPI');
    		
    		$create_request           =    new ExactTarget_CreateRequest();
    		$create_request->Options  =    null;
    		$create_request->Objects  =    array($de_object);
    		
    		$results                  =    $client_de->Create($create_request);
    		
    		if(Mage::getStoreConfig('exacttarget/setup/debug') == 1):
    			$logFile			=	'exacttarget_dataextension.log';
    			$isApiLogRequest	=	true;
    			$wrapperText		=	'Data Extension Creation Debug';
    			Mage::helper('exacttarget/debug')->logDebugText($client_de,$logFile,$isApiLogRequest,$wrapperText);
    		endif;
		
		endif;
	}
}