<?php
class Pd_Initialsetup_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getExtensionInformation()
	{
		try{
			require_once(Mage::helper('exacttarget')->getRequiredFiles());
			
			$extensionArray		=	array();
			
			$i = 0;
			$extensionArray[$i]['name']					=	'Customer';
			$extensionArray[$i]['customerKey']			=	'Customer';
			$extensionArray[$i]['description']			=	'Customer Information';
			$extensionArray[$i]['sendable']				=	true;
			$extensionArray[$i]['sendableField']		=	'email';
			
			$customerFields								=	array(
						'fields' => array(
								array('name'=>'email','type'=>ExactTarget_DataExtensionFieldType::EmailAddress,'primary'=>true,'required'=>true),
								array('name'=>'date_created','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
								array('name'=>'date_modified','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
								array('name'=>'customer_id','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>false,'required'=>false),
								array('name'=>'firstname','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
								array('name'=>'middlename','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
								array('name'=>'lastname','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
								array('name'=>'gender','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
								array('name'=>'dob','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
								array('name'=>'join_date','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
								array('name'=>'prefix','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
								array('name'=>'suffix','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
						        array('name'=>'lifetime_sales','type'=>ExactTarget_DataExtensionFieldType::Decimal,'primary'=>false,'required'=>false,'default'=>'0.0000','scale'=>'4','precision'=>'18'),
						        array('name'=>'avg_sales','type'=>ExactTarget_DataExtensionFieldType::Decimal,'primary'=>false,'required'=>false,'default'=>'0.0000','scale'=>'4','precision'=>'18'),
						        array('name'=>'num_orders','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>false,'required'=>false),
						        
						        array('name'=>'billing_address_1','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'billing_address_2','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'billing_company','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'billing_state','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'billing_country','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'billing_postcode','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'billing_phone','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'shipping_address_1','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'shipping_address_2','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'shipping_company','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'shipping_state','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'shipping_country','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'shipping_postcode','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        array('name'=>'shipping_phone','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						        
								array('name'=>'store_id','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
								array('name'=>'store_name','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
								array('name'=>'website_id','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
								array('name'=>'website_name','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
						)				
					);
			$extensionArray[$i]['fields']				=	$customerFields;
			$i++;
			
			
			
			$extensionArray[$i]['name']					=	'Order Header';
			$extensionArray[$i]['customerKey']			=	'Order_Header';
			$extensionArray[$i]['description']			=	'Top Level Order Information';
			$extensionArray[$i]['sendable']				=	true;
			$extensionArray[$i]['sendableField']		=	'magento_customer_email';
			
			$orderHeaderFields								=	array(
					'fields' => array(
							array('name'=>'magento_order_number','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>true,'required'=>true),
							array('name'=>'magento_customer_email','type'=>ExactTarget_DataExtensionFieldType::EmailAddress,'primary'=>false,'required'=>false),
							array('name'=>'date_created','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
							array('name'=>'date_modified','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
							array('name'=>'order_number','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
							array('name'=>'order_status','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
							array('name'=>'magento_customer_id','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
							array('name'=>'order_subtotal','type'=>ExactTarget_DataExtensionFieldType::Decimal,'primary'=>false,'required'=>false,'default'=>'0.0000','scale'=>'4','precision'=>'18'),
							array('name'=>'order_sh_total','type'=>ExactTarget_DataExtensionFieldType::Decimal,'primary'=>false,'required'=>false,'default'=>'0.0000','scale'=>'4','precision'=>'18'),
							array('name'=>'order_grand_total','type'=>ExactTarget_DataExtensionFieldType::Decimal,'primary'=>false,'required'=>false,'default'=>'0.0000','scale'=>'4','precision'=>'18'),
							array('name'=>'purchase_date','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
					)
			);
			$extensionArray[$i]['fields']				=	$orderHeaderFields;
			$i++;
			
			$extensionArray[$i]['name']					=	'Order Address';
			$extensionArray[$i]['customerKey']			=	'Order_Address';
			$extensionArray[$i]['description']			=	'Billing and Shipping Address information';
			$extensionArray[$i]['sendable']				=	true;
			$extensionArray[$i]['sendableField']		=	'customer_email';
			
			$orderAddressFields								=	array(
					'fields' => array(
							array('name'=>'row_number','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>true,'required'=>true),
							array('name'=>'date_created','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
							array('name'=>'date_modified','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
							array('name'=>'magento_order_number','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>false,'required'=>false),
							array('name'=>'order_number','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
							array('name'=>'customer_email','type'=>ExactTarget_DataExtensionFieldType::EmailAddress,'primary'=>false,'required'=>false),
							array('name'=>'address_type','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
							array('name'=>'address_fname','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
							array('name'=>'address_lname','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
							array('name'=>'address_company','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
							array('name'=>'address_street','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'500','max_length_specified'=>true),
							array('name'=>'address_city','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
							array('name'=>'address_region','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
							array('name'=>'address_country','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'255','max_length_specified'=>true),
							array('name'=>'address_postcode','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
					)
			);
			$extensionArray[$i]['fields']				=	$orderAddressFields;
			$i++;
			
			$extensionArray[$i]['name']					=	'Order Items';
			$extensionArray[$i]['customerKey']			=	'Order_Items';
			$extensionArray[$i]['description']			=	'Order Item Detail Information';
			$extensionArray[$i]['sendable']				=	true;
			$extensionArray[$i]['sendableField']		=	'email';
			
			$orderItemFields								=	array(
					'fields' => array(
							array('name'=>'row_number','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>true,'required'=>true),
							array('name'=>'date_created','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
							array('name'=>'date_modified','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
							array('name'=>'magento_order_number','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>false,'required'=>false),
							array('name'=>'order_number','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'50','max_length_specified'=>true),
							array('name'=>'email','type'=>ExactTarget_DataExtensionFieldType::EmailAddress,'primary'=>false,'required'=>false),
							array('name'=>'magento_product_id','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'150','max_length_specified'=>true),
							array('name'=>'magento_sku','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'150','max_length_specified'=>true),
							array('name'=>'magento_product_name','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'150','max_length_specified'=>true),
							array('name'=>'price_each','type'=>ExactTarget_DataExtensionFieldType::Decimal,'primary'=>false,'required'=>false,'default'=>'0.0000','scale'=>4,'precision'=>18),
							array('name'=>'qty_ordered','type'=>ExactTarget_DataExtensionFieldType::Decimal,'primary'=>false,'required'=>false,'default'=>'0.0000','scale'=>4,'precision'=>18),
							array('name'=>'qty_shipped','type'=>ExactTarget_DataExtensionFieldType::Decimal,'primary'=>false,'required'=>false,'default'=>'0.0000','scale'=>4,'precision'=>18),
					        array('name'=>'purchase_date','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
					)
			);
			$extensionArray[$i]['fields']				=	$orderItemFields;
			$i++;
			
			$extensionArray[$i]['name']					=	'Product Base ';
			$extensionArray[$i]['customerKey']			=	'Product_Base';
			$extensionArray[$i]['description']			=	'Product top level information';
			$extensionArray[$i]['sendable']				=	false;
			$extensionArray[$i]['sendableField']		=	'';
			
			$productFields								=	array(
					'fields' => array(
							array('name'=>'product_id','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>true,'required'=>true),
							array('name'=>'date_created','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
							array('name'=>'date_modified','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
							array('name'=>'product_sku','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'150','max_length_specified'=>true),
							array('name'=>'product_name','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'150','max_length_specified'=>true),
							array('name'=>'product_status','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'150','max_length_specified'=>true),
							array('name'=>'product_description','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'','max_length_specified'=>false),
							array('name'=>'product_price','type'=>ExactTarget_DataExtensionFieldType::Decimal,'primary'=>false,'required'=>false,'default'=>'0.0000','scale'=>4,'precision'=>18),
							array('name'=>'product_weight','type'=>ExactTarget_DataExtensionFieldType::Decimal,'primary'=>false,'required'=>false,'default'=>'0.0000','scale'=>4,'precision'=>18),
							array('name'=>'small_image_url','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'2000','max_length_specified'=>true),
							array('name'=>'thumbail_image_url','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'2000','max_length_specified'=>true),
							array('name'=>'image_url','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'2000','max_length_specified'=>true),
							array('name'=>'product_url','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'2000','max_length_specified'=>true),
							array('name'=>'brand','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'2000','max_length_specified'=>true),
							array('name'=>'flavor','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'2000','max_length_specified'=>true),
							array('name'=>'promo_description','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'4000','max_length_specified'=>true),
							array('name'=>'size', 'type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'2000','max_length_specified'=>true),
							array('name'=>'barcode','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'2000','max_length_specified'=>true),
					)
			);
			$extensionArray[$i]['fields']				=	$productFields;
			$i++;
			
			$extensionArray[$i]['name']					=	'Product Relationships';
			$extensionArray[$i]['customerKey']			=	'Product_Relationships';
			$extensionArray[$i]['description']			=	'Product Relationship Information';
			$extensionArray[$i]['sendable']				=	false;
			$extensionArray[$i]['sendableField']		=	'';
			
			$productRelationshipsFields								=	array(
					'fields' => array(
							array('name'=>'product_id','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>true,'required'=>true),
							array('name'=>'related_product_id','type'=>ExactTarget_DataExtensionFieldType::Number,'primary'=>true,'required'=>true),
							array('name'=>'relationship_type','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>true,'required'=>true,'max_length'=>'100','max_length_specified'=>true),
							array('name'=>'date_created','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
							array('name'=>'date_modified','type'=>ExactTarget_DataExtensionFieldType::Date,'primary'=>false,'required'=>false),
							array('name'=>'product_sku','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'150','max_length_specified'=>true),
							array('name'=>'related_product_sku','type'=>ExactTarget_DataExtensionFieldType::Text,'primary'=>false,'required'=>false,'max_length'=>'150','max_length_specified'=>true),
					)
			);
			$extensionArray[$i]['fields']				=	$productRelationshipsFields;
			
			
			return $extensionArray;
		}catch(Exception $e){
			Mage::logException($e);
		}
	}
}