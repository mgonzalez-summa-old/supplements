<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$connection = $installer->getConnection();
/* @var $connection Varien_Db_Adapter_Pdo_Mysql */

$installer->startSetup();


Mage::getModel('core/config')->saveConfig('et_abandoncart/abandonedcarts/dateformat','us','default',0);
//Mage::getModel('core/config')->saveConfig('et_abandoncart/abandonedcarts/dateformat','international','default',0);


//Delete email if it already exist
$condition = array($connection->quoteInto('template_code=?', 'Abandon Cart - 2nd Email - Incentive'));
$connection->delete($installer->getTable('core/email_template'), $condition);

$connection->insert($installer->getTable('core/email_template'), array(
        'template_code'             => 'Abandon Cart - 2nd Email - Incentive',
        'template_text'     		=> '<body style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
										<div style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
										<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
										    <td align="center" valign="top" style="padding:20px 0 20px 0">
										        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #E0E0E0;">
										            <!-- [ header starts here] -->
										            <tr>
										                <td align="left" valign="top"><a href="{{store url=""}}"><img src="{{skin url="images/logo_email.gif" _area=\'frontend\' _package=\'enterprise\' _theme=\'default\'}}" alt="{{var store.getFrontendName()}}"  style="margin-bottom:10px;" border="0"/></a></td>
										            </tr>
										            <!-- [ middle starts here] -->
										            <tr>
										                <td align="left" valign="top">
										                    <h2 style="font-size:16px; font-weight:normal; line-height:22px; margin:0 0 11px 0;"">{{htmlescape var=$quote.getCustomerFirstname()}}, are you still interested in these products?</h2><!--
										                    <p style="font-size:12px; line-height:16px; margin:0;">
										                        Our system has found that you have an abandoned shopping cart with our store.  If you would like to complete your purchase you
										                        may do so by <a href="{{store url="customer/account/"}}" style="color:#1E7EC8;">logging into your account</a>.
										                    </p>
										                    -->
										                    <a href="{{store url=""}}abandoncart/abandoncart/conversion/token/{{var emailToken}}" style="text-decoration:none;fon-size:16px;background-color:#fe1a00;border-radius:6px;border:1px solid #d83526;display:inline-block;color:#ffffff; font-weight:bold;padding:6px 24px;">Buy Now</a>
										                    <!-- <a href="{{store url="customer/account/"}}" style="text-decoration:none;fon-size:16px;background-color:#fe1a00;border-radius:6px;border:1px solid #d83526;display:inline-block;color:#ffffff;  font-weight:bold;padding:6px 24px;">Buy Now</a> -->
										                </td>
										            </tr>
										            <tr>
										                <td>
										                    {{layout handle="abandoncart_email_quote_items" quote=$quote}}
										                </td>
										            </tr>
										            <tr>
										                <td>
										                    Still need another reason to come back and buy these items? How about {{var couponDescription}} with <strong>{{var couponCode}}</strong>?
										                </td>
										            </tr>
										            <tr>
										                <td align="left" valign="top">
										                    <p>If you would like to stop receiving reminder emails, <a href="{{var url}}" style="color:#1E7EC8;">click here to opt out</a></p>
										                </td>
										            </tr>
										            <tr>
										                <td bgcolor="#EAEAEA" align="center" style="background:#EAEAEA; text-align:center;"><center><p style="font-size:12px; margin:0;">Thank you, <strong>{{var store.getFrontendName()}}</strong></p></center></td>
										            </tr>
										        </table>
										    </td>
										</tr>
										</table>
										</div>
										</body>',
        'template_styles'        	=> "body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }",
        'template_type'				=> "2",
        'template_subject'			=>	"Abandon Shopping Cart at {{var store.getFrontendName()}}",
        'modified_at'     			=> now(),
        'orig_template_code'       	=> "abandon_cart_incentive_email_template",
));
$secondAbandonTemplateId = $connection->lastInsertId();

Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_abandoncart_2_template','ExactTarget','default',0);
Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_abandoncart_2_key','Abandon_Cart','default',0);
Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_abandoncart_2_template_file',$secondAbandonTemplateId,'default',0);
Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_abandoncart_2_async','0','default',0);

$condition = array($connection->quoteInto('template_code=?', 'Abandon Cart - 3rd Email - Incentive'));
$connection->delete($installer->getTable('core/email_template'), $condition);

$connection->insert($installer->getTable('core/email_template'), array(
        'template_code'             => 'Abandon Cart - 3rd Email - Incentive',
        'template_text'     		=> '<body style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
										<div style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
										<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
										    <td align="center" valign="top" style="padding:20px 0 20px 0">
										        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #E0E0E0;">
										            <!-- [ header starts here] -->
										            <tr>
										                <td align="left" valign="top"><a href="{{store url=""}}"><img src="{{skin url="images/logo_email.gif" _area=\'frontend\' _package=\'enterprise\' _theme=\'default\'}}" alt="{{var store.getFrontendName()}}"  style="margin-bottom:10px;" border="0"/></a></td>
										            </tr>
										            <!-- [ middle starts here] -->
										            <tr>
										                <td align="left" valign="top">
										                    <h2 style="font-size:16px; font-weight:normal; line-height:22px; margin:0 0 11px 0;"">{{htmlescape var=$quote.getCustomerFirstname()}}, are you still interested in these products?</h2><!--
										                    <p style="font-size:12px; line-height:16px; margin:0;">
										                        Our system has found that you have an abandoned shopping cart with our store.  If you would like to complete your purchase you
										                        may do so by <a href="{{store url="customer/account/"}}" style="color:#1E7EC8;">logging into your account</a>.
										                    </p>
										                    -->
										                    <a href="{{store url=""}}abandoncart/abandoncart/conversion/token/{{var emailToken}}" style="text-decoration:none;fon-size:16px;background-color:#fe1a00;border-radius:6px;border:1px solid #d83526;display:inline-block;color:#ffffff; font-weight:bold;padding:6px 24px;">Buy Now</a>
										                    <!-- <a href="{{store url="customer/account/"}}" style="text-decoration:none;fon-size:16px;background-color:#fe1a00;border-radius:6px;border:1px solid #d83526;display:inline-block;color:#ffffff;  font-weight:bold;padding:6px 24px;">Buy Now</a> -->
										                </td>
										            </tr>
										            <tr>
										                <td>
										                    {{layout handle="abandoncart_email_quote_items" quote=$quote}}
										                </td>
										            </tr>
										            <tr>
										                <td>
										                    Still need another reason to come back and buy these items? How about {{var couponDescription}} with <strong>{{var couponCode}}</strong>?
										                </td>
										            </tr>
										            <tr>
										                <td align="left" valign="top">
										                    <p>If you would like to stop receiving reminder emails, <a href="{{var url}}" style="color:#1E7EC8;">click here to opt out</a></p>
										                </td>
										            </tr>
										            <tr>
										                <td bgcolor="#EAEAEA" align="center" style="background:#EAEAEA; text-align:center;"><center><p style="font-size:12px; margin:0;">Thank you, <strong>{{var store.getFrontendName()}}</strong></p></center></td>
										            </tr>
										        </table>
										    </td>
										</tr>
										</table>
										</div>
										</body>',
        'template_styles'        	=> "body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }",
        'template_type'				=> "2",
        'template_subject'			=>	"Abandon Shopping Cart at {{var store.getFrontendName()}}",
        'modified_at'     			=> now(),
        'orig_template_code'       	=> "abandon_cart_incentive_email_template",
));
$thirdAbandonTemplateId = $connection->lastInsertId();

Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_abandoncart_3_template','ExactTarget','default',0);
Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_abandoncart_3_key','Abandon_Cart','default',0);
Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_abandoncart_3_template_file',$thirdAbandonTemplateId,'default',0);
Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_abandoncart_3_async','0','default',0);

$installer->endSetup();