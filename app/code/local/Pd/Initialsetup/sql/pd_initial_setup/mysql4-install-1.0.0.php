<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$connection = $installer->getConnection();
/* @var $connection Varien_Db_Adapter_Pdo_Mysql */

$installer->startSetup();
$apiUsername					=	'dbrink_supplementwarehouse';
$apiPassword					=	'welcome@1';

//Could be any of these three
$apiWsdl						=	'https://webservice.exacttarget.com/etframework.wsdl';
// $apiWsdl						=	'https://webservice.s4.exacttarget.com/etframework.wsdl';
// $apiWsdl						=	'https://webservice.s6.exacttarget.com/etframework.wsdl';
$etAccount						=	'Advanced';

//For Enterprise 2.0 accounts only
$memberId						=	'10674919';

$fromName						=	'PD Support';
$fromEmail						=	'support@precisiondialogue.com';
$categoryId						=	'1076957'; //The folder id of the folder created for the data extensions

//In PD's case we use Email Address, some customers use Subscriber Key
$fieldName						=	'Email Address';
//$fieldName						=	'Subscriber Key';

$listId							=	'18325922';
$newAccountExtKey				=	'New_Account';
$abandonCartExtKey				=	'Abandon_Cart';
$forgottenPasswordExtKey		=	'Forgotten_Password';
$newsletterSubExternalKey		=	'Newsletter_Subscription_Success';
$newsletterUnsubExternalKey		=	'Newsletter_Unsub';
$newOrderExtKey					=	'New_Order';
$newOrderGuestExtKey			=	'New_Order_Guest';
$newShipmentExtKey				=	'New_Shipment';
$newShipmentGuestExtKey			=	'New_Shipment_Guest';
$orderUpdateExtKey				=	'Order_Update';
$orderUpdateGuestExtKey			=	'Order_Update_Guest';
$shipmentUpdateExtKey			=	'Shipment_Update';
$shipmentUpdateGuestExtKey		=	'Shipment_Update_Guest';


	//Default Settings - applied because they are the same across all stores
	Mage::getModel('core/config')->saveConfig('exacttarget/setup/active', 1,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/setup/username',$apiUsername,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/setup/apipassword',$apiPassword,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/setup/wsdl',$apiWsdl,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/setup/et_account',$etAccount,'default',0);
//	Mage::getModel('core/config')->saveConfig('exacttarget/setup/member_id',$memberId,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/setup/et_from_name',$fromName,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/setup/et_from_email',$fromEmail,'default',0);
	
	//Customer Data Extension
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/active',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/batch_send',0,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/data_extension_name','Customer','default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/customer_id',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/dob',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/email',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/firstname',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/gender',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/lastname',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/middlename',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/prefix',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/suffix',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/join_date',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/lifetime_sales',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/num_orders',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/avg_sales',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/primary_billing',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/customers_de/primary_shipping',1,'default',0);
	
	//Order Data Extensions
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/active',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/address_city',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/address_company',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/address_country',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/address_email',0,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/address_fname',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/address_lname',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/address_postcode',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/address_region',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/address_street',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/batch_send',0,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/customer',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/customer_email',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/data_extension_name','Order_Header','default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/data_extension_order_addresses_name','Order_Address','default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/data_extension_order_lines_name','Order_Items','default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/entity_id',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/grand',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/increment',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/order_status',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/pname',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/price',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/product_id',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/qty_ordered',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/qty_shipped',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/sh_total',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/sku',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/subtotal',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/order_de/purchase_date',1,'default',0);
	
	//Product Data Extensions
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/active',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/batch_send',0,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/data_extension_name','Product_Base','default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/data_extension_related_name','Product_Relationships','default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/parent_product_id',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/parent_product_sku',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/product_descr',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/product_id',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/product_name',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/product_price',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/product_sku',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/product_status',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/product_weight',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/small_image',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/thumbnail',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/image',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/product_url',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/brand',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/flavor',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/promo_description',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/size',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/barcode',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/related_product_id',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/related_product_sku',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/product_de/relationship_type',1,'default',0);
	
	//Delete email if it already exist
	$condition = array($connection->quoteInto('template_code=?', 'Abandon Cart Email'));
	$connection->delete($installer->getTable('core/email_template'), $condition);
	
	$connection->insert($installer->getTable('core/email_template'), array(
			'template_code'             => 'Abandon Cart Email',
			'template_text'     		=> '<body style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
											<div style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
											<table cellspacing="0" cellpadding="0" border="0" width="100%">
											<tr>
											    <td align="center" valign="top" style="padding:20px 0 20px 0">
											        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #E0E0E0;">
											            <!-- [ header starts here] -->
											            <tr>
											                <td align="left" valign="top"><a href="{{store url=""}}"><img src="{{skin url="images/logo_email.gif" _area=\'frontend\' _package=\'enterprise\' _theme=\'default\'}}" alt="{{var store.getFrontendName()}}"  style="margin-bottom:10px;" border="0"/></a></td>
											            </tr>
											            <!-- [ middle starts here] -->
											            <tr>
											                <td align="left" valign="top">
											                    <h2 style="font-size:16px; font-weight:normal; line-height:22px; margin:0 0 11px 0;"">{{htmlescape var=$quote.getCustomerFirstname()}}, are you still interested in these products?</h2><!--
											                    <p style="font-size:12px; line-height:16px; margin:0;">
											                    	Our system has found that you have an abandoned shopping cart with our store.  If you would like to complete your purchase you 
											                    	may do so by <a href="{{store url="customer/account/"}}" style="color:#1E7EC8;">logging into your account</a>.
											                    </p>
											                    -->
											                    <a href="{{store url=""}}abandoncart/abandoncart/conversion/token/{{var emailToken}}" style="text-decoration:none;fon-size:16px;background-color:#fe1a00;border-radius:6px;border:1px solid #d83526;display:inline-block;color:#ffffff;	font-weight:bold;padding:6px 24px;">Buy Now</a>
											                </td>
											            </tr>
											            <tr>
											            	<td>
											            		{{layout handle="abandoncart_email_quote_items" quote=$quote}}
											           		</td>
											         	</tr>
											         	<tr>
											         		<td align="left" valign="top">
											         			<p>If you would like to stop receiving reminder emails, <a href="{{var url}}" style="color:#1E7EC8;">click here to opt out</a></p>
											         		</td>
											         	</tr>
											            <tr>
											                <td bgcolor="#EAEAEA" align="center" style="background:#EAEAEA; text-align:center;"><center><p style="font-size:12px; margin:0;">Thank you, <strong>{{var store.getFrontendName()}}</strong></p></center></td>
											            </tr>
													</table>
												</td>
											</tr>
											</table>
											</div>
											</body>',
			'template_styles'        	=> "body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }",
			'template_type'				=> "2",
			'template_subject'			=>	"Abandon Shopping Cart at {{var store.getFrontendName()}}",
			'modified_at'     			=> now(),
			'orig_template_code'       	=> "abandon_cart_email_template",
	));
	$abandonTemplateId = $connection->lastInsertId();
	
	//Default Email Templates
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newsletterunsubscriptionsuccess','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newsletterunsubscriptionsuccess_key',$newsletterUnsubExternalKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newsletterunsubscriptionsuccess_template','exacttarget_emails_email_newsletterunsubscriptionsuccess_template','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newsletterunsubscriptionsuccess_async','0','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_orderupdateguest','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_orderupdateguest_key',$orderUpdateGuestExtKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_orderupdateguest_template','exacttarget_emails_email_orderupdateguest_template','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_orderupdateguest_async','0','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_orderupdate','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_orderupdate_key',$orderUpdateExtKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_orderupdate_template','exacttarget_emails_email_orderguest_template','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_orderupdate_async','0','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_shipmentupdate','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_shipmentupdate_key',$shipmentUpdateExtKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_shipmentupdate_template','exacttarget_emails_email_shipmentupdate_template','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_shipmentupdate_async','0','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_shipmentupdateguest','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_shipmentupdateguest_key',$shipmentUpdateGuestExtKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_shipmentupdateguest_template','exacttarget_emails_email_shipmentupdateguest_template','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_shipmentupdateguest_async','0','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_abandoncart_template','ExactTarget','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_abandoncart_key',$abandonCartExtKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_abandoncart_template_file',$abandonTemplateId,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_abandoncart_async','0','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newaccount','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newaccount_key',$newAccountExtKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newaccount_template','exacttarget_emails_email_newaccount_template','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newaccount_async','0','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_accountreset_password','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_accountreset_password_key',$forgottenPasswordExtKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_accountreset_password_template','exacttarget_emails_email_accountreset_password_template','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_accountreset_password_async','0','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_neworder','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_neworder_key',$newOrderExtKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_neworder_template','exacttarget_emails_email_neworder_template','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_neworder_async','0','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_neworderguest','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_neworderguest_key',$newOrderGuestExtKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_neworderguest_template','exacttarget_emails_email_neworderguest_template','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_neworderguest_async','0','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newshipment','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newshipment_key',$newShipmentExtKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newshipment_template','exacttarget_emails_email_newshipment_template','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newshipment_async','0','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newshipmentguest','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newshipmentguest_key',$newShipmentGuestExtKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newshipmentguest_template','exacttarget_emails_email_newshipmentguest_template','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newshipmentguest_async','0','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newsletter','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newsletter_listid',$listId,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newslettersubscriptionsuccess','Magento','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newslettersubscriptionsuccess_key',$newsletterSubExternalKey,'default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newslettersubscriptionsuccess_template','exacttarget_emails_email_newslettersubscriptionsuccess_template','default',0);
	Mage::getModel('core/config')->saveConfig('exacttarget/emails/email_newslettersubscriptionsuccess_async','0','default',0);
	


	//Default setting that will allow for the auto creation of the basic data extensions in ExactTarget once all the proper information has been supplied
	//These config settings are not manually modifiable by a user - set/updated by extension only
	Mage::getModel('core/config')->saveConfig('exacttarget/dataextensions/create',1,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/folder/categoryId',$categoryId,'default',0);
	Mage::getModel('core/config')->saveConfig('et_dataextensions/subscriberField/name',$fieldName,'default',0);
	
	Mage::getModel('core/config')->saveConfig('newsletter/attribute/firstname','First Name','default',0);
	Mage::getModel('core/config')->saveConfig('newsletter/attribute/lastname','Last Name','default',0);
	Mage::getModel('core/config')->saveConfig('newsletter/attribute/gender','Gender','default',0);
	Mage::getModel('core/config')->saveConfig('newsletter/attribute/address','Address 1','default',0);
	Mage::getModel('core/config')->saveConfig('newsletter/attribute/address_2','Address 2','default',0);
	Mage::getModel('core/config')->saveConfig('newsletter/attribute/city','City','default',0);
	Mage::getModel('core/config')->saveConfig('newsletter/attribute/state','Region','default',0);
	Mage::getModel('core/config')->saveConfig('newsletter/attribute/zipcode','Postcode','default',0);
	Mage::getModel('core/config')->saveConfig('newsletter/attribute/phone_number','Phone Number','default',0);
	Mage::getModel('core/config')->saveConfig('newsletter/attribute/country','Country','default',0);
	Mage::getModel('core/config')->saveConfig('newsletter/attribute/dob','DOB','default',0);
	
	
$installer->endSetup();