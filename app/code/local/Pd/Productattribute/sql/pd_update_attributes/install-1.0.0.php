<?php
/**
 * The flavor and brand attributes are too large to update via the admin panel - must be done programmatically
 */

$installer = $this;

$installer->startSetup();

//Updating the flavor attribute to be searchable.
$installer->run("
        UPDATE catalog_eav_attribute SET is_searchable = 1 WHERE attribute_id = 257; 
        ");

$installer->endSetup();