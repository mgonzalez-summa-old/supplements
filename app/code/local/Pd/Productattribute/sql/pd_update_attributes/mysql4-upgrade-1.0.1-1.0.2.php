<?php
/**
 * The flavor and brand attributes are too large to update via the admin panel - must be done programmatically
 */

$installer = $this;

$installer->startSetup();


$installer->run("
        CREATE OR REPLACE VIEW pitney_bowes_order_address AS
SELECT 
    sales_flat_order.increment_id as pitney_bowes_order_number,
    sales_flat_order_address.firstname pitney_bowes_firstname, 
    sales_flat_order_address.lastname pitney_bowes_lastname, 
    sales_flat_order_address.company pitney_bowes_company,
    SUBSTRING_INDEX(sales_flat_order_address.street,'\n',1) as pitney_bowes_address_1,
    IF (LOCATE('\n',sales_flat_order_address.street), SUBSTRING_INDEX(sales_flat_order_address.street,'\n',-1),'' ) as pitney_bowes_address_2,
    sales_flat_order_address.city as pitney_bowes_city,
    directory_country_region.code as pitney_bowes_state, 
    sales_flat_order_address.postcode as pitney_bowes_zip,
    sales_flat_order_address.country_id pitney_bowes_country, 
    sales_flat_order_address.email pitney_bowes_customer_email, 	 
    sales_flat_order_address.telephone as pitney_bowes_customer_phone,
    eav_attribute_option_value.`value` as pitney_bowes_address_type
FROM sales_flat_order_address 
INNER JOIN sales_flat_order ON sales_flat_order_address.parent_id = sales_flat_order.entity_id 
INNER JOIN directory_country_region ON directory_country_region.region_id = sales_flat_order_address.region_id 
LEFT JOIN eav_attribute_option_value on eav_attribute_option_value.option_id = sales_flat_order_address.addr_type 
WHERE address_type = 'shipping'  AND status = 'ready_for_ship'
        ");

$installer->endSetup();