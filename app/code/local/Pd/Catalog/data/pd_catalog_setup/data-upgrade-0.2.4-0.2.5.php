<?php
/**
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */
$installer = $this;

$installer->startSetup();

$stores = Mage::app()->getStores();

/**
 * @var $store Mage_Core_Model_Store
 */

foreach ($stores as $store) {
    $category = Mage::getModel('catalog/category')->load($store->getRootCategoryId());
    if ($category->getId()) {
        $category->setIsAnchor(true)
                 ->save();
    }
}

$installer->endSetup();