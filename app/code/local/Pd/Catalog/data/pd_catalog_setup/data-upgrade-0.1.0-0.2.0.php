<?php
/**
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */
$installer = $this;

$installer->startSetup();

$attributeCode = 'hot_deal';
$attributeModel = Mage::getModel('eav/entity_attribute');
$attributeOptionsModel= Mage::getModel('eav/entity_attribute_source_table') ;

$attributeId = $attributeModel->getIdByCode('catalog_product', $attributeCode);
$attribute = $attributeModel->load($attributeId);

$attributeTable = $attributeOptionsModel->setAttribute($attribute);
$options = $attributeOptionsModel->getAllOptions(false);

$value['option_1'] = array("Hot Deal!");
$value['option_2'] = array("Top Seller!");
$result = array('value' => $value);
$attribute->setData('option',$result);
$attribute->save();

$installer->endSetup();