<?php
/**
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */
$installer = $this;

$installer->startSetup();

$installer->updateAttribute('catalog_product', 'hot_deal', 'is_filterable', 0);

$installer->endSetup();