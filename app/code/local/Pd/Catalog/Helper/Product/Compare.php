<?php
class Pd_Catalog_Helper_Product_Compare
    extends Mage_Catalog_Helper_Product_Compare
{
    public function isInCompareList($productId)
    {
        $ids = $this->getItemCollection()->getProductIds();
        if($ids && in_array($productId, $ids)) {
            return true;
        }
        return false;
    }
}
