<?php

class Pd_Catalog_Helper_Catalog_Product
    extends Mage_Catalog_Helper_Product
{
        public function getSupplierUrl($product)
        {
            $url = Mage::helper('adminhtml')
                ->getUrl('AdvancedStock/Products/Edit', array('product_id' => $product->getId(), 'tab' => 'tab_suppliers'));
            return $url;
        }

        public function getProductSupplierLinkHtml($product)
        {
            if (!empty($product) && $product->getId()) {
                $label = '<div style="margin-top: 20px">'
                    . 'To edit supplier skus please <a href="'
                    . $this->getSupplierUrl($product)
                    . '" target="_blank">go here</a>.</div>';
                return $label;
            }
        }
}