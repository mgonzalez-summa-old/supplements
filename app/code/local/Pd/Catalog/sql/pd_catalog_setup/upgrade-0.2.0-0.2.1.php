<?php
/**
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */
$installer = $this;

$installer->startSetup();

$configData = new Mage_Core_Model_Config();
/*
* set default sort by as name
*/
$configData ->saveConfig('catalog/frontend/default_sort_by', "name", 'default', 0);

$installer->endSetup();
