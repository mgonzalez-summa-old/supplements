<?php
/**
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */
$installer = $this;

$installer->startSetup();

$model = Mage::getModel('catalog/resource_eav_attribute');
$model->loadByCode(4, 'price');
if ($model) {
    $model->setIsFilterable(1)
        ->setIsFilterableInSearch(1)
        ->save();
}

$installer->endSetup();

