<?php

class Pd_Catalog_Block_Product_Compare_List extends Mage_Catalog_Block_Product_Compare_List
{

    protected $_productsInCart = null;

    public function initPriceMatchProducts()
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();

        if ($cartItems = $quote->getAllVisibleItems()) {
            foreach ($cartItems as $item) {
                $this->_productsInCart[$item->getProductId()] = new Varien_Object( array(
                    'item' => $item,
                ));
            }
        }
    }

    public function isPriceMatched($product)
    {
        if(isset($this->_productsInCart[$product->getId()])) {
            $item = $this->_productsInCart[$product->getId()]['item'];
            if ($item->getPrice() != $product->getFinalPrice()) {
                $product->setPrice($item->getPrice());
                $product->setFinalPrice($item->getPrice());
                $product->setIsPriceMatched(true);
                return $product;
            }
        }
        return $product;
    }

    public function isInCart($product)
    {
        if(isset($this->_productsInCart[$product->getId()])) {
            return true;
        }
        return false;
    }
}
