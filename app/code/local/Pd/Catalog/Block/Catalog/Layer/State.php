<?php

class Pd_Catalog_Block_Catalog_Layer_State
    extends Mage_Catalog_Block_Layer_State
{
    public function getActiveFiltersToShow()
    {
        $filters = $this->getActiveFilters();
        if (Mage::registry('is_brand_category')) {
            foreach ($filters as $key => $filter) {
                if ('Brand' == $filter->getName()) {
                    unset($filters[$key]);
                }
            }
        }
        return $filters;
    }
}