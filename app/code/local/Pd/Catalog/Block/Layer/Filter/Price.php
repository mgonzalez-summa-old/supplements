<?php
class Pd_Catalog_Block_Layer_Filter_Price extends Mage_Catalog_Block_Layer_Filter_Price
{
    public function getFilterLabel($alias, $status = true)
    {
        $_items = $this->getItems();
        if(count($_items) > 7 ) {
            $limit = explode('-',$_items[6]->getValue());
            $limit = $this->helper('core')->currency($limit[1], true, false);
            if(!$status) {
                return $this->__('Less than') . ' ' . $limit;
            } else {
                return $this->__('More than') . ' ' . $limit;
            }
        }

        return '';
    }
}
