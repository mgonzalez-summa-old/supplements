<?php
abstract class Pd_Catalog_Block_Layer_Filter_Abstract
    extends Mage_Catalog_Block_Layer_Filter_Abstract
{

    public function getFilterAlias()
    {
        return $this->_alias;
    }
}
