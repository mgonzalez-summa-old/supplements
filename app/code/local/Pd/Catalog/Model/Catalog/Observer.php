<?php

class Pd_Catalog_Model_Catalog_Observer
{
    public function updateProductVendorSku(Varien_Event_Observer $observer)
    {
        if (!Mage::registry('supplier_update')) {
            Mage::register('supplier_update', true);
            $productSupplier = $observer->getDataObject();
            if (!empty($productSupplier)) {
                $product  = Mage::getModel('catalog/product')->load($productSupplier->getPpsProductId());
                $supplier = Mage::getModel('Purchase/supplier')->load($productSupplier->getPpsSupplierNum());
                if ($product->getId() && $supplier->getSupId()) {
                    $product->setData($supplier->getSupName(), $productSupplier->getPpsReference());
                    $product->save();
                }
            }
        }
    }
}