<?php
require_once 'Redstage/Brand/controllers/IndexController.php';

class Pd_Catalog_IndexController
    extends Redstage_Brand_IndexController
{

    public function viewAction()
    {
        $this->_forward('bnd', 'category', 'catalog');
    }
}