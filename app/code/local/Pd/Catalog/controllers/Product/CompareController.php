<?php
require_once('Mage/Catalog/controllers/Product/CompareController.php');
class Pd_Catalog_Product_CompareController extends Mage_Catalog_Product_CompareController
{
    /**
     * Add item to compare list
     */
    public function addAction()
    {
        
        //$compareList = Mage::getSingleton('catalog/product_compare_list')->getItemCollection();
        
        
        $compareItemCollection = Mage::getResourceModel('catalog/product_compare_item_collection')
                ->useProductItem(true)
                ->setStoreId(Mage::app()->getStore()->getId());

            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $compareItemCollection->setCustomerId(Mage::getSingleton('customer/session')->getCustomerId());
            } else {
                $compareItemCollection->setVisitorId(Mage::getSingleton('log/visitor')->getId());
            }

            $compareItemCollection
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->loadComparableAttributes()
                ->addMinimalPrice()
                ->addTaxPercents();

            Mage::getSingleton('catalog/product_visibility')
                ->addVisibleInSiteFilterToCollection($compareItemCollection);
        
        
        $count = $compareItemCollection->count();
        
        if ($count < 3) {
            return parent::addAction();
        } else {
            Mage::getSingleton('catalog/session')->addError(
                    $this->__('There are already three products in the comparison list. The product has not been added to comparison list.')
                );
        }

        return $this->_redirectReferer();
    }
}