<?php

require_once 'Mage/Catalog/controllers/CategoryController.php';

class Pd_Catalog_CategoryController
    extends Mage_Catalog_CategoryController
{

    public function bndAction()
    {
        $brand = $this->getRequest()->getParam('id');
        $brand = Mage::getModel('brand/brand')->load($brand);
        $this->getRequest()->setParam('brand', $brand->getManufacturerId());
        $this->getRequest()->setParam('id', Mage::app()->getStore()->getRootCategoryId());
        if (empty($brand)) {
            $this->getResponse()->setRedirect('/brands');
        } else {
            Mage::register('is_brand_category', true);
            $this->viewAction();
        }
    }

    protected function _initCatagory()
    {
        Mage::dispatchEvent('catalog_controller_category_init_before', array('controller_action' => $this));
        $categoryId = (int) $this->getRequest()->getParam('id', false);
        if (!$categoryId) {
            return false;
        }

        $category = Mage::getModel('catalog/category')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($categoryId);

        if (!$categoryId == Mage::app()->getStore()->getRootCategoryId()
            && !Mage::helper('catalog/category')->canShow($category)) {
            return false;
        }

        Mage::getSingleton('catalog/session')->setLastVisitedCategoryId($category->getId());
        Mage::register('current_category', $category);
        Mage::register('current_entity_key', $category->getPath());

        try {
            Mage::dispatchEvent(
                'catalog_controller_category_init_after',
                array(
                    'category'          => $category,
                    'controller_action' => $this
                )
            );
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            return false;
        }
        return $category;
    }

}