<?php
class Pd_ImportExport_Model_ImportExport_Entity_Customer
    extends Mage_ImportExport_Model_Import_Entity_Customer
{
    /**
     * Initialize existent customers data.
     *
     * @return Mage_ImportExport_Model_Import_Entity_Customer
     */
    protected function _initCustomers()
    {

        /**
         * Get the resource model
         */
        $resource = Mage::getSingleton('core/resource');

        /**
         * Retrieve the read connection
         */
        $readConnection = $resource->getConnection('core_read');

        $query = 'SELECT email, entity_id, website_id FROM ' . $resource->getTableName('customer/entity');

        /**
         * Execute the query and store the results in $results
         */
        $results = $readConnection->fetchAll($query);

        foreach ($results as $customer) {
            $email = $customer['email'];

            if (!isset($this->_oldCustomers[$email])) {
                $this->_oldCustomers[$email] = array();
            }
            $this->_oldCustomers[$email][$this->_websiteIdToCode[$customer['website_id']]] = $customer['entity_id'] ;
        }
        $this->_customerGlobal = Mage::getModel('customer/customer')->getSharingConfig()->isGlobalScope();

        return $this;
    }
}