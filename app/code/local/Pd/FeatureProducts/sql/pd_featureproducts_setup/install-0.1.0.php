<?php
/**
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */
$installer = $this;

$installer->startSetup();

$entityTypeId     = $installer->getEntityTypeId('catalog_product');
$attributeSetName = 'Default';
$attributeSetId   = $installer->getAttributeSetId($entityTypeId, $attributeSetName);
$groupId          = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$attributeCode = 'featured_brand';
$installer->addAttribute($entityTypeId, $attributeCode, array(
    'type' => 'int',
    'label' => 'Featured Brand',
    'input' => 'select',
    'source' => 'eav/entity_attribute_source_boolean',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default'           => '0',
    'searchable' => false,
    'filterable' => true,
    'comparable' => false,
    'visible_on_front' => false,
    'visible_in_advanced_search' => true,
    'used_in_product_listing' => true,
    'unique' => false,
));
$installer->addAttributeToSet($entityTypeId, $attributeSetId, $groupId, $attributeCode, 30);

$installer->endSetup();
