<?php
class Pd_FeatureProducts_Block_Product_Brand_List extends Mage_Catalog_Block_Product_Abstract
{
    protected $_productCollection;
    protected $_type = 'id';
    const XML_PATH_FEATURED_PRODUCTS_LIMIT = 'catalog/frontend/featured_products';

    /*
      * Load featured products collection
      * */
    protected function _getProductCollection()
    {
        $collection_limit = Mage::getStoreConfig(self::XML_PATH_FEATURED_PRODUCTS_LIMIT,Mage::app()->getStore()->getId());
        if(empty($collection_limit)){
            $collection_limit = 3;
        }
        $brandId = Mage::app()->getRequest()->getParam($this->_type);
        $model = Mage::getSingleton('brand/brand');
        if (!is_null($brandId) && $brandId !== $model->getId()) {
            if($this->_type == 'id'){
                if (!$model->load($brandId)) {
                    return null;
                }
            } else {
                if (!$model->load($brandId, 'manufacturer_id')) {
                    return null;
                }
            }
        }
        if (!$model->getId()) {
            return false;
        }
        if (is_null($this->_productCollection)) {
            $collection = Mage::getModel('catalog/product')->getCollection();

            $attributes = Mage::getSingleton('catalog/config')
                ->getProductAttributes();

            $collection->addAttributeToSelect($attributes)
                ->addAttributeToFilter('featured_brand', 1)
                ->addAttributeToFilter('brand', $model->getManufacturerId())
                ->setPageSize($collection_limit);

            $this->_productCollection = $collection;
        }
        return $this->_productCollection;
    }

    /**
     * Retrieve loaded featured products collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getProductCollection()
    {
        return $this->_getProductCollection();
    }


    /**
     * Get HTML if there's anything to show
     */
    protected function _toHtml()
    {
        if ($this->_getProductCollection()->count()) {
            return parent::_toHtml();
        }
        return '';
    }


    public function setTypeId($type)
    {
        if($type) {
            $this->_type = $type;
        }
    }

}
