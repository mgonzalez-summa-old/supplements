<?php
class Pd_FeatureProducts_Block_Product_Category_List extends Mage_Catalog_Block_Product_Abstract
{
    protected $_productCollection;
    protected $_type = 'cat';
    const XML_PATH_FEATURED_PRODUCTS_LIMIT = 'catalog/frontend/featured_products';


    /*
      * Load featured products collection
      * */
    protected function _getProductCollection()
    {

        $collection_limit = Mage::getStoreConfig(self::XML_PATH_FEATURED_PRODUCTS_LIMIT,Mage::app()->getStore()->getId());
        if(empty($collection_limit)){
            $collection_limit = 3;
        }

        $categoryId = Mage::app()->getRequest()->getParam('cat');
        if (empty($categoryId)) {
            $categoryId = Mage::app()->getRequest()->getParam('id');
        }
        $brand = Mage::app()->getRequest()->getParam('brand');
        if(isset($brand)){
            //we're in a category page with a brand selected, we need to show the brand featured products only so we go ... bye!
            return false;
        }
        $category = Mage::getSingleton('catalog/category');
        if (!is_null($categoryId) && $categoryId !== $category->getId()) {
            $category->load($categoryId);
        }
        if (!$category->getId()) {
            return false;
        }
        if (is_null($this->_productCollection)) {
            $attributes = Mage::getSingleton('catalog/config')
                ->getProductAttributes();

            $category = Mage::getModel('catalog/category')->load($categoryId);
            /**
             * @var $product_collection Mage_Catalog_Model_Resource_Product_Collection
             */
            $product_collection = $category->getProductCollection();
            $store = Mage::app()->getStore();
            $product_collection->addAttributeToSelect($attributes)
                ->addStoreFilter($store->getStoreId())
                ->joinField('position', 'catalog/category_product', 'position', 'product_id = entity_id', '{{table}}.category_id = '.$categoryId, 'joinLeft')
                ->addFieldToFilter('position', array('eq'=>'999'))//this is a hack we use to set a product as featured in a category
                ->setPageSize($collection_limit);
            $this->_productCollection = $product_collection;
        }
        return $this->_productCollection;
    }

    /**
     * Retrieve loaded featured products collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getProductCollection()
    {
        return $this->_getProductCollection();
    }


    /**
     * Get HTML if there's anything to show
     */
    protected function _toHtml()
    {
        if ($this->_getProductCollection() && $this->_getProductCollection()->count()) {
            return parent::_toHtml();
        }
        return '';
    }


    public function setTypeId($type)
    {
        if($type) {
            $this->_type = $type;
        }
    }
}
