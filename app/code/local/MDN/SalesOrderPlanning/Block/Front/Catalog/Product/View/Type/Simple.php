<?php

class MDN_SalesOrderPlanning_Block_Front_Catalog_Product_View_Type_Simple extends Mage_Catalog_Block_Product_View_Type_Simple
{
    private $_productAvailabilityStatus = null;

    public function getProductAvailabilityStatus()
    {
        if ($this->_productAvailabilityStatus == null) {
            $productId = $this->getProduct()->getId();
            $ProductAvailabilityStatus = mage::getModel('SalesOrderPlanning/ProductAvailabilityStatus')->load(
                $productId,
                'pa_product_id'
            );
            $this->_productAvailabilityStatus = $ProductAvailabilityStatus;
            $frontEndStockAvailabilityFlag = $ProductAvailabilityStatus->getFrontendStockAvailabilityFlag();
            if (!is_null($frontEndStockAvailabilityFlag) && $frontEndStockAvailabilityFlag == false) {
                $this->_productAvailabilityStatus = null;
            }
        }
        return $this->_productAvailabilityStatus;
    }

}
