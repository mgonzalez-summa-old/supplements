<?php

class MDN_AdvancedStock_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
	
	/**
	 * Add After Column
	 *
	 * @param unknown_type $columnId
	 * @param unknown_type $column
	 * @param unknown_type $indexColumn
	 * @return unknown
	 */
	public function addAfterColumn($columnId, $column,$indexColumn) {
		$columns = array();
		foreach ($this->_columns as $gridColumnKey => $gridColumn) {
			$columns[$gridColumnKey] = $gridColumn;
			if($gridColumnKey == $indexColumn) {
				$columns[$columnId] = $this->getLayout()->createBlock('adminhtml/widget_grid_column')
		                ->setData($column)
		                ->setGrid($this);
		        $columns[$columnId]->setId($columnId);         
			}
		}
		$this->_columns = $columns;
        return $this;
	}
	
	/**
	 * Add Before column
	 *
	 * @param unknown_type $columnId
	 * @param unknown_type $column
	 * @param unknown_type $indexColumn
	 * @return unknown
	 */
	public function addBeforeColumn($columnId, $column,$indexColumn) {
		$columns = array();
		foreach ($this->_columns as $gridColumnKey => $gridColumn) {
			if($gridColumnKey == $indexColumn) {
				$columns[$columnId] = $this->getLayout()->createBlock('adminhtml/widget_grid_column')
		                ->setData($column)
		                ->setGrid($this);
		        $columns[$columnId]->setId($columnId);         
			}
			$columns[$gridColumnKey] = $gridColumn;
		}
		$this->_columns = $columns;
        return $this;
	}
		
	/**
	 * Manage columns
	 *
	 */
	protected function _prepareColumns()
    {
		parent::_prepareColumns();
        
        //raise event to allow other modules to add columns
        Mage::dispatchEvent('salesorder_grid_preparecolumns', array('grid'=>$this));
        
    }

    /**
     * Manage mass actions
     * 
     * @return unknown
     */
    protected function _prepareMassaction()
    {
    	parent::_prepareMassaction();
    	
    	$this->addColumn('status', array(
    	        'header' => Mage::helper('sales')->__('Status'),
    	        'index' => 'status',
    	        'type'  => 'options',
    	        'width' => '70px',
    	        'options' => Mage::getModel('amoaction/command_status')->filterStatusResults(Mage::getSingleton('sales/order_config')->getStatuses()),
    	));

        $this->getMassactionBlock()->setUseSelectAll(true);

        //raise event to allow other extension to add columns
        Mage::dispatchEvent('salesorder_grid_preparemassaction', array('grid'=>$this));
        
        return $this;
    }

}

?>