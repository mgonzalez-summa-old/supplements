<?php
class Redstage_Brand_Model_Brand extends Mage_Core_Model_Abstract

{

    protected $_collection = null;

    protected $_optionCollection = null;

    protected static $_url = null;

    

    public function _construct()

    {

        parent::_construct();

        $this->_init('brand/brand');

    }

    

    protected function _afterLoad()

    {

        parent::_afterLoad();

        if ('' == $this->getData('root_template'))

            $this->setData('root_template', 'two_columns_left');

    }

    

    public function getManufacturerName($manufacturerId)

    {

        return $this->getResource()->getAttributeOptionValue($manufacturerId);

    }

    

    public function loadByManufacturer($manufacturerId)

    {

        $storeId = Mage::app()->getStore()->getId();

        return $this->getCollection()->addStoreFilter($storeId)

            ->addFieldToFilter('main_table.manufacturer_id', array("eq"=>$manufacturerId))->getFirstItem();

    }

    

    public function toManufacturersOptionsArray($storeId = null)

    {

        return $this->getCollection()->toManufacturersOptionsArray($storeId);

    }

    

    public function checkUrlKey($urlKey, $storeId)

    {

        return $this->_getResource()->checkUrlKey($urlKey, $storeId);

    }

    

    public function isUniqueUrlKey($urlKey, $id = 0, $storeId = null)

    {

        if (is_null($storeId)){

            $storeId = Mage::app()->getStore()->getId();

        }

        $id = $this->_getResource()->checkUniqueUrlKey($urlKey, $id, $storeId);

        return (bool)empty($id);

    }

    

    public function getUrl()

    {

        if ($this->getId())

        {

            $storeId = Mage::app()->getStore()->getId();

            $rewriteModel = Mage::getModel('core/url_rewrite');

            $rewriteCollection = $rewriteModel->getCollection();

            $rewriteCollection->addStoreFilter($storeId, true)

                              ->setOrder('store_id', 'DESC')

                              ->addFieldToFilter('target_path', 'brands/index/view/id/' . $this->getId())

                              ->setPageSize(1)

                              ->load();

           if (count($rewriteCollection) > 0)

           {

                foreach ($rewriteCollection as $rewrite) {

                    $rewriteModel->setData($rewrite->getData());

                }

                return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $rewriteModel->getRequestPath();

           } else 

           {

               return $this->getUrlInstance()->getUrl('brands/index/view', array('id' => $this->getId()));

           }

        }

        return '';

//        if ($this->getId())

//            return $this->getUrlInstance()->getUrl(Mage::helper('brand')->getUrlPrefix().'/'.$this->getUrlKey());

    }

    

    /**

     * Retrieve URL instance

     *

     * @return Mage_Core_Model_Url

     */

    public function getUrlInstance()

    {

        if (!self::$_url) {

            self::$_url = Mage::getModel('core/url');

        }

        return self::$_url;

    }

    

    public function getProductsByManufacturer($manufacturerId, $storeId)

    {

        $resource = Mage::getResourceModel('catalogindex/attribute');

        $select = $resource->getReadConnection()->select();



        $select->from($resource->getMainTable(), 'entity_id')

            ->distinct(true)

            ->where('store_id = ?', $storeId)

            ->where('attribute_id = ?', Mage::helper('brand')->getAttributeId())

            ->where('value = ?', $manufacturerId);

        return $resource->getReadConnection()->fetchCol($select);

    }

    

    public function getManufacturersByProducts($productIds, $storeId)

    {

        $resource = Mage::getResourceModel('catalogindex/attribute');

        $select = $resource->getReadConnection()->select();



        if (empty($productIds))

        {

            return array();

        }

        

        $select->from($resource->getMainTable(), 'value')

            ->distinct(true)

            ->where('store_id = ?', $storeId)

            ->where('attribute_id = ?', Mage::helper('brand')->getAttributeId())

            ->where('entity_id IN (?)', $productIds);



        return $resource->getReadConnection()->fetchCol($select);

    }

    

    public function fillOut($storeId = 0)

    {

        Mage::register('brand_fillout_inprogress', true);

        //$stores = array_keys(Mage::app()->getStores(true));

        $resource = $this->getResource();

        $select = $resource->getReadConnection()->select();

        $select->from(array('main_table' => $resource->getMainTable()), array('manufacturer_id', 'url_key'))

            ->distinct(true)

            ->join(

                array('stores_table' => $resource->getTable('brand/brand_stores')),

                'main_table.manufacturer_id = stores_table.manufacturer_id'

                //array('store_id')

            )

            ->where('stores_table.store_id = ?', $storeId);

        //print_r($select->__toString());exit;

        $array = $resource->getReadConnection()->fetchPairs($select);

        $optionIds = array_keys($array);

        $urlKeys = array_values($array);

        

        

        $manufacturersCollection = $this->getCollection()->getManufacturersCollection($optionIds, $storeId);

        //print_r($manufacturersCollection->getItems());exit;

        foreach ($manufacturersCollection as $manufacturer){

            $urlKey = Mage::helper('brand')->toUrlKey($manufacturer->getValue());

            while (in_array($urlKey, $urlKeys)){

                $urlKey .= rand(0, 99);

            }

            $urlKeys[] = $urlKey;

            $this->load(0);

            $data = array(

                'manufacturer_id' => $manufacturer->getOptionId(),

                'title' => $manufacturer->getValue(),

                'url_key' => "brand/$urlKey",

                'status' => 1,

                'stores' => array($storeId),

            );

            $this->setData($data);

            $this->_afterLoad();

            $this->save();

        }

        Mage::unregister('brand_fillout_inprogress');

    }

}