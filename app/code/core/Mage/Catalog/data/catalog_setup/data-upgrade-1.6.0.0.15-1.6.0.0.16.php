<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */
/** @var $this Mage_Catalog_Model_Resource_Setup */

$connection = $this->getConnection();

$entitiesInfo = array(
    Mage_Catalog_Model_Product::ENTITY => 'catalog/product',
    Mage_Catalog_Model_Category::ENTITY => 'catalog/category'
);

foreach ($entitiesInfo as $entityType => $tableName) {

    $entityCode = str_replace('/', '_', $tableName);
    $tmpProductUrlKeyIncrementTableName = 'tmp_' . $entityCode . '_url_key_increment';
    $tmpProductUrlKeyTableName = 'tmp_' . $entityCode . '_url_key';

    $table = $connection
        ->newTable($tmpProductUrlKeyIncrementTableName)
        ->addColumn('value', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false,
            'primary'   => true
        ), 'Value')
        ->addColumn('increment', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned'  => true,
            'nullable'  => true
        ), 'Increment');
    $this->getConnection()->createTemporaryTable($table);

    $table = $connection
        ->newTable($tmpProductUrlKeyTableName)
        ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'nullable'  => false,
            'unsigned'  => true,
            'primary'   => true
        ), 'Store ID')
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable'  => false,
            'primary'   => true
        ), 'Entity ID')
        ->addColumn('value', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false
        ), 'Value')
        ->addColumn('increment', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => 0
        ), 'Increment')
        ->addIndex($this->getIdxName($tmpProductUrlKeyTableName, array('value')), array('value'))
        ->addIndex($this->getIdxName($tmpProductUrlKeyTableName, array('increment')), array('increment'));
    $this->getConnection()->createTemporaryTable($table);

    $entityTypeId = $this->getEntityTypeId($entityType);
    $attributeId = $this->getAttributeId($entityTypeId, 'url_key');

    $select = $connection->select()
        ->from($this->getTable(array($tableName, 'varchar')), array('store_id', 'entity_id', 'value'))
        ->where('attribute_id = ?', $attributeId);

    $insertQuery = $connection->insertFromSelect($select, $tmpProductUrlKeyTableName,
        array('store_id', 'entity_id', 'value')
    );
    $connection->query($insertQuery);

    $simplifiedValue = 'CASE'
        . ' WHEN SUBSTRING(value FROM LOCATE(\'-\', REVERSE(value))*(-1) + 1 ) REGEXP \'^-?[0-9]+$\''
        . ' THEN SUBSTRING(value FROM 1 FOR LENGTH(value) - LOCATE(\'-\', REVERSE(value)))'
        . ' ELSE value END';

    $increment = 'CASE'
        . ' WHEN SUBSTRING(value FROM LOCATE(\'-\', REVERSE(value))*(-1) + 1 ) REGEXP \'^-?[0-9]+$\''
        . ' THEN SUBSTRING(value FROM LOCATE(\'-\', REVERSE(value))*(-1) + 1 )'
        . ' ELSE 0 END';

    $select = $connection->select()
        ->group(new Zend_Db_Expr($simplifiedValue))
        ->from(array('t' => $tmpProductUrlKeyTableName), array())
        ->columns(array(
            'value' => new Zend_Db_Expr($simplifiedValue),
            'increment' => new Zend_Db_Expr('MAX(' . $increment . ')')
        ));

    $insertQuery = $connection->insertFromSelect(
        $select, $tmpProductUrlKeyIncrementTableName, array('value', 'increment')
    );
    $connection->query($insertQuery);

    $updateSelect = $connection->select()
        ->join(
            array('i' => $tmpProductUrlKeyIncrementTableName),
            'i.value = t.value ',
            array('i.increment')
        );
    $connection->query(
        $connection->updateFromSelect($updateSelect, array('t' => $tmpProductUrlKeyTableName))
    );

    $from = $tmpProductUrlKeyTableName
        . ', (' . new Zend_Db_Expr("SELECT @rank := null AS rnk, @value := '' AS val") . ') AS t';
    $columns = array(
        'increment',
        'store_id',
        'entity_id',
        'value',
        $connection->getCaseSql(
            '',
            array('@value = value' => '@rank := ifnull(@rank, increment) + 1'),
            '@rank := increment'
        ) . ' AS rank',
        '@value:= value'
    );
    $subSelect = sprintf('(SELECT %s FROM %s ORDER BY %s)', implode(', ', $columns), $from, 'value');

    $select = $connection->select()
        ->from(
            array('r' => new Zend_Db_Expr($subSelect)),
            array(
                'entity_type_id' => new Zend_Db_Expr($entityTypeId),
                'attribute_id' => new Zend_Db_Expr($attributeId),
                'r.store_id',
                'r.entity_id',
                'value' => $connection->getCaseSql('', array('r.rank = increment' => 'r.value'),
                    $connection->getConcatSql(array('value', 'r.rank'), '-')
                )
            )
        );

    $insertQuery = $connection->insertFromSelect($select, $this->getTable(array($tableName, 'url_key')),
        array('entity_type_id', 'attribute_id', 'store_id', 'entity_id', 'value')
    );
    $connection->query($insertQuery);
}
