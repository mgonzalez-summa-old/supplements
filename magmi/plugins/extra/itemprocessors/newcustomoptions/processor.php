<?php
class NewCustomOptionsItemProcessor
    extends Magmi_ItemProcessor
{
    /**
     * Define base plugin information.
     * 
     * @see Magmi_Plugin::getPluginInfo()
     */
    public function getPluginInfo()
    {
        return array(
            'name'      => 'New Custom Options',
            'author'    => 'Mark Shust',
            'version'   => '1.0.0',
        );
    }
    
    /**
     * Run this block after it processes the id.
     * 
     * @see Magmi_ItemProcessor::processItemAfterId()
     * @param $item array Column data for row
     */
    public function processItemAfterId(&$item, $params=null)
    {
        $item['customoptions_store_id']   = isset($item['customoptions_store_id']) ? $item['customoptions_store_id'] : 0;
        
        // catalog_product_option record
        $optionId = $this->_insertCatalogProductOption(array(
            $this->_getProductIdFromSku($item['sku']),
            $item['customoptions_type'],
            $item['customoptions_is_require'],
        ));
        
        // catalog_product_option_title record
        $optionTitleId = $this->_insertCatalogProductOptionTitle(array(
            $optionId,
            $item['customoptions_store_id'],
            $item['customoptions_title'],
        ));
        
        // Let's loop through values for this multi-value custom option and populate it
        $optionTypeValues = explode('|', $item['customoptions_value']);
        foreach ($optionTypeValues as $optionTypeValue) {
            list($title, $price, $priceType, $sku, $sortOrder) = explode(':', $optionTypeValue);
            
            $price              = isset($price)     ? $price     : 0;
            $priceType          = isset($priceType) ? $priceType : 'fixed';
            $sortOrder          = isset($sortOrder) ? $sortOrder : 0;
            $optionTypeIds      = null;
            
            $optionTypeValueValues = array(
                'title'         => $title,
                'price'         => $price,
                'price_type'    => $priceType,
                'sku'           => $sku,
                'sort_order'    => $sortOrder,
                'store_id'      => $item['customoptions_store_id'],
                'option_id'     => $optionId,
            );
            
            // Insert record for this custom option type (value, title & price)
            if ($sku) {
                $optionTypeIds = $this->_insertCatalogProductOptionType($optionTypeValueValues);
            }
            
            unset($title, $price, $priceType, $sku, $sortOrder, $optionTypeValueValues);
        }
        
        $this->log("Inserted custom option {$item['customoptions_title']}", 'info');
        
        return true;
    }
    
    public function _getProductIdFromSku($sku)
    {
        $sql = "SELECT entity_id
                FROM catalog_product_entity
                WHERE sku = ?";
        
        return $this->selectone($sql, $sku, 'entity_id');
    }
    
    public function _insertCatalogProductOption($data)
    {
        $sql = "INSERT INTO catalog_product_option (
                    product_id,
                    type,
                    is_require
                ) VALUES (?, ?, ?)";
        
        return $this->insert($sql, $data);
    }
    
    public function _insertCatalogProductOptionTitle($data)
    {
        $sql = "INSERT INTO catalog_product_option_title (
                    option_id,
                    store_id,
                    title
                ) VALUES (?, ?, ?)";
        
        return $this->insert($sql, $data);
    }
    
    //unset($title, $price, $priceType, $sku, $sortOrder);
    public function _insertCatalogProductOptionType($data)
    {
        $sql = "INSERT INTO catalog_product_option_type_value (
                    option_id,
                    sku,
                    sort_order
                ) VALUES (?, ?, ?)";
        $value = array(
            $data['option_id'],
            $data['sku'],
            $data['sort_order'],
        );
        $optionTypeId = $this->insert($sql, $value);
        unset($sql, $value);
        
        $sql = "INSERT INTO catalog_product_option_type_title (
                    option_type_id,
                    store_id,
                    title
                ) VALUES (?, ?, ?)";
        $title = array(
            $optionTypeId,
            $data['store_id'],
            $data['title'],
        );
        $optionTypeTitleId = $this->insert($sql, $title);
        unset($sql, $title);
        
        $sql = "INSERT INTO catalog_product_option_type_price (
                    option_type_id,
                    store_id,
                    price,
                    price_type
                ) VALUES (?, ?, ?, ?)";
        $price = array(
            $optionTypeId,
            $data['store_id'],
            $data['price'],
            $data['price_type'],
        );
        $optionTypePriceId = $this->insert($sql, $price);
        unset($sql, $price);
        
        return array(
            'option_type_id'        => $optionTypeId,
            'option_type_title_id'  => $optionTypeTitleId,
            'option_type_price_id'  => $optionTypePriceId,
        );
    }
}
