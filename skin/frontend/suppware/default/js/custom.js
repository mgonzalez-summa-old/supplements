/**
 * Home carousel
 */
//<![CDATA[
jQuery(function($){
	/**
	 * Carousels
	 */
    $('#home-carousel').anythingSlider({
         autoPlay: true,
         autoPlayLocked: true,
         delay: 5000,
         resumeDelay: 2000,
    	 resizeContents: false,
    	 navigationFormatter: function(i, panel){
    	     return ['1', '2', '3', '4', '5'][i - 1];
    	 }
     });
    
    /**
     * Price match
     */
	var productName;
	// Loop through all pricematch elements and create dialog object for each
    $('.pricematch').each(function(index){
    	if ($('body').hasClass('catalog-category-view') || $('body').hasClass('catalogsearch-result-index') || $('body').hasClass('brand-index-view')) {
    		productName = $(this).parent().find('.product-name').html();
    	} else if ($('body').hasClass('catalog-product-view')) {
    		productName = $('.product-main-info .product-name h1').html();
    	} else if($('body').hasClass('catalog-product-compare-index')) {
            productName = $(this).data('name');
        }
    	$(this).dialog({
    		modal: true,
    		autoOpen: false,
    		title: 'Price Match',
    		width: 600,
    		position: ['center', 170]
    	});
    });
    
    // This is adjustprice button outside modal
    $('.btn-adjustprice').click(function(){
    	// Open matching pricematch dialog
        if ($('body').hasClass('catalog-product-compare-index')) {
            var pricematchDiv = $('.pricematch_'+$(this).data('productid'));
        } else {
            var pricematchDiv = $('.pricematch:eq(' + $('.btn-adjustprice').index(this) + ')');
        }
    	pricematchDiv.dialog('open');
    	
    	// Reset pricematch div in the case it is clicked, cancelled then clicked again
    	pricematchDiv.find('.p-pricematch:not(.p-addtocart,.p-updateprice)').remove();
    	
    	// Add in explanation text
    	pricematchDiv.prepend('<p class="p-pricematch"><strong>Instantly take 5% off any competitor price!*</strong><br/><br/>Simply enter the competitor\'s web address and price in the provided boxes shown below and click "Price Match". This will instantly show the adjusted the price to 5% below the submitted competitor\'s price.</p>');
    	
    	// Hide addtocart button
    	if (pricematchDiv.find('.btn-cart-modal').length) {
    		pricematchDiv.find('.btn-cart-modal').parent().hide();
    	}

        // If product is already in cart
        if ($(this).hasClass('update-pricematch')) {
            pricematchDiv.html('<form action="/checkout/cart/updateCartFromCategory/pid/'+$(this).data('productid')+'" method="post" id="update_pricematch_form">' + pricematchDiv.html() + '</form>');
        }

        // Compare products page
        if ($('body').hasClass('catalog-product-compare-index')) {
            if ($(this).hasClass('update-pricematch')) {
                var formAction = '/checkout/cart/updateCartFromCategory/pid/'+$(this).data('productid');
            } else {
                var formAction = $('#add_to_cart_'+$(this).data('productid')).val();
            }
            pricematchDiv.html('<form action="'+formAction+'" method="post" id="compare_pricematch_form">' + pricematchDiv.html() + '</form>');
        } else
    	
    	// Shopping cart page
    	if (!pricematchDiv.find('.btn-cart-modal').length) {
    		pricematchDiv.html('<form action="/checkout/cart/updatepricematchitemPost/" method="post" id="updatepricematch">' + pricematchDiv.html() + '</form>');
    	}
    	
    	// Create Price Match modal button if one doesn't exist
    	if (!pricematchDiv.find('.btn-adjustprice-modal').length) {
    		if (pricematchDiv.find('form').length) {
    			var addAdjustpriceButTo = pricematchDiv.find('form');
    		} else {
    			var addAdjustpriceButTo = pricematchDiv;
    		}
    		addAdjustpriceButTo.append('<p class="p-adjustprice"><button type="button" title="Price Match" class="button btn-adjustprice-modal" data-productid="'+$(this).data('productid')+'"><span><span>Price Match</span></span></button></p>')
    	}
    	
    	// Add Update Cart button to modal
    	if (!pricematchDiv.find('.btn-cart-modal').length) {
    		pricematchDiv.find('.btn-adjustprice-modal').parent().prepend('<p class="p-updateprice" style="display: none; "><button type="submit" title="Update Cart" class="button btn-update-modal"><span><span>Update Cart</span></span></button></p>');
    	}
    	
    	// Make sure input validations are there
        if(!$('body').hasClass('catalog-product-compare-index')) {
    	    pricematchDiv.find('input:first').addClass('first');
    	    pricematchDiv.find('input:eq(1)').addClass('validate-currency-dollar');
        }
    	if (pricematchDiv.find('input:eq(1)').val()) {
    		pricematchDiv.find('input:eq(1)').val(Math.round(pricematchDiv.find('input:eq(1)').val()*100)/100);
    	}
    	pricematchDiv.find('.input-text').addClass('required-entry');

        var pricematchItemIndex = $(this).data('productid');
        if (!pricematchItemIndex) {
            pricematchItemIndex = $('.btn-adjustprice').index(this);
        }
    	$('body').data('pricematch-item-index',pricematchItemIndex);
    	
    	// Make sure to validate formif update page
    	if (!pricematchDiv.find('.btn-cart-modal').length) {
    		var updatepricematchForm = new VarienForm('updatepricematch', true);
    	}
    });
    
    // This is adjustprice button inside modal
    $('.btn-adjustprice-modal:not([disabled="disabled"])').live('click',function(){
    	// Focus on this button to force validation checks
    	$(this).focus();
    	
    	// Check if input elements are filled in
    	$(this).parent().parent().find('.input-text').each(function(){
    		if (!$(this).val()) {
    			$(this).parent().addClass('validation-error');
    		}
    	});
    	
    	// Make sure all fields are validated before continuing
    	if ($(this).parent().parent().find('.validation-error').length) {
    		//$(this).parent().parent().find('.validation-error').removeClass('validation-error');
    		return false;
    	}
    	
    	// Get old price
    	if ($('body').hasClass('catalog-category-view') || $('body').hasClass('catalogsearch-result-index') || $('body').hasClass('brand-index-view')) {
    		var originalPrice = $('#product-price-' + $('body').data('pricematch-item-index') +' .price').text();
    	} else if ($('body').hasClass('catalog-product-view')) {
    		var originalPrice = $('#product_addtocart_form .price-box .regular-price .price:first').text();
    	} else if ($('body').hasClass('checkout-cart-index')) {
    		var originalPrice = $('#shopping-cart-table tbody tr:eq(' + $('body').data('pricematch-item-index') + ') .price:first').text();
    	} else if($('body').hasClass('catalog-product-compare-index')) {
            var originalPrice = $('#product-price-'+$(this).data('productid')+'-compare-list-top .price:first').text();
        }
    	// Get new price
    	var originalPrice =  Number(originalPrice.replace(/[^0-9\.]+/g,''));
    	var competitorsPrice = $('.ui-dialog:visible').find('input.validate-currency-dollar').val();
    	competitorsPrice = Number(competitorsPrice.replace(/[^0-9\.]+/g,''));
    	var newPrice = competitorsPrice * 0.95;
    	// Round to three decimals
    	newPrice = newPrice.toFixed(3);
    	
    	// Check if new price is higher than original price, if so, set to original
    	$('body').data('pricematch-reset-price', '');
    	if (newPrice > originalPrice) {
    		newPrice = originalPrice;
    		// Take 5% off of arbitrary number to make exactly original price (don't question logic, it works...)
    		var resetPrice = newPrice * 1.05263158;
    		$('body').data('pricematch-reset-price', resetPrice.toFixed(3));
    	}
    	
    	// Detach addtocart button and reattach below pricematch button
    	if ($('.ui-dialog:visible').find('.btn-cart-modal').length) {
    		var btnCartModal = $('.ui-dialog:visible').find('.btn-cart-modal').parent().show().detach();
    	} else {
    		var btnCartModal = $('.ui-dialog:visible').find('.btn-update-modal').parent().show().detach();
    	}
    	
    	// Show at random point between 2 and 5 seconds
    	var parentDiv = $(this).parent().parent();
        var randNum = 2000;
    	// Remove button container (to reset), this is later in code to make sure we grab cart button from DOM
    	$(this).parent().parent().find('.bottom-container').remove();
        
    	// Replace 'Price Match' text with ajax loader icon
    	$(this).css('background-image','none');
    	adjustPriceText = $(this).find('span span');
    	adjustPriceText.html('<img src="/skin/frontend/suppware/default/images/ajaxloader-97C900bg-FFFFFFtxt.gif"/>');
    	adjustPriceText.parent().parent().attr('disabled','disabled');
    	
    	setTimeout(function(){
    		adjustPriceText.html('Price Match');
    		adjustPriceText.parent().parent().removeAttr('disabled').css('background-image','');
    		parentDiv.append('<div class="bottom-container">'+'' +
                '<p class="congrats"><strong>Congratulations!</strong> This Item Has Been INSTANTLY Price Matched and Added to your Cart!</p>'+
                '<div class="competitors-price">'+
                '<p class="competitors-price-heading">Competitor\'s Price</p><p class="competitors-price-price">' + competitorsPrice + '</p>'+
                '</div>'+
                '<div class="your-price"><p class="your-price-heading">Your Price</p><p class="your-price-price">' + newPrice + '</p></div>'+
                '<button type="button" title="Close" class="button btn-adjustprice-close '+($(this).hasClass('update-pricematch')?'update-pricematch-close':'')+'" style=""><span><span>Close</span></span></button>'+
                '</div>');
    		parentDiv.find('.competitors-price-price,.your-price-price').formatCurrency();
    		parentDiv.find('.bottom-container').slideDown();
            /* */
            // We need to add elements back to original item div and submit that for it to work properly
            if ($('body').hasClass('catalog-category-view') || $('body').hasClass('catalogsearch-result-index') || $('body').hasClass('brand-index-view')) {
                // Product list view
                var form = $('#product_addtocart_form_' + $('body').data('pricematch-item-index'));
                var appendToClass = '.item';
            } else if ($('body').hasClass('catalog-product-view')) {
                // Product detail view
                var form = $('#product_addtocart_form');
                var appendToClass = '.options-container-big';
            }

            if (form) {
                // Make sure to hide these input elements so user doesn't see them, for processing use only
                form.find(appendToClass).append('<div class="dontshow" />');
                $('.ui-dialog:visible').find('input').each(function(){
                    var inputClone = $(this).clone();
                    form.find(appendToClass + ' .dontshow').append(inputClone);
                });
                // Check if new price is higher than original price, if so, set to original
                if ($('body').data('pricematch-reset-price')) {
                    var originalPrice = $('body').data('pricematch-reset-price');
                    form.find('input.validate-currency-dollar').val(originalPrice.toFixed(2));
                }

            } else {
                if ($('body').data('pricematch-reset-price')) {
                    $('.ui-dialog:visible').find('input.validate-currency-dollar').val($('body').data('pricematch-reset-price'));
                }
            }
    	}, randNum);

    });

    $('.btn-adjustprice-close').live('click',function(){

        $('.ui-widget-overlay, .ui-widget').css('cursor', 'wait');
        $(this).attr('disabled','disabled');
        if ($('body').hasClass('catalog-category-view') || $('body').hasClass('catalogsearch-result-index') || $('body').hasClass('brand-index-view')) {
            // Product list view
            var form = $('#product_addtocart_form_' + $('body').data('pricematch-item-index'));
        } else if ($('body').hasClass('catalog-product-view')) {
            // Product detail view
            var form = $('#product_addtocart_form');
        }

        if($(this).closest('form').attr('id') == 'update_pricematch_form' || $('body').hasClass('catalog-product-compare-index')) {
            $(this).closest('form').submit();
        } else {
            if (form) {
                // Trigger original addtocart button
                form.find('.btn-cart').trigger('click');
            } else {
                $('.ui-dialog:visible form').submit();
            }
        }
    });
    
    $('.btn-update-modal').live('click',function(){
    	// Check if new price is higher than original price, if so, set to original
    	if ($('body').data('pricematch-reset-price')) {
    		$('.ui-dialog:visible').find('input.validate-currency-dollar').val($('body').data('pricematch-reset-price'));
    	}
    	
    	// Submit form
    	$('.ui-dialog:visible form').submit();
    	
    	// Update status while user waits for page to load
    	$('.ui-dialog:visible .ui-dialog-content').html('<div>Item price being updated. Please wait...</div>');
    });
    
    // This is addtocart button inside modal
    $('.btn-cart-modal').live('click',function(){
    	// We need to add elements back to original item div and submit that for it to work properly
    	if ($('body').hasClass('catalog-category-view') || $('body').hasClass('catalogsearch-result-index') || $('body').hasClass('brand-index-view')) {
    		// Product list view
            var form = $('#product_addtocart_form_' + $('body').data('pricematch-item-index'));
    		var appendToClass = '.f-fix';;
    	} else if ($('body').hasClass('catalog-product-view')) {
    		// Product detail view
    		var form = $('#product_addtocart_form');
    		var appendToClass = '.options-container-big';
    	}
    	
    	// Make sure to hide these input elements so user doesn't see them, for processing use only
    	form.find(appendToClass).append('<div class="dontshow" />');
    	$('.ui-dialog:visible').find('input').each(function(){
    		form.find(appendToClass + ' .dontshow').append(this);
    	});
    	
    	// Check if new price is higher than original price, if so, set to original
    	if ($('body').data('pricematch-reset-price')) {
    		var originalPrice = $('body').data('pricematch-reset-price');
    		form.find('input.validate-currency-dollar').val(originalPrice.toFixed(2));
    	}
    	
    	// Update status while user waits for page to load
    	$('.ui-dialog:visible .ui-dialog-content').html('<div>Item being added to cart. Please wait...</div>');
    	
    	// Trigger original addtocart button
    	form.find('.btn-cart').trigger('click');
    });
    
});
//]]>
