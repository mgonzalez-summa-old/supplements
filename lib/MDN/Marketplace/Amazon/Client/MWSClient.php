<?php

require_once('MDN/Marketplace/Amazon/Client/MWS.php');

/**
 * Class MWSClient
 * This class extends MWS class.
 *
 * @author Nicolas Mugnier
 * @see MWS.php
 */
class MWSClient extends MWS {

    /**
     * Constructor
     *
     * @param array $parameters
     *
     * @see addparams
     * @see parent::__construct
     */
    public function __construct($parameters = array()){

        parent::__construct();
        $this->addParams($parameters);
    }

    /**
     * Add query parameters
     *
     * @param array $parameters
     */
    public function addParams($parameters){

        // add parameters
        foreach($parameters as $k => $v){
            $this->_params[$k] = $v;
        }

        // sort array parameters
        uksort($this->_params, 'strcmp');

        // url encode params and values excepted for timestamp value
        foreach($this->_params as $param => $value){
            $param = str_replace('%7E', '~', rawurlencode($param));
            if ($param != 'Timestamp' && $param != 'StartDate' && $param != 'EndDate')  //do not encode timestamp !!!
                $value = str_replace('%7E', '~', rawurlencode($value));
        }

        // add signature
        $this->_params['Signature'] = $this->calculSignature();
    }

    /**
     * Send a request to Amazon MWS
     *
     * @return Zend_Http_Response $response
     */
    public function sendRequest(){
        
        $client = $this->getClient();

        // ajout des headers
        $client->setHeaders(array(
            'User-Agent' => 'MDN Tool/1.0 (Language=PHP/'.phpversion().'; Platform=Linux/Gentoo)',
        ));

        // add parameters
        $client->setParameterGet($this->_params);
        
        // send request
        $response = $client->request('GET');

        return $response;
    }

    /**
     * Send feed to Amazon MWS
     *
     * @param string $filename path to uploaded file
     * @return Zend_Http_Response $resposne
     */
    public function sendFeed($filename){

        // get client
        $client = $this->getClient();

        // read uploaded file
        $handle = fopen($filename, 'r');
        $xml = stream_get_contents($handle);
        fclose($handle);

        // build Content-MD5 header
        $contentMD5 = base64_encode(md5_file($filename, true));

        // ajout des headers
        $client->setHeaders(array(
            'Content-Type'        => 'text/xml; charset=iso-8859-1',
            'Content-MD5'         => $contentMD5,
            'User-Agent'          => 'MDN Tool/1.0 (Language=PHP/'.phpversion().'; Platform=Linux/Gentoo)',
            'Transfert-Encoding'  => 'chunked'
        ));

        // add parameters
        $client->setParameterGet($this->_params);

        // add content to body
        $client->setRawData($xml);

        // send request
        $response = $client->request('POST');
        
        return $response;
    }

    public function cancelFeed($id){
        
    }
    
}
?>
