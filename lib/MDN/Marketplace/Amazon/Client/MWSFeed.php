<?php

class MWSFeed {

    private static $_instance;

    private function __construct() {

    }

    public function setMerchantId($value) {

        $this->_merchantId = $value;
    }

    public static function getInstance() {

        if (self::$_instance == null)
            self::$_instance = new MWSFeed();

        return self::$_instance;
    }

    /**
     * Build product product feed
     * The Product feed contains descriptive information about the products in your catalog. This information allows Amazon
     * to build a record and assign a unique identifier known as an ASIN (Amazon Standard Item Number) to each product.
     * This feed is always the first step in submitting products to Amazon because it establishes the mapping between the
     * seller's unique identifier (SKU) and Amazon's unique identifier (ASIN)
     *
     * @return string (xml)
     *
     * @see getMerchantId
     */
    public function buildProductFeed($merchantId, $products) {

        // add declaration
        $xml = new DOMDocument('1.0', 'utf-8');

        // create AmazonEnvelope
        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        // add attributes
        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        // add Header node
        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        // add document version to header node
        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        // add merchant identifier to header node
        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        // add Message type node to AmazonEnvelope
        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('Product');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        // add purge and replace node to AmazonEnvelope
        $purgeAndReplace = $xml->createElement('PurgeAndReplace', '');
        $purgeAndReplace->appendChild($xml->createTextNode('false'));
        $amazonEnvelope->appendChild($purgeAndReplace);

        $id = 0;
        // add new products to feed
        foreach ($products as $newProduct) {

            $id++;

            // add message node
            $message = $xml->createElement('Message', '');
            $messageId = $xml->createElement('MessageID', '');
            $messageId->appendChild($xml->createTextNode($id));
            $message->appendChild($messageId);

            // add operation type node
            $operationType = $xml->createElement('OperationType', '');
            $operationType->appendChild($xml->createTextNode('Update'));
            $message->appendChild($operationType);

            // add product node
            $product = $xml->createElement('Product', '');

            // add general information to product node
            foreach ($newProduct['general'] as $k => $v) {

                if ($v != "") {

                    $elt = $xml->createElement($k, '');
                    $elt->appendChild($xml->createTextNode($v));
                    $product->appendChild($elt);
                }
            }

            // add standard product id to product node (EAN code)
            if ($newProduct['StandardProductID']['Value'] != "") {

                $standardProductId = $xml->createElement('StandardProductID', '');


                foreach ($newProduct['StandardProductID'] as $k => $v) {

                    $elt = $xml->createElement($k, '');
                    $elt->appendChild($xml->createTextNode($v));
                    $standardProductId->appendChild($elt);
                }

                $product->appendChild($standardProductId);
            }

            // description data to product node
            $descriptionData = $xml->createElement('DescriptionData', '');
            $product->appendchild($descriptionData);

            // walk description data array
            foreach ($newProduct['descriptionData'] as $k => $v) {

                // if simple field
                if (!is_array($v)) {

                    if ($v != "") {

                        $unitOfMeasureTab = Mage::helper('Amazon/XSD')->getUnitOfMeasureForRequiredFields();
                        if (array_key_exists($k, $unitOfMeasureTab)) {
                            $elt = $xml->createElement($k, '');
                            $elt->appendChild($xml->createTextNode($v));
                            $unitOfMeasure = $xml->createAttribute('unitOfMeasure');
                            $unitOfMeasure->appendChild($xml->createTextNode($unitOfMeasureTab[$k]));
                            $elt->appendChild($unitOfMeasure);
                            $descriptionData->appendChild($elt);
                        } else {
                            $elt = $xml->createElement($k, '');
                            $elt->appendChild($xml->createTextNode($v));
                            $descriptionData->appendChild($elt);
                        }
                    }
                } else {
                    // multiple node (more than one occurence)
                    if (count($v) > 0) {
                        // walk occurences node
                        foreach ($v as $input) {

                            $elt = $xml->createElement($k, '');
                            $elt->appendChild($xml->createTextNode($input));
                            $descriptionData->appendChild($elt);
                        }
                    }
                }
            }

            // if there are some product data informations
            if (array_key_exists('ProductData', $newProduct)) {

                // create product data node
                $productData = $xml->createElement('ProductData', '');

                if ($newProduct['ProductData']['name']) {

                    // add main category node
                    $productDataType = $xml->createElement($newProduct['ProductData']['name'], '');
                    $productData->appendChild($productDataType);

                    // check details
                    if (count($newProduct['ProductData']['details']) > 0) {

                        // has subcategory ?
                        if ($newProduct['ProductData']['details'][0]['ProductType'] !== null) {

                            //TODO : take care about ToysBaby category !!!!
                            if($newProduct['ProductData']['name'] == 'ToysBaby'){
                                $productType = $xml->createElement('ProductType', '');
                                $productType->appendChild($xml->createTextNode($newProduct['ProductData']['details'][0]['ProductType']));
                                $productDataType->appendChild($productType);
                            }else{
                                // create product type node
                                $productType = $xml->createElement('ProductType', '');
                                $productDataType->appendChild($productType);
                            }

                            // create subcategory node
                            $productTypeContent = $xml->createElement($newProduct['ProductData']['details'][0]['ProductType'], '');
                            $productType->appendChild($productTypeContent);

                        }

                        // walk product data details
                        foreach ($newProduct['ProductData']['details'] as $k => $v) {

                            // skippe productType
                            if ($k == 0)
                                continue;

                            $this->addProductDataDetail($xml, $productDataType, $v);
                        }
                       
                    }

                }

                // add product data to product node
                $product->appendChild($productData);
            }

            // add product to message node
            $message->appendChild($product);

            // add messgae to AmazonEnvelope
            $amazonEnvelope->appendChild($message);
        }

        $xmlContent = $xml->saveXML();

        return $xmlContent;
    }

    /**
     * Add node to DomDocument by parsing array $var
     *
     * @param DomDocument $xml
     * @param DOMNode $node
     * @param array $var
     * @return
     */
    private function addProductDataDetail($xml, $node, $var) {

        if (is_array($var)) {

            // get first key
            $keys = array_keys($var);
            $elt = $keys[0];

            if($elt != "") {

                // get next elt
                $newVar = $var[$elt];

                // check if $elt node exists
                if (!$node->getElementsByTagName($elt)->item(0)) {
                    // if not create it
                    $newNode = $xml->createElement($elt, '');
                    // check unitOfMeasure
                    $unitOfMeasure = Mage::helper('Amazon/XSD')->getUnitOfMeasureForRequiredFields();
                    if (array_key_exists($elt, $unitOfMeasure)) {
                        $attribute = $xml->createAttribute('unitOfMeasure');
                        $attribute->appendChild($xml->createTextNode($unitOfMeasure[$elt]));
                        $newNode->appendChild($attribute);
                    }
                    $node->appendChild($newNode);
                } else {

                    // set it as newNode
                    $newNode = $node->getElementsByTagName($elt)->item(0);
                }

                // recursive call
                $this->addProductDataDetail($xml, $newNode, $newVar);
            }
            
        } else {
            // add text Node
            $node->appendChild($xml->createTextNode($var));
        }

        return;
    }

    /**
     * Build inventory feed
     * The Inventory feed allows you to update inventory quantities (stock levels) for your items
     *
     * @return string (xml)
     *
     * @see getExportPath
     * @see getMarketPlaceName
     * @see getInventoryFeedName
     * @see getStockToExport
     * @see getMerchantId
     * @see getProductsToExport
     */
    public function buildInventoryFeed($merchantId, $products) {

        $xml = new DomDocument('1.0', 'utf-8');

        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('Inventory');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        $id = 0;
        foreach ($products as $product) {
            $id++;

            $message = $xml->createElement('Message', '');

            $messageId = $xml->createElement('MessageID');
            $txtMessageId = $xml->createTextNode($id);
            $messageId->appendChild($txtMessageId);
            $message->appendChild($messageId);

            $operationType = $xml->createElement('OperationType', '');
            $operationType->appendChild($xml->createTextNode('Update'));
            $message->appendchild($operationType);

            $inventory = $xml->createElement('Inventory', '');

            // add sku
            $sku = $xml->createElement('SKU');
            $skuValue = trim($product['sku']);
            $sku->appendChild($xml->createTextNode($skuValue));
            $inventory->appendChild($sku);

            // add stock
            $quantity = $xml->createElement('Quantity', '');
            $quantity->appendChild($xml->createTextNode((int) $product['stock']));
            $inventory->appendChild($quantity);

            // add delay
            $fulfillmentLatency = $xml->createElement('FulfillmentLatency', '');
            $fulfillmentLatency->appendChild($xml->createTextNode($product['delay']));
            $inventory->appendChild($fulfillmentLatency);

            $message->appendChild($inventory);
            $amazonEnvelope->appendChild($message);
        }

        $xmlContent = $xml->saveXML();

        return $xmlContent;
    }

    /**
     * Build price feed
     * The Price feed allows you to set the current price and sale price (when applicable) for an item. The sale price is
     * optional, but, if used, the start and end date must be provided also
     *
     * @return string (xml)
     *
     * @see getPriceToExport
     * @see getMarketPlaceName
     * @see getExportPath
     * @see getPriceFeedName
     * @see getProductsToExport
     * @see getMerchantId
     */
    public function buildPriceFeed($merchantId, $products) {


        $xml = new DomDocument('1.0', 'utf-8');

        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('Price');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        $id = 0;
        $configCurrency = Mage::getStoreConfig('marketplace/general/default_currency');
        if (!$configCurrency) {
            throw new Exception($this->__('Currency attribute not set in Sytem > Configuration > Marketplace'));
        }

        foreach ($products as $product) {
            $id++;

            $message = $xml->createElement('Message', '');

            $messageId = $xml->createElement('MessageID');
            $txtMessageId = $xml->createTextNode($id);
            $messageId->appendChild($txtMessageId);
            $message->appendChild($messageId);

            $price = $xml->createElement('Price', '');

            $sku = $xml->createElement('SKU');
            $skuValue = trim($product['sku']);
            $sku->appendChild($xml->createTextNode($skuValue));
            $price->appendChild($sku);

            $standardPrice = $xml->createElement('StandardPrice', '');
            $currency = $xml->createAttribute('currency');
            $currencyValue = $xml->createTextNode($configCurrency);
            $currency->appendChild($currencyValue);
            $standardPrice->appendChild($currency);

            $standardPrice->appendChild($xml->createTextNode($product['price']));
            $price->appendChild($standardPrice);

            $message->appendChild($price);
            $amazonEnvelope->appendChild($message);
        }

        $xmlContent = $xml->saveXML();

        return $xmlContent;
    }

    /**
     * Build image feed
     * The Image feed allows you to upload various images for a product. Amazon can display several images for each
     * product. It is in your best interest to provide several high-resolution images for each of your products so customers
     * can make informed buying decisions
     */
    public function buildImageFeed($merchantId, $products) {

        /**
         * Used to identify an individual product.
         * Each product must have a SKU, and each SKU must be unique
         */
        $sku;

        /**
         * The type of image (Main, Alternate, or Swatch)
         */
        $imageType;

        /**
         * The exact location of the image using a full URL
         */
        $imageLocation;

        $xml = new DomDocument('1.0', 'utf-8');

        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('ProductImage');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        $i = 0;
        foreach ($products as $product) {

            if ($product->getimage() == "" && $product->getsmall_image() == "")
                continue;

            // image (manually build image path to avoid magento < 1.3 issue with catalog/image helper)
            $url = "";
            $imageUrl = Mage::getBaseUrl('media') . 'catalog/product' . $product->getimage();
            $smallImageUrl = Mage::getBaseUrl('media') . 'catalog/product' . $product->getsmall_image();

            if (!preg_match('#no_selection#i', $imageUrl)) {
                $url = $imageUrl;
            } elseif (!preg_match('#no_selection#i', $smallImageUrl)) {
                $url = $smallImageUrl;
            } else {
                continue;
            }

            $i++;
            $message = $xml->createElement('Message', '');

            $messageId = $xml->createElement('MessageID', $i);
            $message->appendChild($messageId);

            $operationType = $xml->createElement('OperationType', 'Update');
            $message->appendChild($operationType);

            $productImage = $xml->createElement('ProductImage', '');
            $message->appendChild($productImage);

            $sku = $xml->createElement('SKU', $product->getsku());
            $productImage->appendChild($sku);

            $imageType = $xml->createElement('ImageType', 'Main');
            $productImage->appendChild($imageType);

            $imageLocation = $xml->createElement('ImageLocation', $url);
            $productImage->appendChild($imageLocation);

            $amazonEnvelope->appendChild($message);
        }

        $feed = $xml->saveXML();

        return $feed;
    }

    /**
     * Build relationship feed
     */
    public function buildRelationshipFeed() {

        /**
         * The master SKU for a product with variations
         */
        $parentSku;

        /**
         * Used to identify an individual product, one (child) variation of the parent SKU
         */
        $sku;

        /**
         * Type of relationship, variation or accessory
         */
        $type;
    }

    /**
     * Build override feed
     */
    public function buildOverrideFeed() {

        /**
         * Used to identify an individual product.
         * Each product must have a SKU, and each SKU must be unique
         */
        $sku;

        /**
         * Locale and shipping service
         */
        $shipOption;

        /**
         * Indicates whether the SKU can or cannot be shipped to the
         * specified locale using the specified shipping service (ShipOption)
         */
        $isShippingRestricted;

        /**
         * The type of override shipping charge
         * (Additive or Exclusive) being applied to the SKU
         */
        $type;

        /**
         * The Additive or Exclusive shipping charge amount
         */
        $shipAmount;
    }

    /*     * *********************************************************************** */
    /*                      Amazon order feeds                                */
    /*     * *********************************************************************** */

    /**
     * Build order adjustment feed
     */
    public function setOrderAdjustment() {

    }

    /*     * *
     * Get settlement report
     */

    public function getSettlementReport() {

    }

    public function buildOrderAcknowledgment($orders, $merchantId) {

        $xml = new DomDocument('1.0', 'utf-8');

        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');
        $xmlns = $xml->createAttribute('xmlns:xsi');
        $xmlns->appendChild($xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance'));

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlns);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $documentVersion = $xml->createElement('DocumentVersion', '1.01');
        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier', $merchantId);
        $header->appendChild($merchantIdentifier);

        $amazonEnvelope->appendChild($header);

        $messageType = $xml->createElement('MessageType', 'OrderAcknowledgment');
        $amazoneEnvelope->appendChild($messageType);

        $i = 0;
        foreach ($orders as $order) {

            $i++;
            $message = $xml->createElement('Message', '');
            $messageId = $xml->createElement('MessageID', $i);
            $message->appendChild($messageId);

            $orderAcknowledgment = $xml->createElement('OrderAcknowledgment', '');

            $amazonOrderId = $xml->createElement('AmazonOrderID', $order['amazonOrderId']);
            $orderAcknowledgment->appendChild($amazonOrderId);

            if ($order['merchantOrderId'] != "") {
                $merchantOrderId = $xml->createElement('MerchantOrderId', $order['merchantOrderId']);
                $orderAcknowledgment->appendChild($merchantOrderId);
            }

            $statusCode = $xml->createElement('StatusCode', $order['statusCode']);
            $orderAcknowledgment->appendChild($statusCode);

            foreach ($order['items'] as $item) {

                $item = $xml->createElement('Item', '');

                $amazonOrderItemCode = $xml->createElement('AmazonOrderItemCode', $item['amazonOrderItemCode']);
                $item->appendChild($amazonOrderItemCode);

                if ($item['merchantOrderItemID'] != "") {
                    $merchantOrderItemID = $xml->createElement('MerchantOrderItemID', $item['merchantOrderItemID']);
                    $item->appendChild($merchantOrderItemCode);
                }

                $orderAcknowledgment->appendChild($item);
            }

            $message->appendChild($orderAcknowledgment);
            $amazonEnevelope->appendChild($message);
        }

        $xmlContent = $xml->saveXML();

        return $xmlContent;
    }

    /*     * ************************************************************************* */
    /*                             Trackings                                    */
    /*     * ************************************************************************* */

    public function buildFlatTrackingFile($orderIds) {

        $lineReturn = "\r\n";
        $retour = 'order-id	order-item-id	quantity	ship-date	carrier-code	carrier-name	tracking-number	ship-method' . $lineReturn;

        if (!$orderIds)
            return $retour;

        foreach ($orderIds as $orderId) {
            //init vars
            $lineModel = 'order_ref	order_item_id	qty	ship_date	carrier_code	carrier_name	tracking	ship_method';
            $order = mage::getModel('sales/order')->load($orderId);

            //retrieve shipment
            $shipment = null;
            foreach ($order->getShipmentsCollection() as $item) {
                $shipment = $item;
                break;
            }

            if ($shipment == null) {
                continue;
            }

            //add items
            foreach ($order->getAllItems() as $item) {
                $line = $lineModel;
                $line = str_replace('order_ref', $order->getmarketplace_order_id(), $line);
                $line = str_replace('order_item_id', $item->getmarketplace_item_id(), $line);
                $line = str_replace('qty', (int) $item->getqty_ordered(), $line);

                $shipDate = date('Y-m-d', strtotime($shipment->getcreated_at()));
                $line = str_replace('ship_date', $shipDate, $line);

                $line = str_replace('carrier_code', 'Other', $line);
                $line = str_replace('carrier_name', 'La Poste', $line);

                $tracking = '';
                foreach ($order->getTracksCollection() as $track) {
                    if (is_object($track->getNumberDetail()))
                        $tracking = $track->getNumberDetail()->gettracking();
                }

                $line = str_replace('tracking', $tracking, $line);
                $line = str_replace('ship_method', 'Colissimo', $line);

                $retour .= $line . $lineReturn;
            }
        }

        return $retour;
    }

    public function buildXMLTrackingFile($ordersIds, $merchantId) {

        $carrierCode = array();

        $xsdPath = Mage::getConfig()->getOptions()->getLibDir() . '/MDN/Marketplace/Amazon/Parser/xsd';

        $content = file_get_contents($xsdPath . '/amzn-base.xsd');
        $xmlBase = new DomDocument();
        $xmlBase->loadXML($content);

        $root = $xmlBase->documentElement;

        foreach ($root->getElementsByTagName('element') as $elt) {

            if ($elt->hasAttribute('name') && $elt->getAttribute('name') == 'CarrierCode') {

                $carrierNode = $elt;
                break;
            }
        }

        foreach ($carrierNode->getElementsByTagName('enumeration') as $enum) {

            $code = $enum->getAttribute('value');
            $carrierCode[$code] = strtolower($code);
        }

        $xml = new DomDocument();
        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');
        $xmlns = $xml->createAttribute('xmlns:xsi');
        $xmlns->appendChild($xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance'));

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlns);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $documentVersion = $xml->createElement('DocumentVersion', '1.01');
        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier', $merchantId);
        $header->appendChild($merchantIdentifier);

        $amazonEnvelope->appendChild($header);

        $messageType = $xml->createElement('MessageType', 'OrderFulfillment');
        $amazonEnvelope->appendChild($messageType);

        $i = 0;
        foreach ($ordersIds as $orderId) {

            // retrieve order
            $order = mage::getModel('sales/order')->load($orderId);

            //retrieve shipment
            $shipment = null;
            foreach ($order->getShipmentsCollection() as $item) {
                $shipment = $item;
                break;
            }

            if ($shipment == null) {
                continue;
            }

            $tracking = Mage::Helper('MarketPlace/Tracking')->getTrackingForOrder($order);

            if($tracking == '')
                continue;

            $i++;
            $message = $xml->createElement('Message', '');

            $messageId = $xml->createElement('MessageID', $i);
            $message->appendChild($messageId);

            $orderFulfillment = $xml->createElement('OrderFulfillment', '');

            $orderRef = $order->getmarketplace_order_id();
            $amazonOrderId = $xml->createElement('AmazonOrderID', $orderRef);
            $orderFulfillment->appendChild($amazonOrderId);

            // (opt, depend of matching in orderAcknowledgment)
            //$merchantOrderId = $xml->createElement('MerchanrOrderID', $orderRef);
            //$orderFulfillment->appendChild($merchantOrderId);
            // merchant shipment id (not used by amazon)
            $fulfillmentId = $shipment->getentity_id();
            $merchantfulfillmentOrderId = $xml->createElement('MerchantFulfillmentID', $fulfillmentId);
            $orderFulfillment->appendChild($merchantfulfillmentOrderId);

            $shipDate = date("Y-m-d\TH:i:s", strtotime($shipment->getcreated_at()));
            $fulfillmentDate = $xml->createElement('FulfillmentDate', $shipDate);
            $orderFulfillment->appendChild($fulfillmentDate);

            $fulfillmentData = $xml->createElement('FulfillmentData', '');

            $shipping_method = mage::getStoreConfig('marketplace/general/default_shipment_method');
            $shipping_method_title = mage::helper('MarketPlace')->getShippingMethodTitle($shipping_method);

            $merchantCarrierCode = strtolower($shipping_method_title);
            if (in_array($merchantCarrierCode, $carrierCode)) {
                $code = $carrierCode[$merchantCarrierCode];
                $carrierCode = $xml->createElement('CarrierCode', $code);
                $fulfillmentData->appendChild($carrierCode);
            }

            $carrierName = $xml->createElement('CarrierName', $shipping_method_title);
            $fulfillmentData->appendChild($carrierName);

            $shippingMethod = $xml->createElement('ShippingMethod', $shipping_method_title);
            $fulfillmentData->appendChild($shippingMethod);

            $shipperTrackingNumber = $xml->createElement('ShipperTrackingNumber', $tracking);
            $fulfillmentData->appendChild($shipperTrackingNumber);

            $orderFulfillment->appendChild($fulfillmentData);

            foreach ($order->getAllItems() as $item) {

                $itemNode = $xml->createElement('Item', '');

                $amazonOrderItemCode = $xml->createElement('AmazonOrderItemCode', $item->getmarketplace_item_id());
                $itemNode->appendChild($amazonOrderItemCode);

                //$merchantOrderItemId = $xml->createElement('MerchantOrderItemID', $item->getmarketplace_item_id());
                //$item->appendChild($merchantOrderItemId);

                /* $merchantFulfillmentItemId = $xml->createElement('MerchantFulfillmentItemID', '');
                  $itemNode->appendChild($merchantFulfillmentItemId); */

                $quantity = $xml->createElement('Quantity', (int) $item->getqty_ordered());
                $itemNode->appendChild($quantity);

                $orderFulfillment->appendChild($itemNode);
            }

            $message->appendChild($orderFulfillment);
            $amazonEnvelope->appendChild($message);
        }

        $xmlContent = $xml->saveXML();
        return $xmlContent;
    }

}
