<?php

/**
 * Class MWS
 *
 * @author Nicolas Mugnier
 */
class MWS {

    /**
     * Markeplace id
     *
     * @var string
     */
    protected $_marketplaceId;

    /**
     * Merchant id
     *
     * @var string
     */
    protected $_merchantId;

    /**
     * AWS access key
     *
     * @var string
     */
    protected $_AWSAccessKey;

    /**
     * Version of MWS used
     *
     * @var string
     */
    protected static $_version = '2009-01-01';

    /**
     * Method used for build signature
     *
     * @var string
     */
    protected static $_signatureMethod = 'HmacSHA256';

    /**
     * Signature version
     *
     * @var int
     */
    protected static $_signatureVersion = '2';

    /**
     * Amazon MWS URL
     *
     * @var string
     */
    protected $_baseUrl = '';

    /**
     * Query parameters
     *
     * @var array
     */
    protected $_params = array();

    /**
     * Secret access key
     *
     * @var string
     */
    protected $_secretAccessKey;

    /**
     * Constructor
     *
     * @see setParams
     * @see setSecretAccessKey
     * @see setMarketplaceId
     * @see setAWSAccessKey
     */
    public function __construct(){

        $this->setBaseUrl();
        $this->setSecretAccessKey(mage::getStoreConfig('marketplace/amazon/amazon_secret_key'));
        $this->setMarketplaceId(mage::getStoreConfig('marketplace/amazon/amazon_marketplace_id'));
        $this->setMerchantId(mage::getStoreConfig('marketplace/amazon/amazon_merchant_id'));
        $this->setAWSAccessKey(mage::getStoreConfig('marketplace/amazon/amazon_access_key_id'));
        
        $this->setParams();

    }

    /**
     * Setter query parameters
     */
    public function setParams(){
        $this->_params['AWSAccessKeyId'] = $this->_AWSAccessKey;
        $this->_params['Merchant'] = $this->_merchantId;
        $this->_params['Marketplace'] = $this->_marketplaceId;
        $this->_params['Version'] = self::$_version;
        $this->_params['SignatureMethod'] = self::$_signatureMethod;
        $this->_params['SignatureVersion'] = self::$_signatureVersion;
        //$this->_params['Timestamp'] = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", Mage::getModel('core/date')->timestamp());
        $seconds = 18000;
        $this->_params['Timestamp'] = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", Mage::getModel('core/date')->timestamp() + $seconds);
    }

    /**
     * Setter marketplaceId
     *
     * @param string $value
     */
    public function setMarketplaceId($value){
        $this->_marketplaceId = trim($value);
    }

    /**
     * Setter merchantId
     *
     * @param string $value
     */
    public function setMerchantId($value){
        $this->_merchantId = trim($value);
    }

    /**
     * Setter AWSAccessKey
     *
     * @param string $value
     */
    public function setAWSAccessKey($value){
        $this->_AWSAccessKey = trim($value);
    }

    /**
     * Setter secretAccessKey
     *
     * @param string $value
     */
    public function setSecretAccessKey($value){
        $this->_secretAccessKey = trim($value);
    }

    /**
     * Setter baseUrl
     *
     * @param string $value
     */
    public function setBaseUrl(){

        $base = 'https://mws.amazonservices';

        $this->_baseUrl = $base.Mage::Helper('Amazon/Url')->getExt();

    }

    /**
     * Show query parameters
     *
     * @return string
     */
    public function __toString(){
        return '<pre>'.var_dump($this->_params).'</pre>';
    }

    /**
     * Construct new client
     *
     * @return Zend_Http_Client $client
     */
    protected function getClient(){
        $client = new Zend_Http_Client();
        $client->setUri($this->_baseUrl);
        
        $client->setConfig(array(
                'maxredirects' => 0,
                'timeout'      => 30
            )
        );
        return $client;
    }

    /**
     * Simple call to Amazon MWS
     *
     * @return XML
     */
    public function getAmazonTimestamp(){

        // get client
        $client = $this->getClient();

        // set headers
        $client->setHeaders(array(
            'User-Agent' => 'MDN Tool/1.0 (Language=PHP/'.phpversion().'; Platform=Linux/Gentoo)',
        ));
        // send request
        $response = $client->request('GET');

        // check result
        if($response->getStatus() == 200)
            return $response->getBody();
    }

    /**
     * Build signature
     *
     * @return string $signature
     */
    protected function calculSignature(){

        // set http method to use
        $method = ($this->_params['Action'] == "SubmitFeed") ? 'POST' : 'GET';
        
        $getParameters = array();
        // url encode parameters names and values
        foreach($this->_params as $param => $value){

            if ($param == 'Timestamp' || $param == 'StartDate' || $param == 'EndDate')  //now, encode timestamp ($queryParams used for signing)
                $value = str_replace('%7E', '~', rawurlencode($value));

            $queryParams[] = $param."=".$value;
        }

        // construct query string
        $query = implode('&', $queryParams);

        // construct signToString
        $signToString = $method."\n";
        $signToString .= "mws.amazonservices".Mage::Helper('Amazon/Url')->getExt()."\n";
        $signToString .= "/"."\n";
        $signToString .= $query;

        // HMAC $signToString with $secretAccessKey and convert result to base64
        $signature = base64_encode(hash_hmac("sha256", $signToString, $this->_secretAccessKey, true));

        // add signature
        return $signature;

    }
}
