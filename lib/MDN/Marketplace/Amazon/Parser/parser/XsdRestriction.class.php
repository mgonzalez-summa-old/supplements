<?php

class XsdRestriction{

    /**
     * xsd restrictions and atrtributes
     *
     * @var array
     */
    private static $_facets = array(
        'decimal' => array(
            'enumeration',
            'fractionDigits',
            'maxExclusive',
            'maxInclusive',
            'minExclusive',
            'minInclusive',
            'pattern',
            'totalDigits'
        ),
        'positiveInteger' => array(
            'enumeration',
            'fractionDigits',
            'maxExclusive',
            'maxInclusive',
            'minExclusive',
            'minInclusive',
            'pattern',
            'totalDigits'
        ),
        'string' => array(
            'enumeration',
            'length',
            'maxLength',
            'minLength',
            'pattern'
        ),
    );

    /**
     * Restriction type
     *
     * @var string
     */
    private $type;

    /**
     * Node to parsed
     *
     * @var DOMNode
     */
    private $node;

    /**
     * Type of widget to generate (widget form)
     *
     * @var string
     */
    private $widgetType;

    /**
     * Constructor
     *
     * @param string $type
     * @param DOMNode $node
     *
     * @see setType
     * @see setNode
     */
    public function __construct($type, $node){

        $this->setType($type);
        $this->setNode($node);

    }

    /**
     * Setter $type
     *
     * @param string $value
     */
    public function setType($value){
        
        if(preg_match("/string/i", $value))
            $value = "string";

        $this->type = $value;

    }

    /**
     * Setter $node
     *
     * @param DOMNode $value
     */
    public function setNode($value){

        $this->node = $value;

    }

    /**
     * Setter $widgetType
     *
     * @param string $value
     */
    public function setWidgetType($value){

        $this->widgetType = $value;

    }

    /**
     * Getter $widgetType
     *
     * @return String
     */
    public function getWidgetType(){

        return $this->widgetType;
        
    }

    /**
     * Parse node to array
     *
     * @return array $retour
     */
    public function toArray(){

        // init
        $retour = array();

        // get attributes for current restriction type
        foreach (self::$_facets[$this->type] as $facet) {

            // check if current attribute is used
            if ($this->node->getElementsByTagName($facet)->item(0)) {

                // case of enumeration : build an array which contain options
                if($facet == "enumeration"){

                    foreach ($this->node->getElementsByTagName('enumeration') as $enum) {

                        $retour[$facet][$enum->getAttribute('value')] = $enum->getAttribute('value');
                        
                    }

                    // set widget type as select
                    $this->setWidgetType("select");

                }
                else{
                    // simple attribute
                    $retour[$facet] = $this->node->getElementsByTagName($facet)->item(0)->getAttribute('value');
                    // set widget type as text
                    $this->setWidgetType("text");
                }
            }
        }

        return $retour;
    }

    /**
     * To String method
     *
     * @return string
     */
    public function __toString(){

        $params = "";

        foreach(self::$_facets[$this->type] as $facet){

            if ($this->node->getElementsByTagName($facet)->item(0)) {

                $params .= $facet.' ';

            }

        }
        return $this->type.' : '.$params;

    }
}

?>
