<?php

require_once('MDN/Marketplace/Amazon/Parser/parser/ParseAmazon.class.php');

class ParseAmazonRequiredFields extends ParseAmazon {


    private $required = array();
    private $requiredParentNode;

    public function __construct($filename){

        parent::__construct($filename);

    }

    public function getRequiredFields($category){

        $retour = array();
        $requiredFieldsMainCat = array();
        $requiredFieldsSubCat = array();
        $tmp = array();

        $this->initAll();
        $tabCat = $this->toArray($category, false);

        $requiredFieldsMainCat = $this->retrieveRequiredFields($tabCat);

        $subCategories = $this->getSubcategories();

        foreach($subCategories as $k => $v){

            $this->initAll();
            $tmp[$k] = $this->toArray($v, true);
            
        }

        foreach($tmp as $k => $v){

            $this->init();

            if($v === NULL) continue; // TODO : check why NULL ?????

           $requiredFieldsSubCat[$k] = $this->retrieveRequiredFields($v);
        }

        $retour = array(
                'mainCat' => $requiredFieldsMainCat,
                'subCat' => $requiredFieldsSubCat
         );
        
        return $retour;

    }

    public function retrieveRequiredFields($tab, $fromComplex = false){

        foreach($tab as $k => $widget){

            if($widget['type'] == "simple"){

                if($fromComplex === false)
                    $this->requiredParentNode = "";

                if(array_key_exists('minOccurs', $widget)){

                    if($widget['minOccurs'] > 0){
                        $widget['parentNode'] = ($this->requiredParentNode != "") ? $this->requiredParentNode.'_'.$k : $k;
                        $this->required[] = $widget;
                    }

                }
                else{
                    $widget['parentNode'] = ($this->requiredParentNode != "") ? $this->requiredParentNode.'_'.$k : $k;
                    $this->required[] = $widget;
                }
                

            }

            if($widget['type'] == "complex"){
                if((array_key_exists('minOccurs', $widget) && $widget['minOccurs'] > 0) || !array_key_exists('minOccurs', $widget)){
                    $this->requiredParentNode = ($this->requiredParentNode != "") ? $this->requiredParentNode.'_'.$k : $k;
                    $this->retrieveRequiredFields($widget, true);
                }
            }

        }

        return $this->required;

    }

    protected function init(){
        $this->required = array();
        $this->requiredParentNode = "";
    }

    protected function initAll(){
        $this->init();
        parent::init();
    }

}

?>
